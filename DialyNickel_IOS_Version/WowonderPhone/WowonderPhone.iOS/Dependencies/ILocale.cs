﻿using System;
using System.Threading;
using Foundation;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Contacts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ObjCRuntime;
using QuickLook;
using WowonderPhone.Dependencies;
using WowonderPhone.iOS.Dependencies;

[assembly: Dependency(typeof(Locale))]  

namespace WowonderPhone.iOS.Dependencies
{

    public class Locale : ILocale 
    {
        public void SetLocale()
        {
            try
            {
                //var iosLocaleAuto = NSLocale.AutoUpdatingCurrentLocale.LocaleIdentifier;
                //var netLocale = iosLocaleAuto.Replace("_", "-");
                //System.Globalization.CultureInfo ci;
                //try
                //{
                //    ci = new System.Globalization.CultureInfo(netLocale);
                //}
                //catch
                //{
                //    ci = new System.Globalization.CultureInfo(GetCurrent());
                //}

                //Thread.CurrentThread.CurrentCulture = ci;
                //Thread.CurrentThread.CurrentUICulture = ci;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public string GetCurrent()
        {
            try
            {
                //var iosLocaleAuto = NSLocale.AutoUpdatingCurrentLocale.LocaleIdentifier;
                //var iosLanguageAuto = NSLocale.AutoUpdatingCurrentLocale.LanguageCode;
                //var netLocale = iosLocaleAuto.Replace("_", "-");
                //var netLanguage = iosLanguageAuto.Replace("_", "-");

                //System.Globalization.CultureInfo ci;
                //try
                //{
                //    ci = new System.Globalization.CultureInfo(netLanguage);
                //}
                //catch
                //{
                //    ci = new System.Globalization.CultureInfo(NSLocale.PreferredLanguages[0]);
                //}
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

                //if (NSLocale.PreferredLanguages.Length > 0)
                //{
                //    var pref = NSLocale.PreferredLanguages[0];
                //    netLanguage = pref.Replace("_", "-");
                //    Console.WriteLine("preferred:" + netLanguage);
                //}
                //else
                //{
                //    netLanguage = "en"; // default, shouldn't really happen :)
                //}
                return "en-US";//netLanguage;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "en";
            }
        }
    }
}