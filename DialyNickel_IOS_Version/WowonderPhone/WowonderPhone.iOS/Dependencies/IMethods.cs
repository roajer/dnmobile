﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Foundation;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QuickLook;
using UIKit;
using Acr.UserDialogs;
using Xamarin.Forms;
using WowonderPhone.Pages.Tabs;
using CoreLocation;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using ObjCRuntime;
using WowonderPhone.Dependencies;
using WowonderPhone.iOS.Dependencies;

[assembly: Dependency(typeof(Methods))]

namespace WowonderPhone.iOS.Dependencies
{ 
    public class Methods : IMethods
    {

        public void OpenImage(string Directory, string image, string Userid)
        {
            try
            {
                if (Directory == "Disk")
                {
                    var documentsDirectory =
                        System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/" + Userid;
                    string Image = image.Split('/').Last();
                    string FilePath = System.IO.Path.Combine(documentsDirectory, Image);
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                    {
                        var fileinfo = new FileInfo(FilePath);
                        var previewController = new QuickLook.QLPreviewController
                        {
                            DataSource = new PreviewControllerDataSource(fileinfo.FullName, fileinfo.Name)
                        };

                        var controller = FindNavigationController();

                        controller?.PresentViewController(previewController, true, null);
                    });

                }
                else if (Directory == "Galary")
                {

                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                    {
                        string documentsPath =
                            System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures);
                        string Image = image.Split('/').Last();
                        string jpgFilename =
                            System.IO.Path.Combine(documentsPath + "/" + WowonderPhone.Settings.APP_Name + "/", Image);
                        var fileinfo = new FileInfo(jpgFilename);
                        var previewController = new QuickLook.QLPreviewController
                        {
                            DataSource = new PreviewControllerDataSource(fileinfo.FullName, fileinfo.Name)
                        };

                        var controller = FindNavigationController();
                        controller?.PresentViewController(previewController, true, null);
                    });
                }
                else
                {
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                    {

                        var fileinfo = new FileInfo(image);
                        var previewController = new QuickLook.QLPreviewController
                        {
                            DataSource = new PreviewControllerDataSource(fileinfo.FullName, fileinfo.Name)
                        };

                        var controller = FindNavigationController();

                        controller?.PresentViewController(previewController, true, null);
                    });
                }
            }
            catch (Exception)
            {

            }

        }

        public UINavigationController FindNavigationController()
        {
            foreach (var window in UIApplication.SharedApplication.Windows)
            {
                if (window.RootViewController.NavigationController != null)
                {
                    return window.RootViewController.NavigationController;
                }

                var value = CheckSubs(window.RootViewController.ChildViewControllers);
                if (value != null)
                    return value;
            }
            return null;
        }

        public string CreateProduct(Stream stream, string Filepath, string user_id, string name, string category,
            string description, string price, string location, string type)
        {
            try
            {

                var values = new NameValueCollection();
                values["user_id"] = user_id;
                values["name"] = name;
                values["s"] = Settings.Session;
                values["category"] = category;
                values["description"] = description;
                values["price"] = price;
                values["location"] = location;
                values["type"] = type;

                filepath = Filepath;
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    var files = new[]
                    {
                        new UploadFile
                        {
                            Name = "postPhotos",
                            Filename = System.IO.Path.GetFileName(Filepath),
                            ContentType = "text/plain",
                            Stream = stream
                        }
                    };
                    var result = Upload(Settings.Website + "/app_api.php?application=phone&type=new_product", files,
                        values, "");
                });

                return "Done";
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void ClearWebViewCache()
        {
            NSUrlCache.SharedCache.RemoveAllCachedResponses();
        }

        public void OpenMessengerApp(string packegename)
        {
            try
            {
                bool isSimulator = Runtime.Arch == Arch.SIMULATOR;
                NSUrl itunesLink;
                if (isSimulator)
                {
                    itunesLink = new NSUrl("https://itunes.apple.com/us/genre/ios/" + packegename);
                }
                else
                {
                    itunesLink = new NSUrl("itms://itunes.apple.com");
                }
                UIApplication.SharedApplication.OpenUrl(itunesLink, new NSDictionary() { }, null);
            }
            catch
            {

            }
        }

        public UINavigationController CheckSubs(UIViewController[] controllers)
        {
            foreach (var controller in controllers)
            {
                if (controller.NavigationController != null)
                {
                    return controller.NavigationController;
                }

                var value = CheckSubs(controller.ChildViewControllers);

                return value;
            }

            return null;
        }




        public void OpenWebsiteUrl(string Website)
        {
            try
            {
                //Its working on anouther code
            }
            catch (Exception)
            {

            }
        }

        public void Close_App()
        {
            System.Threading.Thread.CurrentThread.Abort();
        }

        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXdsdaawerthklmnbvcxer46gfdsYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public void showPhoto(string url)
        {
            string AttachmentName = RandomString(9);
            var webClient = new WebClient();
            var AttachmentBytes = webClient.DownloadData(url);

            string dirPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var FileName = dirPath + "/" + AttachmentName;


            //Java.IO.File file = new Java.IO.File(dirPath, FileName);
            var fileinfo = new FileInfo(FileName);


            var filename = System.IO.Path.Combine(dirPath, AttachmentName);
            File.WriteAllBytes(filename, AttachmentBytes);


            Device.BeginInvokeOnMainThread(() =>
            {
                Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                {


                    var previewController = new QuickLook.QLPreviewController
                    {
                        DataSource = new PreviewControllerDataSource(fileinfo.FullName, fileinfo.Name)
                    };

                    var controller = FindNavigationController();

                    controller?.PresentViewController(previewController, true, null);
                });


                try
                {

                    UserDialogs.Instance.HideLoading();
                }
                catch (System.Exception ex)
                {

                    UserDialogs.Instance.HideLoading();
                }
            });
        }

        public static string filepath;

        public string UploudAttachment(Stream stream, string FileNameAttachment, string user_id, string recipient_id,
            string Session, string TextMsg, string time2)
        {
            try
            {
                var values = new NameValueCollection();
                values["user_id"] = user_id;
                values["recipient_id"] = recipient_id;
                values["s"] = Session;
                values["text"] = TextMsg;
                values["send_time"] = time2;


                //Copy to Wowonder Folder application Just For IOS
                //Important to show the image on the Chatwindow when the user clicks
                string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures);
                var DirectoryPath = documentsPath + "/" + WowonderPhone.Settings.APP_Name + "/";
                string ImageName = FileNameAttachment.Split('/').Last();
                string ImageFullPath = Path.Combine(DirectoryPath, ImageName);
                System.IO.File.Copy(FileNameAttachment, ImageFullPath);
                filepath = ImageFullPath;

                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    var files = new[]
                    {
                        new UploadFile
                        {
                            Name = "message_file",
                            Filename = System.IO.Path.GetFileName(FileNameAttachment),
                            ContentType = "text/plain",
                            Stream = stream
                        }
                    };
                    var result =
                        UploadFiles(Settings.Website + "/app_api.php?application=phone&type=insert_new_message", files,
                            values);

                });

                return "Ok";
            }
            catch (Exception)
            {
                return "Error";
            }
        }

        public async Task<byte[]> UploadFiles(string address, IEnumerable<UploadFile> files, NameValueCollection values)
        {
            try
            {
                var request = WebRequest.Create(address);
                request.Method = "POST";
                var boundary = "---------------------------" +
                               DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                boundary = "--" + boundary;

                using (var requestStream = request.GetRequestStream())
                {
                    // Write the values
                    foreach (string name in values.Keys)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(string.Format(
                            "Content-Disposition: form-data; name=\"{0}\"{1}{1}", name, System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(values[name] + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    // Write the files
                    foreach (var file in files)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(string.Format(
                            "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}", file.Name,
                            file.Filename, System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", file.ContentType,
                            System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        file.Stream.CopyTo(requestStream);
                        buffer = Encoding.ASCII.GetBytes(System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                    requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);
                }

                using (var response = request.GetResponse())
                using (var responseStream = response.GetResponseStream())
                using (var stream = new MemoryStream())
                {

                    responseStream.CopyTo(stream);
                    var json = System.Text.Encoding.UTF8.GetString(stream.ToArray(), 0, stream.ToArray().Length);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var messages = JObject.Parse(json).SelectToken("messages").ToString();
                        JArray ChatMessages = JArray.Parse(messages);
                        //Controls.Functions.UpdatelastIdMessage(ChatMessages, filepath);
                    }

                    return stream.ToArray();
                }
            }

            catch (Exception)
            {
                return null;
            }
        }

        private TimelinePostsTab _Page_Timeline;

        public string AddPost(Stream stream, string Filepath, string postText, string postPrivacy, string PostType,
            TimelinePostsTab Page)
        {
            try
            {
                _Page_Timeline = Page;
                var values = new NameValueCollection();
                values["user_id"] = Settings.User_id;
                values["postText"] = postText;
                values["s"] = Settings.Session;
                values["postPrivacy"] = postPrivacy;
                //filepath = Filepath;

                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    if (PostType == "Image")
                    {
                        var files = new[]
                        {
                            new UploadFile
                            {
                                Name = "postPhotos",
                                Filename = System.IO.Path.GetFileName(Filepath),
                                ContentType = "text/plain",
                                Stream = stream
                            }
                        };
                        var result = Upload(Settings.Website + "/app_api.php?application=phone&type=new_post", files,
                            values, Filepath);
                    }
                    else if (PostType == "Video")
                    {
                        var files = new[]
                        {
                            new UploadFile
                            {
                                Name = "postVideo",
                                Filename = System.IO.Path.GetFileName(Filepath),
                                ContentType = "text/plain",
                                Stream = stream
                            }
                        };
                        var result =
                            Upload(Settings.Website + "/app_api.php?application=phone&type=new_post", files, values,
                                Filepath).ConfigureAwait(false);

                    }
                });




                return "Done";
            }
            catch (Exception)
            {
                return null;
            }
        }


        private SocialGroup _SocialGroup;
        private SocialPageViewer _SocialPageViewer;

        public string AddPost_Communities(Stream stream, string Filepath, string postText, string postPrivacy,
            string PostType, string Community_id, SocialGroup pageGroup, SocialPageViewer pageViewer)
        {
            try
            {
                _SocialGroup = pageGroup;
                _SocialPageViewer = pageViewer;

                var values = new NameValueCollection();
                values["user_id"] = Settings.User_id;
                if (_SocialGroup != null)
                {
                    values["group_id"] = Community_id;
                }
                else if (_SocialPageViewer != null)
                {
                    values["page_id"] = Community_id;
                }
                values["postText"] = postText;
                values["s"] = Settings.Session;
                values["postPrivacy"] = postPrivacy;
                //filepath = Filepath;

                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    if (PostType == "Image")
                    {
                        var files = new[]
                        {
                            new UploadFile
                            {
                                Name = "postPhotos",
                                Filename = System.IO.Path.GetFileName(Filepath),
                                ContentType = "text/plain",
                                Stream = stream
                            }
                        };
                        var result = Upload(Settings.Website + "/app_api.php?application=phone&type=new_post", files,
                            values, Filepath);
                    }
                    else if (PostType == "Video")
                    {
                        var files = new[]
                        {
                            new UploadFile
                            {
                                Name = "postVideo",
                                Filename = System.IO.Path.GetFileName(Filepath),
                                ContentType = "text/plain",
                                Stream = stream
                            }
                        };
                        var result = Upload(Settings.Website + "/app_api.php?application=phone&type=new_post", files,
                            values, Filepath).ConfigureAwait(false);
                    }
                });
                return "Done";
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }


        public string AddEvent(Stream stream, string Filepath, string event_name, string event_locat,
            string event_start_date, string event_start_time, string event_end_date, string event_end_time,
            string event_description)
        {

            try
            {

                var values = new NameValueCollection();
                values["user_id"] = Settings.User_id;
                values["s"] = Settings.Session;
                values["event-name"] = event_name;
                values["event-locat"] = event_locat;
                values["event-start-date"] = event_start_date;
                values["event-start-time"] = event_start_time;
                values["event-end-date"] = event_end_date;
                values["event-end-time"] = event_end_time;
                values["event-description"] = event_description;
                values["event-end-time"] = event_end_time;

                filepath = Filepath;
                System.Threading.Tasks.Task.Factory.StartNew(() =>
                {
                    var files = new[]
                    {
                        new UploadFile
                        {
                            Name = "event-cover",
                            Filename = System.IO.Path.GetFileName(Filepath),
                            ContentType = "image/png",
                            Stream = stream
                        }
                    };
                    var result = UploadEventData(Settings.Website + "/app_api.php?application=phone&type=create_event",
                        files, values, "");
                });

                return "Done";
            }
            catch (Exception)
            {
                return null;
            }

        }

        public string AddStory(List<Stream> stream, string Filepath, string title, string description)
        {
            try
            {

                var values = new NameValueCollection();
                values["user_id"] = Settings.User_id;
                values["s"] = Settings.Session;
                values["title"] = title;
                values["description"] = description;

                List<String> filepath = new List<string>();
                filepath.Add(System.IO.Path.GetFileName(Filepath));


                List<string> images = new List<string>();
                foreach (var File in stream)
                {
                    var _ms = new MemoryStream();
                    File.CopyTo(_ms);
                    byte[] imageBytes = _ms.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    images.Add(base64String);
                }



                string json = JsonConvert.SerializeObject(images);

                values["images"] = json;



                using (WebClient client = new WebClient())
                {

                    byte[] response =
                        client.UploadValues(Settings.Website + "/app_api.php?application=phone&type=create_story",
                            values);

                    string result = System.Text.Encoding.UTF8.GetString(response);
                }


                //System.Threading.Tasks.Task.Factory.StartNew(() =>
                //{
                //    using (WebClient client = new WebClient())
                //    {

                //        byte[] response = client.UploadValues(Settings.Website + "/app_api.php?application=phone&type=create_story", values);

                //        string result = System.Text.Encoding.UTF8.GetString(response);
                //    }

                //    //var files = new[] { new UploadFile2 { Name = "statusMedia", Filename = ff, ContentType = "image/png", Stream = stream } };

                //    //var result = UploadStoryData(Settings.Website + "/app_api.php?application=phone&type=create_story", files, values, "");
                //});

                return "Done";
            }
            catch (Exception)
            {
                return null;
            }
        }





        public async Task<byte[]> UploadEventData(string address, IEnumerable<UploadFile> files,
            NameValueCollection values,
            string filee)
        {
            try
            {
                var request = WebRequest.Create(address);
                request.Method = "POST";
                var boundary = "---------------------------" +
                               DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                boundary = "--" + boundary;

                using (var requestStream = request.GetRequestStream())
                {
                    // Write the values
                    foreach (string name in values.Keys)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(
                            string.Format("Content-Disposition: form-data; name=\"{0}\"{1}{1}", name,
                                System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(values[name] + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    // Write the files
                    foreach (var file in files)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(
                            string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}",
                                file.Name, file.Filename, System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", file.ContentType,
                            Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        file.Stream.CopyTo(requestStream);
                        buffer = Encoding.ASCII.GetBytes(System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                    requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);

                }

                using (var response = request.GetResponse())
                using (var responseStream = response.GetResponseStream())

                using (var stream = new MemoryStream())
                {

                    responseStream.CopyTo(stream);
                    responseStream.Close();
                    var json = System.Text.Encoding.UTF8.GetString(stream.ToArray(), 0, stream.ToArray().Length);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        UserDialogs.Instance.ShowSuccess("Event Added", 2000);

                        if (_Page_Timeline != null)
                        {
                            _Page_Timeline.PostAjaxRefresh("");
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.ShowError("Event Faild", 2000);
                    }

                    return null;
                }
            }

            catch (WebException ex)
            {
                UserDialogs.Instance.ShowError("Event Faild", 2000);
                string output = string.Empty;
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    using (var stream = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        output = stream.ReadToEnd();
                    }
                }
                else if (ex.Status == WebExceptionStatus.Timeout)
                {
                    output = "Request timeout is expired.";
                }

                return null;
            }
        }

        public async Task<byte[]> UploadStoryData(string address, IEnumerable<UploadFile> files,
            NameValueCollection values,
            string filee)
        {
            try
            {
                var request = WebRequest.Create(address);
                request.Method = "POST";
                var boundary = "---------------------------" +
                               DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                boundary = "--" + boundary;

                using (var requestStream = request.GetRequestStream())
                {
                    // Write the values
                    foreach (string name in values.Keys)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(
                            string.Format("Content-Disposition: form-data; name=\"{0}\"{1}{1}", name,
                                System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(values[name] + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    // Write the files
                    foreach (var file in files)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(
                            string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}",
                                file.Name, file.Filename, System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", file.ContentType,
                            System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        file.Stream.CopyTo(requestStream);
                        buffer = Encoding.ASCII.GetBytes(System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                    requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);

                }

                using (var response = request.GetResponse())
                using (var responseStream = response.GetResponseStream())

                using (var stream = new MemoryStream())
                {

                    responseStream.CopyTo(stream);
                    responseStream.Close();
                    var json = System.Text.Encoding.UTF8.GetString(stream.ToArray(), 0, stream.ToArray().Length);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        UserDialogs.Instance.ShowSuccess("Story Added", 2000);

                        if (_Page_Timeline != null)
                        {
                            _Page_Timeline.PostAjaxRefresh("");
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.ShowError("Story Faild", 2000);
                    }

                    return null;
                }
            }

            catch (WebException ex)
            {
                UserDialogs.Instance.ShowError("Story Faild", 2000);
                string output = string.Empty;
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    using (var stream = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        output = stream.ReadToEnd();
                    }
                }
                else if (ex.Status == WebExceptionStatus.Timeout)
                {
                    output = "Request timeout is expired.";
                }

                return null;
            }
        }


        public void EnableGPSLocation()
        {
            try
            {
                if (CLLocationManager.Status == CLAuthorizationStatus.Denied)
                {
                    if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
                    {
                        NSString settingsString = UIApplication.OpenSettingsUrlString;
                        NSUrl url = new NSUrl(settingsString);
                        UIApplication.SharedApplication.OpenUrl(url);
                    }
                }
            }
            catch (Exception e)
            {

            }

        }










        public async Task<byte[]> Upload(string address, IEnumerable<UploadFile> files, NameValueCollection values,
            string filee)
        {
            try
            {
                var request = WebRequest.Create(address);
                request.Method = "POST";
                var boundary = "---------------------------" +
                               DateTime.Now.Ticks.ToString("x", NumberFormatInfo.InvariantInfo);
                request.ContentType = "multipart/form-data; boundary=" + boundary;
                boundary = "--" + boundary;

                using (var requestStream = request.GetRequestStream())
                {
                    // Write the values
                    foreach (string name in values.Keys)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(string.Format(
                            "Content-Disposition: form-data; name=\"{0}\"{1}{1}", name, System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(values[name] + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    // Write the files
                    foreach (var file in files)
                    {
                        var buffer = Encoding.ASCII.GetBytes(boundary + System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.UTF8.GetBytes(string.Format(
                            "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"{2}", file.Name,
                            file.Filename, System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        buffer = Encoding.ASCII.GetBytes(string.Format("Content-Type: {0}{1}{1}", file.ContentType,
                            System.Environment.NewLine));
                        requestStream.Write(buffer, 0, buffer.Length);
                        file.Stream.CopyTo(requestStream);
                        buffer = Encoding.ASCII.GetBytes(System.Environment.NewLine);
                        requestStream.Write(buffer, 0, buffer.Length);
                    }

                    var boundaryBuffer = Encoding.ASCII.GetBytes(boundary + "--");
                    requestStream.Write(boundaryBuffer, 0, boundaryBuffer.Length);

                }

                using (var response = request.GetResponse())
                using (var responseStream = response.GetResponseStream())

                using (var stream = new MemoryStream())
                {

                    responseStream.CopyTo(stream);
                    responseStream.Close();
                    var json = System.Text.Encoding.UTF8.GetString(stream.ToArray(), 0, stream.ToArray().Length);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        UserDialogs.Instance.ShowSuccess("Post Added", 2000);

                        if (_Page_Timeline != null)
                        {
                            _Page_Timeline.PostAjaxRefresh("");
                        }
                    }
                    else
                    {
                        UserDialogs.Instance.ShowError("Post Faild", 2000);
                    }

                    return null;
                }
            }

            catch (WebException ex)
            {
                UserDialogs.Instance.ShowError("Post Faild", 2000);
                string output = string.Empty;
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    using (var stream = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        output = stream.ReadToEnd();
                    }
                }
                else if (ex.Status == WebExceptionStatus.Timeout)
                {
                    output = "Request timeout is expired.";
                }

                return null;
            }
        }




        public class UploadFile
        {
            public UploadFile()
            {
                ContentType = "application/octet-stream";
            }

            public string Name { get; set; }
            public string Filename { get; set; }
            public string ContentType { get; set; }
            public Stream Stream { get; set; }
        }

    }

    public class DocumentItem : QLPreviewItem
    {
        private readonly string _uri;

        public DocumentItem(string title, string uri)
        {
            ItemTitle = title;
            _uri = uri;
        }

        public override string ItemTitle { get; }

        public override NSUrl ItemUrl => NSUrl.FromFilename(_uri);
    }

    public class PreviewControllerDataSource : QLPreviewControllerDataSource
    {
        private readonly string _url;
        private readonly string _filename;

        public PreviewControllerDataSource(string url, string filename)
        {
            _url = url;
            _filename = filename;
        }

        public override IQLPreviewItem GetPreviewItem(QLPreviewController controller, nint index)
        {
            return new DocumentItem(_filename, _url);
        }

        public override nint PreviewItemCount(QLPreviewController controller)
        {
            return 1;
        }
    }
}