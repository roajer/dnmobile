﻿using System;
using System.Collections.Generic;

using System.Text;
using UIKit;
using WowonderPhone.Dependencies;
using Xamarin.Forms;

[assembly: Dependency(typeof(WowonderPhone.iOS.Dependencies.ClipboardService))]

namespace WowonderPhone.iOS.Dependencies
{
    public class ClipboardService : IClipboardService
    {
        public void CopyToClipboard(String text)
        {
            UIPasteboard clipboard = UIPasteboard.General;
            clipboard.String = text;
        }

    } 
}