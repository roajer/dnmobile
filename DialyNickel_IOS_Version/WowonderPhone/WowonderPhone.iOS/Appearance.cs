﻿using System;
using UIKit;
using UXDivers.Artina.Grial;
using Xamarin.Forms.Platform.iOS;

namespace WowonderPhone.iOS
{
   
		public class Appearance
		{

			public static UIColor AccentColor = ExportedColors.AccentColor.ToUIColor();
			public static UIColor TextColor = ExportedColors.InverseTextColor.ToUIColor();

			public static void Configure()
			{
				UINavigationBar.Appearance.BarTintColor = UIColor.White;
				UINavigationBar.Appearance.TintColor = UIColor.White;
				UINavigationBar.Appearance.TitleTextAttributes = new UIStringAttributes
				{
					ForegroundColor = TextColor,
				};

				UIProgressView.Appearance.ProgressTintColor = AccentColor;

				UISlider.Appearance.MinimumTrackTintColor = AccentColor;
				UISlider.Appearance.MaximumTrackTintColor = AccentColor;
				UISlider.Appearance.ThumbTintColor = AccentColor;

				UISwitch.Appearance.OnTintColor = AccentColor;

            UITableViewHeaderFooterView.Appearance.TintColor = UIColor.White;

				UITableView.Appearance.SectionIndexBackgroundColor = AccentColor;
				UITableView.Appearance.SeparatorColor = AccentColor;

            UITabBar.Appearance.TintColor = UIColor.White;

				UITextField.Appearance.TintColor = AccentColor;

            UIButton.Appearance.TintColor = UIColor.White;
            UIButton.Appearance.SetTitleColor(UIColor.White, UIControlState.Normal);


			}
		}
}
