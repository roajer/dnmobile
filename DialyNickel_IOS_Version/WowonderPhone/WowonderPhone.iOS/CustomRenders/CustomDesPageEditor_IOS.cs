﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;
using WowonderPhone.CustomRenders;
using WowonderPhone.iOS.CustomRenders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;


[assembly: ExportRenderer(typeof(CustomDesPageEditor), typeof(CustomDesPageEditor_IOS))]
namespace WowonderPhone.iOS.CustomRenders
{
    class CustomDesPageEditor_IOS : EditorRenderer
    {
        private UIColor DefaultPlaceholderColor { get; set; } = Color.FromRgb(199, 199, 205).ToUIColor();
        private UILabel PlaceholderLabel { get; set; }
        private string Placeholder { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {

            base.OnElementChanged(e);
            var element = this.Element as CustomDesPageEditor;

            if (Control != null && element != null)
            {
                //Placeholder = element.Placeholder;
                Control.TextColor = UIColor.LightGray;
                Control.Text = Settings.Label_add_short_description;

                Control.ShouldBeginEditing += (UITextView textView) =>
                {
                    if (textView.Text == Settings.Label_add_short_description)
                    {
                        textView.Text = "";
                        textView.TextColor = UIColor.Black; // Text Color
                    }
                    return true;
                };

                Control.ShouldEndEditing += (UITextView textView) =>
                {
                    if (textView.Text == "")
                    {
                        textView.Text = Settings.Label_add_short_description;
                        textView.TextColor = UIColor.LightGray; // Placeholder Color
                    }
                    return true;
                };
            }
        }
    }

    public class myTextViewDelegate : UITextViewDelegate
    {
        public string Placeholder { get; set; }

        public override void EditingStarted(UITextView textView)
        {
            try
            {
                if (textView.Text == Placeholder)
                {
                    textView.Text = "";
                    textView.TextColor = UIColor.Black;
                }
                textView.BecomeFirstResponder();

                UIView view = getRootSuperView(textView);

                CGRect rect = view.Frame;

                rect.Y -= 80;

                view.Frame = rect;

            }
            catch (Exception)
            {

            }
        }

        private UIView getRootSuperView(UIView view)
        {
            if (view.Superview == null)
                return view;
            else
                return getRootSuperView(view.Superview);
        }

        public override void EditingEnded(UITextView textView)
        {
            try
            {
                if (textView.Text == "")
                {
                    textView.Text = Placeholder;
                    textView.TextColor = UIColor.LightGray;
                }
                textView.ResignFirstResponder();

                UIView view = getRootSuperView(textView);

                CGRect rect = view.Frame;

                rect.Y += 80;

                view.Frame = rect;

            }
            catch (Exception ex)
            {

            }
        }
    }
}