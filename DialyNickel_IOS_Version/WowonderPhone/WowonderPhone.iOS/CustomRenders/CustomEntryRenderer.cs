﻿using System;

using Xamarin.Forms;
using WowonderPhone.CustomRenders;
using WowonderPhone.Languish;
using WowonderPhone.iOS.CustomRenders;
using Xamarin.Forms.Platform.iOS;
using CoreGraphics;
using UIKit;
using Foundation;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEntryRenderer))]

namespace WowonderPhone.iOS.CustomRenders
{
    public class CustomEntryRenderer : EditorRenderer
    {
        private UIColor DefaultPlaceholderColor { get; set; } = Color.FromRgb(199, 199, 205).ToUIColor();
        private UILabel PlaceholderLabel { get; set; }
        private string Placeholder { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {

            base.OnElementChanged(e);
            var element = this.Element as CustomEditor;

            if (Control != null && element != null)
            {
                //Placeholder = element.Placeholder;
                Control.TextColor = UIColor.LightGray;
                Control.Text = Settings.Label_Whats_Going_On;

                Control.ShouldBeginEditing += (UITextView textView) =>
                {
                    if (textView.Text == Settings.Label_Whats_Going_On)
                    {
                        textView.Text = "";
                        textView.TextColor = UIColor.Black; // Text Color
                    }
                    return true;
                };

                Control.ShouldEndEditing += (UITextView textView) =>
                {
                    if (textView.Text == "")
                    {
                        textView.Text = Settings.Label_Whats_Going_On;
                        textView.TextColor = UIColor.LightGray; // Placeholder Color
                    }
                    return true;
                };
            }
        }
    }

   

}