﻿using System.Collections.Generic;
using System.IO;
using WowonderPhone.Pages.Tabs;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;

namespace WowonderPhone.Dependencies
{
    public interface IMethods
    {
        void OpenImage(string Directory, string image, string Userid);

        void OpenWebsiteUrl(string Website);

        string UploudAttachment(Stream stream, string Filepath, string user_id, string recipient_id, string session, string text, string time2);
        string CreateProduct(Stream stream, string Filepath, string user_id, string name, string category, string description, string price, string location, string type);
        string AddPost(Stream stream, string Filepath, string postText, string postPrivacy, string PostType, TimelinePostsTab Page);
        string AddPost_Communities(Stream stream, string Filepath, string postText, string postPrivacy, string PostType,string Community_id, SocialGroup pageGroup, SocialPageViewer pageViewer);

        void OpenMessengerApp(string Packegename);
        void ClearWebViewCache();
        string AddEvent(Stream stream, string Filepath, string event_name , string event_locat, string event_start_date, string event_start_time, string event_end_date, string event_end_time, string event_description);
        string AddStory(List<Stream> stream, string Filepath, string title, string description);
        void Close_App();

        void showPhoto(string url);
        void EnableGPSLocation();
    }
}
