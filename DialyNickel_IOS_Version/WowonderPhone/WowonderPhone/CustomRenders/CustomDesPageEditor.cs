﻿using System;
using Xamarin.Forms;

namespace WowonderPhone.CustomRenders
{
   public class CustomDesPageEditor : Editor
    {
        public static readonly BindableProperty FontProperty = BindableProperty.Create<CustomDesPageEditor, Font>(p => p.Font, default(Font));
        public EventHandler LeftSwipe;
        public EventHandler RightSwipe;


        public Font Font
        {
            get { return (Font)GetValue(FontProperty); }
            set { SetValue(FontProperty, value); }
        }

        internal void OnLeftSwipe(object sender, EventArgs e)
        {
            var handler = this.LeftSwipe;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Handles the <see cref="E:RightSwipe" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        internal void OnRightSwipe(object sender, EventArgs e)
        {
            var handler = this.RightSwipe;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
