﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WowonderPhone.Controls
{
    public class NotifiTypeFunction
    {

        public static string GetIconFontAwesom(string Type)
        {
            try
            {
                var Icon_Type_FO = "#444";

                if (Type == "following")
                {
                    Icon_Type_FO = "\uf234";
                    return Icon_Type_FO;
                }
                else if (Type == "comment" || Type == "comment_reply" || Type == "also_replied")
                {
                    Icon_Type_FO = "\uf075";
                    return Icon_Type_FO;
                }
                else if (Type == "liked_post" || Type == "liked_comment" || Type == "liked_reply_comment")
                {
                    Icon_Type_FO = "\uf164";
                    return Icon_Type_FO;
                }
                else if (Type == "wondered_post" || Type == "wondered_comment" || Type == "wondered_reply_comment" ||
                         Type == "exclamation-circle")
                {
                    Icon_Type_FO = "\uf05a";
                    return Icon_Type_FO;
                }
                else if (Type == "comment_mention" || Type == "comment_reply_mention")
                {
                    Icon_Type_FO = "\uf02b";
                    return Icon_Type_FO;
                }
                else if (Type == "post_mention")
                {
                    Icon_Type_FO = "\uf140";
                    return Icon_Type_FO;
                }
                else if (Type == "share_post")
                {
                    Icon_Type_FO = "\uf14d";
                    return Icon_Type_FO;
                }
                else if (Type == "profile_wall_post")
                {
                    Icon_Type_FO = "\uf0ca";
                    return Icon_Type_FO;
                }
                else if (Type == "visited_profile")
                {

                    Icon_Type_FO = "\uf06e";
                    return Icon_Type_FO;
                }
                else if (Type == "liked_page")
                {

                    Icon_Type_FO = "\uf164";
                    return Icon_Type_FO;
                }
                else if (Type == "joined_group" || Type == "accepted_invite" || Type == "accepted_request")
                {

                    Icon_Type_FO = "\uf046";
                    return Icon_Type_FO;
                }
                else if (Type == "invited_page")
                {

                    Icon_Type_FO = "\uf024";
                    return Icon_Type_FO;
                }
                else if (Type == "accepted_join_request")
                {

                    Icon_Type_FO = "\uf00c";
                    return Icon_Type_FO;
                }

                else if (Type == "added_you_to_group")
                {

                    Icon_Type_FO = "\uf192";
                    return Icon_Type_FO;
                }
                else if (Type == "requested_to_join_group")
                {

                    Icon_Type_FO = "\uf017";
                    return Icon_Type_FO;
                }
                else if (Type == "thumbs-down")
                {
                    Icon_Type_FO = "\uf165";
                    return Icon_Type_FO;
                }
                else if (Type == "going_event")
                {
                    Icon_Type_FO = "\uf133";
                    return Icon_Type_FO;
                }
                else if (Type == "admin_notification")
                {
                    Icon_Type_FO = "\uf06e";
                    return Icon_Type_FO;
                }

                //
                else
                {
                    Icon_Type_FO = "\uf0da";
                    return Icon_Type_FO;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "";
            }
        }

        public static string GetColorFontAwesom(string Type)
        {
            try
            {
                var Icon_Color_FO = "#444";

                if (Type == "following")
                {
                    Icon_Color_FO = "#444";
                    return Icon_Color_FO;
                }
                else if (Type == "comment" || Type == "comment_reply" || Type == "also_replied")
                {
                    Icon_Color_FO = Settings.MainColor;
                    return Icon_Color_FO;
                }
                else if (Type == "liked_post" || Type == "liked_comment" || Type == "liked_reply_comment")
                {
                    Icon_Color_FO = Settings.MainColor;

                    return Icon_Color_FO;
                }
                else if (Type == "wondered_post" || Type == "wondered_comment" || Type == "wondered_reply_comment" || Type == "exclamation-circle")
                {
                    Icon_Color_FO = "#FFA500";
                    return Icon_Color_FO;
                }
                else if (Type == "comment_mention" || Type == "comment_reply_mention")
                {
                    Icon_Color_FO = "#B20000";
                    return Icon_Color_FO;
                }
                else if (Type == "post_mention")
                {
                    Icon_Color_FO = "#B20000";
                    return Icon_Color_FO;
                }
                else if (Type == "share_post")
                {
                    Icon_Color_FO = "#2F2FFF";
                    return Icon_Color_FO;
                }
                else if (Type == "profile_wall_post")
                {
                    Icon_Color_FO = "#444";
                    return Icon_Color_FO;
                }
                else if (Type == "visited_profile")
                {

                    Icon_Color_FO = "#328432";
                    return Icon_Color_FO;
                }
                else if (Type == "liked_page")
                {

                    Icon_Color_FO = "#2F2FFF";
                    return Icon_Color_FO;
                }
                else if (Type == "joined_group" || Type == "accepted_invite" || Type == "accepted_request")
                {

                    Icon_Color_FO = "#2F2FFF";
                    return Icon_Color_FO;
                }
                else if (Type == "invited_page")
                {

                    Icon_Color_FO = "#B20000";
                    return Icon_Color_FO;
                }
                else if (Type == "accepted_join_request")
                {

                    Icon_Color_FO = "#2F2FFF";
                    return Icon_Color_FO;
                }

                else if (Type == "added_you_to_group")
                {

                    Icon_Color_FO = "#444";
                    return Icon_Color_FO;
                }
                else if (Type == "requested_to_join_group")
                {

                    Icon_Color_FO = Settings.MainColor;
                    return Icon_Color_FO;
                }
                else if (Type == "thumbs-down")
                {

                    Icon_Color_FO = Settings.MainColor;
                    return Icon_Color_FO;
                }
                else if (Type == "going_event")
                {
                    Icon_Color_FO = "#444";
                    return Icon_Color_FO;
                }
                else if (Type == "admin_notification")
                {
                    Icon_Color_FO = "#444";
                    return Icon_Color_FO;
                }
                else
                {
                    Icon_Color_FO = "#444";
                    return Icon_Color_FO;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "";
            }
        }
    }
}
