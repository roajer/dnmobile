﻿//WOWONDER Timeline v1.7
//Copyright: DoughouzLight
//Email: alidoughous1993@gmail.com
//Full Documentation https://paper.dropbox.com/doc/WoWonder-Timeline-v1.5-J2AnlcVy05dpGilAJCNzy?_tk
//==============================

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;
using WowonderPhone.AdInterstitial;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using WowonderPhone.SQLite.Tables;

namespace WowonderPhone.Controls
{
    public class Functions
    {
        #region  Lists Items Declaration

        public static ObservableCollection<ChatUsers> ChatList = new ObservableCollection<ChatUsers>();
        public static ObservableCollection<UserContacts> ChatContactsList = new ObservableCollection<UserContacts>();
        public static ObservableCollection<GroupedChatList> grouped = new ObservableCollection<GroupedChatList>();

        public static ObservableCollection<GroupedRandomContacts> groupedRandomlist =
            new ObservableCollection<GroupedRandomContacts>();

        public static ObservableCollection<SearchResult> SearchFilterlist = new ObservableCollection<SearchResult>();
        public static ObservableCollection<MessageViewModal> Messages = new ObservableCollection<MessageViewModal>();

        #endregion

        #region Variables

        public static string NumberOfcontacts;
        public static string Aftercontact = "0";
        public static string NotifiStoper = "0";
        public static Random random = new Random();

        public static string Error =
                "Verify the application from your Admin panel, Or Use the last version of your Script, Or the application is used for illegal use contact support team to remove the ban from your app"
            ;

        #endregion

        public static void Ad_Interstitial()
        {
            try
            {
                if (Settings.Show_ADMOB_Interstitial)
                {
                    DependencyService.Get<IAdInterstitial>().ShowAd();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public static string TrimTo(string str, int maxLength)
        {
            try
            {
                if (str.Length <= maxLength)
                {
                    return str;
                }
                else if (str.Length > 35)
                {
                    str.Remove(0, 10);
                    return str;
                }
                else if (str.Length > 65)
                {
                    str.Remove(0, 30);
                    return str;
                }
                else if (str.Length > 85)
                {
                    str.Remove(0, 50);
                    return str;
                }
                else if (str.Length > 105)
                {
                    str.Remove(0, 70);
                    return str;
                }
                else
                {
                    return str.Substring(maxLength - 17, maxLength);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return str.Substring(maxLength - 17, maxLength);
            }
        }

        //public static async Task<string> GetChatActivity(string userid, string session)
        //{
        //    try
        //    {
        //        #region Client Respone

        //        using (var client = new HttpClient())
        //        {
        //            var formContent = new FormUrlEncodedContent(new[]
        //            {
        //                new KeyValuePair<string, string>("user_id", Settings.User_id),
        //                new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
        //                new KeyValuePair<string, string>("s", Settings.Session),
        //                new KeyValuePair<string, string>("list_type", "all")
        //            });

        //            var response =
        //                await
        //                    client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_users_list",
        //                        formContent).ConfigureAwait(false);
        //            response.EnsureSuccessStatusCode();
        //            string json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
        //            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        //            string apiStatus = data["api_status"].ToString();

        //            #endregion

        //            if (apiStatus == "200")
        //            {
        //                // Functions.ChatList.Clear();

        //                #region Respone Success

        //                var Users = data["users"];
        //                string ThemeUrl = data["theme_url"].ToString();
        //                var users = JObject.Parse(json).SelectToken("users").ToString();

        //                JArray Chatusers = JArray.Parse(users);
        //                if (Chatusers.Count <= 0)
        //                {
        //                    var Cheke =
        //                        ChatList.FirstOrDefault(
        //                            a => a.Username == "  " + "  " + "  " + Settings.Label_NoAnyChatActivityAvailable);
        //                    if (ChatList.Contains(Cheke))
        //                    {

        //                    }
        //                    else
        //                    {
        //                        ChatList.Insert(0, new ChatUsers()
        //                        {
        //                            Username = "  " + "  " + "  " + Settings.Label_NoAnyChatActivityAvailable,
        //                            lastseen = "  ",
        //                            profile_picture = "  ",
        //                            TextMessage = "  ",
        //                            LastMessageDateTime = "  ",
        //                            UserID = "  ",
        //                            SeenMessageOrNo = "Transparent",
        //                            ChekeSeen = "false",
        //                            OnOficon = "  ",
        //                            TimeLong = "  ",
        //                            profile_picture_Url = "  "
        //                        });
        //                    }

        //                    return null;
        //                }

        //                //var ListTodelete = new ChatActivityFunctions();
        //                // ListTodelete.ClearChatUserTable();
        //                foreach (var ChatUser in Chatusers)
        //                {


        //                    JObject ChatlistUserdata = JObject.FromObject(ChatUser);
        //                    var ChatUser_User_ID = ChatlistUserdata["user_id"].ToString();
        //                    var ChatUser_avatar = ChatlistUserdata["profile_picture"].ToString();
        //                    var ChatUser_name = ChatlistUserdata["name"].ToString();
        //                    var ChatUser_lastseen = ChatlistUserdata["lastseen"].ToString();
        //                    // var ChatUser_lastseen_Time_Text = ChatlistUserdata["lastseen_time_text"].ToString();
        //                    //var ChatUser_verified = ChatlistUserdata["verified"].ToString();


        //                    JObject ChatlistuserLastMessage = JObject.FromObject(ChatlistUserdata["last_message"]);
        //                    var listuserLastMessage_Text = ChatlistuserLastMessage["text"].ToString();
        //                    var listuserLastMessage_date_time = ChatlistuserLastMessage["date_time"].ToString();
        //                    var SeenCo = ChatlistuserLastMessage["seen"].ToString();
        //                    var fromId = ChatlistuserLastMessage["from_id"].ToString();
        //                    var Messageid = ChatlistuserLastMessage["id"].ToString();
        //                    var Time = ChatlistuserLastMessage["time"].ToString();
        //                    var mediaFileName = ChatlistuserLastMessage["media"].ToString();

        //                    listuserLastMessage_Text = System.Net.WebUtility.HtmlDecode(listuserLastMessage_Text);
        //                    var MediafileIcon = "";
        //                    if (mediaFileName.Contains("soundFile"))
        //                    {
        //                        MediafileIcon = Settings.FN_SoundFileEmoji;
        //                    }
        //                    else if (mediaFileName.Contains("image"))
        //                    {
        //                        MediafileIcon = Settings.FN_ImageFileEmoji;
        //                    }
        //                    else if (mediaFileName.Contains("video"))
        //                    {
        //                        MediafileIcon = Settings.FN_VideoFileEmoji;
        //                    }
        //                    if (listuserLastMessage_Text == "")
        //                    {
        //                        listuserLastMessage_Text = MediafileIcon;
        //                    }
        //                    else
        //                    {
        //                        listuserLastMessage_Text = listuserLastMessage_Text
        //                            .Replace(":)", "\ud83d\ude0a")
        //                            .Replace(";)", "\ud83d\ude09")
        //                            .Replace("0)", "\ud83d\ude07")
        //                            .Replace("(<", "\ud83d\ude02")
        //                            .Replace(":D", "\ud83d\ude01")
        //                            .Replace("*_*", "\ud83d\ude0d")
        //                            .Replace("(<", "\ud83d\ude02")
        //                            .Replace("<3", "\ud83d\u2764")
        //                            .Replace("/_)", "\ud83d\ude0f")
        //                            .Replace("-_-", "\ud83d\ude11")
        //                            .Replace(":-/", "\ud83d\ude15")
        //                            .Replace(":*", "\ud83d\ude18")
        //                            .Replace(":_p", "\ud83d\ude1b")
        //                            .Replace(":p", "\ud83d\ude1c")
        //                            .Replace("x(", "\ud83d\ude20")
        //                            .Replace("X(", "\ud83d\ude21")
        //                            .Replace(":_(", "\ud83d\ude22")
        //                            .Replace("<5", "\ud83d\u2B50")
        //                            .Replace(":0", "\ud83d\ude31")
        //                            .Replace("B)", "\ud83d\ude0e")
        //                            .Replace("o(", "\ud83d\ude27")
        //                            .Replace("</3", "\uD83D\uDC94")
        //                            .Replace(":o", "\ud83d\ude26")
        //                            .Replace("o(", "\ud83d\ude27")
        //                            .Replace(":__(", "\ud83d\ude2d")
        //                            .Replace("!_", "\uD83D\u2757")
        //                            .Replace("<br> <br>", " ").Replace("<br>", " ");

        //                    }

        //                    var color = "Transparent";
        //                    var Chekicon = "false";

        //                    if (fromId != Settings.User_id)
        //                    {
        //                        if (SeenCo == "0")
        //                        {
        //                            int Messageidx = Int32.Parse(Messageid);
        //                            var Style = "Text";
        //                            color = Settings.UnseenMesageColor;

        //                            using (var Notifi = new NotifiFunctions())
        //                            {
        //                                var Exits = Notifi.GetNotifiDBCredentialsById(Messageidx);
        //                                if (Exits == null)
        //                                {
        //                                    Notifi.InsertNotifiDBCredentials(new NotifiDB
        //                                    {
        //                                        messageid = Messageidx,
        //                                        Seen = 0
        //                                    });
        //                                }

        //                                var Exits2 = Notifi.GetNotifiDBCredentialsById(Messageidx);
        //                                if (Exits2.Seen == 0 && NotifiStoper != ChatUser_User_ID)
        //                                {
        //                                    var Iconavatr =
        //                                        DependencyService.Get<IPicture>()
        //                                            .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID);
        //                                    DependencyService.Get<ILocalNotificationService>()
        //                                        .CreateLocalNotification(listuserLastMessage_Text, ChatUser_name,
        //                                            Iconavatr, ChatUser_User_ID, Style);
        //                                    Exits2.Seen = 1;
        //                                    Notifi.UpdateNotifiDBCredentials(Exits2);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (SeenCo != "0")
        //                        {
        //                            Chekicon = "true";
        //                        }
        //                    }

        //                    var Imagepath = DependencyService.Get<IPicture>()
        //                        .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID);
        //                    var ImageMediaFile = ImageSource.FromFile(Imagepath);

        //                    if (
        //                        DependencyService.Get<IPicture>().GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID) ==
        //                        "File Dont Exists")
        //                    {
        //                        ImageMediaFile = new UriImageSource {Uri = new Uri(ChatUser_avatar)};
        //                        DependencyService.Get<IPicture>().SavePictureToDisk(ChatUser_avatar, ChatUser_User_ID);
        //                    }

        //                    #region Show_Online_Oflline_Icon

        //                    ImageSource OnlineOfflineIcon = ImageSource.FromFile("");
        //                    var OnOficon = "";
        //                    if (Settings.Show_Online_Oflline_Icon)
        //                    {
        //                        if (ChatUser_lastseen == "on")
        //                        {
        //                            OnOficon =
        //                                DependencyService.Get<IPicture>()
        //                                    .GetPictureFromDisk(ThemeUrl + "/img/windows_app/online.png", "Icons");
        //                            OnlineOfflineIcon = ImageSource.FromFile(OnOficon);



        //                            if (
        //                                DependencyService.Get<IPicture>()
        //                                    .GetPictureFromDisk(ThemeUrl + "/img/windows_app/online.png", "Icons") ==
        //                                "File Dont Exists")
        //                            {
        //                                OnlineOfflineIcon = new UriImageSource
        //                                {
        //                                    Uri = new Uri(ThemeUrl + "/img/windows_app/online.png")
        //                                };
        //                                DependencyService.Get<IPicture>()
        //                                    .SavePictureToDisk(ThemeUrl + "/img/windows_app/online.png", "Icons");
        //                            }

        //                        }
        //                        else
        //                        {
        //                            OnOficon =
        //                                DependencyService.Get<IPicture>()
        //                                    .GetPictureFromDisk(ThemeUrl + "/img/windows_app/offline.png", "Icons");
        //                            OnlineOfflineIcon = ImageSource.FromFile(OnOficon);

        //                            if (
        //                                DependencyService.Get<IPicture>()
        //                                    .GetPictureFromDisk(ThemeUrl + "/img/windows_app/offline.png", "Icons") ==
        //                                "File Dont Exists")
        //                            {
        //                                OnlineOfflineIcon = new UriImageSource
        //                                    {Uri = new Uri(ThemeUrl + "/img/windows_app/offline.png")};
        //                                DependencyService.Get<IPicture>()
        //                                    .SavePictureToDisk(ThemeUrl + "/img/windows_app/offline.png", "Icons");
        //                            }
        //                        }

        //                    }

        //                    #endregion

        //                    var ChekeForNotAvailbleLabel =
        //                        ChatList.FirstOrDefault(
        //                            a => a.Username == "  " + "  " + "  " + Settings.Label_NoAnyChatActivityAvailable);
        //                    if (ChatList.Contains(ChekeForNotAvailbleLabel))
        //                    {
        //                        ChatList.Remove(ChekeForNotAvailbleLabel);
        //                    }

        //                    var Cheke = ChatList.FirstOrDefault(a => a.UserID == ChatUser_User_ID);
        //                    if (ChatList.Contains(Cheke))
        //                    {
        //                        //Cheke for online/offline Status
        //                        if (Cheke.OnOficon != OnOficon)
        //                        {
        //                            Cheke.OnOficon = OnOficon;
        //                            Cheke.lastseen = OnlineOfflineIcon;
        //                        }

        //                        if (Cheke.TextMessage != listuserLastMessage_Text)
        //                        {
        //                            var q =
        //                                ChatList.IndexOf(
        //                                    ChatList.Where(X => X.UserID == ChatUser_User_ID).FirstOrDefault());
        //                            if (q > -1)
        //                            {
        //                                Cheke.TextMessage = listuserLastMessage_Text;
        //                                Cheke.Username = ChatUser_name;
        //                                Cheke.TimeLong = Time;
        //                                ChatList.Move(q, 0);
        //                            }

        //                        }
        //                        //Change  User image if the user changed his avatar picture
        //                        if (Cheke.profile_picture_Url != Imagepath)
        //                        {
        //                            Cheke.profile_picture_Url = Imagepath;
        //                            Cheke.profile_picture = ImageMediaFile;
        //                        }
        //                        if (Cheke.SeenMessageOrNo != color ||
        //                            Cheke.LastMessageDateTime != listuserLastMessage_date_time)
        //                        {
        //                            Cheke.SeenMessageOrNo = color;
        //                            Cheke.LastMessageDateTime = listuserLastMessage_date_time;
        //                        }
        //                        if (Cheke.ChekeSeen != Chekicon)
        //                        {
        //                            Cheke.ChekeSeen = Chekicon;
        //                        }


        //                    }
        //                    else
        //                    {
        //                        ChatList.Insert(0, new ChatUsers()
        //                        {
        //                            Username = ChatUser_name,
        //                            lastseen = OnlineOfflineIcon,
        //                            profile_picture = ImageMediaFile,
        //                            TextMessage = listuserLastMessage_Text,
        //                            LastMessageDateTime = listuserLastMessage_date_time,
        //                            UserID = ChatUser_User_ID,
        //                            SeenMessageOrNo = color,
        //                            ChekeSeen = Chekicon,
        //                            OnOficon = OnOficon,
        //                            TimeLong = Time,
        //                            profile_picture_Url = Imagepath
        //                        });
        //                    }

        //                    using (var datas = new ChatActivityFunctions())
        //                    {
        //                        var contact = datas.GetChatUser(ChatUser_User_ID);
        //                        if (contact != null)
        //                        {
        //                            if (contact.UserID == ChatUser_User_ID &
        //                                ((contact.Name != ChatUser_name) || (contact.ProfilePicture != ChatUser_avatar) ||
        //                                 (contact.Username != ChatUser_name) || (contact.ChekeSeen != Chekicon)
        //                                 || (contact.SeenMessageOrNo != color) || (contact.TimeLong != Time)))
        //                            {

        //                                if ((contact.ProfilePicture != ChatUser_avatar))
        //                                {
        //                                    if (Settings.Delete_Old_Profile_Picture_From_Phone)
        //                                    {
        //                                        DependencyService.Get<IPicture>()
        //                                            .DeletePictureFromDisk(contact.ProfilePicture, ChatUser_User_ID);
        //                                    }

        //                                }

        //                                contact.UserID = ChatUser_User_ID;
        //                                contact.LastMessageDateTime = listuserLastMessage_date_time;
        //                                contact.Name = ChatUser_name;
        //                                contact.ProfilePicture = ChatUser_avatar;
        //                                contact.SeenMessageOrNo = color;
        //                                contact.TextMessage = listuserLastMessage_Text;
        //                                contact.Username = ChatUser_name;
        //                                contact.ChekeSeen = Chekicon;
        //                                contact.TimeLong = Time;
        //                                datas.UpdateChatUsers(contact);
        //                            }

        //                        }
        //                        else
        //                        {
        //                            datas.InsertChatUsers(new ChatActivityDB()
        //                            {
        //                                UserID = ChatUser_User_ID,
        //                                LastMessageDateTime = listuserLastMessage_date_time,
        //                                Name = ChatUser_name,
        //                                ProfilePicture = ChatUser_avatar,
        //                                SeenMessageOrNo = color,
        //                                TextMessage = listuserLastMessage_Text,
        //                                Username = ChatUser_name,
        //                                ChekeSeen = Chekicon,
        //                                TimeLong = Time,
        //                                lastseen = OnlineOfflineIcon.ToString()
        //                            });
        //                        }
        //                    }
        //                }

        //                #endregion

        //            }
        //            else if (apiStatus == "400")
        //            {
        //                json = Settings.Label_Error;
        //            }

        //            return json;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return Settings.Label_Error;
        //    }

        //}   

        public static async Task<string> GetChatContacts(string userid, string session)
        {
            #region Client Respone

            try
            {
                using (var client = new HttpClient())
                {

                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("after_user_id", Aftercontact),
                        new KeyValuePair<string, string>("list_type", "all")

                    });
                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_users_friends",formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    string ThemeUrl = data["theme_url"].ToString();

                    #endregion

                    if (apiStatus == "200")
                    {
                        var users = JObject.Parse(json).SelectToken("users").ToString();
                        var usersOnline = JObject.Parse(json).SelectToken("online").ToString();
                        JArray ChatusersOnlineGroup = JArray.Parse(usersOnline);
                        JArray Chatusers = JArray.Parse(users);

                        if (Chatusers.Count == 0)
                        {
                            Aftercontact = "0";
                            ChatContactsList.Add(new UserContacts()
                            {
                                Username = "  " + "  " + "  " + AppResources.FN_NoUsersAreAvailble,

                                Name = "  " + "  " + "  " + AppResources.FN_NoUsersAreAvailble

                            });
                            return null;
                        }
                        ChatContactsList.Clear();

                        foreach (var Onlines in ChatusersOnlineGroup)
                        {
                            JObject ChatlistUserdata = JObject.FromObject(Onlines);
                            var ChatUser_User_ID = ChatlistUserdata["user_id"].ToString();
                            var ChatUser_avatar = ChatlistUserdata["profile_picture"].ToString();
                            var ChatUser_name = ChatlistUserdata["name"].ToString();
                            var Username = ChatlistUserdata["username"].ToString();
                            var ChatUser_lastseen = ChatlistUserdata["lastseen"].ToString();
                            var ChatUser_lastseen_Time_Text = ChatlistUserdata["lastseen_time_text"].ToString();
                            var ChatUser_verified = ChatlistUserdata["verified"].ToString();
                            var UserPlatform = ChatlistUserdata["user_platform"].ToString();

                            Aftercontact = ChatUser_User_ID;

                            if (UserPlatform == "phone")
                            {
                                UserPlatform = AppResources.Label_Mobile;
                            }
                            if (UserPlatform == "web")
                            {
                                UserPlatform = AppResources.Label_Web;
                            }
                            if (UserPlatform == "windows")
                            {
                                UserPlatform = AppResources.Label_Desktop;
                            }

                            #region Saving image

                            var ImageMediaFile =
                                ImageSource.FromFile(DependencyService.Get<IPicture>()
                                    .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID));
                            if (DependencyService.Get<IPicture>()
                                    .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID) == "File Dont Exists")
                            {
                                ImageMediaFile = new UriImageSource {Uri = new Uri(ChatUser_avatar)};

                                ImageMediaFile = new UriImageSource
                                {
                                    Uri = new Uri(ChatUser_avatar),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 0, 0, 0)
                                };
                                DependencyService.Get<IPicture>().SavePictureToDisk(ChatUser_avatar, ChatUser_User_ID);
                            }


                            var OnlineOfflineIcon = ImageSource.FromFile("");
                            //if (Settings.Show_Online_Oflline_Icon)
                            //{
                            //    if (ChatUser_lastseen == "on")
                            //    {
                            //        OnlineOfflineIcon =
                            //            ImageSource.FromFile(
                            //                DependencyService.Get<IPicture>()
                            //                    .GetPictureFromDisk(ThemeUrl + "/img/windows_app/online.png", "Icons"));
                            //        if (
                            //            DependencyService.Get<IPicture>()
                            //                .GetPictureFromDisk(ThemeUrl + "/img/windows_app/online.png", "Icons") ==
                            //            "File Dont Exists")
                            //        {
                            //            OnlineOfflineIcon = new UriImageSource
                            //            {
                            //                Uri = new Uri(ThemeUrl + "/img/windows_app/online.png")
                            //            };
                            //            DependencyService.Get<IPicture>()
                            //                .SavePictureToDisk(ThemeUrl + "/img/windows_app/online.png", "Icons");
                            //        }

                            //    }
                            //    else
                            //    {
                            //        OnlineOfflineIcon =
                            //            ImageSource.FromFile(
                            //                DependencyService.Get<IPicture>()
                            //                    .GetPictureFromDisk(ThemeUrl + "/img/windows_app/offline.png", "Icons"));
                            //        if (
                            //            DependencyService.Get<IPicture>()
                            //                .GetPictureFromDisk(ThemeUrl + "/img/windows_app/offline.png", "Icons") ==
                            //            "File Dont Exists")
                            //        {
                            //            OnlineOfflineIcon = new UriImageSource
                            //            { Uri = new Uri(ThemeUrl + "/img/windows_app/offline.png") };
                            //            DependencyService.Get<IPicture>()
                            //                .SavePictureToDisk(ThemeUrl + "/img/windows_app/offline.png", "Icons");
                            //        }
                            //    }

                            //}

                            #endregion

                            if (ChatUser_lastseen == "on")
                            {
                                ChatContactsList.Add(new UserContacts()
                                {
                                    Username = ChatUser_name,
                                    lastseen = OnlineOfflineIcon,
                                    Name = ChatUser_name,
                                    SeenMessageOrNo = ChatUser_lastseen,
                                    profile_picture = ImageMediaFile,
                                    LastMessageDateTime = AppResources.Label_Online,
                                    //verified = ChatUser_verified_bitmap,
                                    UserID = ChatUser_User_ID,
                                    Platform = UserPlatform,
                                    Checkicon = "\uf1db",
                                    CheckiconColor = Settings.MainColor
                                });

                            }

                            #region adding to Sqlite table  

                            using (var datas = new ContactsFunctions())
                            {
                                var contact = datas.GetContactUser(ChatUser_User_ID);

                                #region Update contact information

                                if (contact != null)
                                {
                                    if (contact.UserID == ChatUser_User_ID &&
                                        ((contact.Name != ChatUser_name) ||
                                         (contact.ProfilePicture != ChatUser_avatar) ||
                                         (contact.Username != Username) ||
                                         (contact.LastMessageDateTime != ChatUser_lastseen_Time_Text) ||
                                         (contact.Platform != UserPlatform)))
                                    {
                                        if ((contact.ProfilePicture != ChatUser_avatar))
                                        {
                                            datas.DeleteContactRow(contact);
                                            DependencyService.Get<IPicture>()
                                                .DeletePictureFromDisk(contact.ProfilePicture, ChatUser_User_ID);
                                            datas.InsertContactUsers(new ContactsTableDB()
                                            {
                                                UserID = ChatUser_User_ID,
                                                Name = ChatUser_name,
                                                ProfilePicture = ChatUser_avatar,
                                                SeenMessageOrNo = ChatUser_lastseen,
                                                LastMessageDateTime = ChatUser_lastseen_Time_Text,
                                                Username = Username,
                                                Platform = UserPlatform
                                            });
                                        }

                                        contact.UserID = ChatUser_User_ID;
                                        contact.Name = ChatUser_name;
                                        contact.ProfilePicture = ChatUser_avatar;
                                        contact.SeenMessageOrNo = ChatUser_lastseen;
                                        contact.LastMessageDateTime = ChatUser_lastseen_Time_Text;
                                        contact.Username = Username;
                                        contact.Platform = UserPlatform;

                                        datas.UpdateContactUsers(contact);


                                    }
                                }

                                #endregion

                                #region Add contact if dont exits

                                else
                                {
                                    datas.InsertContactUsers(new ContactsTableDB()
                                    {
                                        UserID = ChatUser_User_ID,
                                        Name = ChatUser_name,
                                        ProfilePicture = ChatUser_avatar,
                                        SeenMessageOrNo = ChatUser_lastseen,
                                        LastMessageDateTime = ChatUser_lastseen_Time_Text,
                                        Username = Username,
                                        Platform = UserPlatform
                                    });
                                }

                                #endregion

                            }

                            #endregion
                        }

                        foreach (var Offline in Chatusers)
                        {
                            JObject ChatlistUserdata = JObject.FromObject(Offline);
                            var ChatUser_User_ID = ChatlistUserdata["user_id"].ToString();
                            var ChatUser_avatar = ChatlistUserdata["profile_picture"].ToString();
                            var ChatUser_name = ChatlistUserdata["name"].ToString();
                            var Username = ChatlistUserdata["username"].ToString();
                            var ChatUser_lastseen = ChatlistUserdata["lastseen"].ToString();
                            var ChatUser_lastseen_Time_Text = ChatlistUserdata["lastseen_time_text"].ToString();
                            var ChatUser_verified = ChatlistUserdata["verified"].ToString();
                            var UserPlatform = ChatlistUserdata["user_platform"].ToString();
                            Aftercontact = ChatUser_User_ID;


                            if (UserPlatform == "phone")
                            {
                                UserPlatform = AppResources.Label_Mobile;
                            }
                            if (UserPlatform == "web")
                            {
                                UserPlatform = AppResources.Label_Web;
                            }
                            if (UserPlatform == "windows")
                            {
                                UserPlatform = AppResources.Label_Desktop;
                            }


                            #region Saving image

                            var ImageMediaFile =
                                ImageSource.FromFile(DependencyService.Get<IPicture>()
                                    .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID));
                            if (DependencyService.Get<IPicture>()
                                    .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID) == "File Dont Exists")
                            {
                                ImageMediaFile = new UriImageSource {Uri = new Uri(ChatUser_avatar)};

                                ImageMediaFile = new UriImageSource
                                {
                                    Uri = new Uri(ChatUser_avatar),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 0, 0, 0)
                                };
                                DependencyService.Get<IPicture>().SavePictureToDisk(ChatUser_avatar, ChatUser_User_ID);
                            }

                            #endregion


                            #region Data Adding

                            ChatContactsList.Add(new UserContacts()
                            {
                                Username = Username,
                                Name = ChatUser_name,
                                SeenMessageOrNo = ChatUser_lastseen,
                                profile_picture = ImageMediaFile,
                                LastMessageDateTime = AppResources.Label_LastSeen + " " + ChatUser_lastseen_Time_Text,
                                UserID = ChatUser_User_ID,
                                Platform = UserPlatform,
                                Checkicon = "\uf1db",
                                CheckiconColor = Settings.MainColor
                            });

                            #region adding to Sqlite table  

                            using (var datas = new ContactsFunctions())
                            {
                                var contact = datas.GetContactUser(ChatUser_User_ID);

                                #region Update contact information

                                if (contact != null)
                                {
                                    if (contact.UserID == ChatUser_User_ID &&
                                        ((contact.Name != ChatUser_name) ||
                                         (contact.ProfilePicture != ChatUser_avatar) ||
                                         (contact.Username != Username) ||
                                         (contact.LastMessageDateTime != ChatUser_lastseen_Time_Text) ||
                                         (contact.Platform != UserPlatform)))
                                    {
                                        if ((contact.ProfilePicture != ChatUser_avatar))
                                        {
                                            datas.DeleteContactRow(contact);
                                            DependencyService.Get<IPicture>()
                                                .DeletePictureFromDisk(contact.ProfilePicture, ChatUser_User_ID);
                                            datas.InsertContactUsers(new ContactsTableDB()
                                            {
                                                UserID = ChatUser_User_ID,
                                                Name = ChatUser_name,
                                                ProfilePicture = ChatUser_avatar,
                                                SeenMessageOrNo = ChatUser_lastseen,
                                                LastMessageDateTime = ChatUser_lastseen_Time_Text,
                                                Username = Username,
                                                Platform = UserPlatform
                                            });
                                        }

                                        contact.UserID = ChatUser_User_ID;
                                        contact.Name = ChatUser_name;
                                        contact.ProfilePicture = ChatUser_avatar;
                                        contact.SeenMessageOrNo = ChatUser_lastseen;
                                        contact.LastMessageDateTime = ChatUser_lastseen_Time_Text;
                                        contact.Username = Username;
                                        contact.Platform = UserPlatform;

                                        datas.UpdateContactUsers(contact);
                                    }
                                }

                                #endregion

                                #region Add contact if dont exits

                                else
                                {
                                    datas.InsertContactUsers(new ContactsTableDB()
                                    {
                                        UserID = ChatUser_User_ID,
                                        Name = ChatUser_name,
                                        ProfilePicture = ChatUser_avatar,
                                        SeenMessageOrNo = ChatUser_lastseen,
                                        LastMessageDateTime = ChatUser_lastseen_Time_Text,
                                        Username = Username,
                                        Platform = UserPlatform
                                    });
                                }

                                #endregion
                            }

                            #endregion

                            #endregion

                        }
                    }
                    else if (apiStatus == "400")
                    {
                        ChatContactsList.Add(new UserContacts()
                        {
                            Username = "  " + "  " + "  " + AppResources.FN_NoUsersAreAvailble,

                            Name = "  " + "  " + "  " + AppResources.FN_NoUsersAreAvailble

                        });
                        json = AppResources.Label_Error;
                    }
                    UserDialogs.Instance.HideLoading();
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }

        public static async Task SyncContactsFromPhone()
        {
            try
            {
                var Dictionary = new Dictionary<string, string>();
                try
                {
                    if (await CrossContacts.Current.RequestPermission())
                    {
                        CrossContacts.Current.PreferContactAggregation = false;
                        await Task.Run(() =>
                        {
                            if (CrossContacts.Current.Contacts == null)
                                return;
                            var max = 0;
                            foreach (Contact contact in CrossContacts.Current.Contacts)
                            {
                                max += 1;
                                if (max == Settings.LimitContactGetFromPhone)
                                {
                                    continue;
                                }
                                foreach (var Number in contact.Phones)
                                {
                                    if (!Dictionary.ContainsKey(contact.FirstName + ' ' + contact.LastName))
                                    {
                                        Dictionary.Add(contact.FirstName + ' ' + contact.LastName, Number.Number);
                                    }
                                    break;
                                }
                            }
                        });
                    }
                }
                catch (Exception ex)
                {
                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                }

                string Data = JsonConvert.SerializeObject(Dictionary.ToArray());

                UserDialogs.Instance.Toast(AppResources.Label3_Loading_from_your_phone_contacts);

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("contacts", Data),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =
                        await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_suggestions",
                            formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    string ThemeUrl = data["theme_url"].ToString();

                    if (apiStatus == "200")
                    {
                        SearchFilterlist.Clear();
                        var users_sugParse = JObject.Parse(json).SelectToken("users_sug").ToString();
                        JArray Chatusers_sug = JArray.Parse(users_sugParse);

                        if (Chatusers_sug.Count > 0)
                        {
                            foreach (var userssug in Chatusers_sug)
                            {
                                JObject ChatlistUserdata = JObject.FromObject(userssug);
                                var ChatUser_User_ID = ChatlistUserdata["user_id"].ToString();
                                var ChatUser_avatar = ChatlistUserdata["profile_picture"].ToString();
                                var ChatUser_name = ChatlistUserdata["name"].ToString();
                                var ChatUser_lastseen = ChatlistUserdata["lastseen"].ToString();
                                var ChatUser_lastseen_Time_Text = ChatlistUserdata["lastseen_time_text"].ToString();
                                var ChatUser_verified = ChatlistUserdata["verified"].ToString();

                                #region Images

                                var ImageMediaFile =
                                    ImageSource.FromFile(DependencyService.Get<IPicture>()
                                        .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID));
                                if (
                                    DependencyService.Get<IPicture>()
                                        .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID) == "File Dont Exists")
                                {
                                    ImageMediaFile = new UriImageSource
                                    {
                                        Uri = new Uri(ChatUser_avatar)
                                    };
                                    //DependencyService.Get<IPicture>().SavePictureToDisk(ChatUser_avatar, ChatUser_User_ID);
                                }

                                var OnlineOfflineIcon = ImageSource.FromFile("");


                                #endregion

                                var status = AppResources.Label_AddFriend;
                                if (Settings.ConnectivitySystem == "1")
                                {
                                    status = AppResources.Label_Follow;
                                }

                                if (ChatUser_lastseen == "on")
                                {
                                    SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = ChatUser_name,
                                        lastseen = OnlineOfflineIcon,
                                        Name = ChatUser_name,
                                        SeenMessageOrNo = ChatUser_lastseen,
                                        profile_picture = ImageMediaFile,
                                        MiniLabel = AppResources.Label_Online,
                                        //verified = ChatUser_verified_bitmap,
                                        ResultID = ChatUser_User_ID,
                                        connectivitySystem = status,
                                        ResultType = "Users",
                                        ButtonColor = Settings.ButtonColorNormal,
                                        ButtonTextColor = Settings.ButtonTextColorNormal,
                                        ButtonImage = Settings.Add_Icon,
                                        ResultButtonAvailble = "true"
                                    });
                                }
                                else
                                {
                                    SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = ChatUser_name,
                                        lastseen = OnlineOfflineIcon,
                                        Name = ChatUser_name,
                                        SeenMessageOrNo = ChatUser_lastseen,
                                        profile_picture = ImageMediaFile,
                                        MiniLabel = AppResources.Label_LastSeen + " " + ChatUser_lastseen_Time_Text,
                                        //verified = ChatUser_verified_bitmap,
                                        ResultID = ChatUser_User_ID,
                                        connectivitySystem = status,
                                        ResultType = "Users",
                                        ButtonColor = Settings.ButtonColorNormal,
                                        ButtonTextColor = Settings.ButtonTextColorNormal,
                                        ButtonImage = Settings.Add_Icon,
                                        ResultButtonAvailble = "true"
                                    });

                                }
                            }
                        }
                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }

                    if (SearchFilterlist.Count == 0)
                    {
                        SearchFilterlist.Add(new SearchResult()
                        {
                            BigLabel = "  " + AppResources.FN_NoContactPhoneAreAvailble,
                            Name = "  " + AppResources.FN_NoContactPhoneAreAvailble,
                            ButtonColor = Settings.ButtonColorNormal,
                            ButtonTextColor = Settings.ButtonTextColorNormal,
                            ButtonImage = Settings.Add_Icon,
                            ResultButtonAvailble = "false"
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public static async Task<string> GetRandomUsers()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });
                    UserDialogs.Instance.Toast(AppResources.Label3_Loading_Random_Users);
                    var response =
                        await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_suggestions",
                            formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    string ThemeUrl = data["theme_url"].ToString();

                    if (apiStatus == "200")
                    {
                        SearchFilterlist.Clear();
                        var users = JObject.Parse(json).SelectToken("users_random").ToString();
                        JArray Chatusers = JArray.Parse(users);

                        foreach (var ChatUser in Chatusers)
                        {
                            JObject ChatlistUserdata = JObject.FromObject(ChatUser);
                            var ChatUser_User_ID = ChatlistUserdata["user_id"].ToString();
                            var ChatUser_avatar = ChatlistUserdata["profile_picture"].ToString();
                            var ChatUser_name = ChatlistUserdata["name"].ToString();
                            var ChatUser_lastseen = ChatlistUserdata["lastseen"].ToString();
                            var ChatUser_lastseen_Time_Text = ChatlistUserdata["lastseen_time_text"].ToString();
                            var ChatUser_verified = ChatlistUserdata["verified"].ToString();

                            var ImageMediaFile =
                                ImageSource.FromFile(
                                    DependencyService.Get<IPicture>()
                                        .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID));
                            if (
                                DependencyService.Get<IPicture>()
                                    .GetPictureFromDisk(ChatUser_avatar, ChatUser_User_ID) ==
                                "File Dont Exists")
                            {
                                ImageMediaFile = new UriImageSource
                                {
                                    Uri = new Uri(ChatUser_avatar)
                                };
                                //DependencyService.Get<IPicture>().SavePictureToDisk(ChatUser_avatar, ChatUser_User_ID);
                            }

                            var OnlineOfflineIcon = ImageSource.FromFile("");

                            #region Data Adding

                            var status = AppResources.Label_AddFriend;
                            if (Settings.ConnectivitySystem == "1")
                            {
                                status = AppResources.Label_Follow;
                            }

                            if (ChatUser_lastseen == "on")
                            {
                                SearchFilterlist.Add(new SearchResult()
                                {
                                    BigLabel = ChatUser_name,
                                    ResultType = "Users",
                                    lastseen = OnlineOfflineIcon,
                                    Name = ChatUser_name,
                                    SeenMessageOrNo = ChatUser_lastseen,
                                    profile_picture = ImageMediaFile,
                                    MiniLabel = AppResources.Label_Online,
                                    //verified = ChatUser_verified_bitmap,
                                    ResultID = ChatUser_User_ID,
                                    connectivitySystem = status,
                                    ButtonColor = Settings.ButtonColorNormal,
                                    ButtonTextColor = Settings.ButtonTextColorNormal,
                                    ButtonImage = Settings.Add_Icon,
                                    ResultButtonAvailble = "true"
                                });
                            }
                            else
                            {
                                SearchFilterlist.Add(new SearchResult()
                                {
                                    BigLabel = ChatUser_name,
                                    lastseen = OnlineOfflineIcon,
                                    Name = ChatUser_name,
                                    ResultType = "Users",
                                    SeenMessageOrNo = ChatUser_lastseen,
                                    profile_picture = ImageMediaFile,
                                    MiniLabel = AppResources.Label_LastSeen + " " + ChatUser_lastseen_Time_Text,
                                    //verified = ChatUser_verified_bitmap,
                                    ResultID = ChatUser_User_ID,
                                    connectivitySystem = status,
                                    ButtonColor = Settings.ButtonColorNormal,
                                    ButtonTextColor = Settings.ButtonTextColorNormal,
                                    ButtonImage = Settings.Add_Icon,
                                    ResultButtonAvailble = "true"
                                });
                            }

                            #endregion
                        }
                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }

                    if (SearchFilterlist.Count == 0)
                    {
                        SearchFilterlist.Add(new SearchResult()
                        {
                            BigLabel = "  " + "  " + "  " + AppResources.FN_NoUsersAreAvailble,
                            Name = "  " + "  " + "  " + AppResources.FN_NoUsersAreAvailble,
                            ButtonColor = Settings.ButtonColorNormal,
                            ButtonTextColor = Settings.ButtonTextColorNormal,
                            ButtonImage = Settings.Add_Icon,
                            ResultType = "Users",
                            ResultButtonAvailble = "false"
                        });
                    }
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }

        public static string IsUrlValid(string url)
        {
            try
            {
                string pattern =@"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
                Regex reg = new Regex(pattern, RegexOptions.ExplicitCapture | RegexOptions.IgnoreCase);
                Match m = reg.Match(url);
                while (m.Success)
                {
                    //do things with your matching text 
                    m = m.NextMatch();
                    break;
                }
                if (reg.IsMatch(url))
                {
                    return "http://" + m.Value;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "";
            }

        }

        public static int ListInfoResizer(string About)
        {
            try
            {
                if (About.Length <= 40)
                {
                    return 150;
                }
                else if (About.Length <= 90)
                {
                    return 165;
                }
                else if (About.Length <= 130)
                {
                    return 175;
                }
                else if (About.Length <= 175)
                {
                    return 185;
                }
                else if (About.Length <= 205)
                {
                    return 195;
                }
                else if (About.Length <= 250)
                {
                    return 205;
                }
                else if (About.Length <= 290)
                {
                    return 215;
                }
                else if (About.Length <= 335)
                {
                    return 225;
                }
                else if (About.Length <= 350)
                {
                    return 235;
                }
                else if (About.Length <= 375)
                {
                    return 243;
                }
                else if (About.Length <= 390)
                {
                    return 247;
                }
                else if (About.Length <= 405)
                {
                    return 259;
                }
                else if (About.Length <= 445)
                {
                    return 275;
                }
                else if (About.Length <= 485)
                {
                    return 292;
                }
                else if (About.Length <= 525)
                {
                    return 308;
                }
                else if (About.Length <= 555)
                {
                    return 315;
                }
                else if (About.Length <= 570)
                {
                    return 320;
                }
                else if (About.Length <= 590)
                {
                    return 330;
                }
                else if (About.Length <= 610)
                {
                    return 345;
                }
                else if (About.Length <= 650)
                {
                    return 360;
                }
                else if (About.Length <= 685)
                {
                    return 380;
                }
                else if (About.Length <= 695)
                {
                    return 395;
                }
                else if (About.Length <= 711)
                {
                    return 410;
                }
                else if (About.Length <= 735)
                {
                    return 420;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return 0;
            }
        }

        public static string DecodeString(string Content)
        {
            var Decoded = System.Net.WebUtility.HtmlDecode(Content);
            var Stringformater = Decoded.Replace(":)", "\ud83d\ude0a")
                .Replace(";)", "\ud83d\ude09")
                .Replace("0)", "\ud83d\ude07")
                .Replace("(<", "\ud83d\ude02")
                .Replace(":D", "\ud83d\ude01")
                .Replace("*_*", "\ud83d\ude0d")
                .Replace("(<", "\ud83d\ude02")
                .Replace("<3", "\ud83d\u2764")
                .Replace("/_)", "\ud83d\ude0f")
                .Replace("-_-", "\ud83d\ude11")
                .Replace(":-/", "\ud83d\ude15")
                .Replace(":*", "\ud83d\ude18")
                .Replace(":_p", "\ud83d\ude1b")
                .Replace(":p", "\ud83d\ude1c")
                .Replace("x(", "\ud83d\ude20")
                .Replace("X(", "\ud83d\ude21")
                .Replace(":_(", "\ud83d\ude22")
                .Replace("<5", "\ud83d\u2B50")
                .Replace(":0", "\ud83d\ude31")
                .Replace("B)", "\ud83d\ude0e")
                .Replace("o(", "\ud83d\ude27")
                .Replace("</3", "\uD83D\uDC94")
                .Replace(":o", "\ud83d\ude26")
                .Replace("o(", "\ud83d\ude27")
                .Replace(":__(", "\ud83d\ude2d")
                .Replace("!_", "\uD83D\u2757")

                .Replace("<br> <br>", " ")
                .Replace("<br>", " ")
                .Replace("<a href=", "")
                .Replace("target=", "")
                .Replace("_blank", "")
                .Replace(@"""", "")
                .Replace("</a>", "")
                .Replace("class=hash", "")
                .Replace("rel=nofollow>", "")
                .Replace("<p>", "")
                .Replace("</p>", "")
                .Replace("</body>", "")
                .Replace("<body>", "")
                .Replace("<div>", "")
                .Replace("<div ", "")
                .Replace("</div>", "")
                .Replace("<iframe", "")
                .Replace("</iframe>", "")
                .Replace("<table", "")
                .Replace("</table>", " ");

            return Stringformater;
        }


        //Functions SubString Cut Of
        public static string SubStringCutOf(string s, int x)
        {
            try
            {
                String substring = s.Substring(0, x);
                return substring + "...";
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return s;
            }
        }

        //Null Remover >> return Empty
        public static string StringNullRemover(string s)
        {
            try
            {
                if (String.IsNullOrEmpty(s))
                {
                    s = AppResources.Label_Empty;
                }
                return s;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return s;
            }
        }
        
        //creat new Random String 
        public static string RandomString(int length)
        {
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}