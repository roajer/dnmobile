﻿
//Copyright: DoughouzLight
//Email: alidoughous1993@gmail.com
//Full Documentation https://paper.dropbox.com/doc/WoWonder-Timeline-v1.5-J2AnlcVy05dpGilAJCNzy?_tk
//==============================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Com.OneSignal;
using Com.OneSignal.Abstractions;

namespace WowonderPhone.Controls
{
    public class OneSignalNotificationController
    {
        public static void RegisterNotificationDevice()
        {
            try
            {
                if (Settings.NotificationPopup)
                {
                    OneSignal.Current.StartInit(Settings.Onesignal_APP_ID)
                        .InFocusDisplaying(OSInFocusDisplayOption.Notification)
                        .HandleNotificationReceived(HandleNotificationReceived)
                        .HandleNotificationOpened(HandleNotificationOpened)
                        .EndInit();
                    OneSignal.Current.IdsAvailable(IdsAvailable);
                    OneSignal.Current.RegisterForPushNotifications();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public static void Un_RegisterNotificationDevice()
        {
            try
            {
                OneSignal.Current.SetSubscription(false);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        private static void IdsAvailable(string userID, string pushToken)
        {
            try
            {
                Settings.Device_ID = userID;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private static void HandleNotificationReceived(OSNotification notification)
        {
            try
            {
                OSNotificationPayload payload = notification.payload;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private static void HandleNotificationOpened(OSNotificationOpenedResult result)
        {
            try
            {
                OSNotificationPayload payload = result.notification.payload;
                Dictionary<string, object> additionalData = payload.additionalData;
                string message = payload.body;
                string actionID = result.action.actionID;

                if (additionalData != null)
                {
                    if (additionalData.ContainsKey("discount"))
                    {
                        // Take user to your store.
                    }
                }
                if (actionID != null)
                {
                    // actionSelected equals the id on the button the user pressed.
                    // actionSelected will equal "__DEFAULT__" when the notification itself was tapped when buttons were present. 
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
