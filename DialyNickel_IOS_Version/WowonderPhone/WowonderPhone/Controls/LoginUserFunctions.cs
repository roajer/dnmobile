﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.Controls
{
    public class LoginUserFunctions
    {
        public static int itemGroup_Count = 0;
        public static int itemPages_Count = 0;
        public static int itemFollower_Count = 0;

        public static async Task<string> GetSessionProfileData(string userid, string session)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", userid),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =
                        await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_user_data",
                            formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        JObject userdata = JObject.FromObject(data["user_data"]);
                        Settings.UserFullName = userdata["name"].ToString();

                        var user_id = userdata["user_id"].ToString();
                        var username = userdata["username"].ToString();
                        var email = userdata["email"].ToString();
                        var First_name = userdata["first_name"].ToString();
                        var Last_name = userdata["last_name"].ToString();
                        var avatar = userdata["avatar"].ToString();
                        var cover = userdata["cover"].ToString();
                        //var relationship_id = userdata["relationship_id"].ToString();
                        var address = userdata["address"].ToString();
                        var Working = userdata["working"].ToString();
                        //var working_link = userdata["working_link"].ToString();
                        var About = userdata["about"].ToString();
                        var School = userdata["school"].ToString();
                        var gender = userdata["gender"].ToString();
                        var birthday = userdata["birthday"].ToString();
                        var Website = userdata["website"].ToString();
                        var facebook = userdata["facebook"].ToString();
                        var google = userdata["google"].ToString();
                        var twitter = userdata["twitter"].ToString();
                        var linkedin = userdata["linkedin"].ToString();
                        var youtube = userdata["youtube"].ToString();
                        var vk = userdata["vk"].ToString();
                        var instagram = userdata["instagram"].ToString();
                        //var language = userdata["language"].ToString();
                        //var ip_address = userdata["ip_address"].ToString();
                        //var follow_privacy = userdata["follow_privacy"].ToString();
                        //var friend_privacy = userdata["friend_privacy"].ToString();
                        //var post_privacy = userdata["post_privacy"].ToString();
                        //var message_privacy = userdata["message_privacy"].ToString();
                        //var confirm_followers = userdata["confirm_followers"].ToString();
                        //var show_activities_privacy = userdata["show_activities_privacy"].ToString();
                        //var birth_privacy = userdata["birth_privacy"].ToString();
                        //var visit_privacy = userdata["visit_privacy"].ToString();
                        //var verified = userdata["verified"].ToString();
                        //var lastseen = userdata["lastseen"].ToString();
                        //var showlastseen = userdata["showlastseen"].ToString();
                        //var e_sentme_msg = userdata["e_sentme_msg"].ToString();
                        //var e_last_notif = userdata["e_last_notif"].ToString();
                        //var status = userdata["status"].ToString();
                        //var active = userdata["active"].ToString();
                        //var admin = userdata["admin"].ToString();
                        //var registered = userdata["registered"].ToString();
                        var phone_number = userdata["phone_number"].ToString();
                        //var is_pro = userdata["is_pro"].ToString();
                        //var pro_type = userdata["pro_type"].ToString();
                        //var joined = userdata["joined"].ToString();
                        //var timezone = userdata["timezone"].ToString();
                        //var referrer = userdata["referrer"].ToString();
                        //var balance = userdata["balance"].ToString();
                        //var paypal_email = userdata["paypal_email"].ToString();
                        //var notifications_sound = userdata["notifications_sound"].ToString();
                        //var order_posts_by = userdata["order_posts_by"].ToString();
                        //var social_login = userdata["social_login"].ToString();
                        //var device_id = userdata["device_id"].ToString();
                        //var web_device_id = userdata["web_device_id"].ToString();
                        //var wallet = userdata["wallet"].ToString();
                        //var lat = userdata["lat"].ToString();
                        //var lng = userdata["lng"].ToString();
                        //var last_location_update = userdata["last_location_update"].ToString();
                        //var share_my_location = userdata["share_my_location"].ToString();
                        var url = userdata["url"].ToString();
                        var name = userdata["name"].ToString();
                        //var can_follow = userdata["can_follow"].ToString();
                        //var post_count = userdata["post_count"].ToString();
                        //var gender_text = userdata["gender_text"].ToString();
                        //var lastseen_time_text = userdata["lastseen_time_text"].ToString();
                        //var is_blocked = userdata["is_blocked"].ToString();
                        var following_number = userdata["following_number"].ToString();
                        var followers_number = userdata["followers_number"].ToString();

                        var followers = userdata["followers"].ToString();
                        JArray datafollowers = JArray.Parse(followers);
                        if (datafollowers != null)
                        {
                            try
                            {
                                Profile.FollowerList.Clear();
                                MyProfilePage.Follower_ListItems.Clear();

                                foreach (var All in datafollowers)
                                {
                                    Profile.Follower f = new Profile.Follower();

                                    var f_user_id = All["user_id"].ToString();
                                    var f_username = All["username"].ToString();
                                    var f_email = All["email"].ToString();
                                    var f_first_name = All["first_name"].ToString();
                                    var f_last_name = All["last_name"].ToString();
                                    var f_avatar = All["avatar"].ToString();
                                    var f_cover = All["cover"].ToString();
                                    var f_background_image = All["background_image"].ToString();
                                    var f_background_image_status = All["background_image_status"].ToString();
                                    var f_relationship_id = All["relationship_id"].ToString();
                                    var f_address = All["address"].ToString();
                                    var f_working = All["working"].ToString();
                                    var f_working_link = All["working_link"].ToString();
                                    var f_about = All["about"].ToString();
                                    var f_school = All["school"].ToString();
                                    var f_gender = All["gender"].ToString();
                                    var f_birthday = All["birthday"].ToString();
                                    var f_country_id = All["country_id"].ToString();
                                    var f_website = All["website"].ToString();
                                    var f_facebook = All["facebook"].ToString();
                                    var f_google = All["google"].ToString();
                                    var f_twitter = All["twitter"].ToString();
                                    var f_linkedin = All["linkedin"].ToString();
                                    var f_youtube = All["youtube"].ToString();
                                    var f_vk = All["vk"].ToString();
                                    var f_instagram = All["instagram"].ToString();
                                    var f_language = All["language"].ToString();
                                    var f_email_code = All["email_code"].ToString();
                                    var f_src = All["src"].ToString();
                                    var f_ip_address = All["ip_address"].ToString();
                                    var f_follow_privacy = All["follow_privacy"].ToString();
                                    var f_friend_privacy = All["friend_privacy"].ToString();
                                    var f_post_privacy = All["post_privacy"].ToString();
                                    var f_message_privacy = All["message_privacy"].ToString();
                                    var f_confirm_followers = All["confirm_followers"].ToString();
                                    var f_show_activities_privacy = All["show_activities_privacy"].ToString();
                                    var f_birth_privacy = All["birth_privacy"].ToString();
                                    var f_visit_privacy = All["visit_privacy"].ToString();
                                    var f_verified = All["verified"].ToString();
                                    var f_lastseen = All["lastseen"].ToString();
                                    var f_showlastseen = All["showlastseen"].ToString();
                                    var f_emailNotification = All["emailNotification"].ToString();
                                    var f_e_liked = All["e_liked"].ToString();
                                    var f_e_wondered = All["e_wondered"].ToString();
                                    var f_e_shared = All["e_shared"].ToString();
                                    var f_e_followed = All["e_followed"].ToString();
                                    var f_e_commented = All["e_commented"].ToString();
                                    var f_e_visited = All["e_visited"].ToString();
                                    var f_e_liked_page = All["e_liked_page"].ToString();
                                    var f_e_mentioned = All["e_mentioned"].ToString();
                                    var f_e_joined_group = All["e_joined_group"].ToString();
                                    var f_e_accepted = All["e_accepted"].ToString();
                                    var f_e_profile_wall_post = All["e_profile_wall_post"].ToString();
                                    var f_e_sentme_msg = All["e_sentme_msg"].ToString();
                                    var f_e_last_notif = All["e_last_notif"].ToString();
                                    var f_status = All["status"].ToString();
                                    var f_active = All["active"].ToString();
                                    var f_admin = All["admin"].ToString();
                                    var f_type = All["type"].ToString();
                                    var f_registered = All["registered"].ToString();
                                    var f_start_up = All["start_up"].ToString();
                                    var f_start_up_info = All["start_up_info"].ToString();
                                    var f_startup_follow = All["startup_follow"].ToString();
                                    var f_startup_image = All["startup_image"].ToString();
                                    var f_last_email_sent = All["last_email_sent"].ToString();
                                    var f_phone_number = All["phone_number"].ToString();
                                    var f_sms_code = All["sms_code"].ToString();
                                    var f_is_pro = All["is_pro"].ToString();
                                    var f_pro_time = All["pro_time"].ToString();
                                    var f_pro_type = All["pro_type"].ToString();
                                    var f_joined = All["joined"].ToString();
                                    var f_css_file = All["css_file"].ToString();
                                    var f_timezone = All["timezone"].ToString();
                                    var f_referrer = All["referrer"].ToString();
                                    var f_balance = All["balance"].ToString();
                                    var f_paypal_email = All["paypal_email"].ToString();
                                    var f_notifications_sound = All["notifications_sound"].ToString();
                                    var f_order_posts_by = All["order_posts_by"].ToString();
                                    var f_social_login = All["social_login"].ToString();
                                    var f_device_id = All["device_id"].ToString();
                                    var f_web_device_id = All["web_device_id"].ToString();
                                    var f_wallet = All["wallet"].ToString();
                                    var f_lat = All["lat"].ToString();
                                    var f_lng = All["lng"].ToString();
                                    var f_last_location_update = All["last_location_update"].ToString();
                                    var f_share_my_location = All["share_my_location"].ToString();
                                    var f_avatar_org = All["avatar_org"].ToString();
                                    var f_cover_org = All["cover_org"].ToString();
                                    var f_cover_full = All["cover_full"].ToString();
                                    var f_id = All["id"].ToString();
                                    var f_url = All["url"].ToString();
                                    var f_name = All["name"].ToString();
                                    var f_family_member = All["family_member"].ToString();

                                    //style
                                    if (String.IsNullOrEmpty(f_first_name) && String.IsNullOrEmpty(f_first_name))
                                        f.F_full_name = f_username;
                                    else
                                        f.F_full_name = f_first_name + " " + f_last_name;

                                    f.F_user_id = f_user_id;
                                    f.F_username = f_username;
                                    f.F_email = f_email;
                                    f.F_first_name = Functions.StringNullRemover(f_first_name);
                                    f.F_last_name = Functions.StringNullRemover(f_last_name);
                                    f.F_avatar = f_avatar;
                                    f.F_cover = f_cover;
                                    f.F_background_image = f_background_image;
                                    f.F_background_image_status = f_background_image_status;
                                    f.F_relationship_id = f_relationship_id;
                                    f.F_address = f_address;
                                    f.F_working = f_working;
                                    f.F_working_link = f_working_link;
                                    f.F_about = Functions.StringNullRemover(Functions.DecodeString(f_about));
                                    f.F_school = Functions.StringNullRemover(f_school);
                                    f.F_gender = f_gender;
                                    f.F_birthday = f_birthday;
                                    f.F_country_id = f_country_id;
                                    f.F_website = Functions.StringNullRemover(f_website);
                                    f.F_facebook = Functions.StringNullRemover(f_facebook);
                                    f.F_google = Functions.StringNullRemover(f_google);
                                    f.F_twitter = Functions.StringNullRemover(f_twitter);
                                    f.F_linkedin = Functions.StringNullRemover(f_linkedin);
                                    f.F_youtube = Functions.StringNullRemover(f_youtube);
                                    f.F_vk = Functions.StringNullRemover(f_vk);
                                    f.F_instagram = Functions.StringNullRemover(f_instagram);
                                    f.F_language = f_language;
                                    f.F_email_code = f_email_code;
                                    f.F_src = f_src;
                                    f.F_ip_address = f_ip_address;
                                    f.F_follow_privacy = f_follow_privacy;
                                    f.F_friend_privacy = f_friend_privacy;
                                    f.F_post_privacy = f_post_privacy;
                                    f.F_message_privacy = f_message_privacy;
                                    f.F_confirm_followers = f_confirm_followers;
                                    f.F_show_activities_privacy = f_show_activities_privacy;
                                    f.F_birth_privacy = f_birth_privacy;
                                    f.F_visit_privacy = f_visit_privacy;
                                    f.F_verified = f_verified;
                                    f.F_lastseen = f_lastseen;
                                    f.F_showlastseen = f_showlastseen;
                                    f.F_emailNotification = f_emailNotification;
                                    f.F_e_liked = f_e_liked;
                                    f.F_e_wondered = f_e_wondered;
                                    f.F_e_shared = f_e_shared;
                                    f.F_e_followed = f_e_followed;
                                    f.F_e_commented = f_e_commented;
                                    f.F_e_visited = f_e_visited;
                                    f.F_e_liked_page = f_e_liked_page;
                                    f.F_e_mentioned = f_e_mentioned;
                                    f.F_e_joined_group = f_e_joined_group;
                                    f.F_e_accepted = f_e_accepted;
                                    f.F_e_profile_wall_post = f_e_profile_wall_post;
                                    f.F_e_sentme_msg = f_e_sentme_msg;
                                    f.F_e_last_notif = f_e_last_notif;
                                    f.F_status = f_status;
                                    f.F_active = f_active;
                                    f.F_admin = f_admin;
                                    f.F_type = f_type;
                                    f.F_registered = f_registered;
                                    f.F_start_up = f_start_up;
                                    f.F_start_up_info = f_start_up_info;
                                    f.F_startup_follow = f_startup_follow;
                                    f.F_startup_image = f_startup_image;
                                    f.F_last_email_sent = f_last_email_sent;
                                    f.F_phone_number = Functions.StringNullRemover(f_phone_number);
                                    f.F_sms_code = f_sms_code;
                                    f.F_is_pro = f_is_pro;
                                    f.F_pro_time = f_pro_time;
                                    f.F_pro_type = f_pro_type;
                                    f.F_joined = f_joined;
                                    f.F_css_file = f_css_file;
                                    f.F_timezone = f_timezone;
                                    f.F_referrer = f_referrer;
                                    f.F_balance = f_balance;
                                    f.F_paypal_email = f_paypal_email;
                                    f.F_notifications_sound = f_notifications_sound;
                                    f.F_order_posts_by = f_order_posts_by;
                                    f.F_social_login = f_social_login;
                                    f.F_device_id = f_device_id;
                                    f.F_web_device_id = f_web_device_id;
                                    f.F_wallet = f_wallet;
                                    f.F_lat = f_lat;
                                    f.F_lng = f_lng;
                                    f.F_last_location_update = f_last_location_update;
                                    f.F_share_my_location = f_share_my_location;
                                    f.F_avatar_org = f_avatar_org;
                                    f.F_cover_org = f_cover_org;
                                    f.F_cover_full = f_cover_full;
                                    f.F_id = f_id;
                                    f.F_url = f_url;
                                    f.F_name = f_name;
                                    f.F_family_member = f_family_member;

                                    Profile.FollowerList.Add(f);

                                    if (itemFollower_Count < 11)
                                    {
                                        MyProfilePage.Follower_ListItems.Add(f);
                                        itemFollower_Count++;
                                    }
                                }
                                itemFollower_Count = 0;
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        var groups = userdata["groups"].ToString();
                        JArray datagroups = JArray.Parse(groups);
                        if (datagroups != null)
                        {
                            try
                            {
                                Profile.GroupList.Clear();
                                MyProfilePage.Group_ListItems.Clear();

                                foreach (var All in datagroups)
                                {

                                    Profile.Group g = new Profile.Group();

                                    var g_id = All["id"].ToString();
                                    var g_user_id = All["user_id"].ToString();
                                    var g_group_name = All["group_name"].ToString();
                                    var g_group_title = All["group_title"].ToString();
                                    var g_avatar = All["avatar"].ToString();
                                    var g_cover = All["cover"].ToString();
                                    var g_about = All["about"].ToString();
                                    var g_category = All["category"].ToString();
                                    var g_privacy = All["privacy"].ToString();
                                    var g_join_privacy = All["join_privacy"].ToString();
                                    var g_active = All["active"].ToString();
                                    var g_registered = All["registered"].ToString();
                                    var g_group_id = All["group_id"].ToString();
                                    var g_url = All["url"].ToString();
                                    var g_name = All["name"].ToString();
                                    var g_category_id = All["category_id"].ToString();
                                    var g_type = All["type"].ToString();
                                    var g_username = All["username"].ToString();

                                    g.G_id = g_id;
                                    g.G_user_id = g_user_id;
                                    g.G_group_name = g_group_name;
                                    g.G_group_title = g_group_title;
                                    g.G_avatar = g_avatar;
                                    g.G_cover = g_cover;
                                    g.G_about = Functions.StringNullRemover(Functions.DecodeString(g_about));
                                    g.G_category = g_category;
                                    g.G_privacy = g_privacy;
                                    g.G_join_privacy = g_join_privacy;
                                    g.G_active = g_active;
                                    g.G_registered = g_registered;
                                    g.G_group_id = g_group_id;
                                    g.G_url = g_url;
                                    g.G_name = g_name;
                                    g.G_category_id = g_category_id;
                                    g.G_type = g_type;
                                    g.G_username = g_username;

                                    Profile.GroupList.Add(g);
                                    if (itemGroup_Count < 6)
                                    {
                                        MyProfilePage.Group_ListItems.Add(g);
                                        itemGroup_Count++;
                                    }
                                }
                                itemGroup_Count = 0;
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        var Pages = userdata["liked_pages"].ToString();
                        JArray dataPages = JArray.Parse(Pages);
                        if (dataPages != null)
                        {
                            try
                            {
                                Profile.Liked_PagesList.Clear();
                                MyProfilePage.Liked_Pages_ListItems.Clear();

                                foreach (var All in dataPages)
                                {

                                    Profile.Liked_Pages p = new Profile.Liked_Pages();

                                    var P_page_id = All["page_id"].ToString();
                                    var P_user_id = All["user_id"].ToString();
                                    var P_page_name = All["page_name"].ToString();
                                    var P_page_title = All["page_title"].ToString();
                                    var P_page_description = All["page_description"].ToString();
                                    var P_avatar = All["avatar"].ToString();
                                    var P_cover = All["cover"].ToString();
                                    var P_page_category = All["page_category"].ToString();
                                    var P_website = All["website"].ToString();
                                    var P_facebook = All["facebook"].ToString();
                                    var P_google = All["google"].ToString();
                                    var P_vk = All["vk"].ToString();
                                    var P_twitter = All["twitter"].ToString();
                                    var P_linkedin = All["linkedin"].ToString();
                                    var P_company = All["company"].ToString();
                                    var P_phone = All["phone"].ToString();
                                    var P_address = All["address"].ToString();
                                    var P_call_action_type = All["call_action_type"].ToString();
                                    var P_call_action_type_url = All["call_action_type_url"].ToString();
                                    var P_background_image = All["background_image"].ToString();
                                    var P_background_image_status = All["background_image_status"].ToString();
                                    var P_instgram = All["instgram"].ToString();
                                    var P_youtube = All["youtube"].ToString();
                                    var P_verified = All["verified"].ToString();
                                    var P_active = All["active"].ToString();
                                    var P_registered = All["registered"].ToString();
                                    var P_boosted = All["boosted"].ToString();
                                    var P_about = All["about"].ToString();
                                    var P_id = All["id"].ToString();
                                    var P_type = All["type"].ToString();
                                    var P_url = All["url"].ToString();
                                    var P_name = All["name"].ToString();
                                    var P_rating = All["rating"].ToString();
                                    var P_category = All["category"].ToString();
                                    var P_is_page_onwer = All["is_page_onwer"].ToString();
                                    var P_username = All["username"].ToString();

                                    p.P_page_id = P_page_id;
                                    p.P_user_id = P_user_id;
                                    p.P_page_name = P_page_name;
                                    p.P_page_title = P_page_title;
                                    p.P_page_description =
                                        Functions.StringNullRemover(Functions.DecodeString(P_page_description));
                                    p.P_avatar = P_avatar;
                                    p.P_cover = P_cover;
                                    p.P_page_category = P_page_category;
                                    p.P_website = P_website;
                                    p.P_facebook = P_facebook;
                                    p.P_google = P_google;
                                    p.P_vk = P_vk;
                                    p.P_twitter = P_twitter;
                                    p.P_linkedin = P_linkedin;
                                    p.P_company = P_company;
                                    p.P_phone = P_phone;
                                    p.P_address = P_address;
                                    p.P_call_action_type = P_call_action_type;
                                    p.P_call_action_type_url = P_call_action_type_url;
                                    p.P_background_image = P_background_image;
                                    p.P_background_image_status = P_background_image_status;
                                    p.P_instgram = P_instgram;
                                    p.P_youtube = P_youtube;
                                    p.P_verified = P_verified;
                                    p.P_active = P_active;
                                    p.P_registered = P_registered;
                                    p.P_boosted = P_boosted;
                                    p.P_about = Functions.StringNullRemover(Functions.DecodeString(P_about));
                                    p.P_id = P_id;
                                    p.P_type = P_type;
                                    p.P_url = P_url;
                                    p.P_name = P_name;
                                    p.P_rating = P_rating;
                                    p.P_category = P_category;
                                    p.P_is_page_onwer = P_is_page_onwer;
                                    p.P_username = P_username;

                                    Profile.Liked_PagesList.Add(p);
                                    if (itemPages_Count < 6)
                                    {
                                        MyProfilePage.Liked_Pages_ListItems.Add(p);
                                        itemPages_Count++;
                                    }
                                }
                                itemPages_Count = 0;
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        // userdata >> 
                        if (String.IsNullOrEmpty(First_name) && String.IsNullOrEmpty(Last_name))
                            Settings.UserFullName = name;
                        else
                            Settings.UserFullName = First_name + " " + Last_name;

                        if (DependencyService.Get<IPicture>().GetPictureFromDisk(cover, user_id) == "File Dont Exists")
                        {
                            Settings.Coverimage = new UriImageSource
                            {
                                Uri = new Uri(cover)
                            };
                            DependencyService.Get<IPicture>().SavePictureToDisk(cover, user_id);
                        }
                        else
                        {
                            Settings.Coverimage = DependencyService.Get<IPicture>().GetPictureFromDisk(cover, user_id);
                        }
                        if (DependencyService.Get<IPicture>().GetPictureFromDisk(avatar, user_id) == "File Dont Exists")
                        {
                            Settings.Avatarimage = new UriImageSource
                            {
                                Uri = new Uri(avatar)
                            };
                            DependencyService.Get<IPicture>().SavePictureToDisk(avatar, user_id);
                        }
                        else
                        {
                            Settings.Avatarimage = DependencyService.Get<IPicture>()
                                .GetPictureFromDisk(avatar, user_id);
                        }
                        using (var Datas = new LoginUserProfileFunctions())
                        {
                            var contact = Datas.GetProfileCredentialsById(user_id);
                            if (contact != null)
                            {
                                if (contact.UserID == user_id &&
                                    (contact.cover != cover || contact.avatar != avatar ||
                                     contact.birthday != birthday || contact.working != Working ||
                                     contact.name != name || contact.username != username ||
                                     contact.first_name != First_name || contact.last_name != Last_name ||
                                     contact.about != About || contact.website != Website ||
                                     contact.school != School || contact.facebook != facebook ||
                                     contact.google != google || contact.phone_number != phone_number ||
                                     contact.youtube != youtube || contact.vk != vk || contact.instagram != instagram ||
                                     contact.twitter != twitter || contact.linkedin != linkedin ||
                                     contact.following_number != following_number ||
                                     contact.followers_number != followers_number)
                                )
                                {
                                    //Datas.DeleteProfileRow(contact);
                                    if (contact.avatar != avatar)
                                    {
                                        DependencyService.Get<IPicture>()
                                            .DeletePictureFromDisk(contact.avatar, user_id);
                                    }
                                    if (contact.cover != cover)
                                    {
                                        DependencyService.Get<IPicture>().DeletePictureFromDisk(contact.cover, user_id);
                                    }

                                    contact.UserID = user_id;
                                    contact.name = name;
                                    contact.avatar = avatar;
                                    contact.cover = cover;
                                    contact.birthday = birthday;
                                    contact.address =
                                        Functions.StringNullRemover(
                                            Functions.DecodeString(System.Net.WebUtility.HtmlDecode(address)));
                                    contact.gender = gender;
                                    contact.email = email;
                                    contact.username = username;
                                    contact.first_name = Functions.StringNullRemover(First_name);
                                    contact.last_name = Functions.StringNullRemover(Last_name);
                                    contact.about =
                                        Functions.StringNullRemover(
                                            Functions.DecodeString(System.Net.WebUtility.HtmlDecode(About)));
                                    contact.phone_number = Functions.StringNullRemover(phone_number);
                                    contact.website = Functions.StringNullRemover(Website);
                                    contact.school = Functions.StringNullRemover(School);
                                    contact.working = Functions.StringNullRemover(Working);
                                    contact.facebook = Functions.StringNullRemover(facebook);
                                    contact.google = Functions.StringNullRemover(google);
                                    contact.youtube = Functions.StringNullRemover(youtube);
                                    contact.vk = Functions.StringNullRemover(vk);
                                    contact.instagram = Functions.StringNullRemover(instagram);
                                    contact.twitter = Functions.StringNullRemover(twitter);
                                    contact.linkedin = Functions.StringNullRemover(linkedin);
                                    contact.following_number = following_number;
                                    contact.followers_number = followers_number;
                                    contact.Groups_Total = Profile.GroupList.Count.ToString();
                                    //contact.Pages_Total = UD_Pages_Total;

                                    Datas.UpdateProfileCredentials(contact);
                                }
                            }
                            else
                            {
                                Datas.InsertProfileCredentials(new LoginUserProfileTableDB()
                                {
                                    UserID = user_id,
                                    name = name,
                                    avatar = avatar,
                                    cover = cover,
                                    birthday = birthday,
                                    address = address,
                                    gender = gender,
                                    email = email,
                                    username = username,
                                    first_name = First_name,
                                    last_name = Last_name,
                                    about = About,
                                    website = Website,
                                    phone_number = phone_number,
                                    school = School,
                                    working = Working,
                                    facebook = facebook,
                                    google = google,
                                    youtube = youtube,
                                    vk = vk,
                                    instagram = instagram,
                                    twitter = twitter,
                                    linkedin = linkedin,
                                    following_number = following_number,
                                    followers_number = followers_number,
                                    Groups_Total = Profile.GroupList.Count.ToString(),
                                });
                            }
                        }
                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }
    }
}