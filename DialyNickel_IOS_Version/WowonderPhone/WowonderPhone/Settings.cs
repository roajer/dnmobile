﻿//WOWONDER Timeline v1.8
//Copyright: DoughouzLight
//Email: alidoughous1993@gmail.com
//Full Documentation https://paper.dropbox.com/doc/WoWonder-Timeline-Version-1.7-J2AnlcVy05dpGilAJCNzy
//==============================

using System.Collections.Generic;
using PropertyChanged;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone
{
    //1-Add Your logo From Solution Explorer go to WowonderPhone.Android > Resources > Remove logo.png 
    //2-Right click on Resources folder > Add > Existing Item > pick your own logo.png
    //Same thing for all images and icons

    [ImplementPropertyChanged]
    public class Settings
    {
        //Main Settings >>>>>
        //*********************************************************
        public static string Website = "https://demo.wowonder.com/";
        public static string Version = "1.8";

        public static string API_ID = "144235f5702cb70fa6c3f48842738e35";
        public static string API_KEY = "b37bb1cd53d0bc21c13c1644e98a58ca";

        public static string APP_Name = "WoWonder";
        public static string ConnectivitySystem = "1";

        public static bool AcceptTerms_Display = true;
        public static string Market_curency = "$"; //The currency used in the store

        //Main Colors 
        //*********************************************************
        public static string MainColor = "#a84849";
        public static string MainPage_HeaderTextLabel = APP_Name;
        public static string MainPage_HeaderBackround_Color = "#a84849";
        public static string MainPage_HeaderText_Color = "#ffff";

        //Button Colors >> 
        public static string ButtonColorNormal = "#a54647";
        public static string ButtonTextColorNormal = "#fff";
        public static string ButtonLightColor = "#E7E7E7";
        public static string ButtonTextLightColor = "#444";
        public static string UnseenMesageColor = "#ededed";

        //ADMOB
        //*********************************************************
        public static bool Show_ADMOB_On_Timeline = true;
        public static bool Show_ADMOB_On_ItemList = true;
        public static bool Show_ADMOB_Interstitial = false;

        public static string Ad_Unit_ID = "ca-app-pub-5135691635931982/3190433140";
        public static string Ad_Interstitial_Key = "ca-app-pub-5135691635931982/8509862297";
        ///*********************************************************

        public static bool RenderPriorityFastPostLoad = true;
        public static bool ExtraRenderPriorityFastPostLoad = true;
        public static bool UseLocalNotificationPopUp = false;
        public static bool ShowAndroidDefaultImageViewer = true;

        //Main Messenger settings
        public static bool HandelPostViewerErrors = true;
        public static bool Messenger_Integration = true;
        public static string Messenger_Package_Name = "com.wowonder.messenger"; //APK name on Google Play
        ///
        ///*********************************************************

        //Background Images
        ///*********************************************************
        public static string Background_Image_WelcomePage = "aa339.png";
        public static string Background_Image_RegisterPage = "aa336.png";
        public static string Background_Image_LoginPage = "aa1.png";
        public static string Logo_Image_WelcomePage = "logo.png";
        public static string Logo_Image_LoginPage = "logo.png";

        //Social Logins
        ///*********************************************************
        public static bool Show_Facebook_Login = true;
        public static bool Show_Vkontakte_Login = false;
        public static bool Show_Instagram_Login = true;
        public static bool Show_Twitter_Login = true;
        public static bool Show_Google_Login = false;

        //########################### 

        //Main Slider settings
        ///*********************************************************
        public static bool Show_Articles = true;
        public static bool Show_Market = true;
        public static bool Show_Pro_Members = true;
        public static bool Show_Promoted_Pages = true;
        public static bool Show_Event_Pages = true;
        public static bool Show_Trending_Hashtags = true;
        public static bool Show_Block_On_User_Profiles = true;
        public static bool Show_NearBy = true;
        public static bool Show_Movies = true;

        //Main Refresh Posts & Notifications Time
        ///*********************************************************
        public static int  Notifications_Push_Secounds = 25;
        public static int  LoadMore_Post_Secounds = 160;

        //Android Display
        ///*********************************************************
        public static bool TurnFullScreenOn = false;

        public static bool Show_Cutsom_Logo_And_Header_On_the_Top = true;
        public static bool DefaultTheme = true;
        public static bool DarkTheme = false;
        public static bool LightTheme = false;

        //Bypass Web Erros (OnLogin crach or error set it to true)
        ///*********************************************************
        public static bool TurnTrustFailureOn_WebException = true;
        public static bool TurnSecurityProtocolType3072On = true;
        public static bool TurnHost503ErrorOn = true;
        public static bool TurnHost504ErrorOn = true;
        ///*********************************************************

        public static List<string> AddIemsCatigorieses = new List<string>
        {
            AppResources.Label3_Cat_Other,
            AppResources.Label3_Cat_Cars_and_Vehicles,
            AppResources.Label3_Cat_Comedy,
            AppResources.Label3_Cat_Economics_and_Trade,
            AppResources.Label3_Cat_Education,
            AppResources.Label3_Cat_Entertainment,
            AppResources.Label3_Cat_Movies_Animation,
            AppResources.Label3_Cat_Gaming,
            AppResources.Label3_Cat_History_and_Facts,
            AppResources.Label3_Cat_Live_Style,
            AppResources.Label3_Cat_Natural,
            AppResources.Label3_Cat_News_and_Politics,
            AppResources.Label3_Cat_People_and_Nations,
            AppResources.Label3_Cat_Pets_and_Animals,
            AppResources.Label3_Cat_Places_and_Regions,
            AppResources.Label3_Cat_Science_and_Technology,
            AppResources.Label3_Cat_Sport,
            AppResources.Label3_Cat_Travel_and_Events,
        };

        public static List<string> AddIemsMarket = new List<string>
        {
            AppResources.Label_ApparelAccessories,
            AppResources.Label_AutosVehicles,
            AppResources.Label_BabyChildrenProducts,
            AppResources.Label_BeautyProductsServices,
            AppResources.Label_ComputersPeripherals,
            AppResources.Label_ConsumerElectronics,
            AppResources.Label_DatingServices,
            AppResources.Label_FinancialServices,
            AppResources.Label_GiftsOccasions,
            AppResources.Label_HomeGarden,
        };

        //Main Icons
        public static string Like_Icon = "Like_Icon.png";
        public static string Add_Icon = "Plus.png"; 
        public static string CheckMark_Icon = "Checkmark.png";
        public static string Post_Picture_Icon = "PostPicture.png";
        public static string Post_Video_Icon = "PostVideo.png";
        public static string Post_Feeling_Icon = "PostFeeling.png";
        public static string Post_Tags_Icon = "PostTags.png";
        public static string Likecheked_Icon = "P_Checked.png";
        public static string LikePage_Icon = "P_Like.png";
        public static string AddUser_Icon = "P_AddUser.png";
        public static string BlockedUser_Icon = "P_Blocked.png";
        public static string BlockUser_Icon = "P_BlockUser.png";
        public static string Requested_Icon = "P_ Requested.png";

        //User Synic Limit
        ///*********************************************************
        public static int LimitContactGetFromPhone = 300;


        ///*********************************************************
        public static HtmlWebViewSource HTML_LoadingPost_Page = new HtmlWebViewSource
        {
            Html = "<html ><body bgcolor='#E9E9E9'>" +
                  "<img src='Smallloader.gif' style='width:380;height:200'/>" +
                  "<br><img src='Smallloader.gif' style='width:380;height:200'/>" +
                  "<br><img src='Smallloader.gif' style='width:380;height:200'/>" +
                  "</body></html>"
        };

        public static HtmlWebViewSource HTML_OfflinePost_Page = new HtmlWebViewSource
        {
            Html = "<html ><body>" +
                  "</br></br><img src='no_internet.png' style='width:100%;height:40%'/></br>" +
                  "</body></html>"
        };

       

        //############# DONT'T MODIFY HERE #############
        //Auto Session bindable objects 
        public static string Onesignal_APP_ID = "";
        public static string Session { get; set; }
        public static string Cookie { get; set; }
        public static string User_id { get; set; }
        public static string ProfilePicture { get; set; }
        public static string Username { get; set; }
        public static string UserFullName { get; set; }
        public static ImageSource Coverimage { get; set; }
        public static ImageSource Avatarimage { get; set; }
        public static string SearchByGenderValue { get; set; }
        public static string SearchByProfilePictureValue { get; set; }
        public static string SearchByStatusValue { get; set; }
        public static string PostPrivacyValue  = "1";
        public static bool ReLogin { get; set; } = false;
        public static bool NotificationVibrate { get; set; } = true;
        public static bool NotificationSound { get; set; } = true;
        public static bool NotificationPopup { get; set; } = true;
        public static string NotificationLedColor { get; set; } = MainColor;
        public static string NotificationLedColorName { get; set; } = "Default";
        public static string Label_Whats_Going_On = AppResources.Label_Whats_Going_On;
        public static string Label_add_short_description = AppResources.Label2_add_short_description;
        public static string Label_Default = AppResources.Label_Default;
        public static string PR_AboutDefault = "-------------";
        public static string InviteSMSText = AppResources.Label_invite_sms_text + " " + APP_Name;
        public static string Device_ID = "";
        //##############################################
    }
}