﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PropertyChanged;
using Xamarin.Forms;


namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class Notifications
    {
        public string Notifier_id { get; set; }
        public string Post_ID { get; set; }
        public string Page_ID { get; set; }
        public string Group_ID { get; set; }
        public string Seen { get; set; }
        public string SeenUnseenColor { get; set; }
        public string Type_Text { get; set; }
        public string Time_Text { get; set; }
        public string Type { get; set; }
        public string User_id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string MediaSize { get; set; }
        public ImageSource Avatar { get; set; }
        public string Url_Post { get; set; }
        public string Icon_Type_FO { get; set; }
        public string Icon_Color_FO { get; set; }
        public string ID { get; set; }


    }
}
