﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class Movies
    {
        public string id { get; set; }
        public string name { get; set; }
        public string genre { get; set; }
        public string stars { get; set; }
        public string producer { get; set; }
        public string country { get; set; }
        public string release { get; set; }
        public string quality { get; set; }
        public string duration { get; set; }
        public string description { get; set; }
        public string cover { get; set; }
        public string source { get; set; }
        public string iframe { get; set; }
        public string video { get; set; }
        public string views { get; set; }
        public string url { get; set; }
        public string description_Long { get; set; }
    }

}
