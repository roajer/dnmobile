﻿using System.Collections.Generic;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{

    [ImplementPropertyChanged]
    public class Story
    {
        public List<ImageSource> ListofImages { get; set; }
        public List<string> Listofvideos { get; set; }
        public string filename { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string User_id { get; set; }
        public string ID { get; set; }
        public string Author { get; set; }
        public ImageSource Thumbnail { get; set; }
        public string User_Name { get; set; }
        public ImageSource User_avatar { get; set; }
        public string Is_Owner { get; set; }
        
    }
}
