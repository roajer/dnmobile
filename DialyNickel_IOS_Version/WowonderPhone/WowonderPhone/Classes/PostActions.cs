﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class PostActions
    {
       
        public string User_id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public ImageSource Image { get; set; }
        public ImageSource Icon { get; set; }
        
    }
}
