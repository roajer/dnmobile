﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class Communities
    {
        public int ID { get; set; }
        public string CommunityID { get; set; }
        public string CommunityName { get; set; }
        public ImageSource CommunityPicture { get; set; }
        public string Url { get; set; }
        public string CommunityType { get; set; }
        public string CommunityTypeID { get; set; } // category id
        public string CommunityTypeLabel { get; set; } // category Label 
        public string ImageUrlString { get; set; }

        //public string ButtonColor { get; set; }
        //public string ButtonTextColor { get; set; }
        //public string ButtonImage { get; set; }

        public Communities()
        {

        }
    }
}
