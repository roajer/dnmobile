﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class Near_Me
    {
        public string ID { get; set; }
        public string Result_ID { get; set; }
        public string Name { get; set; }
        public ImageSource User_Image { get; set; }
        public string ThumbnailHeight { get; set; }
        public string Url { get; set; }
        public string GPS_place { get; set; }
        public string Last_Seen { get; set; }

        public string ButtonVisibilty { get; set; }
        public string ButtonColor { get; set; }
        public string ButtonTextColor { get; set; }
        public string ButtonImage { get; set; }
        public string connectivitySystem { get; set; }

       
    }
}
