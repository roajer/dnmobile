﻿
using System.Collections.ObjectModel;
using PropertyChanged;

namespace WowonderPhone.Classes
{
    public class Profile
    {

        public static ObservableCollection<Follower> FollowerList = new ObservableCollection<Follower>();
        public static ObservableCollection<Group> GroupList = new ObservableCollection<Group>();
        public static ObservableCollection<Liked_Pages> Liked_PagesList = new ObservableCollection<Liked_Pages>();

        public static ObservableCollection<Follower> FollowerUserList = new ObservableCollection<Follower>();
        public static ObservableCollection<Group> GroupUserList = new ObservableCollection<Group>();
        public static ObservableCollection<Liked_Pages> Liked_PagesUserList = new ObservableCollection<Liked_Pages>();

        [ImplementPropertyChanged]
        public class User_Data
        {
            public string UD_user_id { get; set; }
            public string UD_username { get; set; }
            public string UD_email { get; set; }
            public string UD_first_name { get; set; }
            public string UD_last_name { get; set; }
            public string UD_avatar { get; set; }
            public string UD_cover { get; set; }
            public string UD_relationship_id { get; set; }
            public string UD_address { get; set; }
            public string UD_working { get; set; }
            public string UD_working_link { get; set; }
            public string UD_about { get; set; }
            public string UD_school { get; set; }
            public string UD_gender { get; set; }
            public string UD_birthday { get; set; }
            public string UD_website { get; set; }
            public string UD_facebook { get; set; }
            public string UD_google { get; set; }
            public string UD_twitter { get; set; }
            public string UD_linkedin { get; set; }
            public string UD_youtube { get; set; }
            public string UD_vk { get; set; }
            public string UD_instagram { get; set; }
            public string UD_language { get; set; }
            public string UD_ip_address { get; set; }
            public string UD_follow_privacy { get; set; }
            public string UD_friend_privacy { get; set; }
            public string UD_post_privacy { get; set; }
            public string UD_message_privacy { get; set; }
            public string UD_confirm_followers { get; set; }
            public string UD_show_activities_privacy { get; set; }
            public string UD_birth_privacy { get; set; }
            public string UD_visit_privacy { get; set; }
            public string UD_verified { get; set; }
            public string UD_lastseen { get; set; }
            public string UD_showlastseen { get; set; }
            public string UD_e_sentme_msg { get; set; }
            public string UD_e_last_notif { get; set; }
            public string UD_status { get; set; }
            public string UD_active { get; set; }
            public string UD_admin { get; set; }
            public string UD_registered { get; set; }
            public string UD_phone_number { get; set; }
            public string UD_is_pro { get; set; }
            public string UD_pro_type { get; set; }
            public string UD_joined { get; set; }
            public string UD_timezone { get; set; }
            public string UD_referrer { get; set; }
            public string UD_balance { get; set; }
            public string UD_paypal_email { get; set; }
            public string UD_notifications_sound { get; set; }
            public string UD_order_posts_by { get; set; }
            public string UD_social_login { get; set; }
            public string UD_device_id { get; set; }
            public string UD_web_device_id { get; set; }
            public string UD_wallet { get; set; }
            public string UD_lat { get; set; }
            public string UD_lng { get; set; }
            public string UD_last_location_update { get; set; }
            public string UD_share_my_location { get; set; }
            public string UD_url { get; set; }
            public string UD_name { get; set; }
            public string UD_is_following { get; set; }
            public string UD_can_follow { get; set; }
            public string UD_post_count { get; set; }
            public string UD_gender_text { get; set; }
            public string UD_lastseen_time_text { get; set; }
            public string UD_is_blocked { get; set; }
            public string UD_following_number { get; set; }
            public string UD_followers_number { get; set; }
            public string UD_Pages_Total { get; set; }
            public string UD_Groups_Total { get; set; }

        }

        [ImplementPropertyChanged]
        public class Follower
        {
            public string F_user_id { get; set; }
            public string F_username { get; set; }
            public string F_email { get; set; }
            public string F_first_name { get; set; }
            public string F_last_name { get; set; }
            public string F_full_name { get; set; } //style
            public string F_avatar { get; set; }
            public string F_cover { get; set; }
            public string F_background_image { get; set; }
            public string F_background_image_status { get; set; }
            public string F_relationship_id { get; set; }
            public string F_address { get; set; }
            public string F_working { get; set; }
            public string F_working_link { get; set; }
            public string F_about { get; set; }
            public string F_school { get; set; }
            public string F_gender { get; set; }
            public string F_birthday { get; set; }
            public string F_country_id { get; set; }
            public string F_website { get; set; }
            public string F_facebook { get; set; }
            public string F_google { get; set; }
            public string F_twitter { get; set; }
            public string F_linkedin { get; set; }
            public string F_youtube { get; set; }
            public string F_vk { get; set; }
            public string F_instagram { get; set; }
            public string F_language { get; set; }
            public string F_email_code { get; set; }
            public string F_src { get; set; }
            public string F_ip_address { get; set; }
            public string F_follow_privacy { get; set; }
            public string F_friend_privacy { get; set; }
            public string F_post_privacy { get; set; }
            public string F_message_privacy { get; set; }
            public string F_confirm_followers { get; set; }
            public string F_show_activities_privacy { get; set; }
            public string F_birth_privacy { get; set; }
            public string F_visit_privacy { get; set; }
            public string F_verified { get; set; }
            public string F_lastseen { get; set; }
            public string F_showlastseen { get; set; }
            public string F_emailNotification { get; set; }
            public string F_e_liked { get; set; }
            public string F_e_wondered { get; set; }
            public string F_e_shared { get; set; }
            public string F_e_followed { get; set; }
            public string F_e_commented { get; set; }
            public string F_e_visited { get; set; }
            public string F_e_liked_page { get; set; }
            public string F_e_mentioned { get; set; }
            public string F_e_joined_group { get; set; }
            public string F_e_accepted { get; set; }
            public string F_e_profile_wall_post { get; set; }
            public string F_e_sentme_msg { get; set; }
            public string F_e_last_notif { get; set; }
            public string F_status { get; set; }
            public string F_active { get; set; }
            public string F_admin { get; set; }
            public string F_type { get; set; }
            public string F_registered { get; set; }
            public string F_start_up { get; set; }
            public string F_start_up_info { get; set; }
            public string F_startup_follow { get; set; }
            public string F_startup_image { get; set; }
            public string F_last_email_sent { get; set; }
            public string F_phone_number { get; set; }
            public string F_sms_code { get; set; }
            public string F_is_pro { get; set; }
            public string F_pro_time { get; set; }
            public string F_pro_type { get; set; }
            public string F_joined { get; set; }
            public string F_css_file { get; set; }
            public string F_timezone { get; set; }
            public string F_referrer { get; set; }
            public string F_balance { get; set; }
            public string F_paypal_email { get; set; }
            public string F_notifications_sound { get; set; }
            public string F_order_posts_by { get; set; }
            public string F_social_login { get; set; }
            public string F_device_id { get; set; }
            public string F_web_device_id { get; set; }
            public string F_wallet { get; set; }
            public string F_lat { get; set; }
            public string F_lng { get; set; }
            public string F_last_location_update { get; set; }
            public string F_share_my_location { get; set; }
            public string F_avatar_org { get; set; }
            public string F_cover_org { get; set; }
            public string F_cover_full { get; set; }
            public string F_id { get; set; }
            public string F_url { get; set; }
            public string F_name { get; set; }
            public string F_family_member { get; set; }
        }

        [ImplementPropertyChanged]
        public class Group
        {
            public string G_id { get; set; }
            public string G_user_id { get; set; }
            public string G_group_name { get; set; }
            public string G_group_title { get; set; }
            public string G_avatar { get; set; }
            public string G_cover { get; set; }
            public string G_about { get; set; }
            public string G_category { get; set; }
            public string G_privacy { get; set; }
            public string G_join_privacy { get; set; }
            public string G_active { get; set; }
            public string G_registered { get; set; }
            public string G_group_id { get; set; }
            public string G_url { get; set; }
            public string G_name { get; set; }
            public string G_category_id { get; set; }
            public string G_type { get; set; }
            public string G_username { get; set; }
        }

        [ImplementPropertyChanged]
        public class Liked_Pages
        {
            public string P_page_id { get; set; }
            public string P_user_id { get; set; }
            public string P_page_name { get; set; }
            public string P_page_title { get; set; }
            public string P_page_description { get; set; }
            public string P_avatar { get; set; }
            public string P_cover { get; set; }
            public string P_page_category { get; set; }//id cat
            public string P_website { get; set; }
            public string P_facebook { get; set; }
            public string P_google { get; set; }
            public string P_vk { get; set; }
            public string P_twitter { get; set; }
            public string P_linkedin { get; set; }
            public string P_company { get; set; }
            public string P_phone { get; set; }
            public string P_address { get; set; }
            public string P_call_action_type { get; set; }
            public string P_call_action_type_url { get; set; }
            public string P_background_image { get; set; }
            public string P_background_image_status { get; set; }
            public string P_instgram { get; set; }
            public string P_youtube { get; set; }
            public string P_verified { get; set; }
            public string P_active { get; set; }
            public string P_registered { get; set; }
            public string P_boosted { get; set; }
            public string P_about { get; set; }
            public string P_id { get; set; }
            public string P_type { get; set; }
            public string P_url { get; set; }
            public string P_name { get; set; }
            public string P_rating { get; set; }
            public string P_category { get; set; }//text cat
            public string P_is_page_onwer { get; set; }
            public string P_username { get; set; }
            public string P_post_count { get; set; }
            public string P_is_liked { get; set; }
            public string P_call_action_type_text { get; set; }
        }
    }
}