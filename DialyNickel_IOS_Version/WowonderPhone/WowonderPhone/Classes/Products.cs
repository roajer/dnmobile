﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class Products
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Price { get; set; }
        public ImageSource Image { get; set; }
        public string Manufacturer { get; set; }
        public string Manufacturer_UserID { get; set; }
        public string ThumbnailHeight { get; set; }

        public Products()
        {
        }
    }
}
