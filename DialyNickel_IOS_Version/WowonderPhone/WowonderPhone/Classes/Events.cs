﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
  
        [ImplementPropertyChanged]
        public class Events
        {
            public string Title { get; set; }

            public string Subtitle { get; set; }

            public string Url { get; set; }

            public string Place { get; set; }

            public string Author { get; set; }

            public ImageSource BackgroundImage { get; set; }

            public string Start_Time { get; set; }

            public string End_Time { get; set; }


            public string Going { get; set; }

            public string Intersted { get; set; }


        /////////////////

            public string id { get; set; }
            public string name { get; set; }
            public string location { get; set; }
            public string description { get; set; }
            public string start_date { get; set; }
            public string Profile_url { get; set; }
            public string is_owner { get; set; }
            public string start_date_Sorted { get; set; }
        
            public string end_date { get; set; }
            public string end_time { get; set; }
            public string poster_id { get; set; }
            public string cover { get; set; }
            public string is_going { get; set; }
            public string is_interested { get; set; }


    }
}
