﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class Article
    {
        public string Title { get; set; }

        public string Subtitle { get; set; }

        public string Url { get; set; }

        public string Section { get; set; }

        public string Author { get; set; }

        

      

        public ImageSource BackgroundImage { get; set; }

       

      

        public string Time { get; set; }

        public string Views { get; set; }

        public string Likes { get; set; }

    }
}


