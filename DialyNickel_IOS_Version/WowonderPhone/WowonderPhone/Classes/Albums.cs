﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    public class Albums
    {
        public string Name { get; set; }
        public string Postid { get; set; }
        public ImageSource Image { get; set; }
        public string Imagetext { get; set; }
        public string CommentsCount { get; set; }
        public string LikesCount { get; set; }
        public string WonderCount { get; set; }
        public string Publishername { get; set; }
        public string ImageTime { get; set; }


        public string ThumbnailHeight { get; set; }
        public List<Comment> ImageComments { get; set; }
    
        public class Comment
        {
            public string CommentText { get; set; }
            public string CommenterProfileUrl { get; set; }
            public string CommentLikes { get; set; }
            public ImageSource AvatarUser { get; set; }
            public string NameCommenter { get; set; }
            public string UserID { get; set; }
            public string Username { get; set; }
            public string CommentHeight { get; set; }

        }
    }
}
