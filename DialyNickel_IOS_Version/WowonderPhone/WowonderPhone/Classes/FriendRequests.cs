﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using Xamarin.Forms;

namespace WowonderPhone.Classes
{
    [ImplementPropertyChanged]
    public class FriendRequests
    {
        public string User_id { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }

        public string ConfirmButtonIsvisbile { get; set; }
        public string ConfirmButtonBGColor { get; set; }
        public string ConfirmTextColor { get; set; }
        public string ConfirmButtonText { get; set; }

        public string DeleteButtonIsvisbile { get; set; }
        public string DeleteButtonBGColor { get; set; }
        public string DeleteTextColor { get; set; }
        public string DeleteButtonText { get; set; }

        public string AccepetedIsvisbile { get; set; }
        public string AccepetedText { get; set; }

        public ImageSource Image { get; set; }
        public FriendRequests()
        {

        }
    }
}
