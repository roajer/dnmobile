﻿
using System;
using System.Diagnostics;
using System.Globalization;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages;
using WowonderPhone.Pages.Register_pages;
using WowonderPhone.SQLite;
using Xamarin.Forms;

namespace WowonderPhone
{
    public partial class App : Application
    {
        public static INavigation Navigation { get; set; }
        public static bool UserisStillNew = false;
        public static bool SessionHyberd = false;
        public App()
        {
            try
            {
                L10n.SetLocale();
                DependencyService.Register<ILocale>();
                var netLanguage = DependencyService.Get<ILocale>().GetCurrent();
                AppResources.Culture = new CultureInfo("en");

                using (var Data = new LoginFunctions())
                {
                    //Data.ClearLoginCredentialsList();
                    var CredentialStatus = Data.GetLoginCredentialsStatus();

                    InitializeComponent();
                    //Start Onesignal

                    if (CredentialStatus == "Active")
                    {
                        var Credential = Data.GetLoginCredentials("Active");
                        UserisStillNew = false;

                        Settings.Session = Credential.Session;
                        Settings.User_id = Credential.UserID;
                        Settings.Username = Credential.Username;
                        Settings.Cookie = Credential.Cookie;
                        Settings.Onesignal_APP_ID = Credential.Onesignal_APP_ID;

                        if (Credential.NotificationLedColor != "" || Credential.PrivacyPostValue != null)
                        {
                            Settings.NotificationVibrate = Credential.NotificationVibrate;
                            Settings.NotificationSound = Credential.NotificationSound;
                            Settings.NotificationPopup = Credential.NotificationPopup;
                            Settings.NotificationLedColor = Credential.NotificationLedColor;
                            Settings.NotificationLedColorName = Credential.NotificationLedColor;
                            Settings.PostPrivacyValue = Credential.PrivacyPostValue;
                        }
                        else
                        {
                            Credential.NotificationVibrate = true;
                            Credential.NotificationLedColor = Settings.MainColor;
                            Credential.NotificationLedColorName = AppResources.Label_Led_Color;
                            Credential.NotificationSound = true;
                            Credential.NotificationPopup = true;
                            Credential.PrivacyPostValue = "1";

                            Data.UpdateLoginCredentials(Credential);
                            Settings.NotificationVibrate = true;
                            Settings.NotificationSound = true;
                            Settings.NotificationPopup = true;
                            Settings.NotificationLedColor = Settings.MainColor;
                            Settings.NotificationLedColorName = AppResources.Label_Led_Color;
                        }

                        try
                        {
                            OneSignalNotificationController.RegisterNotificationDevice();

							if (Device.RuntimePlatform == Device.iOS) 
							{
								var navigationPage = new NavigationPage(new TabbedPageView()) { };
								navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
								navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
								navigationPage.Title = Settings.MainPage_HeaderTextLabel;
								navigationPage.Padding = new Thickness(0, 0, 0, 0);
								MainPage = navigationPage;
							}
							else
							{
								var navigationPage = new NavigationPage(new AndroidStyle()) { };
								navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
								navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
								navigationPage.Title = Settings.MainPage_HeaderTextLabel;
								navigationPage.Padding = new Thickness(0, 0, 0, 0);
								MainPage = navigationPage;
							}
                        }

                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                    else
                    {
                        if (CredentialStatus == "Registered")
                        {
                            var Credential = Data.GetLoginCredentials("Registered");
                            Settings.Session = Credential.Session;
                            Settings.User_id = Credential.UserID;
                            Settings.Username = Credential.Username;
                            Settings.Cookie = Credential.Cookie;
                            if (Credential.NotificationLedColor != "")
                            {
                                Settings.NotificationVibrate = Credential.NotificationVibrate;
                                Settings.NotificationSound = Credential.NotificationSound;
                                Settings.NotificationPopup = Credential.NotificationPopup;
                                Settings.NotificationLedColor = Credential.NotificationLedColor;
                                Settings.NotificationLedColorName = Credential.NotificationLedColor;
                            }
                            UserisStillNew = true;
                            Debug.WriteLine("opening WalkthroughVariantPage");
                            Current.MainPage = new NavigationPage(new WalkthroughVariantPage());
                            // MainPage = new NavigationPage(new UploudPicPage());
                        }
                        else
                        {
                            UserisStillNew = true;
                            Debug.WriteLine("opening WelcomePage");
                            Current.MainPage = new NavigationPage(new WelcomePage());
                        }
                    }
                }
                Debug.WriteLine("exit App()");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void GetMainPage()
        {
            try
            {
                var navigationPage = new NavigationPage(new TabbedPageView()) { };
                navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
                navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
                navigationPage.Title = Settings.MainPage_HeaderTextLabel;
                navigationPage.Padding = new Thickness(0, 0, 0, 0);
                MainPage = navigationPage;
            }
            catch (Exception ex)
            {
                App.Current.MainPage = new TabbedPageView();
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void OpenMainPage(WalkThrough_Page1 sss)
        {
            //MainPage = 
        }

        public static void GetLoginPage()
        {
            try
            {
                var navigationPage = new NavigationPage(new MainPage()) { };
                App.Current.MainPage = navigationPage;
            }
            catch (Exception ex)
            {
                App.Current.MainPage = new MainPage();
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            base.OnStart();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            base.OnSleep();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            base.OnResume();
        }
    }
}
