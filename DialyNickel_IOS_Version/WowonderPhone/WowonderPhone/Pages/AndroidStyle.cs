﻿using BottomBar.XamarinForms;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Tabs;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xamarin.Forms;

namespace WowonderPhone.Pages
{
    class AndroidStyle : BottomBarPage
    {
        public static NotificationsPage NotificationsPage;
        public AndroidStyle()
        {
            var HomePage = new TimelinePostsTab() {Icon = "Feedly.png", Title = AppResources.Label2_News_feed, BackgroundColor = Color.White,};

            var ItemListPage = new ItemListPage() { Icon = "NewsFeed.png", Title = AppResources.Label2_More, BackgroundColor = Color.White, };

            NotificationsPage = new NotificationsPage() {Icon = "bell.png", Title = AppResources.Label_Notifications,  BackgroundColor = Color.White,};

            //var UserStatusPage = new Status_Page() {Icon = "Stories.png", Title = AppResources.Label2_Status, BackgroundColor = Color.White,};

            var TrendingPage = new TrendingPage() {Icon = "bolt.png", Title = AppResources.Label_Trending, BackgroundColor = Color.White,};

            if (Settings.DarkTheme)
            {
                BarTheme = BarThemeTypes.DarkWithoutAlpha;
                BarTextColor = Color.White;
            }

            if (Settings.LightTheme)
            {
                this.BarBackgroundColor = Color.FromHex("#f3f3f3");
                BottomBarPageExtensions.SetTabColor(HomePage, Color.FromHex("#f3f3f3"));
                BottomBarPageExtensions.SetTabColor(NotificationsPage, Color.FromHex("#f3f3f3"));
                BottomBarPageExtensions.SetTabColor(ItemListPage, Color.FromHex("#f3f3f3"));
				BottomBarPageExtensions.SetTabColor(TrendingPage, Color.FromHex("#f3f3f3"));
                BarTextColor = Color.Black;
                //FixedMode = true;
            }

            if (Settings.DefaultTheme)
            {
                BottomBarPageExtensions.SetTabColor(HomePage, Color.FromHex(Settings.MainColor));
                BottomBarPageExtensions.SetTabColor(NotificationsPage, Color.FromHex(Settings.MainColor));
                BottomBarPageExtensions.SetTabColor(ItemListPage, Color.FromHex(Settings.MainColor));
                BottomBarPageExtensions.SetTabColor(TrendingPage, Color.FromHex(Settings.MainColor));
                BarTextColor = Color.White;
            }

            if (Settings.Show_Cutsom_Logo_And_Header_On_the_Top)
            {
                NavigationPage.SetHasNavigationBar(this, false);
            }

            Children.Add(HomePage);
            Children.Add(NotificationsPage);
            Children.Add(TrendingPage);
            Children.Add(ItemListPage);

            //NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
