﻿using System;
using System.Diagnostics;
using WowonderPhone.Pages.Register_pages;
using Xamarin.Forms;

namespace WowonderPhone.Pages
{
    public partial class WelcomePage : ContentPage
    {
        public WelcomePage()
        {
            try
            {
                InitializeComponent();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                    //HeaderOfpage.IsVisible = false;
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }
                Debug.WriteLine("exiting WelcomePage()");
            }
            catch (Exception ex)
            {

                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void RegisterButtonTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushModalAsync(new RegisterPage());
            }
            catch (Exception)
            {

                await Navigation.PushAsync(new RegisterPage());
            }
        }

        private async void LoginButtonTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushModalAsync(new MainPage());
            }
            catch (Exception)
            {

                await Navigation.PushAsync(new MainPage());
            }
        }
    }
}
