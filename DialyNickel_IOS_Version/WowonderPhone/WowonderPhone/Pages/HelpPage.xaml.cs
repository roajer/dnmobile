﻿using System;
using Acr.UserDialogs;
using WowonderPhone.Languish;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages
{
    public partial class HelpPage : ContentPage
    {
        public HelpPage()
        {
            try
            {
                InitializeComponent();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                    AboutUSLoader.Source = WowonderPhone.Settings.Website + "/terms/about-us";

                    AboutUSLoader.InjectJavascript("$('.content-container').css('margin-top', '0');");
                    AboutUSLoader.InjectJavascript("$('.header-container').hide();");
                    AboutUSLoader.InjectJavascript("$('.footer-wrapper').hide();");
                }
                else
                {
                    AboutUSLoader.IsVisible = false;
                    OfflinePage.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AboutUSLoader_OnOnContentLoaded(ContentLoadedDelegate eventobj)
        {
            try
            {
                AboutUSLoader.InjectJavascript("$('.content-container').css('margin-top', '0');");
                AboutUSLoader.InjectJavascript("$('.header-container').hide();");
                AboutUSLoader.InjectJavascript("$('.footer-wrapper').hide();");
                OfflinePage.IsVisible = false;
                AboutUSLoader.IsVisible = true;

                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading);
                    AboutUSLoader.Source = WowonderPhone.Settings.Website + "/terms/about-us";

                    AboutUSLoader.InjectJavascript("$('.content-container').css('margin-top', '0');");
                    AboutUSLoader.InjectJavascript("$('.header-container').hide();");
                    AboutUSLoader.InjectJavascript("$('.footer-wrapper').hide();");
                }
                else
                {
                    AboutUSLoader.IsVisible = false;
                    OfflinePage.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void HelpPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
