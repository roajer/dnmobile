﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.Contacts;
using Plugin.Contacts.Abstractions;
using Plugin.Messaging;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PropertyChanged;

namespace WowonderPhone.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InviteFriendsPage : ContentPage
    {
        #region classse

        [ImplementPropertyChanged]
        public class PhoneContacts
        {
            public int ID { get; set; }
            public string Phone { get; set; }
            public string Name { get; set; }
            public string JsonData { get; set; }
        }

        #endregion

        #region Lists Items Declaration

        public static System.Collections.ObjectModel.ObservableCollection<PhoneContacts> PhoneContactsItemsCollection = new System.Collections.ObjectModel.ObservableCollection<PhoneContacts>();

        #endregion

        public InviteFriendsPage()
        {
            try
            {
                InitializeComponent();

                if (PhoneContactsItemsCollection.Count == 0)
                {
                    GetPhoneContacts();
                }
                else
                {
                    ActionsListview.ItemsSource = PhoneContactsItemsCollection;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }       
        }

        public async void GetPhoneContacts()
        {
            try
            {
                try
                {
                    UserDialogs.Instance.ShowLoading("Loading...");
                    var Dictionary = new Dictionary<string, string>();
                    if (await CrossContacts.Current.RequestPermission())
                    {
                        CrossContacts.Current.PreferContactAggregation = false;
                        await Task.Run(() =>
                        {
                            if (CrossContacts.Current.Contacts == null)
                                return;
                            var max = 0;
                            foreach (Contact contact in CrossContacts.Current.Contacts)
                            {
                                foreach (var Number in contact.Phones)
                                {
                                    if (!Dictionary.ContainsKey(contact.FirstName + ' ' + contact.LastName))
                                    {
                                        Dictionary.Add(contact.FirstName + ' ' + contact.LastName, Number.Number);
                                    }

                                    var Cheker = PhoneContactsItemsCollection.FirstOrDefault(a => a.Name == contact.FirstName + ' ' + contact.LastName);
                                    if (Cheker == null)
                                    {
                                        string Data = JsonConvert.SerializeObject(Dictionary.ToArray().FirstOrDefault(a => a.Key == contact.FirstName + ' ' + contact.LastName));
                                        PhoneContactsItemsCollection.Add(new PhoneContacts
                                        {
                                            Name = contact.FirstName + ' ' + contact.LastName,
                                            Phone = Number.Number,
                                            JsonData = Data
                                        });
                                    }
                                    break;
                                }
                            }
                        });
                    }
                    ActionsListview.ItemsSource = PhoneContactsItemsCollection;
                    UserDialogs.Instance.HideLoading();
                }
                catch (Exception ex)
                {
                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActionsListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var item = e.Item as PhoneContacts;
                if (item != null)
                {
                    var smsMessenger = MessagingPlugin.SmsMessenger;
                    if (smsMessenger.CanSendSms)
                        smsMessenger.SendSms(item.Phone, Settings.InviteSMSText);
                    // Navigation.PopAsync();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActionsListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ActionsListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void RefreshContacts_Clicked(object sender, EventArgs e)
        {
            try
            {
                PhoneContactsItemsCollection.Clear();
                GetPhoneContacts();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
