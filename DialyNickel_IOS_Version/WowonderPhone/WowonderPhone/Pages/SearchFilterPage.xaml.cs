﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PropertyChanged;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.Pages
{
    public partial class SearchFilterPage : ContentPage
    {
        #region classes

        [ImplementPropertyChanged]
        public class SerachFilterItems
        {
            public string PrivacyLabel { get; set; }
            public string value { get; set; }
            public string Checkicon { get; set; }
            public string CheckiconColor { get; set; }
        }

        #endregion

        #region Lists Items Declaration 

        public static ObservableCollection<SerachFilterItems> SearchFilterListItemsCollection = new ObservableCollection<SerachFilterItems>();
        public static string SearchFilterType = AppResources.Label_Users;


        #endregion

        public SearchFilterPage()
        {
            try
            {
               
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                AddItemsToList();
                SearchBy.Items.Add(AppResources.Label_All);
                SearchBy.Items.Add(AppResources.Label_Male);
                SearchBy.Items.Add(AppResources.Label_Female);
                ProfilePicture.Items.Add(AppResources.Label_All);
                ProfilePicture.Items.Add(AppResources.Label_Yes);
                ProfilePicture.Items.Add(AppResources.Label_NO);
                Status.Items.Add(AppResources.Label_All);
                Status.Items.Add(AppResources.Label_Online);
                Status.Items.Add(AppResources.Label_Offline);

                using (var Filter = new SearchFilterFunctions())
                {
                    var FilterResult = Filter.GetSearchFilterById(Settings.User_id);
                    if (FilterResult == null)
                    {
                        SearchFilterDB NewSettingsFiltter = new SearchFilterDB();
                        NewSettingsFiltter.UserID = Settings.User_id;
                        NewSettingsFiltter.Gender = 0;
                        NewSettingsFiltter.ProfilePicture = 0;
                        NewSettingsFiltter.Status = 0;
                        Filter.InsertSearchFilter(NewSettingsFiltter);

                        ProfilePicture.SelectedIndex = 0;
                        SearchBy.SelectedIndex = 0;
                        Status.SelectedIndex = 0;
                    }
                    else
                    {
                        ProfilePicture.SelectedIndex = FilterResult.ProfilePicture;
                        SearchBy.SelectedIndex = FilterResult.Gender;
                        Status.SelectedIndex = FilterResult.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddItemsToList()
        {
            try
            {
                if (SearchFilterListItemsCollection.Count == 0)
                {
                    SearchFilterListItemsCollection.Add(new SerachFilterItems()
                    {
                        PrivacyLabel = AppResources.Label_Users,
                        value = "0",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    SearchFilterListItemsCollection.Add(new SerachFilterItems()
                    {
                        PrivacyLabel = AppResources.Label_Pages,
                        value = "1",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    SearchFilterListItemsCollection.Add(new SerachFilterItems()
                    {
                        PrivacyLabel = AppResources.Label_Groups,
                        value = "2",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    if (SearchFilterType == AppResources.Label_Users)
                    {
                        var ss = SearchFilterListItemsCollection.FirstOrDefault(s => s.PrivacyLabel == AppResources.Label_Users);
                        ss.Checkicon = "\uf058";
                    }
                    else if (SearchFilterType == AppResources.Label_Pages)
                    {
                        var ss = SearchFilterListItemsCollection.FirstOrDefault(s => s.PrivacyLabel == AppResources.Label_Pages);
                        ss.Checkicon = "\uf058";
                    }
                    else if (SearchFilterType == AppResources.Label_Groups)
                    {
                        var ss = SearchFilterListItemsCollection.FirstOrDefault(s => s.PrivacyLabel == AppResources.Label_Groups);
                        ss.Checkicon = "\uf058";
                    }
                }

                SearchFilterPListView.ItemsSource = SearchFilterListItemsCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchFilterPListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                SearchFilterPListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchFilterPListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as SerachFilterItems;
                if (Item != null)
                {
                    foreach (var PVitem in SearchFilterListItemsCollection)
                    {
                        PVitem.Checkicon = "\uf1db";
                    }

                    if (Item.Checkicon == "\uf1db")
                    {
                        Item.Checkicon = "\uf058";
                        Search_Page.SearchFilter = Item.PrivacyLabel;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchFilterPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                using (var Filter = new SearchFilterFunctions())
                {
                    var FilterResult = Filter.GetSearchFilterById(Settings.User_id);
                    FilterResult.ProfilePicture = ProfilePicture.SelectedIndex;
                    FilterResult.Gender = SearchBy.SelectedIndex;
                    FilterResult.Status = Status.SelectedIndex;
                    Filter.UpdateSearchFilter(FilterResult);
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    if (SearchBy.SelectedIndex == 0)
                    {
                        Settings.SearchByGenderValue = "";
                    }
                    if (SearchBy.SelectedIndex == 1)
                    {
                        Settings.SearchByGenderValue = "male";
                    }
                    if (SearchBy.SelectedIndex == 2)
                    {
                        Settings.SearchByGenderValue = "female";
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    if (Status.SelectedIndex == 0)
                    {
                        Settings.SearchByStatusValue = "";
                    }
                    if (Status.SelectedIndex == 1)
                    {
                        Settings.SearchByStatusValue = "on";
                    }
                    if (Status.SelectedIndex == 2)
                    {
                        Settings.SearchByStatusValue = "off";
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    if (ProfilePicture.SelectedIndex == 0)
                    {
                        Settings.SearchByProfilePictureValue = "";
                    }
                    if (ProfilePicture.SelectedIndex == 1)
                    {
                        Settings.SearchByProfilePictureValue = "yes";
                    }
                    if (ProfilePicture.SelectedIndex == 2)
                    {
                        Settings.SearchByProfilePictureValue = "no";
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
