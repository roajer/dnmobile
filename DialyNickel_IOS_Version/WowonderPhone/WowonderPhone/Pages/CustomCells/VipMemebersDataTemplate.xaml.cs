﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowonderPhone.Pages.Tabs;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xamarin.Forms;

namespace WowonderPhone.Pages.CustomCells
{
    public partial class VipMemebersDataTemplate : ContentView
    {
        public static BindableProperty ImageProperty =
              BindableProperty.Create(
                  nameof(Image),
                  typeof(ImageSource),
                  typeof(VipMemebersDataTemplate),
                  null,
                  defaultBindingMode: BindingMode.OneWay
              );

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        public VipMemebersDataTemplate()
        {
            InitializeComponent();
        }

        private async void OnImageTapped(object sender, EventArgs e)
        {
            try
            {
                var Item = (ItemListPage.TopUsersItems)((VipMemebersDataTemplate)sender).BindingContext;
                if (Item.User_id == Settings.User_id)
                {
                    await Navigation.PushAsync(new MyProfilePage());
                }
                if (Item.IsVip == "False")
                {
                    await Navigation.PushAsync(new ProUser_Upgrade_Page());
                }
                else
                {
                    await Navigation.PushAsync(new UserProfilePage(Item.User_id, "Vip User"));
                }
               
            }
            catch (Exception)
            {
               
            }
          
        }
    }
}
