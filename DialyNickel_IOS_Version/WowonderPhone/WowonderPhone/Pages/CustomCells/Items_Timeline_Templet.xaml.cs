﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.CustomCells
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Items_Timeline_Templet : ContentView
    {
        public Items_Timeline_Templet()
        {
            InitializeComponent();
        }
    }
}