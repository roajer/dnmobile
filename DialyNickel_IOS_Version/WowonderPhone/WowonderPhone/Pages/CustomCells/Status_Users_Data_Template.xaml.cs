﻿using System;
using System.Collections.Generic;
using System.Linq;
using WowonderPhone.Classes;
using Xamarin.Forms;

namespace WowonderPhone.Pages.CustomCells
{
    public partial class Status_Users_Data_Template : ContentView
    {
        public static BindableProperty ImageProperty = BindableProperty.Create(nameof(Image),typeof(ImageSource),typeof(Status_Users_Data_Template),null,defaultBindingMode: BindingMode.OneWay);

        public ImageSource Image
        {
            get { return (ImageSource)GetValue(ImageProperty); }
            set { SetValue(ImageProperty, value); }
        }

        public Status_Users_Data_Template()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnImageTapped(object sender, EventArgs e)
        {
            //try
            //{
            //   var Item = (Story)((Status_Users_Data_Template)sender).BindingContext;
            //    if (Item != null)
            //    {
            //        if (Item.Title == "Add New")
            //        {
            //            await Navigation.PushAsync(new Create_Story());
            //        }
            //        else
            //        {
            //            if (Item.ListofImages.Count == 0)
            //            {
            //              await Navigation.PushModalAsync(new Story_Page(Item));
            //            }
            //            else
            //            {
            //                await Navigation.PushModalAsync(new Story_Carousel_Page(Item));
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            //}
        }

     
    }
}
