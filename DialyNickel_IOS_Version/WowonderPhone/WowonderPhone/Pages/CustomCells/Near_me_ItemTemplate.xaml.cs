﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using Newtonsoft.Json;
using WowonderPhone.Classes;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Near_by;
using Xamarin.Forms;

namespace WowonderPhone.Pages.CustomCells
{
    public partial class Near_me_ItemTemplate : ContentView
    {
        public Near_me_ItemTemplate()
        {
            InitializeComponent();
        }

        private void AddUserButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Near_Me)((Button)sender).BindingContext;
                if (selectedItem != null)
                {
                    if (selectedItem.connectivitySystem == AppResources.Label_Follow || selectedItem.connectivitySystem == AppResources.Label_AddFriend)
                    {
                       
                            if (selectedItem.connectivitySystem == AppResources.Label_AddFriend)
                            {
                                selectedItem.connectivitySystem = AppResources.Label_Requested;
                            }
                            else
                            {
                                selectedItem.connectivitySystem = AppResources.Label_Following;
                            }

                        selectedItem.ButtonColor = Settings.ButtonColorNormal;
                        selectedItem.ButtonTextColor = Settings.ButtonTextColorNormal;
                        selectedItem.ButtonImage = Settings.Add_Icon;
                       
                       
                    }
                    else
                    {
                       
                        if (selectedItem.connectivitySystem == AppResources.Label_Requested)
                        {
                            selectedItem.connectivitySystem = AppResources.Label_AddFriend;
                        }
                        else
                        {
                            selectedItem.connectivitySystem = AppResources.Label_Follow;
                        }

                        selectedItem.ButtonColor = Settings.ButtonLightColor;
                        selectedItem.ButtonTextColor = Settings.ButtonTextLightColor;
                        selectedItem.ButtonImage = Settings.CheckMark_Icon;

                    }

                }
                SendFriendRequest(selectedItem.Result_ID).ConfigureAwait(false);

            }
            catch
            {

            }
        }

        public static async Task<string> SendFriendRequest(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =
                        await
                            client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=follow_user",
                                formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";

                    }
                }
            }
            catch (Exception)
            {

            }
            return null;
        }

        private void Image_Clicked(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Near_Me)((CachedImage)sender).BindingContext;
                if (selectedItem != null)
                {
                    Navigation.PushAsync(new UserProfilePage(selectedItem.Result_ID, ""));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
