﻿using System;
using WowonderPhone.Pages.Timeline_Pages;
using Xamarin.Forms;

namespace WowonderPhone.Pages.CustomCells
{
    public partial class ProductItemViewPage : ContentPage
    {

        public ProductItemViewPage()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnImageTapped(Object sender, EventArgs e)
        {
            try
            {
                var imagePreview = new ImageFullScreenPage((sender as Image).Source);
                await Navigation.PushModalAsync(new NavigationPage(imagePreview));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var mi = ((Button)sender);
                Navigation.PushAsync(new UserProfilePage(mi.CommandParameter.ToString(),"" ));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ProductItemViewPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                if (ManufacturerLabel.Text == "BY You")
                {
                    ButtonContact.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
