﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using XLabs.Forms.Controls;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using Acr.UserDialogs;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;

namespace WowonderPhone.Pages
{
    public partial class Search_Page : ContentPage
    {
        public static string SearchFilter = AppResources.Label_Users;

        public Search_Page()
        {
            try
            {
                InitializeComponent();

                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    //HeaderOfpage.IsVisible = false;
                    FilterButton.IsVisible = false;
                    BackButton.IsVisible = false;
                    SearchBarCo.TextColor = Color.Black;
                    SearchBarCo.BackgroundColor = Color.FromHex("#efefef");
                    HeaderOfpage.BackgroundColor = Color.FromHex("#efefef");
                    SearchBarCo.PlaceholderColor = Color.FromHex("#444");
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }

                Functions.SearchFilterlist.Clear();
                ChatFriendsListview.ItemsSource = null;
                SearchFilterGet();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    try
                    {

                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
                else
                {
                    ChatFriendsListview.IsVisible = false;

                }
                ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBar_OnSearchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    try
                    {
                        ChatFriendsListview.BeginRefresh();
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void SearchFilterGet()
        {
            try
            {
                using (var Filter = new SearchFilterFunctions())
                {
                    var FilterResult = Filter.GetSearchFilterById(Settings.User_id);
                    if (FilterResult == null)
                    {
                        Settings.SearchByGenderValue = "All";
                        Settings.SearchByProfilePictureValue = "All";
                        Settings.SearchByStatusValue = "All";
                    }
                    else
                    {
                        if (FilterResult.Gender == 0)
                        {
                            Settings.SearchByGenderValue = "";
                        }
                        if (FilterResult.Gender == 1)
                        {
                            Settings.SearchByGenderValue = "male";
                        }
                        if (FilterResult.Gender == 2)
                        {
                            Settings.SearchByGenderValue = "female";
                        }
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (FilterResult.Status == 0)
                        {
                            Settings.SearchByStatusValue = "";
                        }
                        if (FilterResult.Status == 1)
                        {
                            Settings.SearchByStatusValue = "on";
                        }
                        if (FilterResult.Status == 2)
                        {
                            Settings.SearchByStatusValue = "off";
                        }
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (FilterResult.ProfilePicture == 0)
                        {
                            Settings.SearchByProfilePictureValue = "";
                        }
                        if (FilterResult.ProfilePicture == 1)
                        {
                            Settings.SearchByProfilePictureValue = "yes";
                        }
                        if (FilterResult.ProfilePicture == 2)
                        {
                            Settings.SearchByProfilePictureValue = "no";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                EmptySearchPage.IsVisible = false;
                ChatFriendsListview.IsVisible = true;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActionButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var mi = ((ImageButton) sender);
                if (SearchFilter == AppResources.Label_Users)
                {
                    #region Add Friend

                    try
                    {
                        SendFriendRequest(mi.CommandParameter.ToString()).ConfigureAwait(false);
                        var getuser = Functions.SearchFilterlist
                            .Where(a => a.ResultID == mi.CommandParameter.ToString()).ToList().FirstOrDefault();
                        if (getuser != null)
                        {
                            if (getuser.connectivitySystem == AppResources.Label_Follow ||
                                getuser.connectivitySystem == AppResources.Label_AddFriend)
                            {
                                if (getuser.ResultID == mi.CommandParameter.ToString())
                                {
                                    if (getuser.connectivitySystem == AppResources.Label_AddFriend)
                                    {
                                        getuser.connectivitySystem = AppResources.Label_Requested;
                                    }
                                    else
                                    {
                                        getuser.connectivitySystem = AppResources.Label_Following;
                                    }
                                    getuser.ButtonColor = Settings.ButtonLightColor;
                                    getuser.ButtonTextColor = Settings.ButtonTextLightColor;
                                    getuser.ButtonImage = Settings.CheckMark_Icon;
                                }
                            }
                            else
                            {
                                if (getuser.ResultID == mi.CommandParameter.ToString())
                                {
                                    if (getuser.connectivitySystem == AppResources.Label_Requested)
                                    {
                                        getuser.connectivitySystem = AppResources.Label_AddFriend;
                                    }
                                    else
                                    {
                                        getuser.connectivitySystem = AppResources.Label_Follow;
                                    }
                                    getuser.ButtonColor = Settings.ButtonColorNormal;
                                    getuser.ButtonTextColor = Settings.ButtonTextColorNormal;
                                    getuser.ButtonImage = Settings.Add_Icon;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }

                    #endregion
                }
                else if (SearchFilter == AppResources.Label_Pages)
                {
                    try
                    {
                        var getPage = Functions.SearchFilterlist
                            .Where(a => a.ResultID == mi.CommandParameter.ToString()).ToList().FirstOrDefault();
                        if (getPage != null)
                        {
                            if (getPage.connectivitySystem == AppResources.Label_Like)
                            {
                                if (getPage.ResultID == mi.CommandParameter.ToString())
                                {
                                    SocialPageViewer.LikeUnlikePageRequest(mi.CommandParameter.ToString())
                                        .ConfigureAwait(false);
                                    getPage.ButtonColor = Settings.ButtonLightColor;
                                    getPage.ButtonTextColor = Settings.ButtonTextLightColor;
                                    getPage.ButtonImage = Settings.CheckMark_Icon;
                                    getPage.connectivitySystem = AppResources.Label_Unlike;
                                }
                            }
                            else
                            {
                                if (getPage.ResultID == mi.CommandParameter.ToString())
                                {
                                    SocialPageViewer.LikeUnlikePageRequest(mi.CommandParameter.ToString())
                                        .ConfigureAwait(false);
                                    getPage.ButtonColor = Settings.ButtonColorNormal;
                                    getPage.ButtonTextColor = Settings.ButtonTextColorNormal;
                                    getPage.ButtonImage = Settings.Like_Icon;
                                    getPage.connectivitySystem = AppResources.Label_Like;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
                else if (SearchFilter == AppResources.Label_Groups)
                {
                    try
                    {
                        var getGroup = Functions.SearchFilterlist
                            .Where(a => a.ResultID == mi.CommandParameter.ToString()).ToList().FirstOrDefault();
                        if (getGroup != null)
                        {
                            if (getGroup.connectivitySystem == AppResources.Label_Join)
                            {
                                if (getGroup.ResultID == mi.CommandParameter.ToString())
                                {
                                    SocialGroup.AddUnAddRequest(mi.CommandParameter.ToString()).ConfigureAwait(false);
                                    getGroup.ButtonColor = Settings.ButtonLightColor;
                                    getGroup.ButtonTextColor = Settings.ButtonTextLightColor;
                                    getGroup.ButtonImage = Settings.CheckMark_Icon;
                                    getGroup.connectivitySystem = AppResources.Label_Joined;
                                }
                            }
                            else
                            {
                                if (getGroup.ResultID == mi.CommandParameter.ToString())
                                {
                                    SocialGroup.AddUnAddRequest(mi.CommandParameter.ToString()).ConfigureAwait(false);
                                    getGroup.ButtonColor = Settings.ButtonColorNormal;
                                    getGroup.ButtonTextColor = Settings.ButtonTextColorNormal;
                                    getGroup.ButtonImage = Settings.Add_Icon;
                                    getGroup.connectivitySystem = AppResources.Label_Join;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public static async Task<string> SendFriendRequest(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=follow_user",formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
            return null;
        }

        public async Task<string> SearchRequest(string search_key)
        {
            try
            {
                ChatFriendsListview.IsRefreshing = true;

                //ChatFriendsListview.BeginRefresh();
                // UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                if (SearchFilter == AppResources.Label_Users)
                {
                    #region Search Users

                    using (var client = new HttpClient())
                    {
                        var formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("search_key", search_key),
                            new KeyValuePair<string, string>("s", Settings.Session),
                            new KeyValuePair<string, string>("gender", Settings.SearchByGenderValue),
                            new KeyValuePair<string, string>("image", Settings.SearchByProfilePictureValue),
                            new KeyValuePair<string, string>("status", Settings.SearchByStatusValue)
                        });

                        var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=search_public_users",formContent).ConfigureAwait(false);
                        response.EnsureSuccessStatusCode();
                        string json = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        string apiStatus = data["api_status"].ToString();
                        if (apiStatus == "200")
                        {
                            var Users = data["users"];
                            string ThemeUrl = data["theme_url"].ToString();
                            var users = JObject.Parse(json).SelectToken("users").ToString();
                            Object obj = JsonConvert.DeserializeObject(users);
                            JArray Chatusers = JArray.Parse(users);

                            foreach (var ChatUser in Chatusers)
                            {
                                JObject ChatlistUserdata = JObject.FromObject(ChatUser);
                                var ChatUser_User_ID = ChatlistUserdata["user_id"].ToString();
                                var ChatUser_avatar = ChatlistUserdata["profile_picture"].ToString();
                                var ChatUser_name = ChatlistUserdata["name"].ToString();
                                var ChatUser_lastseen = ChatlistUserdata["lastseen"].ToString();
                                var ChatUser_lastseen_Time_Text = ChatlistUserdata["lastseen_time_text"].ToString();
                                var ChatUser_verified = ChatlistUserdata["verified"].ToString();
                                var ChatUser_Isfollowed = ChatlistUserdata["is_following"].ToString();

                                var ImageMediaFile = ImageSource.FromUri(new Uri(ChatUser_avatar));
                                ;
                                var ImageAvatar = ImageSource.FromUri(new Uri(ChatUser_avatar));
                                if (ChatUser_avatar.Contains("d-avatar.jpg"))
                                {

                                    // ImageMediaFile = "P_Man.png";
                                }
                                else
                                {
                                    ImageMediaFile = new UriImageSource
                                    {
                                        Uri = new Uri(ChatUser_avatar)
                                    };
                                }

                                #region Show_Online_Oflline_Icon

                                var OnlineOfflineIcon = ImageSource.FromFile("");

                                #endregion

                                var status = AppResources.Label_AddFriend;
                                if (Settings.ConnectivitySystem == "1")
                                {
                                    status = AppResources.Label_Follow;
                                }

                                if (ChatUser_Isfollowed == "no")
                                {
                                    if (ChatUser_lastseen == "on")
                                    {
                                        Functions.SearchFilterlist.Add(new SearchResult()
                                        {
                                            BigLabel = ChatUser_name,
                                            lastseen = OnlineOfflineIcon,
                                            Name = ChatUser_name,
                                            SeenMessageOrNo = ChatUser_lastseen,
                                            profile_picture = ImageMediaFile,
                                            MiniLabel = AppResources.Label_Online,
                                            ResultID = ChatUser_User_ID,
                                            connectivitySystem = status,
                                            ResultType = AppResources.Label_Users,
                                            ButtonColor = Settings.ButtonColorNormal,
                                            ButtonTextColor = Settings.ButtonTextColorNormal,
                                            ButtonImage = Settings.Add_Icon,
                                            ResultButtonAvailble = "true"
                                        });
                                    }
                                    else
                                    {
                                        Functions.SearchFilterlist.Add(new SearchResult()
                                        {
                                            BigLabel = ChatUser_name,
                                            lastseen = OnlineOfflineIcon,
                                            Name = ChatUser_name,
                                            ResultType = AppResources.Label_Users,
                                            SeenMessageOrNo = ChatUser_lastseen,
                                            profile_picture = ImageMediaFile,
                                            MiniLabel = AppResources.Label_LastSeen + " " + ChatUser_lastseen_Time_Text,
                                            ResultID = ChatUser_User_ID,
                                            connectivitySystem = status,
                                            ButtonColor = Settings.ButtonColorNormal,
                                            ButtonTextColor = Settings.ButtonTextColorNormal,
                                            ButtonImage = Settings.Add_Icon,
                                            ResultButtonAvailble = "true"
                                        });
                                    }
                                }
                                else
                                {
                                    var Added = AppResources.Label_Requested;
                                    if (Settings.ConnectivitySystem == "1")
                                    {
                                        Added = AppResources.Label_Following;
                                    }

                                    Functions.SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = ChatUser_name,
                                        lastseen = OnlineOfflineIcon,
                                        Name = ChatUser_name,
                                        ResultType = AppResources.Label_Users,
                                        SeenMessageOrNo = ChatUser_lastseen,
                                        profile_picture = ImageMediaFile,
                                        MiniLabel = AppResources.Label_LastSeen + " " + ChatUser_lastseen_Time_Text,
                                        ResultID = ChatUser_User_ID,
                                        connectivitySystem = Added,
                                        ButtonColor = Settings.ButtonLightColor,
                                        ButtonTextColor = Settings.ButtonTextLightColor,
                                        ButtonImage = Settings.CheckMark_Icon,
                                        ResultButtonAvailble = "true"
                                    });
                                }
                            }
                        }
                    }

                    #endregion
                }
                else if (SearchFilter == AppResources.Label_Pages)
                {
                    #region Search Pages

                    using (var client = new HttpClient())
                    {
                        var formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("search_key", search_key),
                            new KeyValuePair<string, string>("s", Settings.Session),
                            new KeyValuePair<string, string>("gender", Settings.SearchByGenderValue),
                            new KeyValuePair<string, string>("image", Settings.SearchByProfilePictureValue),
                            new KeyValuePair<string, string>("status", Settings.SearchByStatusValue)
                        });

                        var response = await client .PostAsync(Settings.Website + "/app_api.php?application=phone&type=search_public_users",formContent).ConfigureAwait(false);
                        response.EnsureSuccessStatusCode();
                        string json = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        string apiStatus = data["api_status"].ToString();
                        if (apiStatus == "200")
                        {
                            var Pages = JObject.Parse(json).SelectToken("pages").ToString();
                            Object obj = JsonConvert.DeserializeObject(Pages);
                            JArray PagesResult = JArray.Parse(Pages);

                            foreach (var Page in PagesResult)
                            {
                                JObject Data = JObject.FromObject(Page);
                                var page_id = Data["page_id"].ToString();
                                var page_name = Data["page_name"].ToString();
                                var url = Data["url"].ToString();
                                var avatar = Data["avatar"].ToString();
                                var name = Data["name"].ToString();

                                var Isliked = "False";
                                try
                                {
                                    Isliked = Data["is_liked"].ToString();
                                }
                                catch (Exception ex)
                                {
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                }

                                var ImageAvatar = ImageSource.FromUri(new Uri(avatar));
                                if (avatar.Contains("d-page.jpg"))
                                {
                                    //ImageAvatar = "P_Pagess.png";
                                }

                                if (Isliked == "True")
                                {
                                    Functions.SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = name,
                                        Name = name,
                                        profile_picture = ImageAvatar,
                                        ResultID = page_id,
                                        ResultType = AppResources.Label_Pages,
                                        MiniLabel = page_name,
                                        ButtonColor = Settings.ButtonLightColor,
                                        ButtonTextColor = Settings.ButtonTextLightColor,
                                        ButtonImage = Settings.CheckMark_Icon,
                                        Url = url,
                                        connectivitySystem = AppResources.Label_Unlike,
                                        ResultButtonAvailble = "true"
                                    });
                                }
                                else
                                {
                                    Functions.SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = name,
                                        Name = name,
                                        profile_picture = ImageAvatar,
                                        ResultID = page_id,
                                        ResultType = AppResources.Label_Pages,
                                        MiniLabel = page_name,
                                        ButtonColor = Settings.ButtonColorNormal,
                                        ButtonTextColor = Settings.ButtonTextColorNormal,
                                        ButtonImage = Settings.Like_Icon,
                                        Url = url,
                                        connectivitySystem = AppResources.Label_Like,
                                        ResultButtonAvailble = "true"
                                    });
                                }
                            }
                        }
                    }

                    #endregion
                }
                else if (SearchFilter == AppResources.Label_Groups)
                {
                    #region Search Groups

                    using (var client = new HttpClient())
                    {
                        var formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("search_key", search_key),
                            new KeyValuePair<string, string>("s", Settings.Session),
                            new KeyValuePair<string, string>("gender", Settings.SearchByGenderValue),
                            new KeyValuePair<string, string>("image", Settings.SearchByProfilePictureValue),
                            new KeyValuePair<string, string>("status", Settings.SearchByStatusValue)
                        });

                        var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=search_public_users",formContent).ConfigureAwait(false);
                        response.EnsureSuccessStatusCode();
                        string json = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        string apiStatus = data["api_status"].ToString();
                        if (apiStatus == "200")
                        {
                            var Groups = JObject.Parse(json).SelectToken("groups").ToString();
                            Object obj = JsonConvert.DeserializeObject(Groups);
                            JArray GroupsResult = JArray.Parse(Groups);

                            foreach (var Group in GroupsResult)
                            {
                                JObject Data = JObject.FromObject(Group);
                                var group_title = Data["group_title"].ToString();
                                var user_id = Data["user_id"].ToString();
                                var group_id = Data["group_id"].ToString();
                                var url = Data["url"].ToString();
                                var category_id = Data["group_id"].ToString();
                                var type = Data["type"].ToString();
                                var avatar = Data["avatar"].ToString();
                                var cover = Data["cover"].ToString();
                                var about = Data["about"].ToString();
                                var category = Data["category"].ToString();
                                var username = Data["username"].ToString();
                                var name = Data["name"].ToString();
                                var Isjoined = "False";
                                try
                                {
                                    Isjoined = Data["is_joined"].ToString();
                                }
                                catch (Exception ex)
                                {
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                }

                                var ImageAvatar = ImageSource.FromUri(new Uri(avatar));
                                if (avatar.Contains("d_group.jpg"))
                                {
                                    //ImageAvatar = "P_Group.png";
                                }

                                if (Isjoined == "True")
                                {
                                    Functions.SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = name,
                                        Name = name,
                                        profile_picture = ImageAvatar,
                                        ResultID = group_id,
                                        ResultType = AppResources.Label_Groups,
                                        ButtonColor = Settings.ButtonLightColor,
                                        ButtonTextColor = Settings.ButtonTextLightColor,
                                        ButtonImage = Settings.CheckMark_Icon,
                                        Url = url,
                                        MiniLabel = username,
                                        connectivitySystem = AppResources.Label_Joined,
                                        ResultButtonAvailble = "true"
                                    });
                                }
                                else
                                {
                                    Functions.SearchFilterlist.Add(new SearchResult()
                                    {
                                        BigLabel = name,
                                        Name = name,
                                        profile_picture = ImageAvatar,
                                        ResultID = group_id,
                                        ResultType = AppResources.Label_Groups,
                                        ButtonColor = Settings.ButtonColorNormal,
                                        ButtonTextColor = Settings.ButtonTextColorNormal,
                                        ButtonImage = Settings.Add_Icon,
                                        Url = url,
                                        MiniLabel = username,
                                        connectivitySystem = AppResources.Label_Join,
                                        ResultButtonAvailble = "true"
                                    });
                                }
                            }
                        }
                    }

                    #endregion
                }

                UserDialogs.Instance.HideLoading();
                ChatFriendsListview.EndRefresh();
                ChatFriendsListview.IsRefreshing = false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
               // UserDialogs.Instance.HideLoading();
                ChatFriendsListview.EndRefresh();
                ChatFriendsListview.IsRefreshing = false;

               // UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
            }
            return null;
        }


        private void ChatFriendsListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                ChatFriendsListview.SelectedItem = null;
                var Item = e.Item as SearchResult;
                var resultID = Item.ResultID;
                var UserName = Item.BigLabel;

                if (Item.ResultType == AppResources.Label_Users)
                {
                    Navigation.PushAsync(new UserProfilePage(resultID, "FriendList"));
                }
                else if (Item.ResultType == AppResources.Label_Pages)
                {
                    Navigation.PushAsync(new SocialPageViewer(resultID, Item.BigLabel, "Search"));
                }
                else if (Item.ResultType == AppResources.Label_Groups)
                {
                    Navigation.PushAsync(new SocialGroup(resultID, "FromSearch"));

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ChatFriendsListview_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {

        }

        private async void ChatFriendsListview_OnRefreshing(object sender, EventArgs e)
        {
            try
            {

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    try
                    {
                        EmptySearchPage.IsVisible = false;
                        ChatFriendsListview.IsVisible = true;

                        ChatFriendsListview.IsRefreshing = true;

                        Functions.SearchFilterlist.Clear();
                        ChatFriendsListview.ItemsSource = null;

                        await SearchRequest(SearchBarCo.Text);

                        //this.Title = AppResources.Label_Loading;
                        if (Functions.SearchFilterlist.Count > 0)
                        {
                            ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
                        }
                        else
                        {
                            EmptySearchPage.IsVisible = true;
                            ChatFriendsListview.IsVisible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
                ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
                ChatFriendsListview.EndRefresh();
                ChatFriendsListview.IsRefreshing = false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ChatFriendsListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ChatFriendsListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Sync_OnClicked_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    try
                    {
                        ChatFriendsListview.IsVisible = true;

                        this.Title = AppResources.Label_Loading;
                        Functions.SyncContactsFromPhone().ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
                ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
                ChatFriendsListview.EndRefresh();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Fillter_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new SearchFilterPage());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Recommended_OnClicked_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    try
                    {
                        ChatFriendsListview.IsVisible = true;
                        var ss = Functions.GetRandomUsers().ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    }
                }
                ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
                ChatFriendsListview.EndRefresh();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddFriends_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                //Functions.SearchFilterlist.Clear();
                //ChatFriendsListview.ItemsSource = null;
                ////this.Title = AppResources.Label_Loading;
                ////var ss = Functions.GetRandomUsers().ConfigureAwait(false);
                ////ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
                //EmptySearchPage.IsVisible = false;
                //ChatFriendsListview.IsVisible = true;

                ChatFriendsListview.BeginRefresh();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnSearchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                Functions.SearchFilterlist.Clear();
                ChatFriendsListview.ItemsSource = null;
                this.Title = AppResources.Label_Loading;
                var ss = SearchRequest(SearchBarCo.Text).ConfigureAwait(false);
                ChatFriendsListview.ItemsSource = Functions.SearchFilterlist;
                EmptySearchPage.IsVisible = false;
                ChatFriendsListview.IsVisible = true;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}