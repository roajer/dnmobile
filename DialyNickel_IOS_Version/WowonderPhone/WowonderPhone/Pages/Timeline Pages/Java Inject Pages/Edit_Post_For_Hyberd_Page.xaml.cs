﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Tabs;

using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.Java_Inject_Pages
{
    public partial class Edit_Post_For_Hyberd_Page : ContentPage
    {
        #region Variables

        public static string POST = "";
        public static string ID = "";

        private HyberdPostViewer Page;
        private TimelinePostsTab Page2;

        #endregion

        public Edit_Post_For_Hyberd_Page(string Post, string ID_Post, HyberdPostViewer Hyberdpage, TimelinePostsTab PostTimline)
        {
            try
            {
                InitializeComponent();
                POST = Post;
                ID = ID_Post;
                PostContent.Text = Functions.DecodeString(POST);

                if (Hyberdpage != null)
                {
                    Page = Hyberdpage;
                }
                else
                {
                    Page2 = null;
                }
                if (PostTimline != null)
                {
                    Page2 = PostTimline;
                }
                else
                {
                    Page = null;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostContent_OnTextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void PostButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowLoading(AppResources.Label_Editing_Post, MaskType.Clear);
                Post_Manager(ID, PostContent.Text).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Post_Manager(string postid, string text)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    using (var client = new HttpClient())
                    {
                        var formContent = new FormUrlEncodedContent(new[]
                        {
                        new KeyValuePair<string, string>("user_id", WowonderPhone.Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s",WowonderPhone.Settings.Session),
                        new KeyValuePair<string, string>("action","edit"),
                        new KeyValuePair<string, string>("text",text),
                    });

                        var response =
                            await
                                client.PostAsync(WowonderPhone.Settings.Website + "/app_api.php?application=phone&type=post_manager",
                                    formContent);
                        response.EnsureSuccessStatusCode();
                        string json = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        string apiStatus = data["api_status"].ToString();
                        if (apiStatus == "200")
                        {
                            string Text = data["text"].ToString();

                            UserDialogs.Instance.HideLoading();
                            await Navigation.PopAsync(false);

                            if (Page != null)
                            {
                                Page.EditCommentDone(ID, Text);
                            }
                            else if (Page2 != null)
                            {
                                Page2.EditCommentDone(ID, Text);
                            }
                            else
                            {

                            }
                        }
                    }
                }
                else
                {
                    UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }             
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Edit_Post_For_Hyberd_Page_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
