﻿using System;
using Acr.UserDialogs;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages
{
    public partial class ImageFullScreenPage : ContentPage
    {
        #region Variables

        private const uint animationDuration = 100;
        private TapGestureRecognizer doubleTapGestureRecognizer;

        #endregion

        public ImageFullScreenPage(ImageSource source)
        {
            try
            {
                InitializeComponent();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    //HeaderOfpage.IsVisible = false;
                }
                else
                    NavigationPage.SetHasNavigationBar(this, false);
                ImageLoader.Source = source;
                LoaderSpinner.Color = Color.FromHex(Settings.MainColor);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                if (doubleTapGestureRecognizer == null)
                {
                    doubleTapGestureRecognizer = new TapGestureRecognizer();
                    doubleTapGestureRecognizer.NumberOfTapsRequired = 2;
                }

                doubleTapGestureRecognizer.Tapped += OnImagePreviewDoubleTapped;
                ImageLoader.GestureRecognizers.Add(doubleTapGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
          
        }

        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
                doubleTapGestureRecognizer.Tapped -= OnImagePreviewDoubleTapped;
                ImageLoader.GestureRecognizers.Remove(doubleTapGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnImagePreviewDoubleTapped(object sender, EventArgs args)
        {
            try
            {
                if ((int)ImageLoader.Scale == 1)
                {
                    await ImageLoader.ScaleTo(2, animationDuration, Easing.SinInOut);
                }
                else if ((int)ImageLoader.Scale == 2)
                {
                    await ImageLoader.ScaleTo(3, animationDuration, Easing.SinInOut);
                }
                else if ((int)ImageLoader.Scale == 3)
                {
                    await ImageLoader.ScaleTo(1, animationDuration, Easing.SinInOut);
                }
                else
                {
                    await ImageLoader.ScaleTo(1, animationDuration, Easing.SinInOut);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        async void OnCloseButtonClicked(object sender, EventArgs args)
        {
            try
            {
                await Navigation.PopModalAsync();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Img_OnSuccess(object sender, CachedImageEvents.SuccessEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ImageLoader.IsVisible = true;
                    LoaderSpinner.IsVisible = false;
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ImageLoader_OnFinish(object sender, CachedImageEvents.FinishEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ImageLoader.IsVisible = true;
                    LoaderSpinner.IsVisible = false;
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ImageLoader_OnDownloadStarted(object sender, CachedImageEvents.DownloadStartedEventArgs e)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ImageLoader.IsVisible = false;
                    LoaderSpinner.IsVisible = true;
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ImageLoader_OnError(object sender, CachedImageEvents.ErrorEventArgs e)
        {
            try
            {
                UserDialogs.Instance.ShowError("Image Cannot Be Loaded");
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
