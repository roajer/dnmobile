﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.Java_Inject_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages
{
    public partial class HyberdPostViewer : ContentPage
    {
        #region Variables

        public static string Page_Link = "";
        public static string Page_Type = "";

        #endregion

        public HyberdPostViewer(string PageType, string Link)
        {
            try
            {
                
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                Page_Link = Link;
                Page_Type = PageType;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    LoadingPost.Source = Settings.HTML_LoadingPost_Page;

                    if (PageType == "User")
                    {
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?user_id=" + UserProfilePage.U_UserID;
                        this.Title = UserProfilePage.S_Name + " " + AppResources.Label_Timeline;
                        HeaderLabel.Text = UserProfilePage.S_Name + " " + AppResources.Label_Timeline;
                    }
                    else if (PageType == "MyProfile")
                    {
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?user_id=" + Settings.User_id;
                        this.Title = AppResources.Label_My_Profile;
                        HeaderLabel.Text = AppResources.Label_My_Profile;
                    }
                    else if (PageType == "Saved Post")
                    {
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?save_posts=true" + Settings.User_id;
                        this.Title = AppResources.Label_Saved_Post;
                        HeaderLabel.Text = AppResources.Label_Saved_Post;
                    }
                    else if (PageType == "Page")
                    {
                        this.Title = SocialPageViewer.S_Page_Title;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?page_id=" + SocialPageViewer.P_PageID;
                        HeaderLabel.Text = SocialPageViewer.S_Page_Title;
                    }
                    else if (PageType == "Group")
                    {
                        this.Title = SocialGroup.S_Group_Title;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?group_id=" + SocialGroup.G_GroupID;
                        HeaderLabel.Text = SocialGroup.S_Group_Title;
                    }
                    else if (PageType == "Hashtag")
                    {
                        this.Title = Link;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?hashtag=" + Link;
                        HeaderLabel.Text = "#" + Link;
                    }
                    else if (PageType == "Post")
                    {
                        this.Title = AppResources.Label_Post_Page;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?post_id=" + Link;
                        HeaderLabel.Text = AppResources.Label_Post_Page;
                    }
                    else if (PageType == "Article")
                    {
                        this.Title = Link;
                        HeaderLabel.Text = AppResources.Label_Articles;
                        PostWebLoader.Source = Link;

                        PostWebLoader.InjectJavascript("$('.content-container').css('margin-top', '0');");
                        PostWebLoader.InjectJavascript("$('.header-container').hide();");
                        PostWebLoader.InjectJavascript("$('.footer-wrapper').hide();");
                    }

                    PostWebLoader.OnJavascriptResponse += OnJavascriptResponse;
                    PostWebLoader.RegisterCallback("type", (str) => { });
                }
                else
                {
                    this.Title = AppResources.Label_Offline;
                    HeaderLabel.Text = AppResources.Label_Offline;
                    if (LoadingPost.IsVisible)
                    {
                        LoadingPost.IsVisible = false;
                    }
                    if (PostWebLoader.IsVisible)
                    {
                        PostWebLoader.IsVisible = false;
                    }
                    if (OfflinePage.IsVisible == false)
                    {
                        OfflinePage.IsVisible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CopyUrlButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                DependencyService.Get<IClipboardService>().CopyToClipboard(PostWebLoader.Source.ToString());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnJavascriptResponse(JavascriptResponseDelegate EventObj)
        {
            try
            {
                if (EventObj.Data.Contains("type"))
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(EventObj.Data);
                    string type = data["type"].ToString();
                    if (type == "user")
                    {
                        string Userid = data["profile_id"].ToString();
                        if (WowonderPhone.Settings.User_id == Userid)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new MyProfilePage());
                            });
                        }
                        else
                        {
                            InjectedJavaOpen_UserProfile(Userid);
                        }

                    }
                    else if (type == "lightbox")
                    {
                        string ImageSource = data["image_url"].ToString();
                        if (Settings.ShowAndroidDefaultImageViewer)
                        {
                            if (Device.RuntimePlatform == Device.iOS)
                            {
                                var Image = new UriImageSource
                                {
                                    Uri = new Uri(ImageSource),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(2, 0, 0, 0)
                                };
                                InjectedJavaOpen_OpenImage(Image);
                            }
                            else
                            {
                                UserDialogs.Instance.ShowLoading(AppResources.Label_Please_Wait);
                                var dataViewer = DependencyService.Get<IMethods>();
                                dataViewer.showPhoto(ImageSource);
                            }
                        }
                        else
                        {
                            var Image = new UriImageSource
                            {
                                Uri = new Uri(ImageSource),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };
                            InjectedJavaOpen_OpenImage(Image);
                        }
                    }
                    else if (type == "mention")
                    {
                        string user_id = data["user_id"].ToString();
                        InjectedJavaOpen_UserProfile(user_id);
                    }
                    else if (type == "hashtag")
                    {
                        string hashtag = data["tag"].ToString();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushModalAsync(new HyberdPostViewer("Hashtag", hashtag));
                        });
                    }
                    else if (type == "url")
                    {
                        string link = data["link"].ToString();

                        InjectedJavaOpen_PostLinks(link);
                    }
                    else if (type == "post_wonders" || type == "post_likes")
                    {
                        string Id = data["post_id"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Like_Wonder_Viewer_Page(Id, type));
                        });
                    }
                    else if (type == "edit_post")
                    {
                        string Id = data["post_id"].ToString();
                        string Edit_text = data["edit_text"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Edit_Post_For_Hyberd_Page(Edit_text, Id, this, null));

                        });
                    }
                    else if (type == "delete_post")
                    {
                        string Id = data["post_id"].ToString();

                        var Qussion = await DisplayAlert(AppResources.Label_Question,
                            AppResources.Label_Would_You_like_to_delete_this_post, AppResources.Label_Yes,
                            AppResources.Label_NO);
                        if (Qussion)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                PostWebLoader.InjectJavascript(
                                    "$('#post-' + " + Id + ").slideUp(200, function () { $(this).remove();}); ");
                            });

                            Post_Manager("delete_post", Id).ConfigureAwait(false);
                        }
                    }
                    //else if (type == "publisher-box")
                    //{
                    //    Device.BeginInvokeOnMainThread(() =>
                    //    {
                    //        Navigation.PushAsync(new AddPost_On_Communities(this));
                    //    });

                    //}
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void EditCommentDone(string ID, string Text)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    //var JavaCode = "$('#post-' + " + ID + ").find('#edit-post').attr('onclick', '{*type*:*edit_post*,*post_id*:*" + ID + "*,*edit_text*:*" + Text + "*}');";
                    var JavaCode = "$('#post-' + " + ID + ").find('#edit-post').attr('onclick', 'rr');";
                    var service = DependencyService.Get<IMethods>();
                    service.ClearWebViewCache();
                    var Decode = JavaCode.Replace("*", "&quot;");
                    PostWebLoader.InjectJavascript(Decode);

                    PostWebLoader.InjectJavascript("$('#post-' + " + ID + ").find('.post-description p').html('" + Text +
                                                   "');");
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void PostAjaxRefresh(string type)
        {
            try
            {
                if (type == "Hashtag")
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewHashTagPosts();");
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewPosts();");
                    });

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Post_Manager(string Type, string postid)
        {
            try
            {
                var Action = " ";
                if (Type == "edit_post")
                {
                    Action = "edit";
                }
                else
                {
                    Action = "delete";
                }
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", WowonderPhone.Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s", WowonderPhone.Settings.Session),
                        new KeyValuePair<string, string>("action", Action),
                    });

                    var response =
                        await
                            client.PostAsync(
                                WowonderPhone.Settings.Website + "/app_api.php?application=phone&type=post_manager",
                                formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (Type == "edit_post")
                        {
                            Action = "edit";
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_UserProfile(string Userid)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserProfilePage(Userid, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_OpenImage(ImageSource Image)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushModalAsync(new ImageFullScreenPage(Image));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_PostLinks(string link)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Device.OpenUri(new Uri(link));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Hashtag(string word)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", word));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnContentLoaded(ContentLoadedDelegate eventobj)
        {
            try
            {
                if (LoadingPost.IsVisible)
                {
                    LoadingPost.IsVisible = false;
                    LoadingPost.Source = null;
                }
                if (PostWebLoader.IsVisible == false)
                {
                    PostWebLoader.IsVisible = true;
                }

                if (Page_Type == "Article")
                {
                    PostWebLoader.InjectJavascript("$('.content-container').css('margin-top', '0');");
                    PostWebLoader.InjectJavascript("$('.header-container').hide();");
                    PostWebLoader.InjectJavascript("$('.footer-wrapper').hide();");
                }

                PostWebLoader.OnJavascriptResponse += OnJavascriptResponse;
                PostWebLoader.RegisterCallback("type", (str) => { });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    LoadingPost.IsVisible = true;
                    PostWebLoader.IsVisible = false;
                    OfflinePage.IsVisible = false;
                    LoadingPost.Source = Settings.HTML_LoadingPost_Page;

                    if (Page_Type == "User")
                    {
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?user_id=" + UserProfilePage.U_UserID;
                        Title = UserProfilePage.S_Name + " " + AppResources.Label_Timeline;
                    }
                    else if (Page_Type == "MyProfile")
                    {
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?user_id=" + Settings.User_id;
                        this.Title = AppResources.Label_My_Profile;
                    }
                    else if (Page_Type == "Saved Post")
                    {
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?saved_post=true" + Settings.User_id;
                        this.Title = AppResources.Label_Saved_Post;
                    }
                    else if (Page_Type == "Page")
                    {
                        this.Title = SocialPageViewer.S_Page_Title;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?page_id=" + SocialPageViewer.P_PageID;
                    }
                    else if (Page_Type == "Group")
                    {
                        this.Title = SocialGroup.S_Group_Title;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?group_id=" + SocialGroup.G_GroupID;
                    }
                    else if (Page_Type == "Hashtag")
                    {
                        this.Title = Page_Link;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?hashtag=" + Page_Link;
                    }
                    else if (Page_Type == "Post")
                    {
                        this.Title = AppResources.Label_Post_Page;
                        PostWebLoader.Source = Settings.Website + "/get_news_feed?post_id=" + Page_Link;
                    }
                    else if (Page_Type == "Article")
                    {
                        this.Title = Page_Link;
                        PostWebLoader.Source = Page_Link;

                        PostWebLoader.InjectJavascript("$('.content-container').css('margin-top', '0');");
                        PostWebLoader.InjectJavascript("$('.header-container').hide();");
                        PostWebLoader.InjectJavascript("$('.footer-wrapper').hide();");

                    }
                }
                else
                {
                    UserDialogs.Instance.Toast(AppResources.Label_You_are_still_Offline);
                    if (LoadingPost.IsVisible)
                    {
                        LoadingPost.IsVisible = false;
                    }
                    if (PostWebLoader.IsVisible)
                    {
                        PostWebLoader.IsVisible = false;
                    }
                    if (OfflinePage.IsVisible == false)
                    {
                        OfflinePage.IsVisible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
           
        }

        private void PostWebLoader_OnOnNavigationError(NavigationErrorDelegate eventobj)
        {
            try
            {
                PostWebLoader.IsVisible = false;
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost);
                Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.PopModalAsync(true);
            }
        }
    }
}
