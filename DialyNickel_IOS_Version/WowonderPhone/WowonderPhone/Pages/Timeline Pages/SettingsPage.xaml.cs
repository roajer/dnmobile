﻿using System;
using System.Collections.ObjectModel;
using Acr.UserDialogs;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.SettingsNavPages;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages
{
    public partial class SettingsPage : ContentPage
    {
        #region classes

        public class GeneralItems
        {
            public int ID { get; set; }
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Nav { get; set; }
        }

        public class HelpItems
        {
            public int ID { get; set; }
            public string Label { get; set; }
            public ImageSource Icon { get; set; }
            public string Nav { get; set; }
        }

        public class LogoutItems
        {
            public int ID { get; set; }
            public string Label { get; set; }
            public ImageSource Icon { get; set; }
            public string Nav { get; set; }
        }

        public class EditItems
        {
            public int ID { get; set; }
            public string Label { get; set; }
            public ImageSource Icon { get; set; }
            public string Nav { get; set; }
        }


        #endregion

        #region list

        public static ObservableCollection<GeneralItems> GeneralListItems = new ObservableCollection<GeneralItems>();
        public static ObservableCollection<HelpItems> HelpListItems = new ObservableCollection<HelpItems>();
        public static ObservableCollection<LogoutItems> LogoutListItems = new ObservableCollection<LogoutItems>();
        public static ObservableCollection<EditItems> EditListItems = new ObservableCollection<EditItems>();

        #endregion

        public SettingsPage()
        {
            try
            {
                InitializeComponent();

                Username.Text = Settings.UserFullName;
                AvatarImage.Source = Settings.Avatarimage;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Onlinelabel.Text = AppResources.Label_Online;
                }
                else
                {
                    Onlinelabel.Text = AppResources.Label_Offline;
                }

                if (EditListItems.Count <= 0 && HelpListItems.Count <= 0 && LogoutListItems.Count <= 0 &&
                    GeneralListItems.Count <= 0)
                {
                    EditListItems.Add(new EditItems()
                    {
                        Label = AppResources.Label_Edit_Profile,
                    });
                    LogoutListItems.Add(new LogoutItems()
                    {
                        Label = AppResources.Label_Logout,
                    });
                    HelpListItems.Add(new HelpItems()
                    {
                        Label = AppResources.Label_Help,
                        Icon = "\uf059",
                    });
                    HelpListItems.Add(new HelpItems()
                    {
                        Label = AppResources.Label_Report_TO_us,
                        Icon = "\uf024",
                    });
                    GeneralListItems.Add(new GeneralItems()
                    {
                        Label = AppResources.Label_Notifications,
                        Icon = "\uf0f3",
                    });
                    GeneralListItems.Add(new GeneralItems()
                    {
                        Label = AppResources.Label_Account,
                        Icon = "\uf007",
                    });
                    GeneralListItems.Add(new GeneralItems()
                    {
                        Label = AppResources.Label_Privacy,
                        Icon = "\uf06e",
                    });
                    GeneralListItems.Add(new GeneralItems()
                    {
                        Label = AppResources.Label_Blocked_Users,
                        Icon = "\uf056",
                    });

                    GeneralListItems.Add(new GeneralItems()
                    {
                        Label = AppResources.Label_invite,
                        Icon = "\uf0e0",
                    });

                }
                GeneralList.ItemsSource = GeneralListItems;
                EditList.ItemsSource = EditListItems;
                LogoutList.ItemsSource = LogoutListItems;
                HelpList.ItemsSource = HelpListItems;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }



        private void GeneralList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
            GeneralList.SelectedItem = null;
        }

        private void HelpList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                HelpList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void LogoutList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                LogoutList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void GeneralList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as GeneralItems;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_Blocked_Users)
                    {
                        await Navigation.PushAsync(new BlockedUsersPage());
                    }
                    else if (Item.Label == AppResources.Label_Account)
                    {
                        Account_Page AccountPage = new Account_Page();
                        await Navigation.PushAsync(AccountPage);
                    }
                    else if (Item.Label == AppResources.Label_Privacy)
                    {
                        Privacy_Page PrivacyPage = new Privacy_Page();
                        await Navigation.PushAsync(PrivacyPage);
                    }
                    else if (Item.Label == AppResources.Label_Notifications)
                    {
                        Notification_Page NotificationPage = new Notification_Page();
                        await Navigation.PushAsync(NotificationPage);
                    }
                    else if (Item.Label == AppResources.Label_invite)
                    {
                        InviteFriendsPage InviteFriendsPage = new InviteFriendsPage();
                        await Navigation.PushAsync(InviteFriendsPage);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void EditList_OnItemSelectedList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                EditList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void EditList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is EditItems Item)
                {
                    if (Item.Label == AppResources.Label_Edit_Profile)
                    {
                        MyProfilePage GeneralPage = new MyProfilePage();
                        await Navigation.PushAsync(GeneralPage);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void LogoutList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Qusiion = await DisplayAlert(AppResources.Label2_Note,
                    AppResources.Label2_The_Application_needs_Restart, AppResources.Label_Yes, AppResources.Label_NO);
                if (Qusiion)
                {
                    await DisplayAlert(AppResources.Label2_Note, AppResources.Label2_Session_Dead,
                        AppResources.Label_OK);

                    UserDialogs.Instance.ShowLoading(AppResources.Label2_Clearing_Cach);
                    var Item = e.Item as LogoutItems;

                    if (Item.Label == AppResources.Label_Logout)
                    {

                        using (var Data = new LoginFunctions())
                        {
                            Data.ClearLoginCredentialsList();
                        }
                        using (var Data = new LoginUserProfileFunctions())
                        {
                            Data.ClearProfileCredentialsList();
                        }
                        using (var Data = new MessagesFunctions())
                        {
                            Data.ClearMessageList();
                        }
                        using (var Data = new ContactsFunctions())
                        {
                            Data.DeletAllChatUsersList();
                        }
                        using (var Data = new CommunitiesFunction())
                        {
                            Data.DeletAllCommunityList();
                        }
                        using (var Data = new NotifiFunctions())
                        {
                            Data.ClearNotifiDBCredentialsList();
                        }
                        using (var Data = new PrivacyFunctions())
                        {
                            Data.ClearPrivacyDBCredentialsList();
                        }
                        using (var Data = new ChatActivityFunctions())
                        {
                            Data.ClearChatUserTable();
                            Data.DeletAllChatUsersList();

                            try
                            {
                                UserDialogs.Instance.HideLoading();
                                WowonderPhone.Settings.ReLogin = true;
                                DependencyService.Get<IMethods>().Close_App();
                                await Navigation.PopAsync();
                                App.GetLoginPage();
                            }
                            catch
                            {
                                UserDialogs.Instance.HideLoading();
                                Navigation.RemovePage(this);
                                App.GetLoginPage();
                                DependencyService.Get<IMethods>().Close_App();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                UserDialogs.Instance.HideLoading();
                Navigation.RemovePage(this);
                App.GetLoginPage();
            }

        }

        private void HelpList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is HelpItems Item)
                {
                    if (Item.Label == AppResources.Label_Help)
                    {
                        Navigation.PushAsync(new HelpPage());
                    }
                    else if (Item.Label == AppResources.Label_Report_TO_us)
                    {
                        Device.OpenUri(new Uri(Settings.Website + "/contact-us"));
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
