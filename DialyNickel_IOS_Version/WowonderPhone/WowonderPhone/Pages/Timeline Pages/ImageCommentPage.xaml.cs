﻿using System;
using System.Linq;
using System.Threading.Tasks;

using FFImageLoading.Forms;
using Xamarin.Forms;
using WowonderPhone.Classes;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;

namespace WowonderPhone.Pages.Timeline_Pages
{
    public partial class ImageCommentPage : ContentPage
    {
        //LikeAndWowonderIcon
        public string ID = "";

        public ImageCommentPage(Albums albums)
        {
            try
            {

                InitializeComponent();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    HeaderOfpage.IsVisible = false;
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }


                ID = albums.Postid;
                var list = AlbumPage.Albumlist.FirstOrDefault(a => a.Postid == albums.Postid);
                if (list != null)
                {
                    Title = list.Publishername;
                    HeaderLabel.Text = list.Publishername;
                    img.Source = list.Image;
                    CommentList.ItemsSource = list.ImageComments;
                    WonderSpan.Text = " " + list.WonderCount;
                    LikeSpan.Text = " " + list.LikesCount;
                    Imagetext.Text = list.Imagetext;
                }
                else
                {
                    Title = albums.Publishername;
                    HeaderLabel.Text = albums.Publishername;
                    img.Source = albums.Image;
                    CommentList.ItemsSource = albums.ImageComments;
                    WonderSpan.Text = " " + albums.WonderCount;
                    LikeSpan.Text = " " + albums.LikesCount;
                    Imagetext.Text = albums.Imagetext;
                }

                ProfileImage.Source = Settings.Avatarimage;
                Profilename.Text = Settings.UserFullName;

                AddClickEvents();

                if (list != null)
                {
                    if (list.CommentsCount == "0")
                    {
                        CommentList.IsVisible = false;
                        CommentsCountLabel.Text = list.CommentsCount + " " + AppResources.Label_Comments;
                        ButtonStack.IsVisible = false;
                    }
                    else
                    {
                        CommentsCountLabel.Text = list.CommentsCount + " " + AppResources.Label_Comments;
                        if (list.CommentsCount.Length > 4)
                        {
                            ButtonStack.IsVisible = true;
                        }
                    }
                }
                else
                {
                    if (albums.CommentsCount == "0")
                    {
                        CommentList.IsVisible = false;
                        CommentsCountLabel.Text = albums.CommentsCount + " " + AppResources.Label_Comments;
                        ButtonStack.IsVisible = false;
                    }
                    else
                    {
                        CommentsCountLabel.Text = albums.CommentsCount + " " + AppResources.Label_Comments;
                        if (albums.CommentsCount.Length > 4)
                        {
                            ButtonStack.IsVisible = true;
                        }
                    }
                }

                AnimateIn().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddClickEvents()
        {
            try
            {
                var LikeIconGestureRecognizer = new TapGestureRecognizer();
                LikeIconGestureRecognizer.Tapped += (s, ee) =>
                {
                    Navigation.PushAsync(new Like_Wonder_Viewer_Page(ID, "post_likes"));
                };

                if (LikeIcon.GestureRecognizers.Count > 0)
                {
                    LikeIcon.GestureRecognizers.Clear();
                }

                LikeIcon.GestureRecognizers.Add(LikeIconGestureRecognizer);

                var WowonderIconGestureRecognizer = new TapGestureRecognizer();
                WowonderIconGestureRecognizer.Tapped += (s, ee) =>
                {
                    Navigation.PushAsync(new Like_Wonder_Viewer_Page(ID, "post_wonders"));
                };

                if (WowonderIcon.GestureRecognizers.Count > 0)
                {
                    WowonderIcon.GestureRecognizers.Clear();
                }

                WowonderIcon.GestureRecognizers.Add(WowonderIconGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            outerScrollView.Scrolled += OnScroll;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            outerScrollView.Scrolled -= OnScroll;
        }

        public void OnScroll(object sender, ScrolledEventArgs e)
        {
            try
            {
                var imageHeight = img.Height * 2;
                var scrollRegion = layeringGrid.Height - outerScrollView.Height;
                var parallexRegion = imageHeight - outerScrollView.Height;
                var factor = outerScrollView.ScrollY - parallexRegion * (outerScrollView.ScrollY / scrollRegion);
                img.TranslationY = factor;
                img.Opacity = 1 - (factor / imageHeight);
                headers.Scale = 1 - ((factor) / (imageHeight * 2));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void OnPrimaryActionButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new HyberdPostViewer("Post", ID));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnImageTapped(object sender, EventArgs e)
        {
            try
            {
                var imagePreview = new ImageFullScreenPage((sender as CachedImage).Source);
                await Navigation.PushModalAsync(new NavigationPage(imagePreview));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommentList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                CommentList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }    
        }

        private void CommentList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Albums.Comment Item)
                {
                    Navigation.PushAsync(new UserProfilePage(Item.UserID, "FriendList"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task AnimateIn()
        {
            try
            {
                await Task.WhenAll(new[]
                {
                    AnimateItem(WowonderIcon, 600),
                    AnimateItem(TimeofimageLabel, 700),
                    AnimateItem(LikeIcon, 800),
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async Task AnimateItem(View uiElement, uint duration)
        {
            try
            {
                if (uiElement == null)
                {
                    return;
                }

                await Task.WhenAll(new Task[]
                {
                    uiElement.ScaleTo(1.5, duration, Easing.CubicIn),
                    uiElement.FadeTo(1, duration / 2, Easing.CubicInOut)
                        .ContinueWith(
                            _ =>
                            {
                                // Queing on UI to workaround an issue with Forms 2.1
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    uiElement.ScaleTo(1, duration, Easing.CubicOut);
                                });
                            })
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}