﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.Update_Comunities;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class SocialPageViewer : ContentPage
    {
        #region classes

        public class PageInfoitems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Color { get; set; }
        }

        #endregion

        #region Variables and list

        public static string P_PageID = "";
        public static string S_Page_Description = "";
        public static string S_About = "";
        public static string S_Website = "";
        public static string S_Name = "";
        public static string S_Username = "";
        public static string S_Phone = "";
        public static string S_Address = "";
        public static string S_Facebook = "";
        public static string S_Google = "";
        public static string S_Twitter = "";
        public static string S_Linkedin = "";
        public static string S_Youtube = "";
        public static string S_VK = "";
        public static string S_Instagram = "";
        public static string S_Category = "";
        public static string S_Company = "";
        public static string S_URL = "";
        public static string S_Cover = "";
        public static string S_Avatar = "";
        public static string Call_Action_Type_URL = "";
        public static string S_Page_Title = "";
        public static string PageType = "";
        public static string is_page_onwer = "";

        public static ObservableCollection<PageInfoitems> PageInfoListItems = new ObservableCollection<PageInfoitems>();
        public static ObservableCollection<Profile.Liked_Pages> Liked_PagesDataList = new ObservableCollection<Profile.Liked_Pages>();

        #endregion

        public SocialPageViewer(string Pageid, string pagename, string Pagetype)
        {
            try
            {
                P_PageID = Pageid;

                InitializeComponent();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    HeaderOfpage.IsVisible = false;
                    ToolbarItems.Clear();
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }
                PageTitle.Text = pagename;
                Call_Action_Type_URL = "";
                CoverImage.BackgroundColor = Color.FromHex(Settings.MainColor);
                FirstGridOfprofile.BackgroundColor = Color.FromHex(Settings.MainColor);
                PageInfoListItems.Clear();
                ToolbarItems.Remove(AddPost);

                if (Pagetype == "Liked")
                {
                    PageType = "Liked";
                    LikeText.Text = AppResources.Label_Liked;
                    LikeImage.Source = Settings.Likecheked_Icon;
                }

                AddClickActions();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    GetPageData(Pageid).ConfigureAwait(false);
                    PostWebLoader.Source = Settings.Website + "/get_news_feed?page_id=" + Pageid;
                    PostWebLoader.OnJavascriptResponse += OnJavascriptResponse;
                    PostWebLoader.RegisterCallback("type", (str) => { });
                }
                else
                {
                    this.Title = AppResources.Label_Offline_Mode;
                    // PostWebLoader.Source = Settings.HTML_OfflinePost_Page;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddClickActions()
        {
            try
            {
                var TapFriendGestureRecognizer = new TapGestureRecognizer();
                TapFriendGestureRecognizer.Tapped += (s, ee) =>
                {
                    if (LikeText.Text == AppResources.Label_Like)
                    {
                        var device = Resolver.Resolve<IDevice>();
                        var oNetwork = device.Network; // Create Interface to Network-functions
                        var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                        if (!xx)
                        {
                            LikeUnlikePageRequest(P_PageID).ConfigureAwait(false);
                            LikeText.Text = AppResources.Label_Liked;
                            LikeImage.Source = Settings.Likecheked_Icon;
                            using (var data = new CommunitiesFunction())
                            {

                                if (P_PageID != "" && S_URL != "")
                                {
                                    var Community = data.GetCommunityByID(P_PageID);
                                    if (Community != null)
                                    {
                                        return;
                                    }
                                    DependencyService.Get<IPicture>().SavePictureToDisk(S_Avatar, P_PageID);
                                    data.InsertCommunities(new CommunitiesDB()
                                    {
                                        CommunityID = P_PageID,
                                        CommunityName = S_Page_Title,
                                        CommunityPicture = S_Avatar,
                                        CommunityTypeLabel = AppResources.Label_Pages,
                                        CommunityType = "Pages",
                                        CommunityUrl = S_URL
                                    });
                                }
                            }
                        }
                        else
                        {
                            DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection,AppResources.Label_OK);
                        }
                    }
                    else if (LikeText.Text == AppResources.Label_Liked)
                    {
                        try
                        {
                            var device = Resolver.Resolve<IDevice>();
                            var oNetwork = device.Network;
                            var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                            if (!xx)
                            {
                                LikeUnlikePageRequest(P_PageID).ConfigureAwait(false);
                                LikeText.Text = AppResources.Label_Like;
                                LikeImage.Source = Settings.LikePage_Icon;
                                using (var data = new CommunitiesFunction())
                                {
                                    var Community = data.GetCommunityByID(P_PageID);
                                    if (Community.CommunityID != "")
                                    {
                                        if (Community.CommunityID != null)
                                        {
                                            DependencyService.Get<IPicture>()
                                                .DeletePictureFromDisk(Community.CommunityPicture,
                                                    Community.CommunityID);
                                            data.DeleteCommunitiesRow(Community);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection,
                                    AppResources.Label_OK);
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                };

                if (LikeText.GestureRecognizers.Count > 0)
                {
                    LikeText.GestureRecognizers.Clear();
                }
                if (LikeImage.GestureRecognizers.Count > 0)
                {
                    LikeImage.GestureRecognizers.Clear();
                }

                LikeText.GestureRecognizers.Add(TapFriendGestureRecognizer);
                LikeImage.GestureRecognizers.Add(TapFriendGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public static async Task<string> LikeUnlikePageRequest(string Pageid)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("page_id", Pageid),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client
                        .PostAsync(Settings.Website + "/app_api.php?application=phone&type=like_page", formContent)
                        .ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";

                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        public async Task<string> GetPageData(string Pageid)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("page_profile_id", Pageid),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_page_data",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        JObject userdata = JObject.FromObject(data["page_data"]);

                        var P_page_id = userdata["page_id"].ToString();
                        var P_user_id = userdata["user_id"].ToString();
                        var P_page_name = userdata["page_name"].ToString();
                        var P_page_title = userdata["page_title"].ToString();
                        var P_page_description = userdata["page_description"].ToString();
                        var P_avatar = userdata["avatar"].ToString();
                        var P_cover = userdata["cover"].ToString();
                        var P_page_category = userdata["page_category"].ToString();
                        var P_website = userdata["website"].ToString();
                        var P_facebook = userdata["facebook"].ToString();
                        var P_google = userdata["google"].ToString();
                        var P_vk = userdata["vk"].ToString();
                        var P_twitter = userdata["twitter"].ToString();
                        var P_linkedin = userdata["linkedin"].ToString();
                        var P_company = userdata["company"].ToString();
                        var P_phone = userdata["phone"].ToString();
                        //var P_address = userdata["address"].ToString();
                        var P_call_action_type = userdata["call_action_type"].ToString();
                        var P_call_action_type_url = userdata["call_action_type_url"].ToString();
                        var P_instgram = userdata["instgram"].ToString();
                        var P_youtube = userdata["youtube"].ToString();
                        var P_verified = userdata["verified"].ToString();
                        var P_active = userdata["active"].ToString();
                        var P_registered = userdata["registered"].ToString();
                        var P_boosted = userdata["boosted"].ToString();
                        var P_about = userdata["about"].ToString();
                        var P_url = userdata["url"].ToString();
                        var P_name = userdata["name"].ToString();
                        var P_rating = userdata["rating"].ToString();
                        var P_category = userdata["category"].ToString();
                        var P_is_page_onwer = userdata["is_page_onwer"].ToString();
                        var P_username = userdata["username"].ToString();
                        var P_post_count = userdata["post_count"].ToString();
                        var P_is_liked = userdata["is_liked"].ToString();
                        var P_call_action_type_text = userdata["call_action_type_text"].ToString();

                        if (P_is_page_onwer == "false" || P_is_page_onwer == "False")
                        {
                            ToolbarItems.Remove(AddPost);
                            AddPostLabel.IsVisible = false;
                        }
                        else
                        {
                            ToolbarItems.Add(AddPost);
                            AddPostLabel.IsVisible = true;
                        }

                        if (P_post_count == "0")
                        {
                            ButtonStacklayot.IsVisible = false;
                            PostWebLoader.HeightRequest = 220;
                        }
                        else if (P_post_count == "1")
                        {
                            PostWebLoader.HeightRequest = 280;
                        }
                        else if (P_post_count == "2")
                        {
                            PostWebLoader.HeightRequest = 590;
                        }

                        else if (P_post_count == "3")
                        {
                            PostWebLoader.HeightRequest = 760;
                        }
                        else if (P_post_count == "4")
                        {
                            PostWebLoader.HeightRequest = 970;
                        }
                        else if (P_post_count == "5")
                        {
                            PostWebLoader.HeightRequest = 1170;
                        }
                        else if (P_post_count == "6")
                        {
                            PostWebLoader.HeightRequest = 1380;
                        }
                        else if (P_post_count == "7")
                        {
                            PostWebLoader.HeightRequest = 1580;
                        }
                        else if (P_post_count == "8")
                        {
                            PostWebLoader.HeightRequest = 1890;
                        }

                        if (P_is_liked == "True")
                        {
                            if (LikeText.Text != AppResources.Label_Liked)
                            {
                                LikeText.Text = AppResources.Label_Liked;
                                LikeImage.Source = Settings.Likecheked_Icon;
                            }
                        }
                        else
                        {
                            if (LikeText.Text != AppResources.Label_Like)
                            {
                                LikeText.Text = AppResources.Label_Like;
                                LikeImage.Source = Settings.LikePage_Icon;
                            }
                        }

                        if (P_call_action_type_url == "")
                        {
                            ActionButton.IsVisible = false;
                        }
                        else
                        {
                            ActionButton.IsVisible = true;
                            Call_Action_Type_URL = P_call_action_type_url;
                            ActionButton.Text = P_call_action_type_text;
                        }

                        S_About = Functions.StringNullRemover(Functions.DecodeString(P_about));
                        S_Website = P_website;
                        S_Name = P_name;
                        S_Username = P_username;
                        S_Phone = P_phone;
                        //S_Address = P_address;
                        S_Facebook = P_facebook;
                        S_Google = P_google;
                        S_VK = P_vk;
                        S_Twitter = P_twitter;
                        S_Linkedin = P_linkedin;
                        S_Instagram = P_instgram;
                        S_Youtube = P_youtube;
                        S_Category = P_category;
                        S_Page_Description = Functions.StringNullRemover(Functions.DecodeString(P_page_description));
                        S_Company = P_company;
                        S_URL = P_url;
                        S_Page_Title = P_page_name;
                        S_Avatar = P_avatar;
                        S_Cover = P_cover;
                        is_page_onwer = P_is_page_onwer;

                        PageTitle.Text = S_Name;
                        AboutMini.Text = S_Category;

                        CoverImage.Source = new UriImageSource {Uri = new Uri(S_Cover)};
                        AvatarImage.Source = new UriImageSource {Uri = new Uri(S_Avatar)};

                        Liked_PagesDataList.Clear();

                        Profile.Liked_Pages Data_page = new Profile.Liked_Pages();

                        Data_page.P_page_id = P_page_id;
                        Data_page.P_user_id = P_user_id;
                        Data_page.P_page_name = P_page_name;
                        Data_page.P_page_title = P_page_title;
                        Data_page.P_page_description =Functions.StringNullRemover(Functions.DecodeString(P_page_description));
                        Data_page.P_avatar = P_avatar;
                        Data_page.P_cover = P_cover;
                        Data_page.P_page_category = P_page_category;
                        Data_page.P_website = P_website;
                        Data_page.P_facebook = P_facebook;
                        Data_page.P_google = P_google;
                        Data_page.P_vk = P_vk;
                        Data_page.P_twitter = P_twitter;
                        Data_page.P_linkedin = P_linkedin;
                        Data_page.P_company = P_company;
                        Data_page.P_phone = P_phone;
                        //Data_page.P_address = P_address;
                        Data_page.P_call_action_type = P_call_action_type;
                        Data_page.P_call_action_type_url = P_call_action_type_url;
                        Data_page.P_instgram = P_instgram;
                        Data_page.P_youtube = P_youtube;
                        Data_page.P_verified = P_verified;
                        Data_page.P_active = P_active;
                        Data_page.P_registered = P_registered;
                        Data_page.P_boosted = P_boosted;
                        Data_page.P_about = Functions.StringNullRemover(Functions.DecodeString(P_about));
                        Data_page.P_url = P_url;
                        Data_page.P_name = P_name;
                        Data_page.P_rating = P_rating;
                        Data_page.P_category = P_category;
                        Data_page.P_is_page_onwer = P_is_page_onwer;
                        Data_page.P_username = P_username;
                        Data_page.P_post_count = P_post_count;
                        Data_page.P_is_liked = P_is_liked;
                        Data_page.P_call_action_type_text = P_call_action_type_text;

                        Liked_PagesDataList.Add(Data_page);

                        if (is_page_onwer == "True" || is_page_onwer == "true")
                        {
                            LabelEditData.IsVisible = true;
                        }
                        else
                        {
                            LabelEditData.IsVisible = false;
                        }

                        if (S_Website == "")
                        {
                            S_Website = "............";
                        }
                        if (S_Page_Description == "")
                        {
                            S_Page_Description = "........";
                        }
                        if (S_Phone == "" || S_Phone.Contains("00"))
                        {
                            S_Phone = AppResources.Label_Askme;
                        }
                        if (S_About == "")
                        {
                            S_About = Settings.PR_AboutDefault;
                        }
                        if (S_Address == "")
                        {
                            S_Address = AppResources.Label_Unavailable;
                        }
                        if (S_Company == "")
                        {
                            S_Company = AppResources.Label_Unavailable;
                        }

                        PageInfoListItems.Add(new PageInfoitems()
                        {
                            Label = S_Page_Description,
                            Icon = "\uf040",
                            Color = "#c5c9c8"
                        });
                        PageInfoListItems.Add(new PageInfoitems()
                        {
                            Label = S_Category,
                            Icon = "\uf0ac",
                            Color = "#c5c9c8"
                        });

                        PageInfoListItems.Add(new PageInfoitems()
                        {
                            Label = S_Website,
                            Icon = "\uf0a1",
                            Color = "#c5c9c8"
                        });

                        PageInfoListItems.Add(new PageInfoitems()
                        {
                            Label = AppResources.Label_Show_More,
                            Icon = "\uf0ca",
                            Color = Settings.MainColor,

                        });

                        if (PageType == AppResources.Label_Liked && (P_is_liked == "False" || P_is_liked == "false"))
                        {
                            using (var database = new CommunitiesFunction())
                            {
                                if (P_PageID != "")
                                {
                                    var Community = database.GetCommunityByID(P_PageID);
                                    if (Community != null)
                                    {
                                        database.DeleteCommunitiesRow(Community);
                                    }
                                }
                            }
                        }
                        PageInfoList.ItemsSource = PageInfoListItems;
                        PageInfoList.HeightRequest = Functions.ListInfoResizer(S_Page_Description);
                        this.Title = S_Name;
                        HeaderLabel.Text = S_Name;

                    }
                    else if (apiStatus == "400")
                    {
                        JObject errors = JObject.FromObject(data["errors"]);
                        var errortext = errors["error_text"].ToString();

                        await DisplayAlert(AppResources.Label_Security, AppResources.Label_This_Page_Dont_Exists,AppResources.Label_OK);
                        await Navigation.PopAsync(false);

                        json = AppResources.Label_Error;
                    }
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }

        private void CopyUrlButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Button1.Text = "\uf00c";
                DependencyService.Get<IClipboardService>().CopyToClipboard(S_URL);
                DependencyService.Get<IMessage>().LongAlert(AppResources.Label3_Copyed_URL);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PageInfoList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                PageInfoList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PageInfoList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {

            try
            {
                if (e.Item is PageInfoitems item)
                {
                    if (item.Label == AppResources.Label_Show_More)
                    {
                        Navigation.PushAsync(new InfoViewer("Page"));
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void PostAjaxRefresh(string type)
        {
            try
            {
                if (type == "Hashtag")
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewHashTagPosts();");
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewPosts();");
                    });

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActionButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (Call_Action_Type_URL.Contains("http") || Call_Action_Type_URL.Contains("www") ||
                    Call_Action_Type_URL.Contains(".com") || Call_Action_Type_URL.Contains(".net") ||
                    Call_Action_Type_URL.Contains("/"))
                {
                    Device.OpenUri(new Uri(Call_Action_Type_URL));
                }
                else
                {
                    DisplayAlert(AppResources.Label_Error, AppResources.Label_There_is_no_URL_link,AppResources.Label_OK);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ShowmoreButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new HyberdPostViewer("Page", ""));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnJavascriptResponse(JavascriptResponseDelegate EventObj)
        {
            try
            {
                if (EventObj.Data.Contains("type"))
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(EventObj.Data);
                    string type = data["type"].ToString();
                    if (type == "user")
                    {
                        string Userid = data["profile_id"].ToString();
                        if (WowonderPhone.Settings.User_id == Userid)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new MyProfilePage());
                            });
                        }

                        else
                        {
                            InjectedJavaOpen_UserProfile(Userid);
                        }

                    }
                    else if (type == "lightbox")
                    {
                        string ImageSource = data["image_url"].ToString();
                        if (Settings.ShowAndroidDefaultImageViewer)
                        {
                            if (Device.RuntimePlatform == Device.iOS)
                            {
                                var Image = new UriImageSource
                                {
                                    Uri = new Uri(ImageSource),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(2, 0, 0, 0)
                                };
                                InjectedJavaOpen_OpenImage(Image);
                            }
                            else
                            {
                                UserDialogs.Instance.ShowLoading(AppResources.Label_Please_Wait);
                                var dataViewer = DependencyService.Get<IMethods>();
                                dataViewer.showPhoto(ImageSource);
                            }
                        }
                        else
                        {
                            var Image = new UriImageSource
                            {
                                Uri = new Uri(ImageSource),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };
                            InjectedJavaOpen_OpenImage(Image);
                        }
                    }
                    else if (type == "mention")
                    {
                        string user_id = data["user_id"].ToString();
                        InjectedJavaOpen_UserProfile(user_id);
                    }
                    else if (type == "hashtag")
                    {
                        string hashtag = data["tag"].ToString();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushModalAsync(new HyberdPostViewer("Hashtag", hashtag));
                        });
                    }
                    else if (type == "url")
                    {
                        string link = data["link"].ToString();

                        InjectedJavaOpen_PostLinks(link);
                    }
                    else if (type == "page")
                    {
                        string Id = data["profile_id"].ToString();
                        if (P_PageID == Id)
                        {
                            return;
                        }
                        else
                        {
                            InjectedJavaOpen_LikePage(Id);
                        }

                    }
                    else if (type == "group")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_Group(Id);
                    }
                    else if (type == "post_wonders" || type == "post_likes")
                    {
                        string Id = data["post_id"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Like_Wonder_Viewer_Page(Id, type));
                        });
                    }
                    else if (type == "delete_post")
                    {
                        string Id = data["post_id"].ToString();

                        var Qussion = await DisplayAlert(AppResources.Label_Question,AppResources.Label_Would_You_like_to_delete_this_post, AppResources.Label_Yes,AppResources.Label_NO);
                        if (Qussion)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                PostWebLoader.InjectJavascript(
                                    "$('#post-' + " + Id + ").slideUp(200, function () { $(this).remove();}); ");
                            });

                            Post_Manager("delete_post", Id).ConfigureAwait(false);
                        }
                    }
                    else if (type == "publisher-box")
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new AddPost_On_Communities(null, this, "Page", P_PageID));
                        });

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Post_Manager(string Type, string postid)
        {
            try
            {
                var Action = " ";
                if (Type == "edit_post")
                {
                    Action = "edit";
                }
                else
                {
                    Action = "delete";
                }
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("action", Action),
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=post_manager",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (Type == "edit_post")
                        {
                            Action = "edit";
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_UserProfile(string Userid)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserProfilePage(Userid, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_OpenImage(ImageSource Image)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushModalAsync(new ImageFullScreenPage(Image));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_PostLinks(string link)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Device.OpenUri(new Uri(link));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Hashtag(string word)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", word));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_LikePage(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialPageViewer(id, "", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Group(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialGroup("id", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnContentLoaded(ContentLoadedDelegate eventobj)
        {
            try
            {
                //OfflinePost.IsVisible = false;
                //OfflinePost.Source = null;
                //PostWebLoader.IsVisible = true;
                PostWebLoader.RegisterCallback("type", (str) => { });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnNavigationError(NavigationErrorDelegate eventobj)
        {
            try
            {
                PostWebLoader.IsVisible = false;
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private NavigationRequestedDelegate PostWebLoader_OnOnNavigationStarted(NavigationRequestedDelegate eventobj)
        {
            try
            {
                if (eventobj.Uri.Contains(WowonderPhone.Settings.Website))
                {
                    return eventobj;
                }
                else
                {
                    eventobj.Cancel = true;
                    return eventobj;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return eventobj;
            }
        }

        private void AddPost_OnClicked_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new AddPost_On_Communities(null, this, "Page", P_PageID));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void OnScroll(object sender, ScrolledEventArgs e)
        {
            try
            {
                var Header = CoverImage.Height * 2;
                //
                var scrollRegion = layeringGrid.Height - outerScrollView.Height;
                var parallexRegion = Header - FirstGridOfprofile.Height;
                var factor = outerScrollView.ScrollY - parallexRegion * (outerScrollView.ScrollY / scrollRegion);
                CoverImage.TranslationY = factor;

                CoverImage.Opacity = 1 - (factor / (FirstGridOfprofile.Height - 40) * 2);
                CoverImage.BackgroundColor = Color.FromHex(Settings.MainColor);
                headers.Scale = 1 - ((factor) / (Header * 2));

                if (CoverImage.Opacity == 0)
                {
                    HeaderOfpage.BackgroundColor = Color.FromHex(Settings.MainColor);
                    HeaderLabel.IsVisible = true;
                }
                else
                {
                    if (HeaderLabel.IsVisible)
                    {
                        HeaderLabel.IsVisible = false;
                        HeaderOfpage.BackgroundColor = Color.Transparent;
                    }

                }
                //PostWebLoader.Scale = 1 - ((factor) / (Header * 2));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            outerScrollView.Scrolled += OnScroll;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            outerScrollView.Scrolled -= OnScroll;
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);

                Navigation.PopModalAsync(true);
            }
        }

        private async void EditData_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (is_page_onwer == "True" || is_page_onwer == "true")
                {
                    await Navigation.PushAsync(new Update_Pages_Page(Liked_PagesDataList));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SocialPageViewer_OnAppearing(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Update_Pages_Page.Coverimage))
            {
                CoverImage.Source = Update_Pages_Page.Coverimage;
            }
            if (!String.IsNullOrEmpty(Update_Groups_Pages.Avatarimage))
            {
                AvatarImage.Source = Update_Groups_Pages.Avatarimage;
            }

            var data = GroupsListPage.GroupsListItems.FirstOrDefault(a => a.CommunityID == P_PageID);
            if (data != null)
            {
                PageTitle.Text = data.CommunityName;
                AboutMini.Text = data.CommunityTypeLabel;
            }
        }
    }
}