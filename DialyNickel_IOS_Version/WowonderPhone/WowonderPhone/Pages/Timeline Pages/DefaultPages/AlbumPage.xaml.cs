﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Refractored.XamForms.PullToRefresh;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.CustomCells;

using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class AlbumPage : ContentPage
    {
        #region Event Refresh

        public class RefreshMVVM : INotifyPropertyChanged
        {
            public ObservableCollection<string> Items { get; set; }
            AlbumPage pageChosen;

            public RefreshMVVM(AlbumPage page)
            {
                this.pageChosen = page;
                Items = new ObservableCollection<string>();
            }

            bool isBusy;

            public bool IsBusy
            {
                get { return isBusy; }
                set
                {
                    if (isBusy == value)
                        return;

                    isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }

            ICommand refreshCommand;

            public ICommand RefreshCommand
            {
                get
                {
                    return refreshCommand ?? (refreshCommand = new Command(async () => await ExecuteRefreshCommand()));
                }
            }

            async Task ExecuteRefreshCommand()
            {
                if (IsBusy)
                {
                    return;
                }

                IsBusy = true;

                //Run code
                pageChosen.GetMyAlbums().ConfigureAwait(false);

                IsBusy = false;
            }

            #region INotifyPropertyChanged implementation

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        #endregion

        #region list

        public static List<Albums> Albumlist = new List<Albums>();

        #endregion

        public AlbumPage()
        {
            try
            {
                
                InitializeComponent();

				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

				
                BindingContext = new RefreshMVVM(this);
                PullToRefreshLayoutView.SetBinding<RefreshMVVM>(PullToRefreshLayout.IsRefreshingProperty, vm => vm.IsBusy, BindingMode.OneWay);
                PullToRefreshLayoutView.SetBinding<RefreshMVVM>(PullToRefreshLayout.RefreshCommandProperty, vm => vm.RefreshCommand);

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Albumlist.Count > 0)
                    {
                        PopulateAlbumLists(Albumlist).ConfigureAwait(false);
                    }
                    else
                    {
                        // UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                        PullToRefreshLayoutView.IsRefreshing = true;
                        GetMyAlbums().ConfigureAwait(false);
                    }
                }
                else
                {
                    ShowDataGrid.IsVisible = false;
                    OfflinePage.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task GetMyAlbums()
        {
            try
            {
                Albumlist.Clear();
                var column = LeftColumn;
                column.Children.Clear();
                var columnr = RightColumn;
                columnr.Children.Clear();
                //UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                PullToRefreshLayoutView.IsRefreshing = true;


                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_albums", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var albums = JObject.Parse(json).SelectToken("albums").ToString();
                        JArray ImagedataArray = JArray.Parse(albums);

                        foreach (var Image in ImagedataArray)
                        {
                            var url = Image["url"].ToString();
                            var post_id = Image["post_id"].ToString();
                            var Orginaltext = Image["Orginaltext"].ToString();
                            var post_time = Image["post_time"].ToString();
                            var postFile = Image["postFile_full"].ToString();
                            var limit_comments = Image["post_comments"].ToString();
                            var post_likes = Image["post_likes"].ToString();
                            var post_wonders = Image["post_wonders"].ToString();

                            JObject PublisherImage = JObject.FromObject(Image["publisher"]);
                            var Publishername = PublisherImage["name"].ToString();
                            // var PublisherAvatar = PublisherImage["name"].ToString();

                            var DecodedOrginaltext = System.Net.WebUtility.HtmlDecode(Orginaltext);

                            JArray CommentsArray = JArray.Parse(Image["get_post_comments"].ToString());

                            var image = "";

                            var Commentlist = new List<Albums.Comment>();
                            if (CommentsArray != null)
                            {
                                if (CommentsArray.Count > 0)
                                {
                                    foreach (var Comment in CommentsArray)
                                    {
                                        var comment_likes = Comment["comment_likes"].ToString();
                                        var OrginalText =System.Net.WebUtility.HtmlDecode(Comment["Orginaltext"].ToString());
                                        var Commenterurl = Comment["url"].ToString();

                                        JObject CommentPublisher = JObject.FromObject(Comment["publisher"]);

                                        var avatar = CommentPublisher["avatar"].ToString();
                                        var cover = CommentPublisher["cover"].ToString();
                                        var NameUser = CommentPublisher["name"].ToString();
                                        var user_id = CommentPublisher["user_id"].ToString();
                                        var Username = CommentPublisher["username"].ToString();
                                        var DecodedCommentOrginaltext = System.Net.WebUtility.HtmlDecode(OrginalText);

                                        var Imagepath = DependencyService.Get<IPicture>()
                                            .GetPictureFromDisk(avatar, user_id);
                                        var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                        if (DependencyService.Get<IPicture>().GetPictureFromDisk(avatar, user_id) =="File Dont Exists")
                                        {
                                            ImageMediaFile = new UriImageSource
                                            {
                                                Uri = new Uri(avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(5, 0, 0, 0)
                                            };
                                            DependencyService.Get<IPicture>().SavePictureToDisk(avatar, user_id);
                                        }

                                        Commentlist.Add(new Albums.Comment()
                                        {
                                            CommentText = DecodedCommentOrginaltext,
                                            CommentLikes = comment_likes,
                                            CommenterProfileUrl = Commenterurl,
                                            NameCommenter = NameUser,
                                            UserID = user_id,
                                            Username = Username,
                                            AvatarUser = ImageMediaFile,
                                        });
                                    }
                                }
                            }
                            Albumlist.Add(new Albums()
                            {
                                Name = "My Album",
                                Image = new UriImageSource
                                {
                                    Uri = new Uri(postFile),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 1, 0, 0)
                                },
                                Imagetext = DecodedOrginaltext,
                                ThumbnailHeight = "100",
                                Postid = post_id,
                                WonderCount = post_wonders,
                                LikesCount = post_likes,
                                CommentsCount = limit_comments,
                                ImageComments = Commentlist,
                                Publishername = Publishername,
                                ImageTime = post_time
                            });
                        }
                        PopulateAlbumLists(Albumlist).ConfigureAwait(false);
                    }
                    else
                    {
                        // UserDialogs.Instance.HideLoading();
                        PullToRefreshLayoutView.IsRefreshing = false;
                        UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
                    }

                    // UserDialogs.Instance.HideLoading();
                    PullToRefreshLayoutView.IsRefreshing = false;

                    if (Albumlist.Count == 0)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowDataGrid.IsVisible = false;
                            EmptyAlbumPage.IsVisible = true;
                            if (OfflinePage.IsVisible)
                            {
                                OfflinePage.IsVisible = false;
                            }
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowDataGrid.IsVisible = true;
                            EmptyAlbumPage.IsVisible = false;
                            OfflinePage.IsVisible = false;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                // UserDialogs.Instance.HideLoading();
                PullToRefreshLayoutView.IsRefreshing = false;
                PopulateAlbumLists(Albumlist).ConfigureAwait(false);
            }
        }

        private async Task PopulateAlbumLists(List<Albums> AlbumsList)
        {
            try
            {
                var lastHeight = "128";
                var y = 0;
                var column = LeftColumn;
                var productTapGestureRecognizer = new TapGestureRecognizer();
                productTapGestureRecognizer.Tapped += OnProductTapped;

                for (var i = 0; i < AlbumsList.Count; i++)
                {
                    var item = new ImageGriditemTemplate();

                    if (i % 2 == 0)
                    {
                        column = LeftColumn;
                        y++;
                    }
                    else
                    {
                        column = RightColumn;
                    }

                    AlbumsList[i].ThumbnailHeight = lastHeight;
                    item.BindingContext = AlbumsList[i];
                    item.GestureRecognizers.Add(productTapGestureRecognizer);
                    column.Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnProductTapped(Object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Albums)((ImageGriditemTemplate)sender).BindingContext;
                if (selectedItem != null)
                {
                    var productView = new ImageCommentPage(selectedItem)
                    {
                        BindingContext = selectedItem
                    };

                    await Navigation.PushAsync(productView);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Refresh_OnClicked(object sender, EventArgs e)
        {
            //try
            //{
            //    var device = Resolver.Resolve<IDevice>();
            //    var oNetwork = device.Network; // Create Interface to Network-functions
            //    var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
            //    if (!xx)
            //    {
            //        Albumlist.Clear();
            //        var column = LeftColumn;
            //        column.Children.Clear();
            //        var columnr = RightColumn;
            //        columnr.Children.Clear();
            //        //UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
            //        PullToRefreshLayoutView.IsRefreshing = true;

            //        GetMyAlbums().ConfigureAwait(false);
            //    }
            //    else
            //    {
            //        PullToRefreshLayoutView.IsRefreshing = false;
            //        UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
            //    }
            //}
            //catch (Exception ex)
            //{
            //      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            //}
        }

        private void AlbumPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                var Count = LeftColumn.Children.Count + RightColumn.Children.Count;
                if (Albumlist.Count != Count)
                {
                    var column = LeftColumn;
                    column.Children.Clear();
                    var columnr = RightColumn;
                    columnr.Children.Clear();
                    PopulateAlbumLists(Albumlist).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }           
        }
       
        private void PostButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new AddPost(null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }     

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Albumlist.Count > 0)
                    {
                        PopulateAlbumLists(Albumlist).ConfigureAwait(false);
                    }
                    else
                    {
                        PullToRefreshLayoutView.IsRefreshing = true;
                        //UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                        GetMyAlbums().ConfigureAwait(false);
                    }
                }
                else
                {
                    PullToRefreshLayoutView.IsRefreshing = false;
                    UserDialogs.Instance.Toast(AppResources.Label_You_are_still_Offline);
                    OfflinePage.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}