﻿using System;
using PropertyChanged;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Near_by
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Near_By_Filter_Search : ContentPage
    {
        #region classes

        [ImplementPropertyChanged]
        public class SerachFilterItems
        {
            public string PrivacyLabel { get; set; }
            public string value { get; set; }
            public string Checkicon { get; set; }
            public string CheckiconColor { get; set; }

        }

        #endregion

        public Near_By_Filter_Search()
        {
            try
            {
                
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

				AddItemsToList();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddItemsToList()
        {
            try
            {
                SearchBy.Items.Add(AppResources.Label_All);
                SearchBy.Items.Add(AppResources.Label_Male);
                SearchBy.Items.Add(AppResources.Label_Female);

                Status.Items.Add(AppResources.Label_All);
                Status.Items.Add(AppResources.Label_Online);
                Status.Items.Add(AppResources.Label_Offline);

                using (var Filter = new NearMe_FilterFunction())
                {

                    var FilterResult = Filter.SelecteNearmeDBCredentials();
                    if (FilterResult == null)
                    {
                        Near_meDB Data = new Near_meDB();
                        Data.Status = 0;
                        Data.DistanceValue = 500;
                        Data.Gender = 0;
                        Data.UserID = Settings.User_id;
                        Filter.InsertNearmeDBCredentials(Data);

                        Status.SelectedIndex = 0;
                        SearchBy.SelectedIndex = 0;
                    }
                    else
                    {
                        Status.SelectedIndex = FilterResult.Status;
                        SearchBy.SelectedIndex = FilterResult.Gender;
                        DistanceSlider.Value = FilterResult.DistanceValue;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Status_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void DistanceSlider_OnValueChanged(object sender, ValueChangedEventArgs e)
        {
            try
            {
                DistanceLabel.Text = "Distance/" + DistanceSlider.Value.ToString() + "km";

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Near_By_Filter_Search_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                using (var Filter = new NearMe_FilterFunction())
                {

                    var FilterResult = Filter.SelecteNearmeDBCredentials();
                    if (FilterResult != null)
                    {
                        FilterResult.Status = Status.SelectedIndex;
                        FilterResult.Gender = SearchBy.SelectedIndex;
                        FilterResult.DistanceValue = Int32.Parse(DistanceSlider.Value.ToString()); 
                        Filter.UpdateNearmeDBCredentials(FilterResult);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
