﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator;
using Refractored.XamForms.PullToRefresh;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.CustomCells;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Near_by
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Main_Near_by_Page : ContentPage
    {
        #region Event Refresh

        public class RefreshModel : INotifyPropertyChanged
        {
            public ObservableCollection<string> Items { get; set; }
            Main_Near_by_Page pageChosen;

            public RefreshModel(Main_Near_by_Page page)
            {

                this.pageChosen = page;
                Items = new ObservableCollection<string>();
            }

            bool isBusy;

            public bool IsBusy
            {
                get { return isBusy; }
                set
                {
                    if (isBusy == value)
                        return;

                    isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }

            ICommand refreshCommand;

            public ICommand RefreshCommand
            {
                get
                {
                    return refreshCommand ?? (refreshCommand = new Command(async () => await ExecuteRefreshCommand()));
                }
            }

            async Task ExecuteRefreshCommand()
            {
                if (IsBusy)
                    return;

                IsBusy = true;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    var service = DependencyService.Get<IMethods>();
                    service.ClearWebViewCache();
                    pageChosen.PullToRefreshLayoutView.IsRefreshing = false;

                    pageChosen.MainNearby_Page.IsVisible = false;
                    pageChosen.Icon_page.Text = "\uf119";
                    pageChosen.Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    pageChosen.Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    pageChosen.NearbyButton.IsVisible = true;
                    pageChosen.NearbyButton.Text = AppResources.Label_Try_Again;
                    pageChosen.NearbyButton.Clicked += (s, a) => pageChosen.TryButton_OnClicked(s, a);
                }
                else
                {
                    await pageChosen.GetNearby("", "", "", "", "");
                    IsBusy = false;
                }
            }

            #region INotifyPropertyChanged implementation

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        #endregion

        #region Variables

        public static string Status = "";
        public static string Gender = "";
        public static string Distance = "";

        public static string Lat = "";
        public static string Lng = "";

        public static bool Loadmore = true;

        #endregion

        #region Lists Items Declaration

        public static ObservableCollection<Near_Me> Near_Melist = new ObservableCollection<Near_Me>();

        #endregion

        public Main_Near_by_Page()
        {
            try
            {
                
                InitializeComponent();

				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}


				BindingContext = new RefreshModel(this);
                PullToRefreshLayoutView.SetBinding<RefreshModel>(PullToRefreshLayout.IsRefreshingProperty, vm => vm.IsBusy, BindingMode.OneWay);
                PullToRefreshLayoutView.SetBinding<RefreshModel>(PullToRefreshLayout.RefreshCommandProperty, vm => vm.RefreshCommand);

                using (var Filter = new NearMe_FilterFunction())
                {
                    var FilterResult = Filter.SelecteNearmeDBCredentials();
                    if (FilterResult == null)
                    {
                        Near_meDB Data = new Near_meDB();
                        Data.UserID = Settings.User_id;
                        Data.Status = 0;
                        Data.DistanceValue = 500;
                        Data.Gender = 0;
                        Filter.InsertNearmeDBCredentials(Data);
                        Status = "";
                        Gender = "";
                        Distance = "500";
                    }
                    else
                    {
                        if (FilterResult.Status == 0)
                        {
                            Status = "";
                        }
                        else if (FilterResult.Status == 1)
                        {
                            Status = "1";
                        }
                        else if (FilterResult.Status == 2)
                        {
                            Status = "2";
                        }
                        //////////////
                        if (FilterResult.Gender == 0)
                        {
                            Gender = "";
                        }
                        else if (FilterResult.Gender == 1)
                        {
                            Gender = "male";
                        }
                        else if (FilterResult.Gender == 2)
                        {
                            Gender = "female";
                        }

                        Distance = FilterResult.DistanceValue.ToString();
                    }
                }

                GetPosition();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Near_Melist.Count <= 0)
                    {
                        GetNearby("", Gender, Status, Distance, "").ConfigureAwait(false);
                    }
                    else
                    {
                        PopulateNearMeLists(Near_Melist);
                    }
                }
                else
                {
                    MainNearby_Page.IsVisible = false;
                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    NearbyButton.IsVisible = true;
                    NearbyButton.Text = AppResources.Label_Try_Again;
                    NearbyButton.Clicked += (s, a) => TryButton_OnClicked(s, a);
                }

                // PopulateNearMeLists(Near_Melist).ConfigureAwait(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void GetPosition()
        {
            try
            {
                var IsGeolocationEnabled = await CrossGeolocator.Current.GetIsGeolocationEnabledAsync();
                if (IsGeolocationEnabled)
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 100;
                    var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20));
                    Lat = position.Latitude.ToString();
                    Lng = position.Longitude.ToString();
                }
                else
                {
                    UserDialogs.Instance.Toast(AppResources.Label2_Please_turn_your_GPS_For_better_results);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task GetNearby(string search_key, string gender, string status, string distance, string offset)
        {
            try
            {
                if (offset == "0" || offset == "")
                {
                    Near_Melist.Clear();
                    LeftColumn.Children.Clear();
                    RightColumn.Children.Clear();
                    PullToRefreshLayoutView.IsRefreshing = true;
                    MainNearby_Page.IsVisible = true;
                    EmptyPage.IsVisible = false;
                }
                else
                {
                    Near_Melist = new ObservableCollection<Near_Me>();
                }

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("search_key", search_key),
                        new KeyValuePair<string, string>("gender", gender),
                        new KeyValuePair<string, string>("status", status),
                        new KeyValuePair<string, string>("distance", distance),
                        new KeyValuePair<string, string>("lat", Lat),
                        new KeyValuePair<string, string>("lng", Lng),
                        new KeyValuePair<string, string>("offset", offset),
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=find_nearby", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    var statussystem = AppResources.Label_AddFriend;
                    var statussystemFriends = AppResources.Label_Friends;

                    if (Settings.ConnectivitySystem == "1")
                    {
                        statussystem = AppResources.Label_Follow;
                        statussystemFriends = AppResources.Label_Following;
                    }

                    if (apiStatus == "200")
                    {
                        var Parser2 = JObject.Parse(json).SelectToken("users").ToString();
                        JArray usersArray = JArray.Parse(Parser2);
                        foreach (var User in usersArray)
                        {
                            var user_id = User["user_id"].ToString();
                            var username = User["username"].ToString();
                            var name = User["name"].ToString();
                            var profile_picture = User["profile_picture"].ToString();
                            var lastseen = User["lastseen"].ToString();
                            var genderuser = User["gender"].ToString();
                            var lastseen_time_text = User["lastseen_time_text"].ToString();
                            var url = User["url"].ToString();
                            var is_following = User["is_following"].ToString();

                            Near_Me a6 = new Near_Me();
                            a6.Result_ID = user_id;
                            a6.Name = name;
                            a6.ButtonImage = Settings.Add_Icon;

                            a6.User_Image = new UriImageSource
                            {
                                Uri = new Uri(profile_picture),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };

                            a6.GPS_place = "0.60/k";
                            a6.Last_Seen = lastseen_time_text;
                            a6.Url = url;

                            if (is_following == "no")
                            {
                                a6.connectivitySystem = statussystem;
                                a6.ButtonColor = "#F0F0F0";
                                a6.ButtonTextColor = "#444";
                                a6.ButtonVisibilty = "true";
                            }
                            else
                            {
                                a6.connectivitySystem = statussystemFriends;
                                a6.ButtonColor = Settings.MainColor;
                                a6.ButtonTextColor = "#ffff";
                                a6.ButtonVisibilty = "true";
                            }
                            Near_Melist.Add(a6);
                        }
                        PullToRefreshLayoutView.IsRefreshing = false;
                        Loadmore = true;
                        if (Near_Melist.Count > 0)
                        {
                            MainNearby_Page.IsVisible = true;
                            EmptyPage.IsVisible = false;
                            await PopulateNearMeLists(Near_Melist);
                        }
                        else
                        {
                            MainNearby_Page.IsVisible = false;
                            EmptyPage.IsVisible = true;

                            Icon_page.Text = "\uf21d";
                            Lbl_Empty.Text = AppResources.Label_Empty;
                            Lbl_NoData.Text = AppResources.Label2_Empty_Near_by_Page;
                            NearbyButton.IsVisible = false;
                            //NearbyButton.Text = AppResources.Label_Try_Again;
                            //NearbyButton.Clicked += (s, a) => TryButton_OnClicked(s, a);

                        }
                    }
                    else
                    {
                        MainNearby_Page.IsVisible = false;
                        EmptyPage.IsVisible = true;

                        Icon_page.Text = "\uf21d";
                        Lbl_Empty.Text = AppResources.Label_Empty;
                        Lbl_NoData.Text = AppResources.Label2_Empty_Near_by_Page;
                        NearbyButton.IsVisible = false;
                        //NearbyButton.Text = AppResources.Label_Try_Again;
                        //NearbyButton.Clicked += (s, a) => TryButton_OnClicked(s, a);
                    }
                }
            }

            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Loadmore = true;

                PullToRefreshLayoutView.IsRefreshing = false;
                if (Near_Melist.Count > 0)
                {
                    await PopulateNearMeLists(Near_Melist);
                }
            }
        }

        private async Task PopulateNearMeLists(ObservableCollection<Near_Me> NearMeList)
        {
            try
            {
                var lastHeight = "128";
                var y = 0;
                var column = LeftColumn;
                var NearMeTapGestureRecognizer = new TapGestureRecognizer();
                NearMeTapGestureRecognizer.Tapped += OnNearMeTapped;

                for (var i = 0; i < NearMeList.Count; i++)
                {
                    var item = new Near_me_ItemTemplate();

                    if (i % 2 == 0)
                    {
                        column = LeftColumn;
                        y++;
                    }
                    else
                    {
                        column = RightColumn;
                    }

                    NearMeList[i].ThumbnailHeight = lastHeight;
                    item.BindingContext = NearMeList[i];
                    item.GestureRecognizers.Add(NearMeTapGestureRecognizer);

                    column.Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnNearMeTapped(Object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Near_Me) ((Main_Near_by_Page) sender).BindingContext;

                await Navigation.PushAsync(new UserProfilePage(selectedItem.Result_ID, ""));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Fillter_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Near_By_Filter_Search());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void SearchBar_OnSearchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                await GetNearby(SearchTextbox.Text, Gender, Status, Distance, "");
            }
            catch (Exception ex)
            {
                PullToRefreshLayoutView.IsRefreshing = false;
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ScrollView_OnScrolled(object sender, ScrolledEventArgs e)
        {
            try
            {
                ScrollView scrollView = sender as ScrollView;
                double scrollingSpace = scrollView.ContentSize.Height - scrollView.Height;

                // Touched bottom
                if (scrollingSpace == e.ScrollY)
                {
                    if (Near_Melist.Count > 0)
                    {
                        var Value = Near_Melist.Last();
                        await GetNearby(SearchTextbox.Text, Gender, Status, Distance, Value.Result_ID.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                PullToRefreshLayoutView.IsRefreshing = false;
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {

                    if (Near_Melist.Count <= 0)
                    {
                        EmptyPage.IsVisible = false;
                        MainNearby_Page.IsVisible = true;

                        GetNearby("", Gender, Status, Distance, "").ConfigureAwait(false);
                    }
                    else
                    {
                        EmptyPage.IsVisible = false;
                        MainNearby_Page.IsVisible = true;
                        PopulateNearMeLists(Near_Melist);
                    }
                }
                else
                {
                    MainNearby_Page.IsVisible = false;
                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    NearbyButton.IsVisible = true;
                    NearbyButton.Text = AppResources.Label_Try_Again;
                    NearbyButton.Clicked += (s, a) => TryButton_OnClicked(s, a);
                    //UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
