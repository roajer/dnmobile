﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Articles
{
    public partial class ArticlesListPage : ContentPage
    {
        #region List

        public static ObservableCollection<Article> ArticleListItems = new ObservableCollection<Article>();

        #endregion

        public ArticlesListPage()
        {
            try
            {
                InitializeComponent();

                ListViewArticle.IsRefreshing = true;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (ArticleListItems.Count > 0)
                    {
                        ListViewArticle.IsVisible = true;
                        EmptyArticlePage.IsVisible = false;

                        ListViewArticle.ItemsSource = ArticleListItems;
                        ListViewArticle.IsRefreshing = false;
                        ListViewArticle.EndRefresh();
                    }
                    else
                    {
                        ListViewArticle.IsVisible = true;
                        EmptyArticlePage.IsVisible = false;

                        ListViewArticle.IsRefreshing = true;
                        //UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                        GetLatestBlogs().ConfigureAwait(false);
                    }
                }
                else
                {
                    ListViewArticle.IsRefreshing = false;

                    ListViewArticle.IsVisible = false;
                    EmptyArticlePage.IsVisible = true;

                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    TryButton.IsVisible = true;

                    ListViewArticle.EndRefresh();
                    //UserDialogs.Instance.Toast(AppResources.Label_Offline_Mode);
                }

                ListViewArticle.IsRefreshing = false;
                ListViewArticle.EndRefresh();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                ListViewArticle.IsRefreshing = false;
                ListViewArticle.EndRefresh();
            }
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Article;
                if (Item.Url != null)
                {
                    Navigation.PushAsync(new HyberdPostViewer("Article", Item.Url));
                    //Device.OpenUri(new Uri(Item.Url));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task GetLatestBlogs()
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    ListViewArticle.IsVisible = true;
                    EmptyArticlePage.IsVisible = false;
                    ListViewArticle.IsRefreshing = true;

                });

                if (ArticleListItems.Count > 0)
                {
                    ArticleListItems.Clear();
                }

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_blogs",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var Parser2 = JObject.Parse(json).SelectToken("blogs").ToString();
                        JArray Article_usersArray = JArray.Parse(Parser2);
                        foreach (var Article in Article_usersArray)
                        {
                            var id = Article["id"].ToString();
                            var title = Article["title"].ToString();
                            var posted_Time = Article["posted"].ToString();
                            var category = Article["category"].ToString();
                            var thumbnail = Article["thumbnail"].ToString();
                            var url = Article["url"].ToString();
                            var view = Article["view"].ToString();

                            ArticleListItems.Add(new Article()
                            {
                                BackgroundImage = new UriImageSource
                                {
                                    Uri = new Uri(thumbnail),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(2, 0, 0, 0)
                                },
                                Title = Functions.DecodeString(title),
                                Section = category,
                                Time = posted_Time,
                                Views = view,
                                Url = url
                            });
                        }
                    }
                }

                if (ArticleListItems.Count == 0)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ListViewArticle.IsVisible = false;
                        EmptyArticlePage.IsVisible = true;

                        Icon_page.Text = "\uf073";
                        Lbl_Empty.Text = "";
                        Lbl_NoData.Text = AppResources.Label_no_articles_available;
                        TryButton.IsVisible = false;
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ListViewArticle.IsVisible = true;
                        EmptyArticlePage.IsVisible = false;
                    });
                }
                Device.BeginInvokeOnMainThread(() =>
                {
                    ListViewArticle.IsRefreshing = false;
                    //UserDialogs.Instance.HideLoading();
                    ListViewArticle.ItemsSource = ArticleListItems;
                    ListViewArticle.EndRefresh();
                });

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                ListViewArticle.IsRefreshing = false;
                ListViewArticle.ItemsSource = ArticleListItems;
                //UserDialogs.Instance.HideLoading();
                ListViewArticle.EndRefresh();
            }
        }

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (ArticleListItems.Count > 0)
                    {
                        ArticleListItems.Clear();
                    }
                    ListViewArticle.IsRefreshing = true;

                    GetLatestBlogs().ConfigureAwait(false);

                    EmptyArticlePage.IsVisible = false;
                    ListViewArticle.IsVisible = true;

                    //UserDialogs.Instance.Toast(AppResources.Label_Loading);
                }
                else
                {
                    ListViewArticle.IsVisible = false;
                    EmptyArticlePage.IsVisible = true;

                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    TryButton.IsVisible = true;

                    ListViewArticle.IsRefreshing = false;
                    ListViewArticle.EndRefresh();

                    //UserDialogs.Instance.Toast(AppResources.Label_Offline_Mode);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                ListViewArticle.IsRefreshing = false;
                ListViewArticle.EndRefresh();
            }
        }

        private void ListViewArticle_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                ListViewArticle.IsRefreshing = true;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    var service = DependencyService.Get<IMethods>();
                    service.ClearWebViewCache();
                    ListViewArticle.IsVisible = false;
                    EmptyArticlePage.IsVisible = true;

                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    TryButton.IsVisible = true;

                    ListViewArticle.IsRefreshing = false;
                    ListViewArticle.EndRefresh();
                }
                else
                {
                    ListViewArticle.IsVisible = true;
                    EmptyArticlePage.IsVisible = false;

                    //Run code
                    GetLatestBlogs().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                ListViewArticle.IsRefreshing = false;
                ListViewArticle.EndRefresh();
            }
        }
    }
}
