﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Articles.Templates
{
    public partial class ArticleItemTemplate : ContentView
    {
        public ArticleItemTemplate()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
