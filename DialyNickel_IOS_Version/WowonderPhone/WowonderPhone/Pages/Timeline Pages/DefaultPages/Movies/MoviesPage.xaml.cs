﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Refractored.XamForms.PullToRefresh;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Movies
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MoviesPage : ContentPage
    {

        #region Event Refresh

        public class RefreshMVVM : INotifyPropertyChanged
        {
            public ObservableCollection<string> Items { get; set; }
            MoviesPage pageChosen;

            public RefreshMVVM(MoviesPage page)
            {
                this.pageChosen = page;
                Items = new ObservableCollection<string>();
            }

            bool isBusy;

            public bool IsBusy
            {
                get { return isBusy; }
                set
                {
                    if (isBusy == value)
                        return;

                    isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }

            ICommand refreshCommand;

            public ICommand RefreshCommand
            {
                get
                {
                    return refreshCommand ?? (refreshCommand = new Command(async () => await ExecuteRefreshCommand()));
                }
            }

            async Task ExecuteRefreshCommand()
            {
                if (IsBusy)
                {
                    return;
                }

                IsBusy = true;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    pageChosen.MoviesListView.IsVisible = false;
                    pageChosen.EmptyPage.IsVisible = true;
                    pageChosen.Icon_page.Text = "\uf119";
                    pageChosen.Lbl_Dont_have.Text = AppResources.Label2_Offline_Mode;
                    pageChosen.Lbl_no_Videos.Text = AppResources.Label_CheckYourInternetConnection;
                }
                else
                {
                    pageChosen.MoviesListView.IsVisible = true;
                    pageChosen.EmptyPage.IsVisible = false;

                    //Run code
                    pageChosen.Get_Movies();
                    IsBusy = false;
                }
            }

            #region INotifyPropertyChanged implementation

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        #endregion

        #region list and Variables

        public static ObservableCollection<Classes.Movies> Movieslist = new ObservableCollection<Classes.Movies>();

        private StackLayout scrollableContent = new StackLayout();

        #endregion

        public MoviesPage()
        {
            try
            {
                InitializeComponent();

                Title = AppResources.Label3_Movies;

                BindingContext = new RefreshMVVM(this);
                PullToRefreshLayoutView.SetBinding<RefreshMVVM>(PullToRefreshLayout.IsRefreshingProperty, vm => vm.IsBusy, BindingMode.OneWay);
                PullToRefreshLayoutView.SetBinding<RefreshMVVM>(PullToRefreshLayout.RefreshCommandProperty, vm => vm.RefreshCommand);

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Movieslist.Count > 0)
                    {
                        MoviesListView.ItemsSource = Movieslist;
                    }
                    else
                    {
                        PullToRefreshLayoutView.IsRefreshing = true;
                        Get_Movies();
                    }
                }
                else
                {
                    PullToRefreshLayoutView.IsRefreshing = false;

                    EmptyPage.IsVisible = true;
                    MoviesListView.IsVisible = false;
                    Icon_page.Text = "\uf119";
                    Lbl_Dont_have.Text = AppResources.Label2_Offline_Mode;
                    Lbl_no_Videos.Text = AppResources.Label_CheckYourInternetConnection;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void Get_Movies()
        {
            try
            {
                PullToRefreshLayoutView.IsRefreshing = true;
                if (Movieslist.Count > 0)
                {
                    Movieslist.Clear();
                }

                try
                {//Get Movies Api
                    using (var client = new HttpClient())
                    {
                        var formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("s", Settings.Session),
                        });
                        var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_movies", formContent);
                        response.EnsureSuccessStatusCode();
                        var Response_json = await response.Content.ReadAsStringAsync();
                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(Response_json);
                        string apiStatus = data["api_status"].ToString();
                        if (apiStatus == "200")
                        {
                            var Movies_data = JObject.Parse(Response_json).SelectToken("movies").ToString();
                            JArray MoviesData = JArray.Parse(Movies_data);

                            if (MoviesData != null)
                            {
                                EmptyPage.IsVisible = false;
                                MoviesListView.IsVisible = true;

                                foreach (var All in MoviesData)
                                {
                                    Classes.Movies m = new Classes.Movies();

                                    try
                                    {
                                        var id = All["id"].ToString();
                                        var name = All["name"].ToString();
                                        var genre = All["genre"].ToString();
                                        var stars = All["stars"].ToString();
                                        var producer = All["producer"].ToString();
                                        var country = All["country"].ToString();
                                        var release = All["release"].ToString();
                                        var quality = All["quality"].ToString();
                                        var duration = All["duration"].ToString();
                                        var description = All["description"].ToString();
                                        var cover = All["cover"].ToString();
                                        var source = All["source"].ToString();
                                        var iframe = All["iframe"].ToString();
                                        var video = All["video"].ToString();
                                        var views = All["views"].ToString();
                                        var url = All["url"].ToString();

                                        m.id = id;
                                        m.name = Functions.StringNullRemover(name);
                                        m.genre = Functions.StringNullRemover(genre);
                                        m.stars = Functions.StringNullRemover(stars);
                                        m.producer = Functions.StringNullRemover(producer);
                                        m.country = Functions.StringNullRemover(country);
                                        m.release = Functions.StringNullRemover(release);
                                        m.quality = Functions.StringNullRemover(quality);
                                        m.duration = duration + " " + "Min";
                                        m.description = Functions.DecodeString(Functions.SubStringCutOf(description, 50));
                                        m.description_Long = Functions.DecodeString(description);
                                        m.cover = cover;
                                        m.source = source;
                                        m.iframe = iframe;
                                        m.video = video;
                                        m.views = views;
                                        m.url = url;

                                        Movieslist.Add(m);
                                    }
                                    catch (Exception ex)
                                    {
                                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                    }
                                }
                                if (Movieslist.Count > 0)
                                {
                                    PullToRefreshLayoutView.IsRefreshing = false;
                                    MoviesListView.IsVisible = true;
                                    EmptyPage.IsVisible = false;

                                    MoviesListView.ItemsSource = Movieslist;
                                }
                                else
                                {
                                    PullToRefreshLayoutView.IsRefreshing = false;
                                    EmptyPage.IsVisible = true;
                                    MoviesListView.IsVisible = false;

                                    Icon_page.Text = "\uf03d";
                                    Lbl_Dont_have.Text =AppResources.Label3_Empty_Videos;
                                    Lbl_Dont_have.Text =AppResources.Label3_Dont_you_have_any_video;
                                }
                            }
                            else
                            {
                                PullToRefreshLayoutView.IsRefreshing = false;

                                EmptyPage.IsVisible = true;
                                MoviesListView.IsVisible = false;
                                Icon_page.Text = "\uf03d";
                                Lbl_Dont_have.Text = AppResources.Label3_Empty_Videos;
                                Lbl_Dont_have.Text = AppResources.Label3_Dont_you_have_any_video;
                            }
                        }
                        else
                        {
                            PullToRefreshLayoutView.IsRefreshing = false;

                            EmptyPage.IsVisible = true;
                            MoviesListView.IsVisible = false;
                            Icon_page.Text = "\uf03d";
                            Lbl_Dont_have.Text = AppResources.Label3_Empty_Videos;
                            Lbl_Dont_have.Text = AppResources.Label3_Dont_you_have_any_video;
                        }
                    }
                }
                catch (Exception ex)
                {
                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    PullToRefreshLayoutView.IsRefreshing = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                PullToRefreshLayoutView.IsRefreshing = false;
            }
        }

        private void MoviesListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                MoviesListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item >> open Video_Player_Page
        private async void MoviesListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Classes.Movies selectedItem)
                {
                    await Navigation.PushAsync(new Video_Player_Page(selectedItem));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TryagainButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Movieslist.Count > 0)
                    {
                        Movieslist.Clear();
                    }
                    Get_Movies();
                }
                else
                {
                    MoviesListView.IsRefreshing = false;
                    MoviesListView.EndRefresh();

                    EmptyPage.IsVisible = true;
                    MoviesListView.IsVisible = false;
                    Icon_page.Text = "\uf119";
                    Lbl_Dont_have.Text = AppResources.Label2_Offline_Mode;
                    Lbl_no_Videos.Text = AppResources.Label_CheckYourInternetConnection;
                    tryagainButton.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}