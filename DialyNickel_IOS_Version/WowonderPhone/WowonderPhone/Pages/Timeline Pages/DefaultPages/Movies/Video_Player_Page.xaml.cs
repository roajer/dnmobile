﻿using System;
using System.Collections.ObjectModel;
using Plugin.MediaManager;
using Plugin.MediaManager.Abstractions.Enums;
using Plugin.Share;
using Plugin.Share.Abstractions;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Movies
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Video_Player_Page : ContentPage
    {

        #region classes

        public class InfoItemVideo 
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Color { get; set; }
        }

        #endregion

        #region Variables and Lists Items Declaration

        public static ObservableCollection<InfoItemVideo> InfoItemListItems = new ObservableCollection<InfoItemVideo>();
        private Classes.Movies _movies;
        private static bool stoped;

        #endregion

        public Video_Player_Page(Classes.Movies movies)
        {
            try
            {
                InitializeComponent();

                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    HeaderOfpage.IsVisible = false;
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }
               
                _movies = movies;
                if (_movies != null)
                {
                    HeaderLabel.Text = movies.name +" (" + movies.release + ")";

                    ViewsSpan.Text = movies.views;
                    Quality.Text = movies.quality.ToUpperInvariant();

                    Title.Text = movies.name;
                    Description.Text = movies.description_Long;

                    InfoItemListItems.Clear();

                    InfoItemListItems.Add(new InfoItemVideo()
                    {
                        Label = movies.genre,
                        Icon = "\uf040",
                        Color = "#c5c9c8"
                    });
                    InfoItemListItems.Add(new InfoItemVideo()
                    {
                        Label = movies.stars,
                        Icon = "\uf005",
                        Color = "#c5c9c8"
                    });
                    InfoItemListItems.Add(new InfoItemVideo()
                    {
                        Label = movies.producer,
                        Icon = "\uf198",
                        Color = "#c5c9c8"
                    });
                    InfoItemListItems.Add(new InfoItemVideo()
                    {
                        Label = movies.release,
                        Icon = "\uf274",
                        Color = "#c5c9c8"
                    });

                    InfoList.ItemsSource = InfoItemListItems;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public  void AutoRun_Progress()
        {
            try
            {
                CrossMediaManager.Current.PlayingChanged += (sender, e) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Loaderspinner.IsVisible == true)
                        {
                            Loaderspinner.IsVisible = false;
                        }
                    });
                };

                CrossMediaManager.Current.MediaFinished += (sender, e) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PlayLabel.Text = AppResources.Label3_Play;
                        StopLabel.Text = AppResources.Label3_Stop;
                        PlayIcon.Text = "\uf144"; // play icon
                    });
                };
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PlayButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (stoped)
                {
                    VideoPlayer.Source = _movies.source;
                    PlayLabel.Text = AppResources.Label3_Pause;
                    StopLabel.Text = AppResources.Label3_Stop;
                    PlayIcon.Text = "\uf04c"; // Pause icon
                    stoped = false;
                    CrossMediaManager.Current.Play(_movies.source, MediaFileType.Video);
                }
                else
                {
                    if (PlayLabel.Text == AppResources.Label3_Play)
                    {
                        //VideoPlayer.Source = _movies.source;
                        PlayLabel.Text = AppResources.Label3_Pause;
                        StopLabel.Text = AppResources.Label3_Stop;
                        PlayIcon.Text = "\uf04c"; // Pause icon

                        CrossMediaManager.Current.Play();

                    }
                    else if (PlayLabel.Text == AppResources.Label3_Pause)
                    {
                        //VideoPlayer.Source = _movies.source;
                        PlayLabel.Text = AppResources.Label3_Play;
                        StopLabel.Text = AppResources.Label3_Stop;
                        PlayIcon.Text = "\uf144"; // play icon

                        CrossMediaManager.Current.Pause();
                    }
                }

                AutoRun_Progress();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void StopButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                PlayLabel.Text = AppResources.Label3_Play;
                StopLabel.Text = AppResources.Label3_Stop;
                PlayIcon.Text = "\uf144"; // play icon

                stoped = true;
                CrossMediaManager.Current.Stop();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.PopModalAsync(true);
            }
        }

        private async void ShareRecognizer_OnTapped(object sender, EventArgs e)
        {
            try
            {
                //if (!CrossShare.IsSupported)
                //{
                //    return;
                //}
                //Share Plugin
                await CrossShare.Current.Share(new ShareMessage
                {
                    Title = _movies.name,
                    Text = _movies.description_Long,
                    Url = _movies.source
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CopyUrlButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                LinkLabel.Text = "\uf00c";
                DependencyService.Get<IClipboardService>().CopyToClipboard(_movies.source);
                DependencyService.Get<IMessage>().LongAlert(AppResources.Label3_Copyed_URL);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void InfoList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                InfoList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void InfoList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

  
        private void Video_Player_Page_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                VideoPlayer.Source = _movies.source;
                PlayLabel.Text = AppResources.Label3_Pause;
                StopLabel.Text = AppResources.Label3_Stop;
                PlayIcon.Text = "\uf04c"; // Pause icon
                stoped = false;
                AutoRun_Progress();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}