﻿using System;

using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class ProUser_Upgrade_Page : ContentPage
    {
        public ProUser_Upgrade_Page()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void UpgradeButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Device.OpenUri(new Uri(Settings.Website + "/go-pro"));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
