﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Create_Comunities;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GroupsListPage : ContentPage
    {
        public static ObservableCollection<Communities> GroupsListItems = new ObservableCollection<Communities>();
        public static ObservableCollection<Communities> AddGroupsListItems = new ObservableCollection<Communities>();

        public GroupsListPage()
        {
            try
            {
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
                    ToolbarItems.RemoveAt(0);
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

                var addme = AddGroupsListItems.FirstOrDefault(a => a.CommunityType == "Create_Groups");
                if (addme == null)
                {
                    Communities delta = new Communities();
                    delta.CommunityID = "";
                    delta.CommunityName = AppResources.label_Create_Groups;
                    delta.CommunityType = "Create_Groups";
                    delta.CommunityPicture = "G_Add_ProUsers.png";
                    delta.CommunityTypeLabel = AppResources.Label_Groups;

                    AddGroupsListItems.Add(delta);
                }
                AddCommunitiesListview.ItemsSource = AddGroupsListItems;

                LoadCommunitiesFromCash();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void LoadCommunitiesFromCash()
        {
            try
            {
                using (var data = new CommunitiesFunction())
                {
                    var Data = data.GetGroupsCacheList();
                    if (Data.Count > 0)
                    {
                        CommunitiesListview.ItemsSource = Data;


                        if (CommunitiesListview.IsVisible == false)
                        {
                            CommunitiesListview.IsVisible = true;
                            AddCommunitiesListview.IsVisible = true;
                            EmptyPage.IsVisible = false;
                        }
                    }
                    else
                    {
                        var device = Resolver.Resolve<IDevice>();
                        var oNetwork = device.Network; // Create Interface to Network-functions
                        var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                        if (!xx)
                        {
                            EmptyPage.IsVisible = true;
                            CommunitiesListview.IsVisible = false;
                            AddCommunitiesListview.IsVisible = false;
                            UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                            GetCommunities().ConfigureAwait(false);
                        }
                        else
                        {
                            UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Search_OnClicked(object sender, EventArgs e)
        {
            try
            {
                //if (SearchFrame.IsVisible)
                //{
                //    SearchFrame.IsVisible = false;
                //}
                //else
                //{
                //    SearchFrame.IsVisible = true;
                //}
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnSearchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                using (var data = new CommunitiesFunction())
                {
                    data.GetGroupSearchList(SearchBarCo.Text);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                using (var data = new CommunitiesFunction())
                {
                    data.GetGroupSearchList(SearchBarCo.Text);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommunitiesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                CommunitiesListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommunitiesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Communities;
                if (Item.CommunityType == "Groups" || Item.CommunityType == "Group")
                {
                    Navigation.PushAsync(new SocialGroup(Item.CommunityID, "Joined"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> GetCommunities()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });
                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_my_community", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var CO_Data = new CommunitiesFunction();

                        var Group = JObject.Parse(json).SelectToken("mygroups").ToString();
                        JArray GroupdataArray = JArray.Parse(Group);

                        if (GroupdataArray.Count <= 0)
                        {
                            UserDialogs.Instance.HideLoading();
                            EmptyPage.IsVisible = true;
                            CommunitiesListview.IsVisible = false;
                            AddCommunitiesListview.IsVisible = false;
                            return "emety";
                        }
                        else
                        {
                            foreach (var GroupItem in GroupdataArray)
                            {
                                var g_id = GroupItem["id"].ToString();
                                var g_user_id = GroupItem["user_id"].ToString();
                                var g_group_name = GroupItem["group_name"].ToString();
                                var g_group_title = GroupItem["group_title"].ToString();
                                var g_avatar = GroupItem["avatar"].ToString();
                                var g_cover = GroupItem["cover"].ToString();
                                var g_about = GroupItem["about"].ToString();
                                var g_category = GroupItem["category"].ToString();
                                var g_privacy = GroupItem["privacy"].ToString();
                                var g_join_privacy = GroupItem["join_privacy"].ToString();
                                var g_active = GroupItem["active"].ToString();
                                var g_registered = GroupItem["registered"].ToString();
                                var g_group_id = GroupItem["group_id"].ToString();
                                var g_url = GroupItem["url"].ToString();
                                var g_name = GroupItem["name"].ToString();
                                var g_category_id = GroupItem["category_id"].ToString();
                                var g_type = GroupItem["type"].ToString();
                                var g_username = GroupItem["username"].ToString();

                                var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(g_avatar, g_id);
                                var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                if (Imagepath == "File Dont Exists")
                                {
                                    ImageMediaFile = new UriImageSource {Uri = new Uri(g_avatar) };
                                    DependencyService.Get<IPicture>().SavePictureToDisk(g_avatar, g_id);
                                }
                                var Cheke = GroupsListItems.FirstOrDefault(a => a.CommunityID == g_id);
                                if (GroupsListItems.Contains(Cheke))
                                {
                                    if (Cheke.CommunityName != g_group_name)
                                    {
                                        Cheke.CommunityName = g_group_name;
                                    }
                                    if (Cheke.ImageUrlString != g_avatar)
                                    {
                                        Cheke.CommunityPicture = new UriImageSource {Uri = new Uri(g_avatar) };
                                    }
                                }
                                else
                                {
                                    GroupsListItems.Add(new Communities()
                                    {
                                        CommunityID = g_id,
                                        CommunityName = g_group_name,
                                        CommunityType = "Groups",
                                        CommunityPicture = ImageMediaFile,
                                        CommunityTypeLabel = g_category,
                                        CommunityTypeID = g_category_id,
                                        ImageUrlString = g_avatar
                                    });
                                }

                                CO_Data.InsertCommunities(new CommunitiesDB()
                                {
                                    CommunityID = g_id,
                                    CommunityName = g_group_name,
                                    CommunityPicture = g_avatar,
                                    CommunityType = "Groups",
                                    CommunityTypeLabel = g_category,
                                    CommunityTypeID = g_category_id,
                                    CommunityUrl = g_url
                                });
                            }
                        }
                        UserDialogs.Instance.HideLoading();
                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }
                    return json;
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
                return AppResources.Label_Error;
            }
        }

        private void Refresh_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                    GetCommunities().ConfigureAwait(false);
                }
                else
                {
                    UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommunitiesPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Create_GroupButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Create_Groups_Pages());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchPageButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Search_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void GroupsListPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                if (Create_Groups_Pages.CreateGroups)
                {
                    LoadCommunitiesFromCash();
                    Create_Groups_Pages.CreateGroups = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddCommunitiesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                AddCommunitiesListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddCommunitiesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Communities;
                if (Item.CommunityType == "Create_Groups")
                {
                    Navigation.PushAsync(new Create_Groups_Pages());
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
