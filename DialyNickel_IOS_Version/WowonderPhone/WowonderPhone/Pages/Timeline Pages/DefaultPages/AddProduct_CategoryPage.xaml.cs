﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using PropertyChanged;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class AddProduct_CategoryPage : ContentPage
    {
        #region classes 

        [ImplementPropertyChanged]
        public class CategoryItems
        {
            public string CategoryLabel { get; set; }
            public string value { get; set; }
            public string Checkicon { get; set; }
            public string CheckiconColor { get; set; }
        }

        #endregion

        #region list

        public static ObservableCollection<CategoryItems> CategoryListItemsCollection = new ObservableCollection<CategoryItems>();

        #endregion

        public AddProduct_CategoryPage()
        {
            try
            {
                InitializeComponent();
                AddCategoryToList();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddCategoryToList()
        {
            try
            {
                if (CategoryListItemsCollection.Count == 0)
                {
                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_ApparelAccessories,
                        value = "0",
                        Checkicon = "\uf058",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_AutosVehicles,
                        value = "1",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_BabyChildrenProducts,
                        value = "2",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_BeautyProductsServices,
                        value = "3",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_ComputersPeripherals,
                        value = "4",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_ConsumerElectronics,
                        value = "5",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_DatingServices,
                        value = "6",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_FinancialServices,
                        value = "7",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_GiftsOccasions,
                        value = "8",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                    CategoryListItemsCollection.Add(new CategoryItems()
                    {
                        CategoryLabel = AppResources.Label_HomeGarden,
                        value = "9",
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });
                }
                CategoryList.ItemsSource = CategoryListItemsCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CategoryList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                CategoryList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CategoryList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            var Item = e.Item as CategoryItems;
            if (Item != null)
            {
                foreach (var PVitem in CategoryListItemsCollection)
                {
                    PVitem.Checkicon = "\uf1db";
                }

                if (Item.Checkicon == "\uf1db")
                {
                    Item.Checkicon = "\uf058";
                }
            }
        }

        private async  void CreatProductButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var ProductAttach = AddProductPage.ActivityListItems.FirstOrDefault(a => a.Label == AddProductPage.PikedImage);
                if (ProductAttach != null)
                {
                    var itemCattagory  = CategoryListItemsCollection.FirstOrDefault(a => a.Checkicon == "\uf058");
                    
                        if (itemCattagory!=null)
                        {
                           //var action = DisplayActionSheet("Your Product Type is", null, null, "New", "Used").;

                        Task.Factory.StartNew(() =>
                        {
                            DependencyService.Get<IMethods>().CreateProduct(ProductAttach.ImageStream, ProductAttach.ImageFullPath, Settings.User_id, AddProductPage.ProductName, itemCattagory.CategoryLabel, AddProductPage.ProductDescription, AddProductPage.ProductPrice, AddProductPage.ProductLocation, "New");
                            
                        });

                        if (MarketPage.IsPageInNavigationStack<AddProductPage>(Navigation))
                        {
                            MarketPage.Productlist.Insert(0, new Products
                            {
                                Name = AddProductPage.ProductName,
                                Description = AddProductPage.ProductDescription,
                                Image = ImageSource.FromFile(ProductAttach.ImageFullPath),
                                Price = AddProductPage.ProductPrice,
                                ThumbnailHeight = "100",
                                Manufacturer = "You",

                            });
                        }

                        UserDialogs.Instance.ShowSuccess(AppResources.Label_Product_Added, 2500);
                        AddProductPage.ActivityListItems.Clear();
                        Navigation.PopAsync(false).Wait(200);
                    }
                }  
            }
            catch (Exception)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2500);
            }
        }
    }
}
