﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Create_Comunities;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class CommunitiesPage : ContentPage
    {
        #region Lists Items Declaration

        public static ObservableCollection<Communities> CommunitiesListItems = new ObservableCollection<Communities>();
        public static ObservableCollection<Communities> AddCommunitiesListItems = new ObservableCollection<Communities>();

        #endregion

        public CommunitiesPage()
        {
            try
            {
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
                    ToolbarItems.RemoveAt(0);
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

                var addme = AddCommunitiesListItems.FirstOrDefault(a => a.CommunityType == "Create_Pages");
                if (addme == null)
                {
                    Communities delta = new Communities();
                    delta.CommunityID = "";
                    delta.CommunityName = AppResources.label_Create_Pages;
                    delta.CommunityType = "Create_Pages";
                    delta.CommunityPicture = "G_Add_ProUsers.png";
                    delta.CommunityTypeLabel = AppResources.Label_Pages;

                    AddCommunitiesListItems.Add(delta);
                }
                AddCommunitiesListview.ItemsSource = AddCommunitiesListItems;

                LoadCommunitiesFromCash();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void LoadCommunitiesFromCash()
        {
            try
            {
                using (var data = new CommunitiesFunction())
                {
                    var Data = data.GetPagesCacheList();
                    if (Data.Count > 0)
                    {
                        CommunitiesListview.ItemsSource = Data;

                        if (CommunitiesListview.IsVisible == false)
                        {
                            CommunitiesListview.IsVisible = true;
                            AddCommunitiesListview.IsVisible = true;
                            EmptyPage.IsVisible = false;
                        }
                    }
                    else
                    {
                        var device = Resolver.Resolve<IDevice>();
                        var oNetwork = device.Network; // Create Interface to Network-functions
                        var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                        if (!xx)
                        {
                            EmptyPage.IsVisible = true;
                            CommunitiesListview.IsVisible = false;
                            AddCommunitiesListview.IsVisible = false;
                            UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                            GetCommunities().ConfigureAwait(false);
                        }
                        else
                        {
                            UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Search_OnClicked(object sender, EventArgs e)
        {
            //if (SearchFrame.IsVisible)
            //{
            //    SearchFrame.IsVisible = false;
            //}
            //else
            //{
            //    SearchFrame.IsVisible = true;
            //}
        }

        private void SearchBarCo_OnSearchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                using (var data = new CommunitiesFunction())
                {
                    data.GetCommunitySearchList(SearchBarCo.Text);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                using (var data = new CommunitiesFunction())
                {
                    data.GetCommunitySearchList(SearchBarCo.Text);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommunitiesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                CommunitiesListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in CommunitiesList >> open SocialGroup OR SocialPageViewer OR Create_Pages_Page
        private void CommunitiesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Communities Item)
                {
                    if (Item.CommunityType == "Groups" || Item.CommunityType == "Group")
                    {
                        Navigation.PushAsync(new SocialGroup(Item.CommunityID, "Joined"));
                    }
                    else if (Item.CommunityType == "Pages" || Item.CommunityType == "Page")
                    {
                        Navigation.PushAsync(new SocialPageViewer(Item.CommunityID, Item.CommunityName, "Liked"));
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> GetCommunities()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_my_community", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var CO_Data = new CommunitiesFunction();
                        var Page = JObject.Parse(json).SelectToken("mypage").ToString();
                        JArray PagedataArray = JArray.Parse(Page);

                        foreach (var PageItem in PagedataArray)
                        {
                            var P_page_id = PageItem["page_id"].ToString();
                            var P_user_id = PageItem["user_id"].ToString();
                            var P_page_name = PageItem["page_name"].ToString();
                            var P_page_title = PageItem["page_title"].ToString();
                            var P_page_description = PageItem["page_description"].ToString();
                            var P_avatar = PageItem["avatar"].ToString();
                            var P_cover = PageItem["cover"].ToString();
                            var P_page_category = PageItem["page_category"].ToString();
                            var P_website = PageItem["website"].ToString();
                            var P_facebook = PageItem["facebook"].ToString();
                            var P_google = PageItem["google"].ToString();
                            var P_vk = PageItem["vk"].ToString();
                            var P_twitter = PageItem["twitter"].ToString();
                            var P_linkedin = PageItem["linkedin"].ToString();
                            var P_company = PageItem["company"].ToString();
                            var P_phone = PageItem["phone"].ToString();
                            var P_address = PageItem["address"].ToString();
                            var P_call_action_type = PageItem["call_action_type"].ToString();
                            var P_call_action_type_url = PageItem["call_action_type_url"].ToString();
                            var P_background_image = PageItem["background_image"].ToString();
                            var P_background_image_status = PageItem["background_image_status"].ToString();
                            var P_instgram = PageItem["instgram"].ToString();
                            var P_youtube = PageItem["youtube"].ToString();
                            var P_verified = PageItem["verified"].ToString();
                            var P_active = PageItem["active"].ToString();
                            var P_registered = PageItem["registered"].ToString();
                            var P_boosted = PageItem["boosted"].ToString();
                            var P_about = PageItem["about"].ToString();
                            var P_id = PageItem["id"].ToString();
                            var P_type = PageItem["type"].ToString();
                            var P_url = PageItem["url"].ToString();
                            var P_name = PageItem["name"].ToString();
                            var P_rating = PageItem["rating"].ToString();
                            var P_category = PageItem["category"].ToString();
                            var P_is_page_onwer = PageItem["is_page_onwer"].ToString();
                            var P_username = PageItem["username"].ToString();

                            var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(P_avatar, P_page_id);
                            var ImageMediaFile = ImageSource.FromFile(Imagepath);

                            if (Imagepath == "File Dont Exists")
                            {
                                ImageMediaFile = new UriImageSource {Uri = new Uri(P_avatar) };
                                DependencyService.Get<IPicture>().SavePictureToDisk(P_avatar, P_page_id);
                            }
                            var Cheke = CommunitiesListItems.FirstOrDefault(a => a.CommunityID == P_page_id);
                            if (CommunitiesListItems.Contains(Cheke))
                            {
                                if (Cheke.CommunityName != P_page_name)
                                {
                                    Cheke.CommunityName = P_page_name;
                                }
                                if (Cheke.ImageUrlString != P_avatar)
                                {
                                    Cheke.CommunityPicture = new UriImageSource {Uri = new Uri(P_avatar) };
                                }
                            }
                            else
                            {
                                CommunitiesListItems.Add(new Communities()
                                {
                                    CommunityID = P_page_id,
                                    CommunityName = P_page_name,
                                    CommunityType = "Pages",
                                    CommunityPicture = ImageMediaFile,
                                    CommunityTypeLabel = P_category,
                                    CommunityTypeID = P_page_category,
                                    ImageUrlString = P_avatar
                                });
                            }
                            CO_Data.InsertCommunities(new CommunitiesDB()
                            {
                                CommunityID = P_page_id,
                                CommunityName = P_page_name,
                                CommunityPicture = P_avatar,
                                CommunityType = "Pages",
                                CommunityTypeLabel = P_category,
                                CommunityTypeID = P_page_category,
                                CommunityUrl = P_url
                            });
                        }

                        var Group = JObject.Parse(json).SelectToken("mygroups").ToString();
                        JArray GroupdataArray = JArray.Parse(Group);

                        if (GroupdataArray.Count <= 0 && PagedataArray.Count <= 0)
                        {
                            UserDialogs.Instance.HideLoading();
                            //UserDialogs.Instance.ShowError(AppResources.Label_Community_Empty, 2000);

                            EmptyPage.IsVisible = true;
                            CommunitiesListview.IsVisible = false;
                            AddCommunitiesListview.IsVisible = false;
                            return "emety";
                        }
                        else
                        {
                            foreach (var GroupItem in GroupdataArray)
                            {
                                var g_id = GroupItem["id"].ToString();
                                var g_user_id = GroupItem["user_id"].ToString();
                                var g_group_name = GroupItem["group_name"].ToString();
                                var g_group_title = GroupItem["group_title"].ToString();
                                var g_avatar = GroupItem["avatar"].ToString();
                                var g_cover = GroupItem["cover"].ToString();
                                var g_about = GroupItem["about"].ToString();
                                var g_category = GroupItem["category"].ToString();
                                var g_privacy = GroupItem["privacy"].ToString();
                                var g_join_privacy = GroupItem["join_privacy"].ToString();
                                var g_active = GroupItem["active"].ToString();
                                var g_registered = GroupItem["registered"].ToString();
                                var g_group_id = GroupItem["group_id"].ToString();
                                var g_url = GroupItem["url"].ToString();
                                var g_name = GroupItem["name"].ToString();
                                var g_category_id = GroupItem["category_id"].ToString();
                                var g_type = GroupItem["type"].ToString();
                                var g_username = GroupItem["username"].ToString();

                                var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(g_avatar, g_id);
                                var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                if (Imagepath == "File Dont Exists")
                                {
                                    ImageMediaFile = new UriImageSource {Uri = new Uri(g_avatar) };
                                    DependencyService.Get<IPicture>().SavePictureToDisk(g_avatar, g_id);
                                }
                                var Cheke = CommunitiesListItems.FirstOrDefault(a => a.CommunityID == g_id);
                                if (CommunitiesListItems.Contains(Cheke))
                                {
                                    if (Cheke.CommunityName != g_group_name)
                                    {
                                        Cheke.CommunityName = g_group_name;
                                    }
                                    if (Cheke.ImageUrlString != g_avatar)
                                    {
                                        Cheke.CommunityPicture = new UriImageSource {Uri = new Uri(g_avatar) };
                                    }
                                }
                                else
                                {
                                    CommunitiesListItems.Add(new Communities()
                                    {
                                        CommunityID = g_id,
                                        CommunityName = g_group_name,
                                        CommunityType = "Groups",
                                        CommunityPicture = ImageMediaFile,
                                        CommunityTypeLabel = g_category,
                                        CommunityTypeID = g_category_id,
                                        ImageUrlString = g_avatar
                                    });
                                }
                                CO_Data.InsertCommunities(new CommunitiesDB()
                                {
                                    CommunityID = g_id,
                                    CommunityName = g_group_name,
                                    CommunityPicture = g_avatar,
                                    CommunityType = "Groups",
                                    CommunityTypeLabel = g_category,
                                    CommunityTypeID = g_category_id,
                                    CommunityUrl = g_url
                                });
                            }
                        }

                        if (CommunitiesListItems.Count > 0)
                        {
                            var GetPages = new ObservableCollection<Communities>(CommunitiesListItems.Where(a => a.CommunityType == "Pages").ToList());
                            if (GetPages.Count > 0)
                            {
                                EmptyPage.IsVisible = false;
                                CommunitiesListview.IsVisible = true;
                                AddCommunitiesListview.IsVisible = true;
                                CommunitiesListview.ItemsSource = GetPages;
                            }
                            else
                            {
                                EmptyPage.IsVisible = true;
                                CommunitiesListview.IsVisible = false;
                                AddCommunitiesListview.IsVisible = false;
                            }
                        }

                        UserDialogs.Instance.HideLoading();
                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }
                    return json;
                } 
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
                return AppResources.Label_Error;
            }

        }

        private void Refresh_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                    GetCommunities().ConfigureAwait(false);
                }
                else
                {
                    UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommunitiesPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Create_pageButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Create_Pages_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchPageButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Search_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click back Icon
        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CommunitiesPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                if (Create_Pages_Page.CreatePages)
                {
                    LoadCommunitiesFromCash();
                    Create_Pages_Page.CreatePages = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddCommunitiesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                AddCommunitiesListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddCommunitiesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Communities;
                if (Item.CommunityType == "Create_Pages")
                {
                    Navigation.PushAsync(new Create_Pages_Page());
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
          
        }
    }
}