﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using WowonderPhone.Classes;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Create_Comunities
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Create_Groups_Pages : ContentPage
    {
        public string selectedCatigories = "";
        List<string> ListOfValues = new List<string>();

        public static bool CreateGroups = false;

        public Create_Groups_Pages()
        {
            try
            {
                
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

                //Get Catigories
                ListOfValues = Settings.AddIemsCatigorieses;
                if (ListOfValues.Count > 0)
                    Picker_Category.ItemsSource = ListOfValues;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event Selected Index Category Picker
        private void CategoryPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var Picker_Category = (Picker)sender;
                if (Picker_Category != null)
                {
                    var selectedValue = Picker_Category.SelectedIndex - 1;
                    selectedCatigories = selectedValue.ToString();
                    //selectedCatigories = ListOfValues.Keys.FirstOrDefault(a => a.Contains(selectedValue.ToString()));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Create_Pages API
        public static async Task<string> Create_Pages_Http(string group_name, string title ,string about, string category)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("group_name", group_name),
                        new KeyValuePair<string, string>("group_title", title),
                        new KeyValuePair<string, string>("about", about),
                        new KeyValuePair<string, string>("category", category)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=create_group", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        string api_text = data["api_text"].ToString();
                        if (api_text == "success")
                        {
                            string G_name = data["group_name"].ToString();
                            string G_id = data["group_id"].ToString();

                            return "name=" + G_name + "=id=" + G_id;
                        }
                    }
                    else if (apiStatus == "400")
                    {
                        return "Error creating group";
                    }
                }
                return "";
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "Error creating group";
            }
        }

        //Event Create_Group
        private async void Btn_Create_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }
                else
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                    var result = await Create_Pages_Http(Txt_Group_name.Text, Txt_Group_title.Text, Editor_About.Text,selectedCatigories);
                    if (!String.IsNullOrEmpty(result))
                    {
                        if (result.Contains("Error"))
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowError(AppResources.Label_Error);
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowSuccess(AppResources.Label2_Done);

                            var data = result.Split('=');
                            string name = data[1];
                            string id = data[3];
                            ImageSource avatar = "d_group.jpg";

                            GroupsListPage.GroupsListItems.Insert(1, new Communities()
                            {
                                CommunityID = id,
                                CommunityName = name,
                                CommunityType = "Groups",
                                CommunityPicture = avatar,
                                CommunityTypeLabel = AppResources.Label_Groups,
                                //ImageUrlString = avatar
                            });

                            var CO_Data = new CommunitiesFunction();

                            CO_Data.InsertCommunities(new CommunitiesDB()
                            {
                                CommunityID = id,
                                CommunityName = name,
                                CommunityPicture = avatar.ToString(),
                                CommunityType = "Groups",
                                CommunityTypeLabel = AppResources.Label_Groups,
                                //CommunityUrl = url
                            });
                        }
                        await Navigation.PopAsync(true);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.ShowError(AppResources.Label_Error);
                        await Navigation.PopAsync(true);
                        // await DisplayAlert("Create", result, AppResources.Label_OK);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Error);
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Editor_About_OnTextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
