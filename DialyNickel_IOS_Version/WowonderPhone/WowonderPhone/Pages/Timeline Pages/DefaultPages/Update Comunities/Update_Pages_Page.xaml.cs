﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.Media;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.Update_Comunities
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Update_Pages_Page : ContentPage
    {

        public string selectedCatigories = "";
        List<string> ListOfValues = new List<string>();
        public static string page_id;
        public static string Coverimage;
        public static string Avatarimage;

        private ObservableCollection<Profile.Liked_Pages> PageDataList = new ObservableCollection<Profile.Liked_Pages>();

        public Update_Pages_Page(ObservableCollection<Profile.Liked_Pages> PageList)
        {
            InitializeComponent();

            //Get Catigories
            ListOfValues = Settings.AddIemsCatigorieses;
            if (ListOfValues.Count > 0)
                Picker_Category.ItemsSource = ListOfValues;

            PageDataList = PageList;

            if (PageDataList.Count > 0)
            {
                var data = PageDataList.FirstOrDefault();
                if (data != null)
                {
                    page_id = data.P_page_id;
                    Txt_Page_name.Text = data.P_page_name;
                    Txt_Page_title.Text = data.P_page_title;
                    Picker_Category.SelectedIndex = Convert.ToInt32(data.P_page_category) - 1;
                    Editor_description.Text = data.P_about;
                }
            }
        }

        private void CategoryPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var Picker_Category = (Picker)sender;
                if (Picker_Category != null)
                {
                    var selectedValue = Picker_Category.SelectedIndex;
                    selectedCatigories = selectedValue.ToString();
                    //selectedCatigories = ListOfValues.Keys.FirstOrDefault(a => a.Contains(selectedValue));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Editor_description_OnTextChanged(object sender, TextChangedEventArgs e)
        {

        }

        //update_Pages API
        public static async Task<string> update_Pages_Http(string data_Pages)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("page_id", page_id),
                        new KeyValuePair<string, string>("type", "general_settings"),
                        new KeyValuePair<string, string>("data", data_Pages),
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=u_page", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Error Update page";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "Error Update page";
            }
        }

        private async void Btn_Update_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }
                else
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);

                    var dictionary = new Dictionary<string, string>
                    {
                        {"page_name", Txt_Page_name.Text},
                        {"page_title", Txt_Page_title.Text},
                        {"page_description", Editor_description.Text},
                        {"page_category", selectedCatigories},
                    };

                    var result = await update_Pages_Http(JsonConvert.SerializeObject(dictionary));
                    if (!String.IsNullOrEmpty(result))
                    {
                        if (result.Contains("Error"))
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowError(AppResources.Label_Error);
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowSuccess(AppResources.Label2_Done);

                            var data = GroupsListPage.GroupsListItems.FirstOrDefault(a => a.CommunityID == page_id);
                            if (data != null)
                            {
                                data.CommunityID = page_id;
                                data.CommunityName = Txt_Page_name.Text;
                                data.CommunityType = "Pages";
                                //data.CommunityPicture = avatar,
                                //data.ImageUrlString = avatar
                                data.CommunityTypeID = selectedCatigories;
                                data.CommunityTypeLabel = Picker_Category.SelectedItem.ToString();
                            }

                            var CO_Data = new CommunitiesFunction();
                            CO_Data.InsertCommunities(new CommunitiesDB()
                            {
                                CommunityID = page_id,
                                CommunityName = Txt_Page_name.Text,
                                //CommunityPicture = avatar.ToString(),
                                CommunityType = "Pages",
                                CommunityTypeID = selectedCatigories,
                                CommunityTypeLabel = Picker_Category.SelectedItem.ToString(),
                                //CommunityUrl = url
                            });
                        }
                        await Navigation.PopAsync(true);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.ShowError(AppResources.Label_Error);
                        await Navigation.PopAsync(true);
                        // await DisplayAlert("Create", result, AppResources.Label_OK);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Error);
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void UploadAvatar_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync();
                if (file == null)
                    return;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_CannotUpload, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
                }
                else
                {
                    var streamReader = new StreamReader(file.GetStream());
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                    }
                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        UploadPhoto(file.GetStream(), file.Path, MimeTipe, "avatar").ConfigureAwait(false);
                    });

                    Avatarimage = file.Path;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void UploadCover_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync();
                if (file == null)
                    return;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_CannotUpload, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
                }
                else
                {
                    var streamReader = new StreamReader(file.GetStream());
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                    }
                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        UploadPhoto(file.GetStream(), file.Path, MimeTipe, "cover").ConfigureAwait(false);
                    });

                    Coverimage = file.Path;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> UploadPhoto(Stream stream, string image, string Mimetype, string image_type)
        {
            try
            {
                string Imagename = image.Split('/').Last();
                StreamContent scontent = new StreamContent(stream);
                scontent.Headers.ContentDisposition = new ContentDispositionHeaderValue("image")
                {
                    FileName = Imagename,
                    Name = "image"
                };
                scontent.Headers.ContentType = new MediaTypeHeaderValue(Mimetype);

                var dictionary = new Dictionary<string, string>
                {
                    {"user", "test"},
                };
                var dataavatar = JsonConvert.SerializeObject(dictionary);

                var client = new HttpClient();
                var multi = new MultipartFormDataContent();
                var values = new[]
                {
                    new KeyValuePair<string, string>("user_id", Settings.User_id),
                    new KeyValuePair<string, string>("s", Settings.Session),
                    new KeyValuePair<string, string>("page_id", page_id),
                    new KeyValuePair<string, string>("type", "avatar_cover"),
                    new KeyValuePair<string, string>("image_type", image_type), // avatar || cover
                    //new KeyValuePair<string, Stream>("image", stream),
                    new KeyValuePair<string, string>("data", dataavatar),
                };
                foreach (var keyValuePair in values)
                {
                    multi.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                }
                multi.Add(scontent);
                client.BaseAddress = new Uri(Settings.Website);
                var result = client.PostAsync("/app_api.php?application=phone&type=u_page", multi).Result;
                string json = await result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                string apiStatus = data["api_status"].ToString();
                if (apiStatus == "200")
                {
                    return AppResources.Label3_Success;
                }
                else
                {
                    return AppResources.Label_Error;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }
    }
}