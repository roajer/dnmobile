﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.Media;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.Update_Comunities
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Update_Groups_Pages : ContentPage
    {

        public string selectedCatigories = "";
        List<string> ListOfValues = new List<string>();
        public static string group_id;
        public static string Coverimage;
        public static string Avatarimage;

        private ObservableCollection<Profile.Group> GroupDataList = new ObservableCollection<Profile.Group>();

        public Update_Groups_Pages(ObservableCollection<Profile.Group> groupList)
        {
            try
            {
                InitializeComponent();

                //Get Catigories
                ListOfValues = Settings.AddIemsCatigorieses;
                if (ListOfValues.Count > 0)
                    Picker_Category.ItemsSource = ListOfValues;

                GroupDataList = groupList;
            
                if (GroupDataList.Count > 0)
                {
                    var data = GroupDataList.FirstOrDefault();
                    if (data != null)
                    {
                        group_id = data.G_id;
                        Txt_Group_name.Text = data.G_group_name;
                        Txt_Group_title.Text = data.G_group_title;
                        Picker_Category.SelectedIndex = Convert.ToInt32(data.G_category_id) - 1;
                        Editor_About.Text = data.G_about;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CategoryPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var Picker_Category = (Picker)sender;
                if (Picker_Category != null)
                {
                    var selectedValue = Picker_Category.SelectedIndex;
                    selectedCatigories = selectedValue.ToString();
                    //selectedCatigories = ListOfValues.Keys.FirstOrDefault(a => a.Contains(selectedValue.ToString()));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Editor_About_OnTextChanged(object sender, TextChangedEventArgs e)
        {

        }

        //update_Pages API
        public static async Task<string> update_groups_Http(string data_group)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("group_id", group_id),
                        new KeyValuePair<string, string>("type", "general_settings"),
                        new KeyValuePair<string, string>("data", data_group),
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=u_group", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "success";
                    }
                    else
                    {
                        return "Error Update group";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "Error Update group";
            }
        }

        private async void Btn_Update_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }
                else
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);

                    var dictionary = new Dictionary<string, string>
                    {
                        {"group_name", Txt_Group_name.Text},
                        {"group_title", Txt_Group_title.Text},
                        {"about", Editor_About.Text},
                        {"group_category", selectedCatigories},
                    };

                    var result = await update_groups_Http(JsonConvert.SerializeObject(dictionary));
                    if (!String.IsNullOrEmpty(result))
                    {
                        if (result.Contains("Error"))
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowError(AppResources.Label_Error);
                        }
                        else
                        {
                            UserDialogs.Instance.HideLoading();
                            UserDialogs.Instance.ShowSuccess(AppResources.Label2_Done);

                            var data = GroupsListPage.GroupsListItems.FirstOrDefault(a => a.CommunityID == group_id);
                            if (data != null)
                            {
                                data.CommunityID = group_id;
                                data.CommunityName = Txt_Group_name.Text;
                                data.CommunityType = "Groups";
                                //data.CommunityPicture = avatar,
                                //data.ImageUrlString = avatar
                                data.CommunityTypeID = selectedCatigories;
                                data.CommunityTypeLabel = Picker_Category.SelectedItem.ToString();
                            }

                            var CO_Data = new CommunitiesFunction();
                            CO_Data.InsertCommunities(new CommunitiesDB()
                            {
                                CommunityID = group_id,
                                CommunityName = Txt_Group_name.Text,
                                //CommunityPicture = avatar.ToString(),
                                CommunityType = "Groups",
                                CommunityTypeID = selectedCatigories,
                                CommunityTypeLabel = Picker_Category.SelectedItem.ToString(),
                                //CommunityUrl = url
                            });
                        }
                        await Navigation.PopAsync(true);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.ShowError(AppResources.Label_Error);
                        await Navigation.PopAsync(true);
                        // await DisplayAlert("Create", result, AppResources.Label_OK);
                    }
                }
            }
            catch (Exception ex)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Error);
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void UploadAvatar_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync();
                if (file == null)
                    return;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_CannotUpload, AppResources.Label_CheckYourInternetConnection,AppResources.Label_OK);
                }
                else
                {
                    var streamReader = new StreamReader(file.GetStream());
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                    }
                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        UploadPhoto(file.GetStream(), file.Path, MimeTipe, "avatar").ConfigureAwait(false);
                    });

                    Avatarimage = file.Path;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void UploadCover_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image,AppResources.Label_OK);
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync();
                if (file == null)
                    return;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_CannotUpload, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
                }
                else
                {
                    var streamReader = new StreamReader(file.GetStream());
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                    }
                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        UploadPhoto(file.GetStream(), file.Path, MimeTipe, "cover").ConfigureAwait(false);
                    });

                    Coverimage = file.Path;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> UploadPhoto(Stream stream, string image, string Mimetype, string image_type)
        {
            try
            {
                string Imagename = image.Split('/').Last();
                StreamContent scontent = new StreamContent(stream);
                scontent.Headers.ContentDisposition = new ContentDispositionHeaderValue("image")
                {
                    FileName = Imagename,
                    Name = "image"
                };
                scontent.Headers.ContentType = new MediaTypeHeaderValue(Mimetype);

                var dictionary = new Dictionary<string, string>
                {
                    {"user", "test"},
                };
                var dataavatar = JsonConvert.SerializeObject(dictionary);

                var client = new HttpClient();
                var multi = new MultipartFormDataContent();
                var values = new[]
                {
                    new KeyValuePair<string, string>("user_id", Settings.User_id),
                    new KeyValuePair<string, string>("s", Settings.Session),
                    new KeyValuePair<string, string>("group_id", group_id),
                    new KeyValuePair<string, string>("type", "avatar_cover"),
                    new KeyValuePair<string, string>("image_type", image_type), // avatar || cover
                    //new KeyValuePair<string, Stream>("image", stream),
                    new KeyValuePair<string, string>("data", dataavatar),
                };
                foreach (var keyValuePair in values)
                {
                    multi.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                }
                multi.Add(scontent);
                client.BaseAddress = new Uri(Settings.Website);
                var result = client.PostAsync("/app_api.php?application=phone&type=u_group", multi).Result;
                string json = await result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                string apiStatus = data["api_status"].ToString();
                if (apiStatus == "200")
                {
                    return AppResources.Label3_Success;
                }
                else
                {
                    return AppResources.Label_Error;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }
    }
}