﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class AddProductPage : ContentPage
    {
        #region classes

        public class Catatgorytems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public ImageSource Image { get; set; }
            public Stream ImageStream { get; set; }
            public string ImageFullPath { get; set; }
        }

        #endregion

        #region list and Variables

        public static ObservableCollection<Catatgorytems> ActivityListItems = new ObservableCollection<Catatgorytems>();
        public string selectedCatigories = "";
        List<string> ListOfValues = new List<string>();

        public static string ProductName = "";
        public static string ProductLocation = "";
        public static string ProductPrice = "";
        public static string ProductDescription = "";
        public static string PikedImage = "";

        #endregion

        public AddProductPage()
        {
            try
            {
                InitializeComponent();

                ActivityList.ItemsSource = ActivityListItems;

                if (ActivityListItems.Count == 0)
                {
                    ActivityListItems.Add(new Catatgorytems()
                    {
                        Label = AppResources.Label_Pick_Take_Photo,
                        Icon = "\uf03e",
                    });
                }

                ListOfValues = Settings.AddIemsMarket;
                if (ListOfValues.Count > 0)
                    Picker_Category.ItemsSource = ListOfValues;

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void UploudButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                ProductName = ProductNameEntry.Text;
                ProductLocation = locationEntry.Text;
                ProductPrice = priceEntry.Text;
                ProductDescription = ProductDescriptionEntry.Text;
                // Navigation.PushAsync(new AddProduct_CategoryPage());

                var ProductAttach = AddProductPage.ActivityListItems.FirstOrDefault(a => a.Label == AddProductPage.PikedImage);
                if (ProductAttach != null)
                {
                    Task.Factory.StartNew(() =>
                    {
                        DependencyService.Get<IMethods>().CreateProduct(ProductAttach.ImageStream, ProductAttach.ImageFullPath, Settings.User_id, AddProductPage.ProductName, selectedCatigories, AddProductPage.ProductDescription, AddProductPage.ProductPrice, AddProductPage.ProductLocation, "New");
                    });

                    if (MarketPage.IsPageInNavigationStack<AddProductPage>(Navigation))
                    {
                        MarketPage.Productlist.Insert(0, new Products
                        {
                            Name = AddProductPage.ProductName,
                            Description = AddProductPage.ProductDescription,
                            Image = ImageSource.FromFile(ProductAttach.ImageFullPath),
                            Price = AddProductPage.ProductPrice,
                            ThumbnailHeight = "100",
                            Manufacturer = "You",
                        });
                    }

                    UserDialogs.Instance.ShowSuccess(AppResources.Label_Product_Added, 2500);
                    AddProductPage.ActivityListItems.Clear();
                    Navigation.PopAsync(false).Wait(200);

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActivityList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ActivityList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ActivityList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Catatgorytems;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_Pick_Take_Photo)
                    {
                        var action = await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null, AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                        if (action == AppResources.Label_Choose_from_Galery)
                        {
                            await CrossMedia.Current.Initialize();
                            if (!CrossMedia.Current.IsPickPhotoSupported)
                            {
                                await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                                return;
                            }
                            var file = await CrossMedia.Current.PickPhotoAsync();

                            if (file == null)
                                return;

                            ActivityList.IsVisible = true;
                            if (ActivityListItems.Count > 0)
                            {
                                ActivityListItems.Clear();
                            }
                            PikedImage = file.Path.Split('/').Last();
                            ActivityListItems.Add(new Catatgorytems
                            {
                                Label = file.Path.Split('/').Last(),
                                Icon = "\uf030",
                                ImageFullPath = file.Path,
                                ImageStream = file.GetStream()
                            });

                            ActivityList.ItemsSource = ActivityListItems;

                        }
                        else if (action == AppResources.Label_Take_a_Picture)
                        {
                            await CrossMedia.Current.Initialize();

                            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                            {
                                await DisplayAlert(AppResources.Label2_No_Camera, AppResources.Label_No_camera_avaialble, AppResources.Label_OK);
                                return;
                            }
                            var time = DateTime.Now.ToString("hh:mm");
                            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                            {
                                PhotoSize = PhotoSize.Medium,
                                CompressionQuality = 92,
                                SaveToAlbum = true,
                                Name = time + "PictureProduct.jpg"
                            });

                            if (file == null)
                                return;

                            ActivityList.IsVisible = true;
                            if (ActivityListItems.Count > 0)
                            {
                                ActivityListItems.Clear();
                            }
                            PikedImage = file.Path.Split('/').Last();
                            ActivityListItems.Add(new Catatgorytems
                            {
                                Label = file.Path.Split('/').Last(),
                                Icon = "\uf030",
                                ImageFullPath = file.Path,
                                ImageStream = file.GetStream()
                            });

                            ActivityList.ItemsSource = ActivityListItems;
                        }
                    }
                    else
                    {
                        try
                        {
                            var Function = await DisplayAlert(AppResources.Label_Question, AppResources.Label_Do_you_want_to_add_new_image, AppResources.Label_Yes, AppResources.Label_NO);
                            if (Function)
                            {
                                ActivityListItems.Clear();
                                var action =
                                    await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null, AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                                if (action == AppResources.Label_Choose_from_Galery)
                                {
                                    await CrossMedia.Current.Initialize();
                                    if (!CrossMedia.Current.IsPickPhotoSupported)
                                    {
                                        await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                                        return;
                                    }
                                    var file = await CrossMedia.Current.PickPhotoAsync();

                                    if (file == null)
                                        return;

                                    var streamReader = new StreamReader(file.GetStream());
                                    var bytes = default(byte[]);
                                    using (var memstream = new MemoryStream())
                                    {
                                        streamReader.BaseStream.CopyTo(memstream);
                                        bytes = memstream.ToArray();
                                    }
                                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);


                                    ActivityList.IsVisible = true;
                                    if (ActivityListItems.Count > 0)
                                    {
                                        ActivityListItems.Clear();
                                    }
                                    PikedImage = file.Path.Split('/').Last();
                                    ActivityListItems.Add(new Catatgorytems
                                    {
                                        Label = file.Path.Split('/').Last(),
                                        Icon = "\uf030",
                                        ImageFullPath = file.Path,
                                        ImageStream = file.GetStream()

                                    });

                                    ActivityList.ItemsSource = ActivityListItems;

                                }
                                else if (action == AppResources.Label_Take_a_Picture)
                                {
                                    await CrossMedia.Current.Initialize();

                                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                                    {
                                        await DisplayAlert(AppResources.Label2_No_Camera,AppResources.Label_No_camera_avaialble, AppResources.Label_OK);
                                        return;
                                    }
                                    var time = DateTime.Now.ToString("hh:mm");
                                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                                    {
                                        PhotoSize = PhotoSize.Medium,
                                        CompressionQuality = 92,
                                        SaveToAlbum = true,
                                        Name = time + "PictureProduct.jpg"
                                    });

                                    if (file == null)
                                        return;

                                    ActivityList.IsVisible = true;
                                    if (ActivityListItems.Count > 0)
                                    {
                                        ActivityListItems.Clear();
                                    }
                                    PikedImage = file.Path.Split('/').Last();
                                    ActivityListItems.Add(new Catatgorytems
                                    {
                                        Label = file.Path.Split('/').Last(),
                                        Icon = "\uf030",
                                        ImageFullPath = file.Path,
                                        ImageStream = file.GetStream()
                                    });
                                    ActivityList.ItemsSource = ActivityListItems;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ProductDescriptionEntry_OnFocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (ProductDescriptionEntry.Text == AppResources.Label_Product_Description)
                {
                    ProductDescriptionEntry.Text = "";
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Picker_Category_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var Picker_Category = (Picker)sender;
                if (Picker_Category != null)
                {
                    var selectedValue = Picker_Category.SelectedIndex;
                    selectedCatigories = selectedValue.ToString();
                    //selectedCatigories = ListOfValues.Keys.FirstOrDefault(a => a.Contains(selectedValue.ToString()));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
