﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.Update_Comunities;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class SocialGroup : ContentPage
    {
        #region classes
        public class GroupInfoitems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Color { get; set; }
        }

        #endregion

        #region Variables and list

        public static ObservableCollection<GroupInfoitems> GroupInfoListItems = new ObservableCollection<GroupInfoitems>();
        public static ObservableCollection<Profile.Group> GroupDataList = new ObservableCollection<Profile.Group>();

        public static string G_GroupID = "";
        public static string S_Name = "";
        public static string S_Username = "";
        public static string S_Category = "";
        public static string S_About = "";
        public static string S_URL = "";
        public static string S_Cover = "";
        public static string S_Avatar = "";
        public static string S_Group_Title = "";
        public static string S_privacy = "";
        public static string S_join_privacy = "";
        public static string S_active = "";
        public static string S_registered = "";
        public static string S_category_id = "";
        public static string S_post_count = "";
        public static string S_is_owner = "";

        #endregion

        public SocialGroup(string GroupID, string GroupType)
        {
            try
            {
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
                    ToolbarItems.Clear();
				}
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }
				
                G_GroupID = GroupID;
                CoverImage.BackgroundColor = Color.FromHex(Settings.MainColor);
                FirstGridOfprofile.BackgroundColor = Color.FromHex(Settings.MainColor);

                if (GroupType == "Joined")
                {
                    ActionButton.Text = AppResources.Label_Joined;
                }
                else
                {
                    ActionButton.Text = AppResources.Label_Join_Group;
                }
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    GetGroupData(GroupID).ConfigureAwait(false);
                    PostWebLoader.Source = Settings.Website + "/get_news_feed?group_id=" + GroupID;
                    PostWebLoader.OnJavascriptResponse += OnJavascriptResponse;
                    PostWebLoader.RegisterCallback("type", (str) => { });
                }
                else
                {
                    this.Title = AppResources.Label_Connection_Lost;
                    HeaderLabel.Text = AppResources.Label_Connection_Lost;
                    //PostWebLoader.Source = Settings.HTML_LoadingPost_Page;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public static async Task<string> AddUnAddRequest(string GroupID)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("group_id", GroupID),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=join_group",formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";

                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        public void PostAjaxRefresh(string type)
        {
            try
            {
                if (type == "Hashtag")
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewHashTagPosts();");
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewPosts();");
                    });

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> GetGroupData(string Pageid)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("group_profile_id", G_GroupID),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_group_data", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        JObject userdata = JObject.FromObject(data["group_data"]);
                        var group_id = userdata["group_id"].ToString();
                        var group_name = userdata["group_name"].ToString();
                        S_Group_Title = userdata["group_title"].ToString();
                        S_Avatar = userdata["avatar"].ToString();
                        S_Cover = userdata["cover"].ToString();
                        S_About = Functions.StringNullRemover(Functions.DecodeString(userdata["about"].ToString()));
                        S_Category = userdata["category"].ToString();
                        S_privacy = userdata["privacy"].ToString();
                        S_join_privacy = userdata["join_privacy"].ToString();
                        S_active = userdata["active"].ToString();
                        S_registered = userdata["registered"].ToString();
                        S_URL = userdata["url"].ToString();
                        S_Name = userdata["name"].ToString();
                        S_category_id = userdata["category_id"].ToString();
                        S_Username = userdata["username"].ToString();
                        S_post_count = userdata["post_count"].ToString();
                        var Isjoined = userdata["is_joined"].ToString();
                        S_is_owner = userdata["is_owner"].ToString();

                        GroupTitle.Text = S_Group_Title;
                        AboutMini.Text = S_Category;

                        if (S_is_owner == "True" || S_is_owner == "true")
                        {
                            LabelEditData.IsVisible = true;
                        }
                        else
                        {
                            LabelEditData.IsVisible = false;
                        }

                        GroupDataList.Clear();

                        Profile.Group Data_Group = new Profile.Group();

                        Data_Group.G_id = group_id;
                        Data_Group.G_user_id = Settings.User_id;
                        Data_Group.G_group_name = group_name;
                        Data_Group.G_group_title = S_Group_Title;
                        Data_Group.G_avatar = S_Avatar;
                        Data_Group.G_cover = S_Cover;
                        Data_Group.G_about = S_About;
                        Data_Group.G_category = S_Category;
                        Data_Group.G_privacy = S_privacy;
                        Data_Group.G_join_privacy = Isjoined;
                        Data_Group.G_active = S_active;
                        Data_Group.G_registered = S_registered;
                        Data_Group.G_group_id = group_id;
                        Data_Group.G_url = S_URL;
                        Data_Group.G_name = S_Name;
                        Data_Group.G_category_id = S_category_id;
                        Data_Group.G_username = S_Username;

                        GroupDataList.Add(Data_Group);

                        if (Isjoined == "True")
                        {
                            ActionButton.Text = AppResources.Label_Joined;
                        }

                        var Post_Count = userdata["post_count"].ToString();
                        if (Post_Count == "0")
                        {
                            ButtonStacklayot.IsVisible = false;
                            PostWebLoader.HeightRequest = 330;
                        }
                        else if (Post_Count == "1")
                        {
                            PostWebLoader.HeightRequest = 760;
                        }
                        else if (Post_Count == "2")
                        {
                            PostWebLoader.HeightRequest = 870;
                        }

                        else if (Post_Count == "3")
                        {
                            PostWebLoader.HeightRequest = 1040;
                        }
                        else if (Post_Count == "4")
                        {
                            PostWebLoader.HeightRequest = 1250;
                        }
                        else if (Post_Count == "5")
                        {
                            PostWebLoader.HeightRequest = 1350;
                        }
                        else if (Post_Count == "6")
                        {
                            PostWebLoader.HeightRequest = 1550;
                        }
                        else if (Post_Count == "7")
                        {
                            PostWebLoader.HeightRequest = 1750;
                        }
                        else if (Post_Count == "8")
                        {
                            PostWebLoader.HeightRequest = 2050;
                        }
                        CoverImage.Source = new UriImageSource {Uri = new Uri(S_Cover)};
                        AvatarImage.Source = new UriImageSource {Uri = new Uri(S_Avatar)};
                        if (S_About == "")
                        {
                            GroupInfoListItems.Add(new GroupInfoitems()
                            {
                                Label = AppResources.Label_No_Description,
                                Icon = "\uf0a1",
                                Color = "#c5c9c8"
                            });
                        }
                        else if (S_About != "")
                        {
                            if (S_About.Length > 30)
                            {
                                GroupInfoList.HeightRequest = 45;
                                GroupInfoList.MinimumHeightRequest = 45;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }
                            if (S_About.Length > 60)
                            {
                                GroupInfoList.HeightRequest = 65;
                                GroupInfoList.MinimumHeightRequest = 65;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }
                            if (S_About.Length > 150)
                            {
                                GroupInfoList.HeightRequest = 75;
                                GroupInfoList.MinimumHeightRequest = 75;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }
                            if (S_About.Length > 250)
                            {
                                GroupInfoList.HeightRequest = 105;
                                GroupInfoList.MinimumHeightRequest = 100;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }

                            if (S_About.Length > 350)
                            {
                                GroupInfoList.HeightRequest = 145;
                                GroupInfoList.MinimumHeightRequest = 130;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }

                            if (S_About.Length > 450)
                            {
                                GroupInfoList.HeightRequest = 165;
                                GroupInfoList.MinimumHeightRequest = 140;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }

                            if (S_About.Length > 550)
                            {
                                GroupInfoList.HeightRequest = 195;
                                GroupInfoList.MinimumHeightRequest = 170;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }

                            if (S_About.Length > 650)
                            {
                                GroupInfoList.HeightRequest = 210;
                                GroupInfoList.MinimumHeightRequest = 170;
                                GroupInfoListItems.Add(new GroupInfoitems()
                                {
                                    Label = S_About,
                                    Icon = "\uf0a1",
                                    Color = "#c5c9c8"
                                });
                            }

                        }

                        GroupInfoList.ItemsSource = GroupInfoListItems;
                        this.Title = S_Name;
                        HeaderLabel.Text = S_Name;

                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }

        private void CopyUrlButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Button1.Text = "\uf00c";
                DependencyService.Get<IClipboardService>().CopyToClipboard(S_URL);
                DependencyService.Get<IMessage>().LongAlert(AppResources.Label3_Copyed_URL);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void GroupInfoList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                GroupInfoList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void GroupInfoList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
           
        }

        private  void ActionButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (ActionButton.Text == AppResources.Label_Join_Group)
                    {
                        ActionButton.Text = AppResources.Label_Joined;
                        using (var data = new CommunitiesFunction())
                        {
                            if (G_GroupID != "" && S_URL != "")
                            {
                                DependencyService.Get<IPicture>().SavePictureToDisk(S_Avatar, G_GroupID);
                                data.InsertCommunities(new CommunitiesDB()
                                {
                                    CommunityID = G_GroupID,
                                    CommunityName = S_Group_Title,
                                    CommunityPicture = S_Avatar,
                                    CommunityType = "Groups",
                                    CommunityTypeLabel = AppResources.Label_Groups,
                                    CommunityUrl = S_URL
                                });
                            }

                        }
                    }
                    else if (ActionButton.Text == AppResources.Label_Joined)
                    {
                        ActionButton.Text = AppResources.Label_Join;
                        using (var data = new CommunitiesFunction())
                        {
                            var Community = data.GetCommunityByID(G_GroupID);
                            if (Community.CommunityID != "")
                            {
                                if (Community.CommunityID != null)
                                {
                                    DependencyService.Get<IPicture>().DeletePictureFromDisk(Community.CommunityPicture, Community.CommunityID);
                                    data.DeleteCommunitiesRow(Community);
                                }
                            }
                        }
                    }
                    AddUnAddRequest(G_GroupID).ConfigureAwait(false);
                }
                else
                {
                    DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SocialGroup_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                GroupInfoListItems.Clear();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ShowmoreButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new HyberdPostViewer("Group", ""));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnJavascriptResponse(JavascriptResponseDelegate EventObj)
        {
            try
            {
                if (EventObj.Data.Contains("type"))
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(EventObj.Data);
                    string type = data["type"].ToString();
                    if (type == "user")
                    {
                        string Userid = data["profile_id"].ToString();
                        if (WowonderPhone.Settings.User_id == Userid)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new MyProfilePage());
                            });
                        }
                        else
                        {
                            InjectedJavaOpen_UserProfile(Userid);
                        }

                    }
                    else if (type == "lightbox")
                    {
                        string ImageSource = data["image_url"].ToString();
                        if (Settings.ShowAndroidDefaultImageViewer)
                        {
                            if (Device.RuntimePlatform == Device.iOS)
                            {
                                var Image = new UriImageSource
                                {
                                    Uri = new Uri(ImageSource),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(2, 0, 0, 0)
                                };
                                InjectedJavaOpen_OpenImage(Image);
                            }
                            else
                            {
                                UserDialogs.Instance.ShowLoading(AppResources.Label_Please_Wait);
                                var dataViewer = DependencyService.Get<IMethods>();
                                dataViewer.showPhoto(ImageSource);
                            }
                        }
                        else
                        {
                            var Image = new UriImageSource
                            {
                                Uri = new Uri(ImageSource),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };
                            InjectedJavaOpen_OpenImage(Image);
                        }
                    }
                    else if (type == "mention")
                    {
                        string user_id = data["user_id"].ToString();
                        InjectedJavaOpen_UserProfile(user_id);
                    }
                    else if (type == "hashtag")
                    {
                        string hashtag = data["tag"].ToString();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushModalAsync(new HyberdPostViewer("Hashtag", hashtag));
                        });
                    }
                    else if (type == "url")
                    {
                        string link = data["link"].ToString();

                        InjectedJavaOpen_PostLinks(link);
                    }
                    else if (type == "page")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_LikePage(Id);
                    }
                    else if (type == "group")
                    {
                        string Id = data["profile_id"].ToString();

                        if (G_GroupID != Id)
                        {
                            InjectedJavaOpen_Group(Id);
                        }

                    }
                    else if (type == "post_wonders" || type == "post_likes")
                    {
                        string Id = data["post_id"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Like_Wonder_Viewer_Page(Id, type));
                        });
                    }
                    else if (type == "delete_post")
                    {
                        string Id = data["post_id"].ToString();

                        var Qussion = await DisplayAlert(AppResources.Label_Question,
                            AppResources.Label_Would_You_like_to_delete_this_post, AppResources.Label_Yes,
                            AppResources.Label_NO);
                        if (Qussion)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                PostWebLoader.InjectJavascript(
                                    "$('#post-' + " + Id + ").slideUp(200, function () { $(this).remove();}); ");
                            });

                            Post_Manager("delete_post", Id).ConfigureAwait(false);
                        }
                    }
                    else if (type == "publisher-box")
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new AddPost_On_Communities(this, null, "Group", G_GroupID));
                        });

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Post_Manager(string Type, string postid)
        {
            try
            {
                var Action = " ";
                if (Type == "edit_post")
                {
                    Action = "edit";
                }
                else
                {
                    Action = "delete";
                }
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", WowonderPhone.Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s",WowonderPhone.Settings.Session),
                        new KeyValuePair<string, string>("action",Action),

                    });

                    var response =await client.PostAsync(WowonderPhone.Settings.Website + "/app_api.php?application=phone&type=post_manager",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (Type == "edit_post")
                        {
                            Action = "edit";
                        }
                        else
                        {

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_UserProfile(string Userid)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserProfilePage(Userid, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        public void InjectedJavaOpen_OpenImage(ImageSource Image)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushModalAsync(new ImageFullScreenPage(Image));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        public void InjectedJavaOpen_PostLinks(string link)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Device.OpenUri(new Uri(link));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        public void InjectedJavaOpen_Hashtag(string word)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", word));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        public void InjectedJavaOpen_LikePage(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialPageViewer(id, "", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        public void InjectedJavaOpen_Group(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialGroup("id", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    
        private void PostWebLoader_OnOnNavigationError(NavigationErrorDelegate eventobj)
        {
            try
            {
                PostWebLoader.IsVisible = false;
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnContentLoaded(ContentLoadedDelegate eventobj)
        {
            try
            {
                PostWebLoader.RegisterCallback("type", (str) => { });

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private NavigationRequestedDelegate PostWebLoader_OnOnNavigationStarted(NavigationRequestedDelegate eventobj)
        {
            try
            {
                if (eventobj.Uri.Contains(WowonderPhone.Settings.Website))
                {
                    return eventobj;
                }
                else
                {
                    eventobj.Cancel = true;
                    return eventobj;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return eventobj;
            }

        }

        private void AddPost_OnClicked_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new AddPost_On_Communities(this, null, "Group", G_GroupID));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void OnScroll(object sender, ScrolledEventArgs e)
        {
            try
            {
                var Header = CoverImage.Height * 2;
                //
                var scrollRegion = layeringGrid.Height - outerScrollView.Height;
                var parallexRegion = Header - FirstGridOfprofile.Height;
                var factor = outerScrollView.ScrollY - parallexRegion * (outerScrollView.ScrollY / scrollRegion);
                CoverImage.TranslationY = factor;

                CoverImage.Opacity = 1 - (factor / (FirstGridOfprofile.Height - 10) * 2);
                CoverImage.BackgroundColor = Color.FromHex(Settings.MainColor);
                headers.Scale = 1 - ((factor) / (Header * 2));

                AvatarImage.TranslationY = factor;
                AvatarImage.Opacity = 1 - (factor / (FirstGridOfprofile.Height - 10) * 2);
                //AvatarImage.BackgroundColor = Color.FromHex(Settings.MainColor);

                if (CoverImage.Opacity == 0)
                {
                    HeaderOfpage.BackgroundColor = Color.FromHex(Settings.MainColor);
                    HeaderLabel.IsVisible = true;
                }
                else
                {
                    if (HeaderLabel.IsVisible)
                    {
                        HeaderLabel.IsVisible = false;
                        HeaderOfpage.BackgroundColor = Color.Transparent;
                    }
                }
                //PostWebLoader.Scale = 1 - ((factor) / (Header * 2));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                outerScrollView.Scrolled += OnScroll;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
           
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            outerScrollView.Scrolled -= OnScroll;
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.PopModalAsync(true);
            }
        }

        private async void EditData_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (S_is_owner == "True" || S_is_owner == "true")
                {
                   await Navigation.PushAsync(new Update_Groups_Pages(GroupDataList));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SocialGroup_OnAppearing(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Update_Groups_Pages.Coverimage))
            {
                CoverImage.Source = Update_Groups_Pages.Coverimage;
            }
            if (!String.IsNullOrEmpty(Update_Groups_Pages.Avatarimage))
            {
                AvatarImage.Source = Update_Groups_Pages.Avatarimage;
            }

            var data = GroupsListPage.GroupsListItems.FirstOrDefault(a => a.CommunityID == G_GroupID);
            if (data !=null)
            {
                GroupTitle.Text = data.CommunityName;
                AboutMini.Text = data.CommunityTypeLabel;
            }
        }
    }
}