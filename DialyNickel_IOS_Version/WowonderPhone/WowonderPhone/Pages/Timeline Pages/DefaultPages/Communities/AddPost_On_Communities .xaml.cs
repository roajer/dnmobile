﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.Media;
using Plugin.Media.Abstractions;
using PropertyChanged;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.AddPostNavPages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages
{
    public partial class AddPost_On_Communities : ContentPage
    {
        #region classes
        public class PostItems
        {
            public string Label { get; set; }
            public ImageSource Icon { get; set; }
        }

        [ImplementPropertyChanged]
        public class Activitytems
        {
            public string Label { get; set; }
            public string Content { get; set; }
            public string Icon { get; set; }
            public string TypePost { get; set; }
            public ImageSource Image { get; set; }
            public Stream Stream { get; set; }
            public string Filepath { get; set; }
        }


        #endregion

        #region  Lists Items Declaration and Variables

        public static ObservableCollection<PostItems> PostListItems = new ObservableCollection<PostItems>();

        public static ObservableCollection<Activitytems> ActivityListItems = new ObservableCollection<Activitytems>();

        public static string Contenttextcach = "";
        private SocialGroup Social_Group_Page;
        private SocialPageViewer SocialPageViewer_Page;
        private string Type_page_and_kind;
        private string Community_id;

        #endregion

        public static bool IsPageInNavigationStack<TPage>(INavigation navigation) where TPage : Page
        {
            try
            {
                if (navigation.NavigationStack.Count > 1)
                {
                    var last = navigation.NavigationStack[navigation.NavigationStack.Count - 2];

                    if (last is TPage)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return false;
            }
        }

        public AddPost_On_Communities(SocialGroup SocialGroupPage, SocialPageViewer SocialPagePage, string Type_page,string id)
        {
            try
            {
               
                InitializeComponent();

				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

                Social_Group_Page = SocialGroupPage;
                SocialPageViewer_Page = SocialPagePage;
                Community_id = id;
                Type_page_and_kind = Type_page;

                if (Type_page_and_kind == "Page")
                {
                    PostToLabel.Text = AppResources.Label2_Post_To + " " + AppResources.Label2_Page;
                }
                else
                {
                    PostToLabel.Text = AppResources.Label2_Post_To + " " + AppResources.Labe2_Group;
                }

                PostListItems.Clear();

                if (Contenttextcach != "")
                {
                    PostContent.Text = Contenttextcach;
                }

                ActivityList.ItemsSource = ActivityListItems;

                if (PostListItems.Count == 0)
                {
                    PostListItems.Add(new PostItems()
                    {
                        Label = AppResources.Label_Pick_Take_Photo,
                        Icon = Settings.Post_Picture_Icon,
                    });

                    PostListItems.Add(new PostItems()
                    {
                        Label = AppResources.Label_Pick_Video_Recourd,
                        Icon = Settings.Post_Video_Icon,
                    });

                    PostListItems.Add(new PostItems()
                    {
                        Label = AppResources.Label_Feeling_Activity,
                        Icon = Settings.Post_Feeling_Icon,
                    });
                }
                PostList.ItemsSource = PostListItems;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostContent_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                PostContent.TextColor = Color.FromHex("#888");
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                PostList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void PostList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as PostItems;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_Pick_Take_Photo)
                    {
                        var action = await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null,
                            AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                        if (action == AppResources.Label_Choose_from_Galery)
                        {
                            await CrossMedia.Current.Initialize();
                            if (!CrossMedia.Current.IsPickPhotoSupported)
                            {
                                await DisplayAlert("Oops", "You Cannot pick an image", AppResources.Label_OK);
                                return;
                            }
                            var file = await CrossMedia.Current.PickPhotoAsync();

                            if (file == null)
                                return;

                            ActivityList.IsVisible = true;
                            if (ActivityListItems.Count() > 0)
                            {
                                ActivityListItems.Clear();
                            }

                            ActivityListItems.Add(new Activitytems()
                            {
                                Label = file.Path.Split('/').Last(),
                                Icon = "\uf030",
                                TypePost = "Image",
                                Stream = file.GetStream(),
                                Filepath = file.Path
                            });

                            ActivityList.ItemsSource = ActivityListItems;

                        }
                        else if (action == AppResources.Label_Take_a_Picture)
                        {
                            await CrossMedia.Current.Initialize();

                            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                            {
                                await DisplayAlert("No Camera", ":( No camera avaialble.", AppResources.Label_OK);
                                return;
                            }
                            var time = DateTime.Now.ToString("hh:mm");
                            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                            {
                                PhotoSize = PhotoSize.Medium,
                                CompressionQuality = 92,
                                SaveToAlbum = true,
                                Name = time + "Picture.jpg",

                            });

                            if (file == null)
                                return;

                            ActivityList.IsVisible = true;
                            if (ActivityListItems.Count() > 0)
                            {
                                ActivityListItems.Clear();
                            }

                            ActivityListItems.Add(new Activitytems()
                            {
                                Label = file.Path.Split('/').Last(),
                                Icon = "\uf030",
                                TypePost = "Image",
                                Stream = file.GetStream(),
                                Filepath = file.Path
                            });

                            ActivityList.ItemsSource = ActivityListItems;
                        }

                    }
                    else if (Item.Label == AppResources.Label_Pick_Video_Recourd)
                    {
                        try
                        {
                            var action = await DisplayActionSheet(AppResources.Label_Video, AppResources.Label_Cancel, null,
                                AppResources.Label_Choose_from_Galery, AppResources.Label_Recourd_Video);


                            if (action == AppResources.Label_Choose_from_Galery)
                            {
                                if (CrossMedia.Current.IsPickVideoSupported)
                                {
                                    var file = await CrossMedia.Current.PickVideoAsync();
                                    if (file == null)
                                        return;

                                    if (file.GetStream() != null)
                                    {
                                        ActivityList.IsVisible = true;
                                        if (ActivityListItems.Count() > 0)
                                        {
                                            ActivityListItems.Clear();
                                        }

                                        ActivityListItems.Add(new Activitytems()
                                        {
                                            Label = file.Path.Split('/').Last(),
                                            Icon = "\uf03d",
                                            TypePost = "Video",
                                            Stream = file.GetStream(),
                                            Filepath = file.Path
                                        });

                                        ActivityList.ItemsSource = ActivityListItems;
                                    }
                                }
                            }
                            else if (action == AppResources.Label_Recourd_Video)
                            {
                                await CrossMedia.Current.Initialize();

                                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
                                {
                                    await DisplayAlert("No Camera", ":( No camera avaialble.", AppResources.Label_OK);
                                    return;
                                }

                                var file = await CrossMedia.Current.TakeVideoAsync(new Plugin.Media.Abstractions.StoreVideoOptions
                                    {
                                        Name = "video.mp4",
                                        Directory = "DefaultVideos",
                                        SaveToAlbum = true
                                    });

                                if (file == null)
                                    return;

                                if (file.GetStream() != null)
                                {
                                    ActivityList.IsVisible = true;
                                    if (ActivityListItems.Count() > 0)
                                    {
                                        ActivityListItems.Clear();
                                    }

                                    ActivityListItems.Add(new Activitytems()
                                    {
                                        Label = file.Path.Split('/').Last(),
                                        Icon = "\uf03d",
                                        TypePost = "Video",
                                        Stream = file.GetStream(),
                                        Filepath = file.Path
                                    });

                                    ActivityList.ItemsSource = ActivityListItems;
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                    else if (Item.Label == AppResources.Label_Feeling_Activity)
                    {
                        var action = await DisplayActionSheet(AppResources.Label_Add, AppResources.Label_Cancel, null,AppResources.Label_Your_Feeling, AppResources.Label_Your_Activity);

                        if (action == AppResources.Label_Your_Feeling)
                        {
                            FeelingsPage Feelings_Page = new FeelingsPage();
                            await Navigation.PushAsync(Feelings_Page);
                        }
                        else if (action == AppResources.Label_Your_Activity)
                        {
                            ActivitiesPage Activities_Page = new ActivitiesPage();
                            await Navigation.PushAsync(Activities_Page);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void PostButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Settings.PostPrivacyValue != null)
                    {
                        if (PostContent.Text == "" && ActivityListItems.Count == 0)
                        {
                            UserDialogs.Instance.Alert(AppResources.Label_You_Cannot_Post_an_Empty_Post,AppResources.Label_Error);
                        }
                        else
                        {
                            if (PostContent.Text != "" && ActivityListItems.Count == 0)
                            {
                                await AddPostFunction(PostContent.Text, Settings.PostPrivacyValue, "", "", Community_id);
                                await Navigation.PopAsync(false);
                            }

                            foreach (var Item in ActivityListItems)
                            {
                                if (Item.TypePost == "Traveling")
                                {
                                    await AddPostFunction(PostContent.Text, Settings.PostPrivacyValue, "traveling",Item.Content, Community_id);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }
                                if (Item.TypePost == "Watching")
                                {
                                    await AddPostFunction(PostContent.Text, Settings.PostPrivacyValue, "watching",Item.Content, Community_id);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }
                                if (Item.TypePost == "Playing")
                                {
                                    await AddPostFunction(PostContent.Text, Settings.PostPrivacyValue, "playing",Item.Content, Community_id);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }
                                if (Item.TypePost == "Listening")
                                {
                                    await AddPostFunction(PostContent.Text, Settings.PostPrivacyValue, "Listening",Item.Content, Community_id);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }
                                if (Item.TypePost == "Feelings")
                                {
                                    await AddPostFunction(PostContent.Text, Settings.PostPrivacyValue, "feelings",Item.Content, Community_id);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }

                                if (Item.TypePost == "Image")
                                {
                                    UserDialogs.Instance.Toast(AppResources.Label_Uploading_Image_started);
                                    DependencyService.Get<IMethods>().AddPost_Communities(Item.Stream, Item.Filepath, PostContent.Text, Settings.PostPrivacyValue, "Image", Community_id, Social_Group_Page, SocialPageViewer_Page);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }

                                if (Item.TypePost == "Video")
                                {
                                    UserDialogs.Instance.Toast(AppResources.Label_Uploading_Video_started);
                                     DependencyService.Get<IMethods>().AddPost_Communities(Item.Stream, Item.Filepath, PostContent.Text, Settings.PostPrivacyValue, "Video", Community_id, Social_Group_Page, SocialPageViewer_Page);
                                    ActivityListItems.Clear();
                                    await Navigation.PopAsync(false);
                                    return;
                                }

                            }
                        }
                    }
                }
                else
                {
                    UserDialogs.Instance.ShowError(AppResources.Label_Post_Faild, 2000);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        public async Task AddPostFunction(string TextContent, string Privacy, string feeling_type, string feeling,string ID)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Posting, MaskType.Clear);

                    var formContent = new FormUrlEncodedContent(new[]
                        {new KeyValuePair<string, string>("user_id", Settings.User_id)});

                    if (Type_page_and_kind == "Page")
                    {
                        formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("postText", TextContent),
                            new KeyValuePair<string, string>("s", Settings.Session),
                            new KeyValuePair<string, string>("postPrivacy", Privacy),
                            new KeyValuePair<string, string>("feeling_type", feeling_type),
                            new KeyValuePair<string, string>("feeling", feeling),
                            new KeyValuePair<string, string>("page_id", ID),
                        });
                    }
                    else
                    {
                        formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("postText", TextContent),
                            new KeyValuePair<string, string>("s", Settings.Session),
                            new KeyValuePair<string, string>("postPrivacy", Privacy),
                            new KeyValuePair<string, string>("feeling_type", feeling_type),
                            new KeyValuePair<string, string>("feeling", feeling),
                            new KeyValuePair<string, string>("group_id", ID),
                        });
                    }
                    var response = await client .PostAsync(Settings.Website + "/app_api.php?application=phone&type=new_post", formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        UserDialogs.Instance.HideLoading();

                        if (Type_page_and_kind == "Page")
                        {
                            if (SocialPageViewer_Page != null)
                            {
                                SocialPageViewer_Page.PostAjaxRefresh("Page");
                            }
                        }
                        else
                        {
                            if (Social_Group_Page != null)
                            {
                                Social_Group_Page.PostAjaxRefresh("Group");
                            }
                        }
                        UserDialogs.Instance.ShowSuccess(AppResources.Label_Post_Added, 2000);
                    }
                    else
                    {
                        UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.ShowError(AppResources.Label_Post_Faild, 2000);
                    }
                }
            }
            catch (Exception)
            { 
                UserDialogs.Instance.HideLoading();
            }
        }

        private void ActivityList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ActivityList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ActivityList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var item = e.Item as Activitytems;
                var Function = await DisplayAlert(AppResources.Label_Question, AppResources.Label_Request_removed,
                    AppResources.Label_NO, AppResources.Label_Yes);
                if (Function)
                {
                }
                else
                {
                    ActivityListItems.Clear();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void PrivacyToolBar_OnClicked(object sender, EventArgs e)
        {
            try
            {
                PrivacyAddPostPage PrivacyAddPost_Page = new PrivacyAddPostPage();
                await Navigation.PushAsync(PrivacyAddPost_Page);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddPost_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}