﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using BottomBar.XamarinForms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using WowonderPhone.Pages.Tabs;
using WowonderPhone.Pages.Timeline_Pages.Friend_System;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{

    public partial class NotificationsPage : ContentPage
    {
        public ObservableCollection<Notifications> SeenNotifications = new ObservableCollection<Notifications>();
        public NotificationsPage()
        {
            try
            {
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
                    SearchIcon.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

                if (ItemListPage.FriendRequestsItemsCollection.Count > 0)
                {
                    if (Settings.ConnectivitySystem == "1")
                    {
                        FriendRequestLabel.Text = "( " + ItemListPage.FriendRequestsItemsCollection.Count + " ) " + AppResources.Label_Follow_Requests;
                    }
                    else
                    {
                        FriendRequestLabel.Text = "( " + ItemListPage.FriendRequestsItemsCollection.Count + " ) " + AppResources.Label_Friend_Requests;
                    }
                    FriendGridBanner.IsVisible = true;
                }
                else
                {
                    FriendGridBanner.IsVisible = false;
                }

                NotificationsListview.ItemsSource = ItemListPage.NotificationsList;
                NotificationsHeader.Text = AppResources.Label_Notifications + " (" +ItemListPage.NotificationsList.Count + ") ";
              
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Notifications_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                NotificationsListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void NotificationsListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Notifications Item)
                {
                    if (Item.Type == "following" || Item.Type == "visited_profile" || Item.Type == "accepted_request")
                    {
                        Navigation.PushAsync(new UserProfilePage(Item.User_id, ""));
                    }
                    else if (Item.Type == "liked_page" || Item.Type == "invited_page" || Item.Type == "accepted_invite")
                    {
                        Navigation.PushAsync(new SocialPageViewer(Item.Page_ID, "", ""));
                    }
                    else if (Item.Type == "joined_group" || Item.Type == "accepted_join_request" ||
                             Item.Type == "added_you_to_group")
                    {
                        Navigation.PushAsync(new SocialGroup(Item.Group_ID, ""));
                    }
                    else if (Item.Type == "comment" ||
                             Item.Type == "wondered_post" ||
                             Item.Type == "wondered_comment" ||
                             Item.Type == "wondered_reply_comment" ||
                             Item.Type == "comment_mention" ||
                             Item.Type == "comment_reply_mention" ||
                             Item.Type == "liked_post" ||
                             Item.Type == "liked_comment" ||
                             Item.Type == "liked_reply_comment" ||
                             Item.Type == "post_mention" ||
                             Item.Type == "share_post" ||
                             Item.Type == "comment_reply" ||
                             Item.Type == "also_replied" ||
                             Item.Type == "profile_wall_post")
                    {
                        Navigation.PushAsync(new HyberdPostViewer("Post", Item.Post_ID));
                    }
                    else
                    {
                        Navigation.PushAsync(new HyberdPostViewer("Post", Item.Post_ID));
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Send_View_notifications_Request()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website +"/app_api.php?application=phone&type=get_notifications&seen=view_all",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var Parser = JObject.Parse(json).SelectToken("notifications").ToString();
                        JArray NotificationsArray = JArray.Parse(Parser);
                        foreach (var Notification in NotificationsArray)
                        {

                            JObject Data = JObject.FromObject(Notification);
                            JObject Notifier = JObject.FromObject(Data["notifier"]);
                            var Notifier_id = Data["notifier_id"].ToString();
                            var Post_ID = Data["post_id"].ToString();
                            var Page_ID = Data["page_id"].ToString();
                            var Group_ID = Data["group_id"].ToString();
                            var Count_Notification = JObject.Parse(json).SelectToken("count_notifications").ToString();
                            var Seen = Data["seen"].ToString();
                            var Type_Text = Data["type_text"].ToString();
                            var Time_Text = Data["time_text"].ToString();
                            var Type = Data["type"].ToString();
                            var Icon = Data["icon"].ToString();
                            var ID = Data["id"].ToString();
                            var User_id = Notifier["user_id"].ToString();
                            var Username = Notifier["username"].ToString();
                            var Name = Notifier["name"].ToString();
                            var Avatar = Notifier["avatar"].ToString();

                            var checkantilist = SeenNotifications.FirstOrDefault(a => a.ID == ID);
                            if (checkantilist == null)
                            {
                                SeenNotifications.Add(new Notifications()
                                {
                                    ID = ID,
                                    Notifier_id = Notifier_id,
                                    Seen = Seen,
                                    Time_Text = Time_Text,
                                    Type = Type,
                                    User_id = User_id,
                                    Username = Username,
                                    Name = Name,
                                    SeenUnseenColor = "#fff",
                                    Post_ID = Post_ID,
                                    Group_ID = Group_ID,
                                    Page_ID = Page_ID,
                                });
                            }
                           
                        }
                        BottomBarPageExtensions.SetBadgeCount(AndroidStyle.NotificationsPage, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void NotificationsListview_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                await Get_notifications();

                NotificationsListview.EndRefresh();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Get_notifications()
        {
            try
            {
                ItemListPage.NotificationsList.Clear();

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_notifications",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        #region Notification

                        var Parser = JObject.Parse(json).SelectToken("notifications").ToString();
                        JArray NotificationsArray = JArray.Parse(Parser);

                        foreach (var Notification in NotificationsArray)
                        {

                            JObject Data = JObject.FromObject(Notification);
                            JObject Notifier = JObject.FromObject(Data["notifier"]);
                            var Notifier_id = Data["notifier_id"].ToString();
                            var Post_ID = Data["post_id"].ToString();
                            var Page_ID = Data["page_id"].ToString();
                            var Group_ID = Data["group_id"].ToString();
                            var Count_Notification = JObject.Parse(json).SelectToken("count_notifications").ToString();
                            var Seen = Data["seen"].ToString();
                            var Type_Text = Data["type_text"].ToString();
                            var Time_Text = Data["time_text"].ToString();
                            var Type = Data["type"].ToString();
                            var Icon = Data["icon"].ToString();
                            var ID = Data["id"].ToString();
                            var User_id = Notifier["user_id"].ToString();
                            var Username = Notifier["username"].ToString();
                            var Name = Notifier["name"].ToString();
                            var Avatar = Notifier["avatar"].ToString();

                            if (Count_Notification != "0")
                            {
                                var Alfa =MasterMainSlidePage.ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Notifications);
                                if (Alfa.BadgeNumber != Count_Notification)
                                {
                                    Alfa.BadgeIsVisible = "true";
                                    Alfa.BadgeNumber = Count_Notification;
                                }
                            }
                            else
                            {
                                var Alfa =MasterMainSlidePage.ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Notifications);
                                Alfa.BadgeIsVisible = "false";
                                Alfa.BadgeNumber = "0";
                                Alfa.Badgecolor = "#ffff";
                            }

                            var ColorIcon = NotifiTypeFunction.GetColorFontAwesom(Type);
                            var TypeIcon = NotifiTypeFunction.GetIconFontAwesom(Type);
                            if (Icon == "exclamation-circle" || Icon == "thumbs-down")
                            {
                                TypeIcon = NotifiTypeFunction.GetIconFontAwesom(Icon);
                                ColorIcon = NotifiTypeFunction.GetColorFontAwesom(Icon);
                            }

                            var DecodedTypetext = System.Net.WebUtility.HtmlDecode(Type_Text);

                            var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(Avatar, User_id);
                            var ImageMediaFile = ImageSource.FromFile(Imagepath);

                            if (DependencyService.Get<IPicture>().GetPictureFromDisk(Avatar, User_id) == "File Dont Exists")
                            {
                                ImageMediaFile = new UriImageSource
                                {
                                    Uri = new Uri(Avatar),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 0, 0, 0)
                                };
                                DependencyService.Get<IPicture>().SavePictureToDisk(Avatar, User_id);
                            }

                            if (Count_Notification != "0")
                            {
                                MasterMainSlidePage.NotificationsList.Insert(0, new Notifications()
                                {
                                    ID = ID,
                                    Notifier_id = Notifier_id,
                                    Seen = Seen,
                                    Type_Text = DecodedTypetext,
                                    Time_Text = Time_Text,
                                    Type = Type,
                                    User_id = User_id,
                                    Username = Username,
                                    Name = Name,
                                    Avatar = ImageMediaFile,
                                    SeenUnseenColor = "#fff",
                                    Icon_Color_FO = ColorIcon,
                                    Icon_Type_FO = TypeIcon,
                                    Post_ID = Post_ID,
                                    Group_ID = Group_ID,
                                    Page_ID = Page_ID,
                                });
                            }
                            else
                            {
                                MasterMainSlidePage.NotificationsList.Add(new Notifications()
                                {
                                    ID = ID,
                                    Notifier_id = Notifier_id,
                                    Seen = Seen,
                                    Type_Text = DecodedTypetext,
                                    Time_Text = Time_Text,
                                    Type = Type,
                                    User_id = User_id,
                                    Username = Username,
                                    Name = Name,
                                    Avatar = ImageMediaFile,
                                    SeenUnseenColor = "#fff",
                                    Icon_Color_FO = ColorIcon,
                                    Icon_Type_FO = TypeIcon,
                                    Post_ID = Post_ID,
                                    Group_ID = Group_ID,
                                    Page_ID = Page_ID,
                                });
                            }
                        }

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void NotificationsListview_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            try
            {
                NotificationsHeader.Text = AppResources.Label_Notifications + " (" + ItemListPage.NotificationsList.Count + ")";
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Send_View_notifications_Request().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void TapSearchIcon_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new Search_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #region Friend_Request

        private TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();

        private void NotificationsPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                tapGestureRecognizer.Tapped += OnBannerTapped;
                FriendGridBanner.GestureRecognizers.Add(tapGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnBannerTapped(Object sender, EventArgs e)
        {
            try
            {
                uint duration = 500;
                var visualElement = (VisualElement)sender;

                await Task.WhenAll(visualElement.FadeTo(0, duration / 2, Easing.CubicIn), visualElement.ScaleTo(0, duration / 2, Easing.CubicInOut));

                visualElement.HeightRequest = 0;
                await Navigation.PushAsync(new Friend_Request_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void NotificationsPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                tapGestureRecognizer.Tapped -= OnBannerTapped;
                FriendGridBanner.GestureRecognizers.Remove(tapGestureRecognizer);
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion

    }
}