﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Refractored.XamForms.PullToRefresh;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.CustomCells;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class MarketPage : ContentPage
    {
        #region Event Refresh

        public class RefreshMVVM : INotifyPropertyChanged
        {
            public ObservableCollection<string> Items { get; set; }
            MarketPage pageChosen;

            public RefreshMVVM(MarketPage page)
            {
                this.pageChosen = page;
                Items = new ObservableCollection<string>();
            }

            bool isBusy;

            public bool IsBusy
            {
                get { return isBusy; }
                set
                {
                    if (isBusy == value)
                        return;

                    isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }

            ICommand refreshCommand;

            public ICommand RefreshCommand
            {
                get
                {
                    return refreshCommand ?? (refreshCommand = new Command(async () => await ExecuteRefreshCommand()));
                }
            }

            async Task ExecuteRefreshCommand()
            {
                if (IsBusy)
                {
                    return;
                }

                IsBusy = true;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    var service = DependencyService.Get<IMethods>();
                    service.ClearWebViewCache();
                    pageChosen.ShowDataGrid.IsVisible = false;
                    pageChosen.EmptyMarketPage.IsVisible = true;
                    pageChosen.PullToRefreshLayoutView.IsRefreshing = false;

                    pageChosen.Icon_page.Text = "\uf119";
                    pageChosen.Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    pageChosen.Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    pageChosen.MarketButton.IsVisible = true;
                    pageChosen.MarketButton.Text = AppResources.Label_Try_Again;
                    pageChosen.MarketButton.Clicked += (s, a) => pageChosen.TryButton_OnClicked(s, a);
                }
                else
                {
                    //Run code
                    pageChosen.GetProducts();

                    IsBusy = false;
                }   
            }

            #region INotifyPropertyChanged implementation

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        #endregion

        #region list and Variables

        public static ObservableCollection<Products> Productlist = new ObservableCollection<Products>();
        private TapGestureRecognizer tapGestureRecognizer = new TapGestureRecognizer();

        #endregion

        public static bool IsPageInNavigationStack<TPage>(INavigation navigation) where TPage : Page
        {
            try
            {
                if (navigation.NavigationStack.Count > 1)
                {
                    var last = navigation.NavigationStack[navigation.NavigationStack.Count - 2];

                    if (last is TPage)
                    {
                        navigation.PopAsync(false);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return false;
            }
        }

        public MarketPage()
        {
            try
            {
                
                InitializeComponent();

				//if (Device.RuntimePlatform == Device.iOS)
				//{
				//	NavigationPage.SetHasNavigationBar(this, true);
				//	HeaderOfpage.IsVisible = false;
				//}
                //else
                //{
                //   NavigationPage.SetHasNavigationBar(this, false);
                //}
                BindingContext = new RefreshMVVM(this);
                PullToRefreshLayoutView.SetBinding<RefreshMVVM>(PullToRefreshLayout.IsRefreshingProperty, vm => vm.IsBusy, BindingMode.OneWay);
                PullToRefreshLayoutView.SetBinding<RefreshMVVM>(PullToRefreshLayout.RefreshCommandProperty, vm => vm.RefreshCommand);

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (Productlist.Count == 0)
                    {
                        PullToRefreshLayoutView.IsRefreshing = true;
                        //UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                        GetProducts();
                    }
                    else
                    {
                        PopulateProductsLists(Productlist);
                    }
                }
                else
                {
                    if (Productlist.Count > 0)
                    {
                        PopulateProductsLists(Productlist);
                    }
                    else
                    {
                        ShowDataGrid.IsVisible = false;
                        EmptyMarketPage.IsVisible = true;

                        Icon_page.Text = "\uf119";
                        Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                        Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                        MarketButton.IsVisible = true;
                        MarketButton.Text = AppResources.Label_Try_Again;
                        MarketButton.Clicked += (s, a) => TryButton_OnClicked(s, a); 
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void GetProducts()
        {
            try
            {
                PullToRefreshLayoutView.IsRefreshing = true;
                Productlist.Clear();
                var column = LeftColumn;
                column.Children.Clear();
                var columnr = RightColumn;
                columnr.Children.Clear();

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_products",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var Products = JObject.Parse(json).SelectToken("products").ToString();
                        JArray ProductsdataArray = JArray.Parse(Products);

                        foreach (var PRODUCT in ProductsdataArray)
                        {
                            var name = PRODUCT["name"].ToString();
                            var description = Functions.DecodeString(PRODUCT["description"].ToString());
                            var price = PRODUCT["price"].ToString();
                            var location = PRODUCT["location"].ToString();
                            
                            JArray ImagesArray = JArray.Parse(PRODUCT["images"].ToString());
                            var image = "";
                            if (ImagesArray.Count > 0)
                            {
                                foreach (var Img in ImagesArray)
                                {
                                    image = Img["image"].ToString();
                                    break;
                                }

                                JObject Seller = JObject.FromObject(PRODUCT["seller"]);
                                var Seller_name = Seller["name"].ToString();
                                var Seller_User_id = Seller["user_id"].ToString();

                                Productlist.Add(new Products
                                {
                                    Name = Functions.DecodeString(name),
                                    Description = Functions.DecodeString(description),
                                    Image = new UriImageSource
                                    {
                                        Uri = new Uri(image),
                                        CachingEnabled = true,
                                        CacheValidity = new TimeSpan(5, 1, 0, 0)
                                    },
                                    Price = price + " " + Settings.Market_curency,
                                    ThumbnailHeight = "100",
                                    Manufacturer = AppResources.Label2_by + " " + Seller_name,
                                    Manufacturer_UserID = Seller_User_id
                                });
                            }
                        }
                        PopulateProductsLists(Productlist);
                    }
                    else
                    {
                        PullToRefreshLayoutView.IsRefreshing = false;

                        //UserDialogs.Instance.HideLoading();
                        UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
                    }
                    PullToRefreshLayoutView.IsRefreshing = false;
                    //UserDialogs.Instance.HideLoading();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                PullToRefreshLayoutView.IsRefreshing = false;
                //UserDialogs.Instance.HideLoading();
                PopulateProductsLists(Productlist);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();
                tapGestureRecognizer.Tapped += OnBannerTapped;
                //EcommerceProductGridBanner.GestureRecognizers.Add(tapGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();
                tapGestureRecognizer.Tapped -= OnBannerTapped;
                //EcommerceProductGridBanner.GestureRecognizers.Remove(tapGestureRecognizer);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void PopulateProductsLists(ObservableCollection<Products> productsList)
        {
            try
            {
                if (productsList.Count == 0)
                {
                    ShowDataGrid.IsVisible = false;
                    EmptyMarketPage.IsVisible = true;

                    Icon_page.Text = "\uf07a";
                    Lbl_Empty.Text = AppResources.Label_No_Products;
                    Lbl_NoData.Text = AppResources.Label_Start_adding_your_product;
                    MarketButton.IsVisible = true;
                    MarketButton.Text = AppResources.Label_Add_Product;
                    MarketButton.Clicked += (s, e) =>  AddproductButton_OnClicked(s, e);
                }
                else
                {
                    ShowDataGrid.IsVisible = true;
                    EmptyMarketPage.IsVisible = false;
                }
                var lastHeight = "100";
                var y = 0;
                var column = LeftColumn;

                for (var i = 0; i < productsList.Count; i++)
                {
                    var productTapGestureRecognizer = new TapGestureRecognizer();
                    var item = new ProductGridItemTemplate();

                    if (i > 0)
                    {
                        if (i == 3 || i == 4 || i == 7 || i == 8 || i == 11 || i == 12)
                        {
                            lastHeight = "100";
                        }
                        else
                        {
                            lastHeight = "190";
                        }

                        if (i % 2 == 0)
                        {
                            column = LeftColumn;
                            y++;
                        }
                        else
                        {
                            column = RightColumn;
                        }
                    }
                    productTapGestureRecognizer.Tapped += OnProductTapped;

                    item.BindingContext = productsList[i];
                    item.GestureRecognizers.Add(productTapGestureRecognizer);
                    column.Children.Add(item);
                    productsList[i].ThumbnailHeight = lastHeight;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnProductTapped(Object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Products)((ProductGridItemTemplate)sender).BindingContext;
                if (selectedItem != null)
                {
                    var productView = new ProductItemViewPage()
                    {
                        BindingContext = selectedItem
                    };
                    await Navigation.PushAsync(productView);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnBannerTapped(Object sender, EventArgs e)
        {
            try
            {
                uint duration = 500;
                var visualElement = (VisualElement)sender;

                await Task.WhenAll(
                    visualElement.FadeTo(0, duration / 2, Easing.CubicIn),
                    visualElement.ScaleTo(0, duration / 2, Easing.CubicInOut)
                );

                visualElement.HeightRequest = 0;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void LoadMore_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Productlist.Clear();
                    var column = LeftColumn;
                    column.Children.Clear();
                    var columnr = RightColumn;
                    columnr.Children.Clear();

                    PullToRefreshLayoutView.IsRefreshing = true;
                    //UserDialogs.Instance.ShowLoading("Loading", MaskType.Clear);
                    GetProducts();
                }
                else
                {
                    PullToRefreshLayoutView.IsRefreshing = false;
                    //UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
                    ShowDataGrid.IsVisible = false;
                    EmptyMarketPage.IsVisible = true;

                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    MarketButton.IsVisible = true;
                    MarketButton.Text = AppResources.Label_Try_Again;
                    MarketButton.Clicked += (s, a) => TryButton_OnClicked(s, a);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }  
        }

        private async void AddProducts_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new AddProductPage());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void MarketPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                var Count = LeftColumn.Children.Count + RightColumn.Children.Count;
                if (Productlist.Count != Count)
                {
                    var column = LeftColumn;
                    column.Children.Clear();
                    var columnr = RightColumn;
                    columnr.Children.Clear();
                    PopulateProductsLists(Productlist);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }           
        }

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    PullToRefreshLayoutView.IsRefreshing = true;

                    ShowDataGrid.IsVisible = true;
                    EmptyMarketPage.IsVisible = false;

                    //UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                    GetProducts();
                }
                else
                {
                    PullToRefreshLayoutView.IsRefreshing = false;
                    //UserDialogs.Instance.Toast(AppResources.Label_You_are_still_Offline);
                    ShowDataGrid.IsVisible = false;
                    EmptyMarketPage.IsVisible = true;

                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    MarketButton.IsVisible = true;
                    MarketButton.Text = AppResources.Label_Try_Again;
                    MarketButton.Clicked += (s, a) => TryButton_OnClicked(s, a);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void AddproductButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new AddProductPage());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void MarketPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                PullToRefreshLayoutView.IsRefreshing = false;
                //UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}