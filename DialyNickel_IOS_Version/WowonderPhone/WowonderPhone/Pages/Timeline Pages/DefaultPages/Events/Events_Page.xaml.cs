﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using WowonderPhone.Languish;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Events_Page : ContentPage
    {
        #region List

        public static ObservableCollection<Classes.Events> EventsListItems = new ObservableCollection<Classes.Events>();
        public static ObservableCollection<Classes.Events> MyEventsListItems = new ObservableCollection<Classes.Events>();

        #endregion

        public Events_Page()
        {
            try
            {
                InitializeComponent();
				//if (Device.RuntimePlatform == Device.iOS)
				//{
				//	NavigationPage.SetHasNavigationBar(this, true);
				//	HeaderOfpage.IsVisible = false;
				//}
				//else
				//{
				//	NavigationPage.SetHasNavigationBar(this, false);
				//}

                if (EventsListItems.Count > 0)
                {
                    ListViewEvents.ItemsSource = EventsListItems;
                }
                else
                {
                    ListViewEvents.IsRefreshing = true;
                    GetLatestEvents("").ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                ListViewEvents.IsRefreshing = false;
                ListViewEvents.EndRefresh();

            }
        }

        private void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Classes.Events Item)
                {
                    Navigation.PushAsync(new Event_info_page(Item));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task GetLatestEvents(string offset)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (offset == "")
                    {
                        ListViewEvents.IsRefreshing = true;
                    }
                    ListViewEvents.IsVisible = true;
                    EmptyPage.IsVisible = false;

                    using (var client = new HttpClient())
                    {
                        var formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("user_id", Settings.User_id),
                            new KeyValuePair<string, string>("s", Settings.Session),
                            new KeyValuePair<string, string>("offset", offset),
                        });

                        var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_events", formContent);
                        response.EnsureSuccessStatusCode();
                        string json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                        string apiStatus = data["api_status"].ToString();
                        if (apiStatus == "200")
                        {
                            var Parser2 = JObject.Parse(json).SelectToken("events").ToString();
                            JArray Events_usersArray = JArray.Parse(Parser2);

                            if (Events_usersArray.Count > 0)
                            {
                                if (offset == "")
                                {
                                    EventsListItems.Clear();
                                }
                               
                                foreach (var Event in Events_usersArray)
                                {
                                    var id = Event["id"].ToString();
                                    var name = Event["name"].ToString();
                                    var location = Event["location"].ToString();
                                    var description = Event["description"].ToString();
                                    var start_date = Event["start_date"].ToString();
                                    var start_time = Event["start_time"].ToString();
                                    var end_date = Event["end_date"].ToString();
                                    var end_time = Event["end_time"].ToString();
                                    var poster_id = Event["poster_id"].ToString();
                                    var cover = Event["cover"].ToString();
                                    var is_going = Event["is_going"].ToString();
                                    var is_interested = Event["is_interested"].ToString();
                                    var going_count = Event["going_count"].ToString();
                                    var interested_count = Event["interested_count"].ToString();
                                    var Url = Event["url"].ToString();
                                    var Userdata = Event["user_data"];
                                    var nameofuser = Userdata["name"].ToString();
                                    var last_name = Userdata["last_name"].ToString();
                                    var avatar = Userdata["avatar"].ToString();
                                    var Profile_url = Userdata["url"].ToString();

                                    DateTime d = DateTime.Parse(start_date);
                                    string s = d.ToString("dd/MM");

                                    var dd = EventsListItems.FirstOrDefault(a => a.id == id);
                                    if (dd == null)
                                    {
                                        EventsListItems.Add(new Classes.Events()
                                        {
                                            BackgroundImage = new UriImageSource
                                            {
                                                Uri = new Uri(cover),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            },
                                            start_date_Sorted = s,
                                            start_date = start_date,
                                            Start_Time = start_time,
                                            end_date = end_date,
                                            end_time = end_time,
                                            Going = going_count,
                                            Intersted = interested_count,
                                            id = id,
                                            is_going = is_going,
                                            is_interested = is_interested,
                                            poster_id = poster_id,
                                            description = Functions.DecodeString(description),
                                            Place = Functions.DecodeString(location),
                                            name = nameofuser,
                                            Profile_url = Profile_url,
                                            Title = Functions.DecodeString(name),
                                            Url = Url
                                        });
                                    }
                                }
                            }

                            var Parser3 = JObject.Parse(json).SelectToken("my_events").ToString();
                            JArray My_Events_usersArray = JArray.Parse(Parser3);

                            if (My_Events_usersArray.Count > 0)
                            {
                                foreach (var Event in My_Events_usersArray)
                                {

                                    var id = Event["id"].ToString();
                                    var name = Event["name"].ToString();
                                    var location = Event["location"].ToString();
                                    var description = Event["description"].ToString();
                                    var start_date = Event["start_date"].ToString();
                                    var start_time = Event["start_time"].ToString();
                                    var end_date = Event["end_date"].ToString();
                                    var end_time = Event["end_time"].ToString();
                                    var poster_id = Event["poster_id"].ToString();
                                    var cover = Event["cover"].ToString();
                                    var is_owner = Event["is_owner"].ToString();

                                    var going_count = "";
                                    var interested_count = "";
                                    try
                                    {
                                        going_count = Event["going_count"].ToString();
                                        interested_count = Event["interested_count"].ToString();
                                    }

                                    catch (Exception ex)
                                    {
                                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                    }

                                    var is_interested = Event["is_interested"].ToString();
                                    var Url = Event["url"].ToString();
                                    var Userdata = Event["user_data"];
                                    var nameofuser = Userdata["name"].ToString();
                                    var avatar = Userdata["avatar"].ToString();
                                    var Profile_url = Userdata["url"].ToString();

                                    DateTime d = DateTime.Parse(start_date);
                                    string s = d.ToString("dd/MM");

                                    var dd = MyEventsListItems.FirstOrDefault(a => a.id == id);
                                    if (dd == null)
                                    {
                                        MyEventsListItems.Add(new Classes.Events()
                                        {
                                            BackgroundImage = new UriImageSource
                                            {
                                                Uri = new Uri(cover),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            },
                                            start_date_Sorted = s,
                                            start_date = start_date,
                                            Start_Time = start_time,
                                            end_date = end_date,
                                            end_time = end_time,
                                            id = id,
                                            Going = going_count,
                                            Intersted = interested_count,
                                            is_owner = is_owner,
                                            poster_id = poster_id,
                                            description = Functions.DecodeString(description),
                                            Place = Functions.DecodeString(location),
                                            name = nameofuser,
                                            Profile_url = Profile_url,
                                            Title = Functions.DecodeString(name),
                                            Url = Url
                                        });
                                    }
                                }
                            }
                        }
                    }

                    if (EventsListItems.Count == 0)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ListViewEvents.IsVisible = false;
                            EmptyPage.IsVisible = true;

                            Icon_page.Text = "\uf073";
                            Lbl_Empty.Text = AppResources.Label2_Empty_Events;
                            Lbl_NoData.Text = AppResources.Label2_no_events_to_view;
                            EventButton.IsVisible = true;
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ListViewEvents.IsVisible = true;
                            EmptyPage.IsVisible = false;
                        });
                    }
                    ListViewEvents.IsRefreshing = false;

                    Title = AppResources.Label_Events;
                    ListViewEvents.ItemsSource = EventsListItems;
                    ListViewEvents.EndRefresh();
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ListViewEvents.IsVisible = false;
                        EmptyPage.IsVisible = true;

                        Icon_page.Text = "\uf119";
                        Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                        Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                        EventButton.IsVisible = false;
                    });

                    ListViewEvents.IsRefreshing = false;
                    ListViewEvents.EndRefresh();
                }
            }
            catch (Exception ex)
            {
                ListViewEvents.IsRefreshing = false;
                ListViewEvents.ItemsSource = EventsListItems;
                ListViewEvents.EndRefresh();
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddEvents_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Add_New_Event());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ListViewEvents_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            try
            {
                var currentItem = e.Item as Classes.Events;
                var beforelastItem = EventsListItems[EventsListItems.Count - 2];
                if (currentItem == beforelastItem)
                {
                    var device = Resolver.Resolve<IDevice>();
                    var oNetwork = device.Network; // Create Interface to Network-functions
                    var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                    if (!xx)
                    {
                       // Title = AppResources.Label_Loading;
                        var lastItem = EventsListItems[EventsListItems.Count - 1];
                        if (lastItem != null)
                        {
                             await GetLatestEvents(lastItem.id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnCogIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Add_New_Event());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void MyEvent_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new My_Event_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CreateEvent_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Add_New_Event());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ListViewEvents_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    var service = DependencyService.Get<IMethods>();
                    service.ClearWebViewCache();

                    ListViewEvents.IsVisible = false;
                    EmptyPage.IsVisible = true;

                    Icon_page.Text = "\uf119";
                    Lbl_Empty.Text = AppResources.Label_Offline_Mode;
                    Lbl_NoData.Text = AppResources.Label_CheckYourInternetConnection;
                    EventButton.IsVisible = false;

                    ListViewEvents.IsRefreshing = false;
                    ListViewEvents.EndRefresh();

                }
                else
                {
                    ListViewEvents.IsVisible = true;
                    EmptyPage.IsVisible = false;

                    //Run code
                    GetLatestEvents("").ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}