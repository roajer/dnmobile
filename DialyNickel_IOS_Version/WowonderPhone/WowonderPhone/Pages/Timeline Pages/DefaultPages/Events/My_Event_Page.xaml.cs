﻿using System;
using WowonderPhone.Languish;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class My_Event_Page : ContentPage
    {
        public My_Event_Page()
        {
            try
            {
                
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

				Title = AppResources.Label2_My_event;
                HeaderLabel.Text = AppResources.Label2_My_event;

                if (Events_Page.MyEventsListItems.Count > 0)
                {
                    EmptyPage.IsVisible = false;
                    MyEventPage.IsVisible = true;
                    MyEventListview.ItemsSource = Events_Page.MyEventsListItems;
                }
                else
                {
                    MyEventPage.IsVisible = false;
                    EmptyPage.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Classes.Events Item)
                {
                    Navigation.PushAsync(new Event_info_page(Item));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void MyEventListview_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                Events_Page ff = new Events_Page();
                await ff.GetLatestEvents("");
                MyEventListview.EndRefresh();

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddEvents_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Add_New_Event());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
