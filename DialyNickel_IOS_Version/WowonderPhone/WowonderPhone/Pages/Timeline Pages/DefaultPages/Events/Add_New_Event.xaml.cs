﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Add_New_Event : ContentPage
    {
        #region classes

        public class Catatgorytems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public ImageSource Image { get; set; }
            public Stream ImageStream { get; set; }
            public string ImageFullPath { get; set; }
        }

        #endregion

        #region Variables and list

        public static ObservableCollection<Catatgorytems> ActivityListItems = new ObservableCollection<Catatgorytems>();

        public string PikedImage = "";


        #endregion

        public Add_New_Event()
        {
            try
            {
                
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

				if (ActivityListItems.Count == 0)
                {
                    ActivityListItems.Add(new Catatgorytems()
                    {
                        Label = AppResources.Label_Pick_Take_Photo,
                        Icon = "\uf03e",
                    });
                }

                StartTimePicker.Time = DateTime.Now.TimeOfDay;
                EndTimePicker.Time = DateTime.Now.TimeOfDay;
                StartTimePicker.Format = "hh:mm";
                EndTimePicker.Format = "hh:mm";
                ActivityList.ItemsSource = ActivityListItems;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActivityList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ActivityList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ActivityList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Catatgorytems Item)
                {
                    if (Item.Label == AppResources.Label_Pick_Take_Photo)
                    {
                        var action = await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null, AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                        if (action == AppResources.Label_Choose_from_Galery)
                        {
                            await CrossMedia.Current.Initialize();
                            if (!CrossMedia.Current.IsPickPhotoSupported)
                            {
                                await DisplayAlert("Oops", "You Cannot pick an image", AppResources.Label_OK);
                                return;
                            }
                            var file = await CrossMedia.Current.PickPhotoAsync();

                            if (file == null)
                                return;




                            ActivityList.IsVisible = true;
                            if (ActivityListItems.Count() > 0)
                            {
                                ActivityListItems.Clear();
                            }
                            PikedImage = file.Path.Split('/').Last();
                            ActivityListItems.Add(new Catatgorytems
                            {
                                Label = file.Path.Split('/').Last(),
                                Icon = "\uf030",
                                ImageFullPath = file.Path,
                                ImageStream = file.GetStream()
                            });

                            ActivityList.ItemsSource = ActivityListItems;

                        }
                        else if (action == AppResources.Label_Take_a_Picture)
                        {
                            await CrossMedia.Current.Initialize();

                            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                            {
                                await DisplayAlert("No Camera", ":( No camera avaialble.", AppResources.Label_OK);
                                return;
                            }
                            var time = DateTime.Now.ToString("hh:mm");
                            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                            {
                                PhotoSize = PhotoSize.Medium,
                                CompressionQuality = 92,
                                SaveToAlbum = true,
                                Name = time + "PictureProduct.jpg"
                            });

                            if (file == null)
                                return;

                            ActivityList.IsVisible = true;
                            if (ActivityListItems.Count() > 0)
                            {
                                ActivityListItems.Clear();
                            }
                            PikedImage = file.Path.Split('/').Last();
                            ActivityListItems.Add(new Catatgorytems
                            {
                                Label = file.Path.Split('/').Last(),
                                Icon = "\uf030",
                                ImageFullPath = file.Path,
                                ImageStream = file.GetStream()
                            });

                            ActivityList.ItemsSource = ActivityListItems;
                        }

                    }
                    else
                    {
                        try
                        {
                            var Function = await DisplayAlert(AppResources.Label_Question, AppResources.Label_Do_you_want_to_add_new_image, AppResources.Label_Yes, AppResources.Label_NO);
                            if (Function)
                            {
                                ActivityListItems.Clear();
                                var action =
                                    await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null, AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                                if (action == AppResources.Label_Choose_from_Galery)
                                {
                                    await CrossMedia.Current.Initialize();
                                    if (!CrossMedia.Current.IsPickPhotoSupported)
                                    {
                                        await DisplayAlert("Oops", "You Cannot pick an image", AppResources.Label_OK);
                                        return;
                                    }
                                    var file = await CrossMedia.Current.PickPhotoAsync();

                                    if (file == null)
                                        return;

                                    var streamReader = new StreamReader(file.GetStream());
                                    var bytes = default(byte[]);
                                    using (var memstream = new MemoryStream())
                                    {
                                        streamReader.BaseStream.CopyTo(memstream);
                                        bytes = memstream.ToArray();
                                    }
                                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);


                                    ActivityList.IsVisible = true;
                                    if (ActivityListItems.Count() > 0)
                                    {
                                        ActivityListItems.Clear();
                                    }
                                    PikedImage = file.Path.Split('/').Last();
                                    ActivityListItems.Add(new Catatgorytems
                                    {
                                        Label = file.Path.Split('/').Last(),
                                        Icon = "\uf030",
                                        ImageFullPath = file.Path,
                                        ImageStream = file.GetStream()

                                    });

                                    ActivityList.ItemsSource = ActivityListItems;

                                }
                                else if (action == AppResources.Label_Take_a_Picture)
                                {
                                    await CrossMedia.Current.Initialize();

                                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                                    {
                                        await DisplayAlert("No Camera", ":( No camera avaialble.", AppResources.Label_OK);
                                        return;
                                    }
                                    var time = DateTime.Now.ToString("hh:mm");
                                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                                    {
                                        PhotoSize = PhotoSize.Medium,
                                        CompressionQuality = 92,
                                        SaveToAlbum = true,
                                        Name = time + "PictureProduct.jpg"
                                    });

                                    if (file == null)
                                        return;

                                    ActivityList.IsVisible = true;
                                    if (ActivityListItems.Count() > 0)
                                    {
                                        ActivityListItems.Clear();
                                    }
                                    PikedImage = file.Path.Split('/').Last();
                                    ActivityListItems.Add(new Catatgorytems
                                    {
                                        Label = file.Path.Split('/').Last(),
                                        Icon = "\uf030",
                                        ImageFullPath = file.Path,
                                        ImageStream = file.GetStream()
                                    });

                                    ActivityList.ItemsSource = ActivityListItems;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CreateButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var Attach = ActivityListItems.FirstOrDefault(a => a.Label == PikedImage);
                if (Attach != null)
                {
                    if (String.IsNullOrEmpty(DescriptionEntry.Text) || DescriptionEntry.Text.Length<32)
                    {
                        DisplayAlert(AppResources.Label2_Note, AppResources.Label2_Description_shoul_be_not_empty, AppResources.Label_OK); 
                        return;
                    }

                    if (String.IsNullOrEmpty(EventNameEntry.Text) || EventNameEntry.Text.Length < 4)
                    {
                        DisplayAlert(AppResources.Label2_Note, AppResources.Label2_Name_shoul_be_not_empty, AppResources.Label_OK);
                        return;
                    }

                    if (String.IsNullOrEmpty(locationEntry.Text) || locationEntry.Text.Length < 4)
                    {
                        DisplayAlert(AppResources.Label2_Note, AppResources.Label2_Location_shoul_be_not_empty, AppResources.Label_OK);
                        return;
                    }

                    var start_Date = StartDatePicker.Date.ToString("yyyy-MM-dd");
                    var start_Time = StartTimePicker.Time.ToString();
                    var End_Date = EndDatePicker.Date.ToString("yyyy-MM-dd");
                    var End_Time = EndTimePicker.Time.ToString();

                    Task.Factory.StartNew(() =>
                    {
                        DependencyService.Get<IMethods>()
                        .AddEvent(Attach.ImageStream, Attach.ImageFullPath, EventNameEntry.Text, locationEntry.Text,
                            start_Date, start_Time,
                            End_Date, End_Time, DescriptionEntry.Text);

                    });

                    DateTime d = DateTime.Parse(start_Date);
                    string s = d.ToString("dd/MM");
                    Events_Page.EventsListItems.Insert(0, new Classes.Events()
                    {
                        BackgroundImage = ImageSource.FromFile(Attach.ImageFullPath),
                        start_date_Sorted = s,
                        start_date = start_Date,
                        Start_Time = start_Time,
                        end_date = End_Date,
                        end_time = End_Time,
                        id = "0",
                        is_going = "",
                        is_interested = "",
                        poster_id = Settings.User_id,
                        description = Functions.DecodeString(DescriptionEntry.Text),
                        Place = Functions.DecodeString(locationEntry.Text),
                        name = AppResources.Label2_by_you,
                        Title = Functions.DecodeString(EventNameEntry.Text),
                    });

                    Events_Page.MyEventsListItems.Insert(0, new Classes.Events()
                    {

                        BackgroundImage = ImageSource.FromFile(Attach.ImageFullPath) ,
                        is_owner = "True",
                        start_date_Sorted = s,
                        start_date = start_Date,
                        Start_Time = start_Time,
                        end_date = End_Date,
                        end_time = End_Time,
                        id = "0",
                        is_going = "",
                        is_interested = "",
                        poster_id = Settings.User_id,
                        description = Functions.DecodeString(DescriptionEntry.Text),
                        Place = Functions.DecodeString(locationEntry.Text),
                        name = AppResources.Label2_by_you,
                        Title = Functions.DecodeString(EventNameEntry.Text),
                    });
                       // UserDialogs.Instance.ShowSuccess(AppResources.Label_Product_Added, 2500);
                        Navigation.PopAsync(false).Wait(200);
                }
                else
                {
                    DisplayAlert(AppResources.Label2_Note, AppResources.Label2_You_cannot_without_any_image, AppResources.Label_OK);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2500);
            }
        }
    
        private void DescriptionEntry_OnFocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (DescriptionEntry.Text == AppResources.Label2_Description)
                {
                    DescriptionEntry.Text = "";
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
