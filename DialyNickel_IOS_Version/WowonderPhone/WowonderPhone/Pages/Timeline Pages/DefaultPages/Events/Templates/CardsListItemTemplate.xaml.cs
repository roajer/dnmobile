﻿using System;

using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events.Templates
{
    public partial class CardsListItemTemplate : ContentView
    {
        public CardsListItemTemplate()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
