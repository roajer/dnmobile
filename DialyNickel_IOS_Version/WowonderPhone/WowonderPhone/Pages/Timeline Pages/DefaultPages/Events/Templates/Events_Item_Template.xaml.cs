﻿using System;

using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events.Templates
{
    public partial class Events_Item_Template : ContentView
    {
        public Events_Item_Template()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
