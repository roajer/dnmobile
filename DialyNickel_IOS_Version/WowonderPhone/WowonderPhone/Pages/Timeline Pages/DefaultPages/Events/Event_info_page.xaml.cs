﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PropertyChanged;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Event_info_page : ContentPage
    {
        [ImplementPropertyChanged]
        public class Eventitemsinfo
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Color { get; set; }        
        }

        public static ObservableCollection<Eventitemsinfo> EventListItems = new ObservableCollection<Eventitemsinfo>();
        public Classes.Events EventData = null;

        public Event_info_page(Classes.Events Data)
        {
            try
            {
                
                EventData = Data;
                InitializeComponent();

				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}

				EventCover.Source = Data.BackgroundImage;
                EventTitile.Text = Data.Title;

                Eventitemsinfo Alfa = new Eventitemsinfo();
                Alfa.Color = "#fa3c4c";
                Alfa.Icon = "\uf041";
                Alfa.Label = Data.Place;

                Eventitemsinfo Alfa0 = new Eventitemsinfo();
                Alfa0.Color = Settings.MainColor;
                Alfa0.Icon = "\uf007";
                Alfa0.Label = Data.name;

                Eventitemsinfo Alfa2 = new Eventitemsinfo();
                Alfa2.Color = "#888";
                Alfa2.Icon = "\uf017";
                Alfa2.Label = AppResources.Label2_Start_Date + ": " + Data.start_date;

                Eventitemsinfo Alfa3 = new Eventitemsinfo();
                Alfa3.Color = "#888";
                Alfa3.Icon = "\uf017";
                Alfa3.Label = AppResources.Label2_End_Date + ": " + Data.end_date;

                Eventitemsinfo Alfa4 = new Eventitemsinfo();
                Alfa4.Color = "#ffc300";
                Alfa4.Icon = "\uf005";
                Alfa4.Label = Data.Intersted + " " + AppResources.Label2_Interested_people;

                Eventitemsinfo Alfa5 = new Eventitemsinfo();
                Alfa5.Color = "#7646ff";
                Alfa5.Icon = "\uf08b";
                Alfa5.Label = Data.Going + " " + AppResources.Label2_Going_people;

                if (!String.IsNullOrEmpty(Data.is_owner) || Data.poster_id == Settings.User_id)
                {
                    InerestedButton2.IsVisible = false;
                    GoingButton.IsVisible = false;
                }
                else
                {
                    ToolbarItems.RemoveAt(1);
                }

                EventListItems.Clear();

                EventListItems.Add(Alfa0);
                EventListItems.Add(Alfa5);
                EventListItems.Add(Alfa);
                EventListItems.Add(Alfa2);
                EventListItems.Add(Alfa3);
                EventListItems.Add(Alfa4);

                EventInfoList.ItemsSource = EventListItems;

                if (Data.is_interested == "True")
                {
                    InerestedButton2.BackgroundColor = Color.FromHex("#efefef");
                    InerestedButton2.TextColor = Color.FromHex("#444");
                    InerestedButton2.Text = AppResources.Label2_I_am_Interested;
                }

                if (Data.is_going == "True")
                {
                    GoingButton.BackgroundColor = Color.FromHex("#efefef");
                    GoingButton.TextColor = Color.FromHex("#444");
                    GoingButton.Text = AppResources.Label2_Going;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void EventInfoList_OnItemSelectedInfoList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                EventInfoList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void EventInfoList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Eventitemsinfo Item)
                {
                    if (Item.Icon == "\uf007")
                    {
                        Navigation.PushAsync(new UserProfilePage(EventData.poster_id, ""));
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CopyUrlButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Button1.Text = "\uf00c";
                DependencyService.Get<IClipboardService>().CopyToClipboard(EventData.Url);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void EditEvent_OnClicked_OnClicked(object sender, EventArgs e)
        {

        }

        private  void GoingButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                SendGoingRequest().ConfigureAwait(false);
                if (GoingButton.Text == AppResources.Label2_Iam_Going)
                {
                    GoingButton.BackgroundColor = Color.FromHex("#efefef");
                    GoingButton.TextColor = Color.FromHex("#444");
                    GoingButton.Text = AppResources.Label2_Going;

                    InerestedButton2.Text = AppResources.Label2_Inerested;
                    InerestedButton2.BackgroundColor = Color.FromHex("#fa3c4c");
                    InerestedButton2.TextColor = Color.FromHex("#ffff");
                }
                else
                {
                    GoingButton.Text = AppResources.Label2_Iam_Going;
                    GoingButton.BackgroundColor = Color.FromHex(Settings.MainColor);
                    GoingButton.TextColor = Color.FromHex("#ffff");
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task SendGoingRequest()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("event_id", EventData.id),
                    });

                    var response =
                        await
                            client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=go_to_event",
                                formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                       

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task SendInteristedRequest()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("event_id", EventData.id),
                    });

                    var response =
                        await
                            client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=interested_in_event",
                                formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void InerestedButton2_OnClicked(object sender, EventArgs e)
        {
            try
            {
                SendInteristedRequest().ConfigureAwait(false);
                if (InerestedButton2.Text == AppResources.Label2_Inerested)
                {
                    InerestedButton2.BackgroundColor = Color.FromHex("#efefef");
                    InerestedButton2.TextColor = Color.FromHex("#444");
                    InerestedButton2.Text = AppResources.Label2_I_am_Interested;

                    GoingButton.Text = AppResources.Label2_Iam_Going;
                    GoingButton.BackgroundColor = Color.FromHex(Settings.MainColor);
                    GoingButton.TextColor = Color.FromHex("#ffff");
                }
                else
                {
                    InerestedButton2.Text = AppResources.Label2_Inerested;
                    InerestedButton2.BackgroundColor = Color.FromHex("#fa3c4c");
                    InerestedButton2.TextColor = Color.FromHex("#ffff");
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
