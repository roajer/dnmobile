﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Languish;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages
{
    public partial class Like_Wonder_Viewer_Page : ContentPage
    {
        public string POST_ID = "";
        public string POST_TYPE = "";

        public static ObservableCollection<PostActions> PostActionsItemsCollection = new ObservableCollection<PostActions>();

        public Like_Wonder_Viewer_Page(string Post_id, string Post_Type)
        {
            try
            {
                InitializeComponent();

                POST_ID = Post_id;
                POST_TYPE = Post_Type;

                if (POST_TYPE == "post_likes")
                {
                    Title = AppResources.Label_Post_Likes;
                }
                else if (POST_TYPE == "post_wonders")
                {
                    Title = AppResources.Label_Post_Wonders;
                }

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Clear);
                    if (PostActionsItemsCollection.Count > 0)
                    {
                        PostActionsItemsCollection.Clear();
                    }
                    Send_View_notifications_Request().ConfigureAwait(false);
                }
                else
                {
                    PostActionsItemsCollection.Clear();
                    UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }

        }

        private void ActionsListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ActionsListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ActionsListview_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                ActionsListview.BeginRefresh();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    if (PostActionsItemsCollection.Count > 0)
                    {
                        PostActionsItemsCollection.Clear();
                    }
                    await Send_View_notifications_Request();
                }
                else
                {
                    PostActionsItemsCollection.Clear();
                    UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                }

                ActionsListview.EndRefresh();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
          
        }

        public async Task Send_View_notifications_Request()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("post_id", POST_ID)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_post_data", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (POST_TYPE == "post_likes")
                        {
                            var Parser2 = JObject.Parse(json).SelectToken("post_likes").ToString();
                            JArray UsersArray = JArray.Parse(Parser2);
                            foreach (var UserLike in UsersArray)
                            {
                                JObject Data = JObject.FromObject(UserLike);
                                var User_id = Data["user_id"].ToString();
                                var Username = Data["username"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Name = Data["name"].ToString();

                                PostActionsItemsCollection.Insert(0, new PostActions()
                                {
                                    Name = Name,
                                    User_id = User_id,
                                    Username = Username,
                                    Image = new UriImageSource
                                    {
                                        Uri = new Uri(Avatar),
                                        CachingEnabled = true,
                                        CacheValidity = new TimeSpan(2, 0, 0, 0)
                                    },
                                    Icon = "Like_Icon.png"
                                });
                            }
                        }
                        else if (POST_TYPE == "post_wonders")
                        {
                            var Parser2 = JObject.Parse(json).SelectToken("post_wonders").ToString();
                            JArray UsersArray = JArray.Parse(Parser2);
                            foreach (var UserLike in UsersArray)
                            {
                                JObject Data = JObject.FromObject(UserLike);
                                var User_id = Data["user_id"].ToString();
                                var Username = Data["username"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Name = Data["name"].ToString();

                                PostActionsItemsCollection.Insert(0, new PostActions()
                                {
                                    Name = Name,
                                    User_id = User_id,
                                    Username = Username,
                                    Image = new UriImageSource
                                    {
                                        Uri = new Uri(Avatar),
                                        CachingEnabled = true,
                                        CacheValidity = new TimeSpan(2, 0, 0, 0)
                                    },
                                    Icon = "Wownder_Icon.png"
                                });
                            }
                        }
                        UserDialogs.Instance.HideLoading();
                        ActionsListview.ItemsSource = PostActionsItemsCollection;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActionsListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {

                if (e.Item is PostActions item)
                {
                    Navigation.PushAsync(new UserProfilePage(item.User_id, ""));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Like_Wonder_Viewer_Page_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
