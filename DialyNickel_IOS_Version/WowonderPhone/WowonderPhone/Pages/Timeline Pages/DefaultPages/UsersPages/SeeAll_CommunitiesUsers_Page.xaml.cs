﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using WowonderPhone.Classes;
using WowonderPhone.Pages.CustomCells;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.UsersPages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SeeAll_CommunitiesUsers_Page : ContentPage
    {
        private ObservableCollection<Profile.Follower> ContactsUsersList = new ObservableCollection<Profile.Follower>();
        private ObservableCollection<Profile.Group> GroupUsersList = new ObservableCollection<Profile.Group>();
        private ObservableCollection<Profile.Liked_Pages> Liked_PagesUsersList = new ObservableCollection<Profile.Liked_Pages>();
        private List<Albums> AlbumUsersList = new List<Albums>();

        public SeeAll_CommunitiesUsers_Page(ObservableCollection<Profile.Follower> ContactsList, ObservableCollection<Profile.Group> GroupList, ObservableCollection<Profile.Liked_Pages> Liked_PagesList, List<Albums> AlbumList)
        {
            try
            {
                InitializeComponent();

                ContactsUsersList.Clear();
                GroupUsersList.Clear();
                AlbumUsersList.Clear();
                LeftColumn.Children.Clear();
                RightColumn.Children.Clear();

                if (ContactsList != null)
                {
                    ContactsUsersList = ContactsList;
                    ContactsListview.ItemsSource = ContactsUsersList;
                    ContactsListview.IsVisible = true;
                    PagesListview.IsVisible = false;
                    GroupListview.IsVisible = false;
                    ShowDataImgGrid.IsVisible = false;
                    Title = "All Contacts";
                }
                else if (GroupList != null)
                {
                    GroupUsersList = GroupList;
                    GroupListview.ItemsSource = GroupUsersList;
                    GroupListview.IsVisible = true;
                    ContactsListview.IsVisible = false;
                    PagesListview.IsVisible = false;
                    ShowDataImgGrid.IsVisible = false;
                    Title = "All Groups";
                }
                else if (Liked_PagesList != null)
                {
                    Liked_PagesUsersList = Liked_PagesList;
                    PagesListview.ItemsSource = Liked_PagesUsersList;
                    PagesListview.IsVisible = true;
                    ContactsListview.IsVisible = false;
                    GroupListview.IsVisible = false;
                    ShowDataImgGrid.IsVisible = false;
                    Title = "All Pages";
                }
                else if (AlbumList != null)
                {
                    AlbumUsersList = AlbumList;
                    PopulateAlbumLists(AlbumUsersList);
                    ShowDataImgGrid.IsVisible = true;
                    GroupListview.IsVisible = false;
                    ContactsListview.IsVisible = false;
                    PagesListview.IsVisible = false;
                    Title = "All Photos";
                }
                else
                {
                    
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        //All Photo
        private void PopulateAlbumLists(List<Albums> AlbumsList)
        {
            try
            {
                var lastHeight = "128";
                var y = 0;
                var column = LeftColumn;
                var productTapGestureRecognizer = new TapGestureRecognizer();
                productTapGestureRecognizer.Tapped += OnProductTapped;

                for (var i = 0; i < AlbumsList.Count; i++)
                {
                    var item = new ImageGriditemTemplate();

                    if (i % 2 == 0)
                    {
                        column = LeftColumn;
                        y++;
                    }
                    else
                    {
                        column = RightColumn;
                    }

                    AlbumsList[i].ThumbnailHeight = lastHeight;
                    item.BindingContext = AlbumsList[i];
                    item.GestureRecognizers.Add(productTapGestureRecognizer);
                    column.Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnProductTapped(Object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Albums)((ImageGriditemTemplate)sender).BindingContext;
                if (selectedItem != null)
                {
                    var productView = new ImageCommentPage(selectedItem)
                    {
                        BindingContext = selectedItem
                    };

                    await Navigation.PushAsync(productView);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        // All Contacts
        private void ContactsListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ContactsListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ContactsListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Profile.Follower user)
                {
                    await Navigation.PushAsync(new UserProfilePage(user.F_user_id, "FriendList"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        // All Pages
        private void PagesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PagesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        // All Groups
        private void GroupListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                GroupListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void GroupListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Profile.Group selectedItem)
                {
                    await Navigation.PushAsync(new SocialGroup(selectedItem.G_id, "Joined"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}