﻿using System;
using System.Threading.Tasks;
using UXDivers.Artina.Shared;
using WowonderPhone.Classes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Stories
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Story_Page : ContentPage
    {
        private bool _animate;
        public static Story Trachdata = null;
        public static Story DataofStory = null;

        public Story_Page(Story DataStory)
        {
            try
            {
                InitializeComponent();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    HeaderOfpage.IsVisible = false;
                }
                else
                {
                    NavigationPage.SetHasNavigationBar(this, false);
                }
                    
                DataofStory = DataStory;

                img.Source = DataStory.Thumbnail;
                TitleLabel.Text = DataStory.Title;
                DescriptionLabel.Text = DataofStory.Description;
                Username.Text = DataofStory.User_Name;
                ImageAvatar.Source = DataofStory.User_avatar;

                if (DataStory.User_id != Settings.User_id)
                {
                    TrashImage.IsVisible = false;
                }
                else
                {
                    TrashImage.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnAppearing()
        {
            try
            {
                base.OnAppearing();

                // Workaround to fix image placement issue in iOS. It occurrs only when this 
                // page (within a NavigationPage) is set as the detail of the MasterDetailPage 
                // i.e. Display it from the RootPage
                // navigation view
                var content = this.Content;
                this.Content = null;
                this.Content = content;

                _animate = true;

                AnimateIn().Forget();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnDisappearing()
        {
            try
            {
                base.OnDisappearing();

                _animate = false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void OnCloseButtonClicked(object sender, EventArgs args)
        {
            try
            {
                await Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task AnimateIn()
        {
            try
            {
                await AnimateItem(img, 10500);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async Task AnimateItem(View uiElement, uint duration)
        {
            try
            {
                while (_animate)
                {
                    await uiElement.ScaleTo(1.05, duration, Easing.SinInOut);

                    await Task.WhenAll(
                        uiElement.FadeTo(1, duration, Easing.SinInOut),
                        uiElement.LayoutTo(new Rectangle(new Point(0, 0), new Size(uiElement.Width, uiElement.Height))),
                        uiElement.FadeTo(.9, duration, Easing.SinInOut),
                        uiElement.ScaleTo(1.15, duration, Easing.SinInOut)
                    );
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnUsernameTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new UserProfilePage(DataofStory.User_id, "FriendList"));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.PushModalAsync(new UserProfilePage(DataofStory.User_id, "FriendList"));
            }
        }

        private void OnTrashImageTapped(object sender, EventArgs e)
        {
            try
            {
                Trachdata = DataofStory;
                try
                {
                    Navigation.PopAsync(true);
                }
                catch (Exception ex)
                {
                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    Navigation.PopModalAsync(true);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

    }
}
