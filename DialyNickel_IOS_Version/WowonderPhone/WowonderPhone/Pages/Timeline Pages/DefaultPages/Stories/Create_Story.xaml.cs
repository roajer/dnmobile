﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Plugin.Media;
using Plugin.Media.Abstractions;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Tabs;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Stories
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Create_Story : ContentPage
    {
        #region classes

        public class Catatgorytems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public ImageSource Image { get; set; }
            public Stream ImageStream { get; set; }
            public string ImageFullPath { get; set; }
        }

        #endregion


        #region Variables and list

        public static ObservableCollection<Catatgorytems> ActivityListItems = new ObservableCollection<Catatgorytems>();
        public string PikedImage = "";

        #endregion


        public Create_Story()
        {
            try
            {
                InitializeComponent();

                ActivityListItems.Clear();

                if (ActivityListItems.Count == 0)
                {
                    ActivityListItems.Add(new Catatgorytems()
                    {
                        Label = AppResources.Label_Pick_Take_Photo,
                        Icon = "\uf03e",
                    });
                }
                AttachmentList.ItemsSource = ActivityListItems;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void DescriptionEntry_OnFocused(object sender, FocusEventArgs e)
        {
            try
            {
                if (DescriptionEntry.Text == AppResources.Label2_Description)
                {
                    DescriptionEntry.Text = "";
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AttachmentList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                AttachmentList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void AttachmentList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Catatgorytems;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_Pick_Take_Photo)
                    {
                        var action = await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null, AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                        if (action == AppResources.Label_Choose_from_Galery)
                        {
                            await CrossMedia.Current.Initialize();
                            if (!CrossMedia.Current.IsPickPhotoSupported)
                            {
                                await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                                return;
                            }
                            var file = await CrossMedia.Current.PickPhotoAsync();

                            if (file == null)
                                return;

                            AttachmentList.IsVisible = true;

                            PikedImage = Functions.TrimTo(file.Path.Split('/').Last(), 30);
                            ActivityListItems.Add(new Catatgorytems
                            {
                                Label = PikedImage,
                                Icon = "\uf030",
                                ImageFullPath = file.Path,
                                ImageStream = file.GetStream()
                            });

                            AttachmentList.ItemsSource = ActivityListItems;

                        }
                        else if (action == AppResources.Label_Take_a_Picture)
                        {
                            await CrossMedia.Current.Initialize();

                            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                            {
                                await DisplayAlert(AppResources.Label2_No_Camera, AppResources.Label_No_camera_avaialble, AppResources.Label_OK);
                                return;
                            }
                            var time = DateTime.Now.ToString("hh:mm");
                            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                            {
                                PhotoSize = PhotoSize.Medium,
                                CompressionQuality = 92,
                                SaveToAlbum = true,
                                Name = time + "PictureProduct.jpg"
                            });

                            if (file == null)
                                return;

                            AttachmentList.IsVisible = true;
                            PikedImage = Functions.TrimTo(file.Path.Split('/').Last(), 30);

                            ActivityListItems.Add(new Catatgorytems
                            {
                                Label = PikedImage,
                                Icon = "\uf030",
                                ImageFullPath = file.Path,
                                ImageStream = file.GetStream()
                            });
                            AttachmentList.ItemsSource = ActivityListItems;
                        }
                    }
                    else
                    {
                        try
                        {
                            var Function = await DisplayAlert(AppResources.Label_Question, AppResources.Label_Do_you_want_to_add_new_image, AppResources.Label_Yes, AppResources.Label_NO);
                            if (Function)
                            {
                                //ActivityListItems.Clear();
                                var action =
                                    await DisplayActionSheet(AppResources.Label_Photo, AppResources.Label_Cancel, null, AppResources.Label_Choose_from_Galery, AppResources.Label_Take_a_Picture);
                                if (action == AppResources.Label_Choose_from_Galery)
                                {
                                    await CrossMedia.Current.Initialize();
                                    if (!CrossMedia.Current.IsPickPhotoSupported)
                                    {
                                        await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image, AppResources.Label_OK);
                                        return;
                                    }
                                    var file = await CrossMedia.Current.PickPhotoAsync();
                                    if (file == null)
                                        return;

                                    var streamReader = new StreamReader(file.GetStream());
                                    var bytes = default(byte[]);
                                    using (var memstream = new MemoryStream())
                                    {
                                        streamReader.BaseStream.CopyTo(memstream);
                                        bytes = memstream.ToArray();
                                    }
                                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                                    AttachmentList.IsVisible = true;
                                    //if (ActivityListItems.Count() > 0)
                                    //{
                                    //    ActivityListItems.Clear();
                                    //}

                                    PikedImage = Functions.TrimTo(file.Path.Split('/').Last(), 30);
                                    ActivityListItems.Add(new Catatgorytems
                                    {
                                        Label = PikedImage,
                                        Icon = "\uf030",
                                        ImageFullPath = file.Path,
                                        ImageStream = file.GetStream()
                                    });

                                    AttachmentList.ItemsSource = ActivityListItems;

                                }
                                else if (action == AppResources.Label_Take_a_Picture)
                                {
                                    await CrossMedia.Current.Initialize();

                                    if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                                    {
                                        await DisplayAlert(AppResources.Label2_No_Camera, AppResources.Label_No_camera_avaialble, AppResources.Label_OK);
                                        return;
                                    }
                                    var time = DateTime.Now.ToString("hh:mm");
                                    var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                                    {
                                        PhotoSize = PhotoSize.Medium,
                                        CompressionQuality = 92,
                                        SaveToAlbum = true,
                                        Name = time + "PictureProduct.jpg"
                                    });

                                    if (file == null)
                                        return;

                                    AttachmentList.IsVisible = true;
                                    //if (ActivityListItems.Count() > 0)
                                    //{
                                    //    ActivityListItems.Clear();
                                    //}
                                    PikedImage = Functions.TrimTo(file.Path.Split('/').Last(), 30);
                                    ActivityListItems.Add(new Catatgorytems
                                    {
                                        Label = PikedImage,
                                        Icon = "\uf030",
                                        ImageFullPath = file.Path,
                                        ImageStream = file.GetStream()
                                    });

                                    AttachmentList.ItemsSource = ActivityListItems;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void CreateButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var Attach = ActivityListItems.FirstOrDefault(a => a.Label == PikedImage);
                if (Attach != null)
                {
                    List<Stream> Streams = new List<Stream>();

                    foreach (var Atta in ActivityListItems)
                    {
                        if (Atta.ImageStream != null)
                        {
                            Streams.Add(Atta.ImageStream);
                        }
                    }

                    UserDialogs.Instance.ShowLoading(AppResources.Label_Uploading);
                    await Task.Factory.StartNew(() =>
                   {
                     var data =  DependencyService.Get<IMethods>().AddStory(Streams, Attach.ImageFullPath, TitileNameEntry.Text, DescriptionEntry.Text);
                   });

                    Status_Page.StoriesListCollection.Insert(0, new Story()
                    {
                        Thumbnail = Attach.ImageFullPath,
                        User_avatar = Settings.Avatarimage,
                        ID =Functions.RandomString(5),
                        User_id = Settings.User_id,
                        Title = TitileNameEntry.Text,
                        Description = DescriptionEntry.Text,
                        Is_Owner = "True",
                        User_Name = Settings.UserFullName,
                        ListofImages = new List<ImageSource>(Attach.ImageFullPath[0])
                    });

                    UserDialogs.Instance.HideLoading();
                    UserDialogs.Instance.ShowSuccess(AppResources.Label2_Story_Added, 2500);

                    await Navigation.PopAsync(false);
                }
                else
                {
                   await DisplayAlert(AppResources.Label2_Note, AppResources.Label2_You_cannot_without_any_image, AppResources.Label_OK);
                }
            }
            catch (Exception)
            {
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2500);
            }
        }
    }
}
