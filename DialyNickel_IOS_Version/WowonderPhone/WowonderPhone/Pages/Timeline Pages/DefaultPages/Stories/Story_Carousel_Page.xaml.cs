﻿using System;
using WowonderPhone.Classes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Stories
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Story_Carousel_Page : CarouselPage
    {
        public Story_Carousel_Page(Story DataStory)
        {
            try
            {
                InitializeComponent();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    //HeaderOfpage.IsVisible = false;
                }
                else{
                    NavigationPage.SetHasNavigationBar(this, false);
                }
                    
                for (var i = 0; i < DataStory.ListofImages.Count; i++)
                {

                    Story ff = new Story();
                    ff.Title = DataStory.Title;
                    ff.Description = DataStory.Description;
                    ff.User_Name = DataStory.User_Name;
                    ff.User_avatar = DataStory.User_avatar;
                    ff.User_id = DataStory.User_id;
                    ff.Author = DataStory.Author;
                    ff.ID = DataStory.ID;
                    ff.Is_Owner = DataStory.Is_Owner;
                    ff.Thumbnail = DataStory.ListofImages[i];

                    var item = new Story_Page(ff);
                    item.BindingContext = ff;
                    Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
