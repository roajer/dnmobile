﻿using System;
using Plugin.MediaManager;
using WowonderPhone.Classes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages.DefaultPages.Stories
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Storyvideos_Page : ContentPage
    {
        private Story _Story = null;
        public static Story Trachdata = null;
        public Storyvideos_Page(Story Story)
        {
            try
            {
                InitializeComponent();

                _Story = Story;
                if (_Story != null)
                {
                    Title = _Story.User_Name;

                    TitleLabel.Text = _Story.Title;
                    DescriptionLabel.Text = _Story.Description;

                    if (_Story.Is_Owner == "True" || _Story.Is_Owner == "true")
                    {
                        TrashImage.IsVisible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AutoRun_Progress()
        {
            try
            {
                CrossMediaManager.Current.PlayingChanged += (sender, e) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (Loaderspinner.IsVisible == true)
                        {
                            Loaderspinner.IsVisible = false;
                        }
                    });
                };

                CrossMediaManager.Current.MediaFinished += (sender, e) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        try
                        {
                            Navigation.PopAsync(true);
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    });
                };
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Storyvideos_Page_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                if (_Story != null)
                {
                    VideoPlayer.Source = _Story.filename;
                    AutoRun_Progress();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnTrashImageTapped(object sender, EventArgs e)
        {
            try
            {
                Trachdata = _Story;
                try
                {
                    Navigation.PopAsync(true);
                }
                catch (Exception ex)
                {
                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                    Navigation.PopModalAsync(true);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}