﻿using System;
using System.Collections.ObjectModel;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages
{
    public partial class InfoViewer : ContentPage
    {
        #region classes

        public class Infotems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Color { get; set; }
        }

        #endregion

        #region list

        public static ObservableCollection<Infotems> InfotemsListCollection = new ObservableCollection<Infotems>();

        #endregion

        public InfoViewer(string Page)
        {
            try
            {
                InitializeComponent();

                InfotemsListCollection.Clear();

                if (Page == "User")
                {
                    #region User list

                    Title = AppResources.Label_About + " " + UserProfilePage.S_Name;
                    if (UserProfilePage.S_About != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_About,
                            Icon = "\uf040",
                            Color = "#c5c9c8"
                        });
                    }

                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = UserProfilePage.S_Gender,
                        Icon = "\uf224",
                        Color = "#c5c9c8"
                    });

                    if (UserProfilePage.S_Birthday != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Birthday,
                            Icon = "\uf133",
                            Color = "#c5c9c8"
                        });
                    }

                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = UserProfilePage.S_Address,
                        Icon = "\uf041",
                        Color = "#c5c9c8"
                    });

                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = UserProfilePage.S_Website,
                        Icon = "\uf0ac",
                        Color = "#c5c9c8"
                    });

                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = UserProfilePage.S_School,
                        Icon = "\uf19d",
                        Color = "#c5c9c8"
                    });

                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = UserProfilePage.S_Working,
                        Icon = "\uf0b1",
                        Color = "#c5c9c8"
                    });

                    if (UserProfilePage.S_Facebook != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Facebook,
                            Icon = "\uf09a",
                            Color = "#00487b"
                        });

                    }
                    if (UserProfilePage.S_Google != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Google,
                            Icon = "\uf0d5",
                            Color = "#be2020"
                        });
                    }

                    if (UserProfilePage.S_VK != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_VK,
                            Icon = "\uf189",
                            Color = "#326c95"
                        });
                    }

                    if (UserProfilePage.S_Twitter != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Twitter,
                            Icon = "\uf099",
                            Color = "#5a89aa"
                        });
                    }

                    if (UserProfilePage.S_Youtube != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Youtube,
                            Icon = "\uf16a",
                            Color = "#be2020"
                        });
                    }

                    if (UserProfilePage.S_Linkedin != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Linkedin,
                            Icon = "\uf0e1",
                            Color = "#5a89aa"
                        });
                    }

                    if (UserProfilePage.S_Instagram != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = UserProfilePage.S_Instagram,
                            Icon = "\uf16d",
                            Color = "#5a89aa"
                        });
                    }
                    InfoList.ItemsSource = InfotemsListCollection;

                    #endregion
                }
                else if (Page == "Page")
                {

                    Title = AppResources.Label_About + " " + SocialPageViewer.S_Name;

                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = SocialPageViewer.S_About,
                        Icon = "\uf040",
                        Color = "#c5c9c8"
                    });
                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = SocialPageViewer.S_Category,
                        Icon = "\uf0a1",
                        Color = "#c5c9c8"
                    });
                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = SocialPageViewer.S_Address,
                        Icon = "\uf041",
                        Color = "#c5c9c8"
                    });
                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = SocialPageViewer.S_Website,
                        Icon = "\uf0ac",
                        Color = "#c5c9c8"
                    });
                    InfotemsListCollection.Add(new Infotems()
                    {
                        Label = SocialPageViewer.S_Company,
                        Icon = "\uf0b1",
                        Color = "#c5c9c8"
                    });
                    if (SocialPageViewer.S_Facebook != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_Facebook,
                            Icon = "\uf09a",
                            Color = "#00487b"
                        });
                    }
                    if (SocialPageViewer.S_Google != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_Google,
                            Icon = "\uf0d5",
                            Color = "#be2020"
                        });
                    }
                    if (SocialPageViewer.S_VK != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_VK,
                            Icon = "\uf189",
                            Color = "#326c95"
                        });
                    }
                    if (SocialPageViewer.S_Twitter != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_Twitter,
                            Icon = "\uf099",
                            Color = "#5a89aa"
                        });
                    }
                    if (SocialPageViewer.S_Youtube != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_Youtube,
                            Icon = "\uf16a",
                            Color = "#be2020"
                        });
                    }
                    if (SocialPageViewer.S_Linkedin != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_Linkedin,
                            Icon = "\uf0e1",
                            Color = "#5a89aa"
                        });
                    }
                    if (SocialPageViewer.S_Instagram != "")
                    {
                        InfotemsListCollection.Add(new Infotems()
                        {
                            Label = SocialPageViewer.S_Instagram,
                            Icon = "\uf16d",
                            Color = "#5a89aa"
                        });
                    }
                    InfoList.ItemsSource = InfotemsListCollection;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void InfoList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                if (e.Item is Infotems item)
                {
                    #region For Users

                    if (item.Label == UserProfilePage.S_Facebook && UserProfilePage.S_Facebook != "")
                    {
                        Device.OpenUri(new Uri("https://facebook.com/" + UserProfilePage.S_Facebook));
                    }
                    if (item.Label == UserProfilePage.S_Twitter && UserProfilePage.S_Twitter != "")
                    {
                        Device.OpenUri(new Uri("https://twitter.com/" + UserProfilePage.S_Twitter));
                    }
                    if (item.Label == UserProfilePage.S_VK && UserProfilePage.S_VK != "")
                    {
                        Device.OpenUri(new Uri("https://vk.com/" + UserProfilePage.S_VK));
                    }

                    if (item.Label == UserProfilePage.S_Linkedin && UserProfilePage.S_Linkedin != "")
                    {
                        Device.OpenUri(new Uri("https://www.linkedin.com/" + UserProfilePage.S_Linkedin));
                    }

                    if (item.Label == UserProfilePage.S_Google && UserProfilePage.S_Google != "")
                    {
                        Device.OpenUri(new Uri("https://plus.google.com/" + UserProfilePage.S_Google));
                    }

                    if (item.Label == UserProfilePage.S_Youtube && UserProfilePage.S_Youtube != "")
                    {
                        Device.OpenUri(new Uri("https://www.youtube.com/" + UserProfilePage.S_Youtube));
                    }

                    if (item.Label == UserProfilePage.S_Instagram && UserProfilePage.S_Instagram != "")
                    {
                        Device.OpenUri(new Uri("https://www.youtube.com/" + UserProfilePage.S_Instagram));
                    }

                    if (item.Label == UserProfilePage.S_Website)
                    {
                        if (UserProfilePage.S_Website != "" || UserProfilePage.S_Website.Contains(".com") || UserProfilePage.S_Website.Contains("http:") || UserProfilePage.S_Website.Contains("www."))
                            Device.OpenUri(new Uri(UserProfilePage.S_Website));
                    }


                    #endregion
                    //>>>>>>>>>>>>>>>
                    #region for Pages

                    if (item.Label == SocialPageViewer.S_Facebook && SocialPageViewer.S_Facebook != "")
                    {
                        Device.OpenUri(new Uri("https://facebook.com/" + SocialPageViewer.S_Facebook));
                    }
                    if (item.Label == SocialPageViewer.S_Twitter && SocialPageViewer.S_Twitter != "")
                    {
                        Device.OpenUri(new Uri("https://twitter.com/" + SocialPageViewer.S_Twitter));
                    }
                    if (item.Label == SocialPageViewer.S_VK && SocialPageViewer.S_VK != "")
                    {
                        Device.OpenUri(new Uri("https://vk.com/" + SocialPageViewer.S_VK));
                    }

                    if (item.Label == SocialPageViewer.S_Linkedin && SocialPageViewer.S_Linkedin != "")
                    {
                        Device.OpenUri(new Uri("https://www.linkedin.com/" + SocialPageViewer.S_Linkedin));
                    }

                    if (item.Label == SocialPageViewer.S_Google && SocialPageViewer.S_Google != "")
                    {
                        Device.OpenUri(new Uri("https://plus.google.com/" + SocialPageViewer.S_Google));
                    }

                    if (item.Label == SocialPageViewer.S_Youtube && SocialPageViewer.S_Youtube != "")
                    {
                        Device.OpenUri(new Uri("https://www.youtube.com/" + SocialPageViewer.S_Youtube));
                    }

                    if (item.Label == SocialPageViewer.S_Instagram && SocialPageViewer.S_Instagram != "")
                    {
                        Device.OpenUri(new Uri("https://www.youtube.com/" + SocialPageViewer.S_Instagram));
                    }

                    if (item.Label == SocialPageViewer.S_Website)
                    {
                        if (SocialPageViewer.S_Website != "" || SocialPageViewer.S_Website.Contains(".com") || SocialPageViewer.S_Website.Contains("http:") || SocialPageViewer.S_Website.Contains("www."))
                            Device.OpenUri(new Uri(SocialPageViewer.S_Website));
                    }

                    #endregion
                    //>>>>>>>>>>>>>>
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void InfoList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                InfoList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
