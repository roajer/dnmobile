﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Tabs;
using Xamarin.Forms;
using XLabs.Forms.Controls;

namespace WowonderPhone.Pages.Timeline_Pages.Friend_System
{
    public partial class Friend_Request_Page : ContentPage
    {
        public Friend_Request_Page()
        {
            try
            {
                InitializeComponent();

                FriendsRequestListview.ItemsSource = ItemListPage.FriendRequestsItemsCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void FriendsRequestListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                FriendsRequestListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void FriendsRequestListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                FriendsRequestListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ConfirmButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var mi = ((ImageButton)sender);
                if (mi != null)
                {
                    var Item = ItemListPage.FriendRequestsItemsCollection.FirstOrDefault(a => a.User_id == mi.CommandParameter.ToString());
                    if (Item != null)
                    {
                        Item.AccepetedIsvisbile = "True";
                        Item.ConfirmButtonIsvisbile = "False";
                        Item.DeleteButtonIsvisbile = "False";
                        Accept_Request(Item.User_id).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void DeleteButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var mi = ((ImageButton)sender);
                if (mi != null)
                {
                    var Item = ItemListPage.FriendRequestsItemsCollection.FirstOrDefault(a => a.User_id == mi.CommandParameter.ToString());
                    var Index = ItemListPage.FriendRequestsItemsCollection.IndexOf(Item);
                    if (Item != null)
                    {

                        Item.AccepetedIsvisbile = "True";
                        Item.AccepetedText = AppResources.Label_Request_removed;
                        Item.ConfirmButtonIsvisbile = "False";
                        Item.DeleteButtonIsvisbile = "False";
                        Decline_Request(Item.User_id).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Accept_Request(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("request", "accept")
                    });

                    var response =
                        await
                            client.PostAsync(
                                Settings.Website + "/app_api.php?application=phone&type=accept_decline_request", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Decline_Request(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("request", "decline")
                    });

                    var response =
                        await
                            client.PostAsync(
                                Settings.Website + "/app_api.php?application=phone&type=accept_decline_request", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
