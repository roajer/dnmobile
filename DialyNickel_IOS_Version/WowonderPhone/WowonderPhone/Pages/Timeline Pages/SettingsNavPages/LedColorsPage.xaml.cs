﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PropertyChanged;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.SettingsNavPages
{
    [ImplementPropertyChanged]
    public class LedColorstems
    {
        public string Checkicon { get; set; }
        public string ColorText { get; set; }
        public string CheckiconColor { get; set; }

    }

    public partial class LedColorsPage : ContentPage
    {
        public static ObservableCollection<LedColorstems> LedColorsListItems =
            new ObservableCollection<LedColorstems>();

        public LedColorsPage()
        {
            try
            {
                InitializeComponent();
                Addtolistcolors();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void LedList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                LedList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void Addtolistcolors()
        {
            try
            {
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Default,
                    CheckiconColor = Settings.MainColor
                });

                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Green,
                    CheckiconColor = "#00FF00"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Blue,
                    CheckiconColor = "#0000FF"
                });

                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Magenta,
                    CheckiconColor = "#FF00FF"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Orange,
                    CheckiconColor = "#ffa500"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Silver,
                    CheckiconColor = "#C0C0C0"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Yellow,
                    CheckiconColor = "#FFFF00"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Maroon,
                    CheckiconColor = "#800000"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Purple,
                    CheckiconColor = "#800080"
                });
                LedColorsListItems.Add(new LedColorstems()
                {
                    Checkicon = "\uf192",
                    ColorText = AppResources.Label_Red,
                    CheckiconColor = "#FF0000"
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }

            LedList.ItemsSource = LedColorsListItems;
        }

        private void LedList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as LedColorstems;
                var SettingsLedColor = Notification_Page.LedListItems.First();
                SettingsLedColor.CheckiconColor = Item.CheckiconColor;
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void LedColorsPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                LedColorsListItems.Clear();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
