﻿using System;
using System.Linq;
using PropertyChanged;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;

namespace WowonderPhone.Pages.Timeline_Pages.SettingsNavPages
{
    public partial class Notification_Page : ContentPage
    {
        #region classes

        [ImplementPropertyChanged]
        public class Ledtems
        {
            public string Checkicon { get; set; }
            public string ColorText { get; set; }
            public string CheckiconColor { get; set; }

        }

        #endregion

        #region list

        public static ObservableCollection<Ledtems> LedListItems = new ObservableCollection<Ledtems>();

        #endregion

        public Notification_Page()
        {
            try
            {
                InitializeComponent();
                LedListItems.Clear();
                using (var Data = new LoginFunctions())
                {
                    //Data.ClearLoginCredentialsList();
                    var SettingsNotify = Data.GetLoginCredentialsByUserID(Settings.User_id);
                    if (SettingsNotify != null)
                    {
                        if (SettingsNotify.NotificationLedColor == null)
                        {
                            SettingsNotify.NotificationLedColor = Settings.MainColor;
                            SettingsNotify.NotificationLedColorName = AppResources.Label_Led_Color;


                            LedListItems.Add(new Ledtems()
                            {
                                Checkicon = "\uf111",
                                ColorText = AppResources.Label_Led_Color,
                                CheckiconColor = Settings.MainColor
                            });
                        }
                        else
                        {
                            LedListItems.Add(new Ledtems()
                            {
                                Checkicon = "\uf111",
                                ColorText = AppResources.Label_Led_Color,
                                CheckiconColor = SettingsNotify.NotificationLedColor
                            });
                        }

                        NotifyVibrate.IsToggled = Settings.NotificationVibrate;
                        NotifyPopup.IsToggled = Settings.NotificationPopup;
                        NotifySound.IsToggled = Settings.NotificationSound;
                    }
                }
                LedList.ItemsSource = LedListItems;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void LedList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                LedList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void LedList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                Navigation.PushAsync(new LedColorsPage());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        private void NotifyVibrate_OnToggled(object sender, ToggledEventArgs e)
        {

        }

        private void NotifySound_OnToggled(object sender, ToggledEventArgs e)
        {

        }

        private void NotifyPopup_OnToggled(object sender, ToggledEventArgs e)
        {
            try
            {
                if (NotifyPopup.IsToggled)
                {
                    OneSignalNotificationController.RegisterNotificationDevice();
                }
                else
                {
                    OneSignalNotificationController.Un_RegisterNotificationDevice();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Notification_Page_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                var Data = new LoginFunctions();
                var SettingsNotify = Data.GetLoginCredentialsByUserID(Settings.User_id);
                var Coler = LedListItems.First();
                if (SettingsNotify != null)
                {
                    Settings.NotificationPopup = NotifyPopup.IsToggled;
                    SettingsNotify.NotificationPopup = NotifyPopup.IsToggled;
                    Settings.NotificationSound = NotifySound.IsToggled;
                    SettingsNotify.NotificationSound = NotifySound.IsToggled;
                    Settings.NotificationVibrate = NotifyVibrate.IsToggled;
                    SettingsNotify.NotificationVibrate = NotifyVibrate.IsToggled;
                    SettingsNotify.NotificationLedColor = Coler.CheckiconColor;
                    SettingsNotify.NotificationLedColorName = Coler.ColorText;
                    Settings.NotificationLedColor = Coler.CheckiconColor;
                    Settings.NotificationLedColorName = Coler.ColorText;
                    Data.UpdateLoginCredentials(SettingsNotify);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
