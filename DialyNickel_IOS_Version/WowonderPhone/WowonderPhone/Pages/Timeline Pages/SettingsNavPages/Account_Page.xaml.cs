﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using XLabs;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.SettingsNavPages
{
    public partial class Account_Page : ContentPage
    {
        #region Variables

        public string Username = "";
        public string Email = "";
        public string Phone_number = "";
        public string GenderStatus = "";

        #endregion

        public Account_Page()
        {
            try
            {
                InitializeComponent();

                StyleChanger();
                using (var DataLoader = new LoginUserProfileFunctions())
                {
                    var DataRow = DataLoader.GetProfileCredentialsById(Settings.User_id);

                    UsernameEntry.Text = DataRow.username;
                    EmailEntry.Text = DataRow.email;
                    PhoneEntry.Text = DataRow.phone_number;

                    Username = DataRow.username;
                    Email = DataRow.email;
                    Phone_number = DataRow.phone_number;

                    if (DataRow.gender == "Male" || DataRow.gender == "male")
                    {
                        Male.Checked = true;
                        GenderStatus = "male";
                        Female.Checked = false;
                    }
                    else
                    {
                        Male.Checked = false;
                        Female.Checked = true;
                        GenderStatus = "female";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Male_OnCheckedChanged(object sender, EventArgs<bool> e)
        {
            try
            {
                if (Male.Checked)
                {
                    Female.Checked = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Female_OnCheckedChanged(object sender, EventArgs<bool> e)
        {
            try
            {
                if (Female.Checked)
                {
                    Male.Checked = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async Task<string> UpdateSettings()
        {
            try
            {
                string Gender = "male";
                if (Female.Checked == true)
                {
                    Gender = "female";
                }
                var dictionary = new Dictionary<string, string>
                {
                    {"username", UsernameEntry.Text},
                    {"email", EmailEntry.Text},
                    {"gender", Gender},
                    {"phone_number", PhoneEntry.Text},
                };

                string Data = JsonConvert.SerializeObject(dictionary);

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("type", "general_settings"),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("user_data", Data)
                    });

                    var response = await client
                        .PostAsync(Settings.Website + "/app_api.php?application=phone&type=update_user_data",
                            formContent)
                        .ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {

                    }
                    else if (apiStatus == "500")
                    {
                        var errors_sugParse = JObject.Parse(json).SelectToken("errors").ToString();
                        JArray errors = JArray.Parse(errors_sugParse);
                        foreach (var error in errors)
                        {
                            await DisplayAlert(AppResources.Label_Error, error.ToString(), AppResources.Label_OK);
                        }

                    }
                    else
                    {

                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        public void StyleChanger()
        {
            try
            {
                UsernameLabel.Text = AppResources.Label_Username;
                EmailLabel.Text = AppResources.Label_Email;
                Phone.Text = AppResources.Label_Phone;
                Gender.Text = AppResources.Label_Gender;
                Male.CheckedText = AppResources.Label_Male;
                Female.CheckedText = AppResources.Label_Female;
                CurrentPassword.Text = AppResources.Label_CurrentPassword;
                NewPassword.Text = AppResources.Label_NewPassword;
                RepeatPassword.Text = AppResources.Label_RepeatPassword;
                PhoneEntry.Placeholder = AppResources.Label_Empty;
                EmailEntry.Placeholder = AppResources.Label_Empty;
                UsernameEntry.Placeholder = AppResources.Label_Empty;
                CurrentPasswordEntry.Placeholder = AppResources.Label_Empty;
                NewpasswordEntry.Placeholder = AppResources.Label_Empty;
                RepeatPasswordEntry.Placeholder = AppResources.Label_Empty;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void Account_Page_OnDisappearing(object sender, EventArgs e)
        {
            //try
            //{
            //    var device = Resolver.Resolve<IDevice>();
            //    var oNetwork = device.Network; // Create Interface to Network-functions
            //    var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
            //    if (xx == true)
            //    {
            //        await DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
            //    }
            //    else
            //    {
            //        if (NewpasswordEntry.Text != RepeatPasswordEntry.Text)
            //        {
            //            await DisplayAlert(AppResources.Label_Error, AppResources.Label_Your_Password_Dont_Match, AppResources.Label_OK);
            //            return;
            //        }
            //        else
            //        {
            //            #region Password Update

            //            //if (NewpasswordEntry.Text != null && RepeatPasswordEntry.Text != null &&
            //            //    CurrentPasswordEntry.Text != null)
            //            //{

            //            //    using (var client = new HttpClient())
            //            //    {
            //            //        var Passwords = new Dictionary<string, string>
            //            //        {
            //            //            {"current_password", CurrentPasswordEntry.Text},
            //            //            {"new_password", NewpasswordEntry.Text},
            //            //            {"repeat_new_password", RepeatPasswordEntry.Text}
            //            //        };

            //            //        string Pass = JsonConvert.SerializeObject(Passwords);

            //            //        var formContent = new FormUrlEncodedContent(new[]
            //            //        {
            //            //            new KeyValuePair<string, string>("user_id", Settings.User_id),
            //            //            new KeyValuePair<string, string>("type", "password_settings"),
            //            //            new KeyValuePair<string, string>("s", Settings.Session),
            //            //            new KeyValuePair<string, string>("user_data", Pass)
            //            //        });

            //            //        var response =
            //            //            await
            //            //                client.PostAsync(
            //            //                    Settings.Website + "/app_api.php?application=phone&type=update_user_data",
            //            //                    formContent);
            //            //        response.EnsureSuccessStatusCode();
            //            //        string json = await response.Content.ReadAsStringAsync();
            //            //        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            //            //        string apiStatus = data["api_status"].ToString();
            //            //        if (apiStatus == "200")
            //            //        {

            //            //        }

            //            //        else if (apiStatus == "500")
            //            //        {

            //            //            var errors_sugParse = JObject.Parse(json).SelectToken("errors").ToString();
            //            //            Object usersobj = JsonConvert.DeserializeObject(errors_sugParse);
            //            //            JArray errors = JArray.Parse(errors_sugParse);


            //            //            foreach (var error in errors)
            //            //            {
            //            //                await DisplayAlert(AppResources.Label_Error, error.ToString(), AppResources.Label_OK);
            //            //            }

            //            //        }
            //            //        else
            //            //        {

            //            //        }
            //            //    }
            //            //}

            //            #endregion

            //            #region General Settings

            //            try
            //            {
            //                string Gender = "male";
            //                if (Female.Checked == true)
            //                {
            //                    Gender = "female";
            //                }

            //                if (UsernameEntry.Text != Username || EmailEntry.Text != Email ||
            //                    PhoneEntry.Text != Phone_number ||
            //                    Gender != GenderStatus)
            //                {
            //                    var dictionary = new Dictionary<string, string>
            //                    {
            //                        {"username", UsernameEntry.Text},
            //                        {"email", EmailEntry.Text},
            //                        {"gender", Gender},
            //                        {"phone_number", PhoneEntry.Text},
            //                    };

            //                    string Data = JsonConvert.SerializeObject(dictionary);

            //                    using (var client = new HttpClient())
            //                    {
            //                        var formContent1 = new FormUrlEncodedContent(new[]
            //                        {
            //                            new KeyValuePair<string, string>("user_id", Settings.User_id),
            //                            new KeyValuePair<string, string>("type", "general_settings"),
            //                            new KeyValuePair<string, string>("s", Settings.Session),
            //                            new KeyValuePair<string, string>("user_data", Data)
            //                        });

            //                        var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=u_user_data&report_errors=1", formContent1);
            //                        response.EnsureSuccessStatusCode();

            //                        string json = await response.Content.ReadAsStringAsync();
            //                        var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
            //                        string apiStatus = data["api_status"].ToString();
            //                        if (apiStatus == "200")
            //                        {
            //                            using (var DataLoader = new LoginUserProfileFunctions())
            //                            {
            //                                var Update = DataLoader.GetProfileCredentialsById(Settings.User_id);
            //                                Update.Email = EmailEntry.Text;
            //                                Update.Username = UsernameEntry.Text;
            //                                Update.Phone_number = PhoneEntry.Text;
            //                                Update.Gender = Gender;
            //                                DataLoader.UpdateProfileCredentials(Update);
            //                            }
            //                        }
            //                        else if (apiStatus == "500")
            //                        {
            //                            var errors_sugParse = JObject.Parse(json).SelectToken("errors").ToString();
            //                            Object usersobj = JsonConvert.DeserializeObject(errors_sugParse);
            //                            JArray errors = JArray.Parse(errors_sugParse);

            //                            foreach (var error in errors)
            //                            {
            //                                await DisplayAlert(AppResources.Label_Error, error.ToString(), AppResources.Label_OK);
            //                            }
            //                        }
            //                        else
            //                        {

            //                        }
            //                    }
            //                }
            //            }
            //            catch (Exception ex)
            //            {
            //                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            //            }

            //            #endregion
            //        }
            //    }
            //}
            //catch (Exception)
            //{

            //}
        }

        private async void Btn_Updatedata_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
                }
                else
                {
                    if (NewpasswordEntry.Text != RepeatPasswordEntry.Text)
                    {
                        await DisplayAlert(AppResources.Label_Error, AppResources.Label_Your_Password_Dont_Match, AppResources.Label_OK);
                    }
                    else
                    {
                        #region Password Update

                        if (NewpasswordEntry.Text != null && RepeatPasswordEntry.Text != null &&
                            CurrentPasswordEntry.Text != null)
                        {
                            try
                            {
                                using (var client = new HttpClient())
                                {
                                    var Passwords = new Dictionary<string, string>
                                    {
                                        {"current_password", CurrentPasswordEntry.Text},
                                        {"new_password", NewpasswordEntry.Text},
                                        {"repeat_new_password", RepeatPasswordEntry.Text}
                                    };

                                    string Pass = JsonConvert.SerializeObject(Passwords);

                                    var formContent = new FormUrlEncodedContent(new[]
                                    {
                                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                                        new KeyValuePair<string, string>("type", "password_settings"),
                                        new KeyValuePair<string, string>("s", Settings.Session),
                                        new KeyValuePair<string, string>("user_data", Pass)
                                    });

                                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=update_user_data", formContent);
                                    response.EnsureSuccessStatusCode();
                                    string json = await response.Content.ReadAsStringAsync();
                                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                                    string apiStatus = data["api_status"].ToString();
                                    if (apiStatus == "200")
                                    {
                                         await DisplayAlert(AppResources.Label2_successfull_completed, AppResources.Label2_Password_changed, AppResources.Label_OK);
                                    }
                                    else if (apiStatus == "500")
                                    {

                                        var errors_sugParse = JObject.Parse(json).SelectToken("errors").ToString();
                                        Object usersobj = JsonConvert.DeserializeObject(errors_sugParse);
                                        JArray errors = JArray.Parse(errors_sugParse);

                                        foreach (var error in errors)
                                        {
                                            await DisplayAlert(AppResources.Label_Error, error.ToString(), AppResources.Label_OK);
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        #endregion

                        #region General Settings

                        try
                        {
                            string Gender = "male";
                            if (Female.Checked == true)
                            {
                                Gender = "female";
                            }

                            if (UsernameEntry.Text != Username || EmailEntry.Text != Email ||
                                PhoneEntry.Text != Phone_number ||
                                Gender != GenderStatus)
                            {
                                var dictionary = new Dictionary<string, string>
                                {
                                    {"username", UsernameEntry.Text},
                                    {"email", EmailEntry.Text},
                                    {"gender", Gender},
                                    {"phone_number", PhoneEntry.Text},
                                };

                                string Data = JsonConvert.SerializeObject(dictionary);

                                using (var client = new HttpClient())
                                {
                                    var formContent1 = new FormUrlEncodedContent(new[]
                                    {
                                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                                        new KeyValuePair<string, string>("type", "general_settings"),
                                        new KeyValuePair<string, string>("s", Settings.Session),
                                        new KeyValuePair<string, string>("user_data", Data)
                                    });

                                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=u_user_data&report_errors=1", formContent1);
                                    response.EnsureSuccessStatusCode();

                                    string json = await response.Content.ReadAsStringAsync();
                                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                                    string apiStatus = data["api_status"].ToString();
                                    if (apiStatus == "200")
                                    {
                                        using (var DataLoader = new LoginUserProfileFunctions())
                                        {
                                            var Update = DataLoader.GetProfileCredentialsById(Settings.User_id);
                                            Update.email = EmailEntry.Text;
                                            Update.username = UsernameEntry.Text;
                                            Update.phone_number = PhoneEntry.Text;
                                            Update.gender = Gender;
                                            DataLoader.UpdateProfileCredentials(Update);
                                        }
                                    }
                                    else if (apiStatus == "500")
                                    {
                                        var errors_sugParse = JObject.Parse(json).SelectToken("errors").ToString();
                                        Object usersobj = JsonConvert.DeserializeObject(errors_sugParse);
                                        JArray errors = JArray.Parse(errors_sugParse);

                                        foreach (var error in errors)
                                        {
                                            await DisplayAlert(AppResources.Label_Error, error.ToString(), AppResources.Label_OK);
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                       
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
