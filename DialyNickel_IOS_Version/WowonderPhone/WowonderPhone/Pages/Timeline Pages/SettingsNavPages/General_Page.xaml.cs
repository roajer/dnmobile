﻿using System;

using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.SettingsNavPages
{
    public partial class General_Page : ContentPage
    {
        public General_Page()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
