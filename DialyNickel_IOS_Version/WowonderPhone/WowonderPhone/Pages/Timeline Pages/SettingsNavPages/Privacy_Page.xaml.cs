﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Timeline_Pages.SettingsNavPages
{
    public partial class Privacy_Page : ContentPage
    {

        #region Variables and list

        public int WhoCanMessageMePicker_E = 0;
        public int WhoCanFollowMe_E = 0;
        public int WhoCanSeeMyBirthday_E = 0;

        List<string> ListOfValues1 = new List<string>();
        List<string> ListOfValues2 = new List<string>();
        List<string> ListOfValues3 = new List< string>();

        #endregion

        public Privacy_Page()
        {
            try
            {
                InitializeComponent();
                StyleChanger();
                AddPickerItems();

                using (var DataLoader = new PrivacyFunctions())
                {
                    var data = DataLoader.GetPrivacyDBCredentialsById(Settings.User_id);
                    if (data != null)
                    {
                        WhocamessagemePicker.SelectedIndex = data.WhoCanMessageMe;
                        Whocanfollowme.SelectedIndex = data.WhoCanFollowMe;
                        Whocanseemybirthday.SelectedIndex = data.WhoCanSeeMyBirday;

                        //>>>>>>>>
                        WhoCanMessageMePicker_E = data.WhoCanMessageMe;
                        WhoCanFollowMe_E = data.WhoCanFollowMe;
                        WhoCanSeeMyBirthday_E = data.WhoCanSeeMyBirday;
                        //>>>>>>>>
                    }
                    else
                    {
                        WhocamessagemePicker.SelectedIndex = 0;
                        Whocanfollowme.SelectedIndex = 0;
                        Whocanseemybirthday.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddPickerItems()
        {
            try
            {
                ListOfValues1.Add(AppResources.Label_Everyone);
                ListOfValues1.Add(AppResources.Label_People_I_Follow);

                ListOfValues1.Add(AppResources.Label_Everyone);
                ListOfValues2.Add(AppResources.Label_People_I_Follow);

                ListOfValues3.Add( AppResources.Label_Everyone);
                ListOfValues3.Add(AppResources.Label_People_I_Follow);
                ListOfValues3.Add(AppResources.Label2_No_Body);

                if (ListOfValues1.Count > 0)
                    Whocanfollowme.ItemsSource = ListOfValues1;

                if (ListOfValues2.Count > 0)
                    WhocamessagemePicker.ItemsSource = ListOfValues2;

                if (ListOfValues3.Count > 0)
                    Whocanseemybirthday.ItemsSource = ListOfValues3;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
         }

        public void StyleChanger()
        {
            try
            {
                FolowOrFriendLabel.Text = AppResources.Label_WhoCan_Follow_me;
                WhoCanSeemybirthdaylabel.Text = AppResources.Label_WhoCan_See_my_Birthday;
                WhoCanmessgamelabel.Text = AppResources.Label_WhoCan_Message_me;

                if (Settings.ConnectivitySystem == "0")
                {
                    FolowOrFriendLabel.Text = AppResources.Label_WhoCan_Add_me;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void PrivacyPage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                if (WhoCanFollowMe_E != Whocanfollowme.SelectedIndex ||
                    WhoCanMessageMePicker_E != WhocamessagemePicker.SelectedIndex ||
                    WhoCanSeeMyBirthday_E != Whocanseemybirthday.SelectedIndex)
                {
                    var device = Resolver.Resolve<IDevice>();
                    var oNetwork = device.Network; // Create Interface to Network-functions
                    var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                    if (xx == true)
                    {
                        await DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection,
                            AppResources.Label_OK);
                    }
                    else
                    {
                        #region Send Data 

                        using (var client = new HttpClient())
                        {
                            var Passwords = new Dictionary<string, string>
                            {
                                {"message_privacy", WhocamessagemePicker.SelectedIndex.ToString()},
                                {"follow_privacy", Whocanfollowme.SelectedIndex.ToString()},
                                {"birth_privacy", Whocanseemybirthday.SelectedIndex.ToString()}
                            };

                            string Pass = JsonConvert.SerializeObject(Passwords);
                            var formContent = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("user_id", Settings.User_id),
                                new KeyValuePair<string, string>("type", "privacy_settings"),
                                new KeyValuePair<string, string>("s", Settings.Session),
                                new KeyValuePair<string, string>("user_data", Pass)
                            });

                            var response =
                                await client.PostAsync(
                                    Settings.Website + "/app_api.php?application=phone&type=update_user_data",
                                    formContent);
                            response.EnsureSuccessStatusCode();
                            string json = await response.Content.ReadAsStringAsync();
                            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                            string apiStatus = data["api_status"].ToString();
                            if (apiStatus == "200")
                            {

                                using (var DataLoader = new PrivacyFunctions())
                                {
                                    var ID = DataLoader.GetPrivacyDBCredentialsById(Settings.User_id);
                                    if (ID != null)
                                    {
                                        DataLoader.UpdatePrivacyDBCredentials(new PrivacyDB()
                                        {
                                            UserID = Settings.User_id,
                                            WhoCanSeeMyBirday = Whocanseemybirthday.SelectedIndex,
                                            WhoCanFollowMe = Whocanfollowme.SelectedIndex,
                                            WhoCanMessageMe = WhocamessagemePicker.SelectedIndex
                                        });
                                    }
                                    else
                                    {
                                        DataLoader.InsertPrivacyDBCredentials(new PrivacyDB()
                                        {
                                            UserID = Settings.User_id,
                                            WhoCanSeeMyBirday = Whocanseemybirthday.SelectedIndex,
                                            WhoCanFollowMe = Whocanfollowme.SelectedIndex,
                                            WhoCanMessageMe = WhocamessagemePicker.SelectedIndex
                                        });
                                    }
                                }
                            }
                            else
                            {

                            }
                        }
                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

      
    }
}
