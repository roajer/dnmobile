﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.AddPostNavPages
{
    public partial class ActivitySelectedPage : ContentPage
    {
        public static string ActivityText = "";

        public string SelectedText = "";

        public ActivitySelectedPage(string ActivitySelected)
        {
            try
            {
                InitializeComponent();
                SelectedText = ActivitySelected;
                this.Title = AppResources.Label_Write_Your_Activity;

                if (ActivitySelected == AppResources.Label_Traveling_To)
                {
                    ActivityEntry.Placeholder = AppResources.Label_Where_are_You_traveling_to;
                }
                else if (ActivitySelected == AppResources.Label_Watching)
                {
                    ActivityEntry.Placeholder = AppResources.Label_What_are_you_watching;
                }
                else if (ActivitySelected == AppResources.Label_Playing_Ac)
                {
                    ActivityEntry.Placeholder = AppResources.Label_What_are_you_playing;
                }
                else if (ActivitySelected == AppResources.Label_Listening_To)
                {
                    ActivityEntry.Placeholder = AppResources.Label_What_are_you_Listening;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActivityEntry_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                ActivityText = ActivityEntry.Text;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }           
        }

        private void AddButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (AddPost.ActivityListItems.Count > 0 )
                {
                    AddPost.ActivityListItems.Clear();
                }

                if (AddPost_On_Communities.ActivityListItems.Count > 0)
                {
                    AddPost_On_Communities.ActivityListItems.Clear();
                }

                if (SelectedText == AppResources.Label_Traveling_To)
                {
                    AddPost.ActivityListItems.Add(new AddPost.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf072",
                        TypePost = "Traveling",
                        Content = ActivityText
                    });

                    AddPost_On_Communities.ActivityListItems.Add(new AddPost_On_Communities.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf072",
                        TypePost = "Traveling",
                        Content = ActivityText
                    });
                }
                else if (SelectedText == AppResources.Label_Watching)
                {
                    AddPost.ActivityListItems.Add(new AddPost.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf06e",
                        TypePost = "Watching",
                        Content = ActivityText
                    });

                    AddPost_On_Communities.ActivityListItems.Add(new AddPost_On_Communities.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf06e",
                        TypePost = "Watching",
                        Content = ActivityText
                    });
                }
                else if (SelectedText == AppResources.Label_Playing_Ac)
                {
                    AddPost.ActivityListItems.Add(new AddPost.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf11b",
                        TypePost = "Playing",
                        Content = ActivityText
                    });

                    AddPost_On_Communities.ActivityListItems.Add(new AddPost_On_Communities.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf11b",
                        TypePost = "Playing",
                        Content = ActivityText
                    });
                }
                else if (SelectedText == AppResources.Label_Listening_To)
                {
                    AddPost.ActivityListItems.Add(new AddPost.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf025",
                        TypePost = "Listening",
                        Content = ActivityText
                    });

                    AddPost_On_Communities.ActivityListItems.Add(new AddPost_On_Communities.Activitytems()
                    {
                        Label = SelectedText + " " + ActivityText,
                        Icon = "\uf025",
                        TypePost = "Listening",
                        Content = ActivityText
                    });
                }

                if (AddPost.IsPageInNavigationStack<ActivitiesPage>(Navigation))
                {
                    Navigation.PopAsync(false);
                    //return;
                }

                if (AddPost_On_Communities.IsPageInNavigationStack<ActivitiesPage>(Navigation))
                {
                    Navigation.PopAsync(false);
                    //return;
                }
                //Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 1]);
                Navigation.PopAsync(false);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
