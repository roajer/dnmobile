﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.AddPostNavPages
{
    public partial class UsersTagPage : ContentPage
    {
        public static ObservableCollection<UserContacts> TagContactsList = new ObservableCollection<UserContacts>();

        public UsersTagPage()
        {
            try
            {
                InitializeComponent();

                Title = AppResources.Label_Mention_Friend;

                using (var data = new ContactsFunctions())
                {
                    TagListview.ItemsSource = data.GetContactCacheList();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnSearchButtonPressed(object sender, EventArgs e)
        {
            try
            {
                if (SearchBarCo.Text != "")
                {
                    TagContactsList.Clear();
                    using (var data = new ContactsFunctions())
                    {
                        TagListview.ItemsSource = data.GetContactTagSearchList(SearchBarCo.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SearchBarCo_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (SearchBarCo.Text != "")
                {
                    TagContactsList.Clear();
                    using (var data = new ContactsFunctions())
                    {
                        var ss = data.GetContactTagSearchList(SearchBarCo.Text);
                        TagListview.ItemsSource = ss;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TagListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                TagListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
       
        private void TagListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as UserContacts;
                if (Item != null)
                {
                    Item.Checkicon = "\uf058";
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void DoneButton_OnClickedButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var Count = Functions.ChatContactsList.Count(a => a.Checkicon == "\uf058");
                var TagGroup = "";
                foreach (var item in Functions.ChatContactsList)
                {
                    if (item.Checkicon == "\uf058")
                    {
                        TagGroup += " @" + item.Username;
                    }
                }

                if (Count > 0)
                {
                    AddPost.ActivityListItems.Add(new AddPost.Activitytems()
                    {
                        Label = "( " + Count + " )" + " " + AppResources.Label_Mentions,
                        Icon = "\uf02c",
                        TypePost = "Mention",
                        Content = TagGroup
                    });

                }

                Navigation.PopAsync();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.RemovePage(this);
            }
        }
    }
}
