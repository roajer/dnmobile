﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.AddPostNavPages
{
   
    public partial class FeelingsPage : ContentPage
    {
        #region classes

        public class Feelingstems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
            public string Value { get; set; }
            public ImageSource Image { get; set; }
        }

        #endregion


        #region Lists Items Declaration

        public static ObservableCollection<Feelingstems> FeelingListItems = new ObservableCollection<Feelingstems>();

        #endregion 

        public FeelingsPage()
        {
            try
            {
                InitializeComponent();

                FeelingListItems.Clear();
                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Angry,
                    Icon = "\uD83D\uDE20",
                    Value = "angry",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Funny,
                    Icon = "\uD83D\uDE02",
                    Value = "funny",
                });
                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Loved,
                    Icon = "\uD83D\uDE0D",
                    Value = "loved",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Cool,
                    Icon = "\uD83D\uDE0E",
                    Value = "cool",
                });
                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Happy,
                    Icon = "\uD83D\uDE03",
                    Value = "happy",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Tired,
                    Icon = "\uD83D\uDE2B",
                    Value = "tired",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Sleepy,
                    Icon = "\uD83D\uDE34",
                    Value = "sleepy",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Expressionless,
                    Icon = "\uD83D\uDE11",
                    Value = "expressionless",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Confused,
                    Icon = "\uD83D\uDE15",
                    Value = "confused",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Shocked,
                    Icon = "\uD83D\uDE31",
                    Value = "shocked",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Very_sad,
                    Icon = "\uD83D\uDE2D",
                    Value = "so_sad",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Blessed,
                    Icon = "\uD83D\uDE07",
                    Value = "blessed",
                });

                FeelingListItems.Add(new Feelingstems()
                {
                    Label = AppResources.Label_Bored,
                    Icon = "\uD83D\uDE15",
                    Value = "bored",
                });

                FeelingsListView.ItemsSource = FeelingListItems;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void FeelingsListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as Feelingstems;
                if (Item != null)
                {
                    if (AddPost.ActivityListItems.Count > 0)
                    {
                        AddPost.ActivityListItems.Clear();
                    }
                    else
                    {
                        AddPost.ActivityListItems.Add(new AddPost.Activitytems()
                        {
                            Label = AppResources.Label_Feeling + " " + Item.Label,
                            Icon = Item.Icon,
                            Content = Item.Value,
                            TypePost = "Feelings",
                        });
                    }

                    if (AddPost_On_Communities.ActivityListItems.Count > 0)
                    {
                        AddPost_On_Communities.ActivityListItems.Clear();
                    }
                    else
                    {
                        AddPost_On_Communities.ActivityListItems.Add(new AddPost_On_Communities.Activitytems()
                        {
                            Label = AppResources.Label_Feeling + " " + Item.Label,
                            Icon = Item.Icon,
                            Content = Item.Value,
                            TypePost = "Feelings",
                        });
                    }

                    Navigation.PopAsync(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void FeelingsListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                FeelingsListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
