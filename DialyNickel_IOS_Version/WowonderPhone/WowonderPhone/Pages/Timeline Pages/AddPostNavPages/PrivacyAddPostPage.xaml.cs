﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using PropertyChanged;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.AddPostNavPages
{
    public partial class PrivacyAddPostPage : ContentPage
    {
        [ImplementPropertyChanged]
        public class PrivacyItems
        {
            public string PrivacyLabel { get; set; }
            public string value { get; set; }
            public string Checkicon { get; set; }
            public string CheckiconColor { get; set; }

        }

        public static ObservableCollection<PrivacyItems> PrivacyListItemsCollection = new ObservableCollection<PrivacyItems>();

        //public static string PostPrivacyValueNumber = "1";

        public PrivacyAddPostPage()
        {
            try
            {
                
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                PrivacyPostList.ItemsSource = PrivacyListItemsCollection;
                AddPrivacyToList();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddPrivacyToList()
        {
            try
            {
                if (PrivacyListItemsCollection.Count == 0)
                {
                    if (Settings.ConnectivitySystem == "1")
                    {
                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_Everyone,
                            value = "0",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_People_I_Follow,
                            value = "1",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_People_Follow_Me,
                            value = "2",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_Only_me,
                            value = "3",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                    }
                    else
                    {
                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_Everyone,
                            value = "0",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_My_Friends,
                            value = "1",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                        PrivacyListItemsCollection.Add(new PrivacyItems()
                        {
                            PrivacyLabel = AppResources.Label_Only_me,
                            value = "2",
                            Checkicon = "\uf1db",
                            CheckiconColor = Settings.MainColor
                        });

                        PrivacyPostList.ItemsSource = PrivacyListItemsCollection;
                    }
                }
                using (var Data = new LoginFunctions())
                {
                    var Database = Data.GetLoginCredentialsByUserID(Settings.User_id);
                    var SelectedValue =
                        PrivacyListItemsCollection.FirstOrDefault(a => a.value == Database.PrivacyPostValue);

                    if (SelectedValue != null)
                    {
                        SelectedValue.Checkicon = "\uf058";
                        //PostPrivacyValueNumber = SelectedValue.value;
                        Settings.PostPrivacyValue = SelectedValue.value;
                    }
                    else
                    {
                        var ss =
                            PrivacyListItemsCollection.FirstOrDefault(
                                a => a.PrivacyLabel == AppResources.Label_Everyone);
                        ss.Checkicon = "\uf058";
                        Settings.PostPrivacyValue = ss.value;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PrivacyPostList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as PrivacyItems;

                foreach (var PVitem in PrivacyListItemsCollection)
                {
                    PVitem.Checkicon = "\uf1db";
                }

                if (Item.Checkicon == "\uf1db")
                {
                    Item.Checkicon = "\uf058";
                }
                using (var Data = new LoginFunctions())
                {
                    var Database = Data.GetLoginCredentialsByUserID(Settings.User_id);
                    //var SelectedValue = PrivacyListItemsCollection.FirstOrDefault(a => a.Checkicon == "\uf058");
                    Database.PrivacyPostValue = Item.value;
                    Settings.PostPrivacyValue = Item.value;
                    Data.UpdateLoginCredentials(Database);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PrivacyPostList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                PrivacyPostList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PrivacyAddPostPage_OnDisappearing(object sender, EventArgs e)
        {
           
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void SaveButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
