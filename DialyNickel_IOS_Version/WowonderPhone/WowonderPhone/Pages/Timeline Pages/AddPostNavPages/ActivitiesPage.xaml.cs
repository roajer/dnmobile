﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PropertyChanged;
using WowonderPhone.Languish;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Timeline_Pages.AddPostNavPages
{
    public partial class ActivitiesPage : ContentPage
    {
        #region classes

        [ImplementPropertyChanged]
        public class ActivitiesItems
        {
            public string ActivityLabel { get; set; }
            public string value { get; set; }
            public string Checkicon { get; set; }
            public string CheckiconColor { get; set; }
        }

        #endregion

        #region Lists Items Declaration

        public static ObservableCollection<ActivitiesItems> ActivitiesListItemsCollection =
            new ObservableCollection<ActivitiesItems>();

        #endregion

        public ActivitiesPage()
        {
            try
            {
                InitializeComponent();

                if (ActivitiesListItemsCollection.Count == 0)
                {
                    ActivitiesListItemsCollection.Add(new ActivitiesItems()
                    {
                        ActivityLabel = AppResources.Label_Traveling_To,
                        value = "0",
                        Checkicon = "\uf072",
                        CheckiconColor = Settings.MainColor
                    });
                    ActivitiesListItemsCollection.Add(new ActivitiesItems()
                    {
                        ActivityLabel = AppResources.Label_Watching,
                        value = "0",
                        Checkicon = "\uf06e",
                        CheckiconColor = Settings.MainColor
                    });
                    ActivitiesListItemsCollection.Add(new ActivitiesItems()
                    {
                        ActivityLabel = AppResources.Label_Playing_Ac,
                        value = "0",
                        Checkicon = "\uf11b",
                        CheckiconColor = Settings.MainColor
                    });
                    ActivitiesListItemsCollection.Add(new ActivitiesItems()
                    {
                        ActivityLabel = AppResources.Label_Listening_To,
                        value = "0",
                        Checkicon = "\uf025",
                        CheckiconColor = Settings.MainColor
                    });
                }
                ActivityListView.ItemsSource = ActivitiesListItemsCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ActivityListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ActivityListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ActivityListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as ActivitiesItems;
                if (Item != null)
                {
                    ActivitySelectedPage ActivitySelected_Page = new ActivitySelectedPage(Item.ActivityLabel);
                    await Navigation.PushAsync(ActivitySelected_Page);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}