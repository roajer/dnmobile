﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.CustomCells;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.UsersPages;
using WowonderPhone.SQLite;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages
{
    public partial class UserProfilePage : ContentPage
    {

        #region classes

        public class Userprofiletems
        {
            public string Label { get; set; }
            public string Icon { get; set; }
        }


        #endregion

        #region Variables and Lists Items Declaration

        private string F_Still_NotFriend = "";
        private string F_Request = "";
        private string F_AlreadyFriend = "";

        public static string U_UserID = "";
        public static string S_About = "";
        public static string S_Website = "";
        public static string S_Name = "";
        public static string S_Username = "";
        public static string S_Gender = "";
        public static string S_Email = "";
        public static string S_Birthday = "";
        public static string S_Address = "";
        public static string S_URL = "";
        public static string S_School = "";
        public static string S_Working = "";
        public static string S_Facebook = "";
        public static string S_Google = "";
        public static string S_Twitter = "";
        public static string S_Linkedin = "";
        public static string S_Youtube = "";
        public static string S_VK = "";
        public static string S_Instagram = "";
        public static string S_phone_number = "";
        public static string S_Can_follow = "";
        public static string S_Following_number = "";
        public static string S_Followers_number = "";

        public static int itemGroup_Count = 0;
        public static int itemFollower_Count = 0;
        public static int itemPages_Count = 0;

        public static ObservableCollection<Userprofiletems> UserprofileListItems = new ObservableCollection<Userprofiletems>();
        public static ObservableCollection<Profile.Follower> FollowerUserListItems = new ObservableCollection<Profile.Follower>();
        public static ObservableCollection<Profile.Group>    GroupUserListItems = new ObservableCollection<Profile.Group>();
        public static ObservableCollection<Profile.Liked_Pages> Liked_PagesUserListItems = new ObservableCollection<Profile.Liked_Pages>();
        public static List<Albums> AlbumlistUsersItems = new List<Albums>();

        #endregion

        public UserProfilePage(string userid, string con)
        {
            try
            {                    
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
                    ToolbarItems.Clear();
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                U_UserID = userid;

                GetMyAlbums();
                if (Settings.Messenger_Integration == false)
                {
                    MessageImage.IsVisible = false;
                    MessageText.IsVisible = false;
                }

                if (Settings.Show_Block_On_User_Profiles == false)
                {
                    blockImage.IsVisible = false;
                    BlockLabel.IsVisible = false;
                }
                FirstGridOfprofile.BackgroundColor = Color.FromHex(Settings.MainColor);

                if (Settings.ConnectivitySystem == "1")
                {
                    F_Still_NotFriend = AppResources.Label_Follow;
                    F_Request = AppResources.Label_Requested;
                    F_AlreadyFriend = AppResources.Label_Following;
                }
                else if (Settings.ConnectivitySystem == "0")
                {
                    F_Still_NotFriend = AppResources.Label_AddFriend;
                    F_Request = AppResources.Label_Requested;
                    F_AlreadyFriend = AppResources.Label_Friends;
                }

                if (con == "FriendList")
                {
                    FriendStatusText.Text = F_AlreadyFriend;
                    FriendStatusImage.Source = Settings.Likecheked_Icon;
                }
                else
                {
                    FriendStatusText.Text = F_Still_NotFriend;
                }
                UserprofileListItems.Clear();

                GetUserProfileFromChach(userid);
               // AddClickActions();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                   // this.Title = AppResources.Label_Loading;
                   
                    PostWebLoader.Source = Settings.Website + "/get_news_feed?user_id=" + userid;
                    PostWebLoader.OnJavascriptResponse += OnJavascriptResponse;
                    PostWebLoader.RegisterCallback("type", (str) => { });
                }
                else
                {
                    //PostWebLoader.Source = Settings.HTML_LoadingPost_Page;
                }

                AboutIcon.Text = Ionic_Font.Compose;
                FeedIcon.Text = Ionic_Font.SocialRssOutline;
                MoreIcon.Text = Ionic_Font.AndroidMoreHorizontal;

                FeedStack.IsVisible = true;
                AboutStack.IsVisible = false;
                MoreStack.IsVisible = false;

                FeedLabel.FontAttributes = FontAttributes.Bold;
                AboutLabel.FontAttributes = FontAttributes.None;
                MoreLabel.FontAttributes = FontAttributes.None;

                Functions.Ad_Interstitial();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddClickActions()
        {
            //try
            //{
            //    if (FriendStatusText.GestureRecognizers.Count > 0)
            //    {
            //        FriendStatusText.GestureRecognizers.Clear();
            //    }
            //    if (MessageText.GestureRecognizers.Count > 0)
            //    {
            //        MessageText.GestureRecognizers.Clear();
            //    }
            //    if (MessageImage.GestureRecognizers.Count > 0)
            //    {
            //        MessageImage.GestureRecognizers.Clear();
            //    }
            //    if (BlockLabel.GestureRecognizers.Count > 0)
            //    {
            //        BlockLabel.GestureRecognizers.Clear();
            //    }
            //    if (blockImage.GestureRecognizers.Count > 0)
            //    {
            //        blockImage.GestureRecognizers.Clear();
            //    }
            //}
            //catch (Exception ex)
            //{
            //      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            //}
        }

        public void GetUserProfileFromChach(string userid)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                using (var ProfileFn = new ContactsFunctions())
                {
                    var Profile = ProfileFn.GetContactUser(userid);
                    if (Profile == null)
                    {
                        if (xx == true)
                        {
                            UserDialogs.Instance.Toast(AppResources.Label_CheckYourInternetConnection);
                        }
                        else
                        {
                            GetUserProfile(userid).ConfigureAwait(false);
                        }
                    }
                    else
                    {
                        this.Title = Profile.Name;
                        HeaderLabel.Text = Profile.Name;
                        Username.Text = Profile.Name;

                        S_About = Profile.About;
                        S_Website = Profile.Website;
                        S_Name = Profile.Website;
                        S_Username = Profile.Username;
                        S_Gender = Profile.Gender;
                        S_Email = Profile.Email;
                        S_Birthday = Profile.Birthday;
                        S_Address = Profile.Address;
                        S_URL = Profile.Url;
                        S_School = Profile.School;
                        S_Working = Profile.Working;
                        S_Facebook = Profile.Facebook;
                        S_Google = Profile.Google;
                        S_Twitter = Profile.Twitter;
                        S_Linkedin = Profile.Linkedin;
                        S_VK = Profile.vk;
                        S_Instagram = Profile.instagram;
                        S_Following_number = Profile.following_number;
                        S_Followers_number = Profile.followers_number;

                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.About,
                            Icon = "\uf040",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Gender,
                            Icon = "\uf228",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Address,
                            Icon = "\uf041",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Phone_number,
                            Icon = "\uf10b",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Website,
                            Icon = "\uf282",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.School,
                            Icon = "\uf19d",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Working,
                            Icon = "\uf0b1",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Facebook,
                            Icon = "\uf09a",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Google,
                            Icon = "\uf0d5",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Twitter,
                            Icon = "\uf099",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.vk,
                            Icon = "\uf189",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.instagram,
                            Icon = "\uf16d",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = Profile.Youtube,
                            Icon = "\uf167",
                        });

                        LastseenLabel.Text = Profile.lastseen;
                        //UserInfoList.ItemsSource = UserprofileListItems;
                        AvatarImage.Source = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Profile.Avatar, userid));
                        CoverImage.Source = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Profile.Cover, userid));
                        var tapGestureRecognizer = new TapGestureRecognizer();

                        tapGestureRecognizer.Tapped += (s, ee) =>
                        {
                            DependencyService.Get<IMethods>().OpenImage("Disk", Profile.Avatar, userid);
                        };

                        if (AvatarImage.GestureRecognizers.Count > 0)
                        {
                            AvatarImage.GestureRecognizers.Clear();
                        }

                        AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);

                        //Start updating the profile again
                        if (xx != true)
                        {
                            GetUserProfile(userid).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> GetUserProfile(string userid)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", userid),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_user_data",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        itemGroup_Count = 0;
                        itemFollower_Count = 0;
                        FollowerUserListItems.Clear();
                        GroupUserListItems.Clear();

                        JObject userdata = JObject.FromObject(data["user_data"]);
                        //Settings.UserFullName = userdata["name"].ToString();

                        var avatar = userdata["avatar"].ToString();
                        var cover = userdata["cover"].ToString();
                        var First_name = userdata["first_name"].ToString();
                        var Last_name = userdata["last_name"].ToString();
                        var Website = userdata["website"].ToString();
                        var user_id = userdata["user_id"].ToString();
                        var Lastseen = userdata["lastseen_time_text"].ToString();
                        var url = userdata["url"].ToString();
                        var is_following = userdata["is_following"].ToString();
                        var Pro_Type = userdata["pro_type"].ToString();
                        var Is_Pro = userdata["is_pro"].ToString();
                        var Is_blocked = userdata["is_blocked"].ToString();

                        S_About = Functions.StringNullRemover(Functions.DecodeString(userdata["about"].ToString()));
                        S_Website = Functions.StringNullRemover(userdata["website"].ToString());
                        S_Name = Functions.StringNullRemover(userdata["name"].ToString());
                        S_Username = Functions.StringNullRemover(userdata["username"].ToString());
                        S_Gender = Functions.StringNullRemover(userdata["gender"].ToString());
                        S_Email = Functions.StringNullRemover(userdata["email"].ToString());
                        S_Birthday = Functions.StringNullRemover(userdata["birthday"].ToString());
                        S_Address = Functions.StringNullRemover(userdata["address"].ToString());
                        S_URL = Functions.StringNullRemover(userdata["url"].ToString());
                        S_School = Functions.StringNullRemover(userdata["school"].ToString());
                        S_Working = Functions.StringNullRemover(userdata["working"].ToString());
                        S_Facebook = Functions.StringNullRemover(userdata["facebook"].ToString());
                        S_Google = Functions.StringNullRemover(userdata["google"].ToString());
                        S_Twitter = Functions.StringNullRemover(userdata["twitter"].ToString());
                        S_Linkedin = Functions.StringNullRemover(userdata["linkedin"].ToString());
                        S_Youtube = Functions.StringNullRemover(userdata["youtube"].ToString());
                        S_VK = Functions.StringNullRemover(userdata["vk"].ToString());
                        S_Instagram = Functions.StringNullRemover(userdata["instagram"].ToString());
                        S_phone_number = Functions.StringNullRemover(userdata["phone_number"].ToString());
                        S_Can_follow = Functions.StringNullRemover(userdata["can_follow"].ToString());
                        S_Following_number = Functions.StringNullRemover(userdata["following_number"].ToString());
                        S_Followers_number = Functions.StringNullRemover(userdata["followers_number"].ToString());

                        #region User Data

                        if (Is_Pro == "1")
                        {
                            if (Pro_Type == "1")
                            {
                                StarIcon.IsVisible = true;
                            }
                            else if (Pro_Type == "2")
                            {
                                HotIcon.IsVisible = true;
                            }
                            else if (Pro_Type == "3")
                            {
                                UltimaIcon.IsVisible = true;
                            }
                            else if (Pro_Type == "4")
                            {
                                VIPIcon.IsVisible = true;
                            }
                        }
                        if (Is_blocked == "true")
                        {
                            BlockLabel.Text = AppResources.Label_Blocked;
                            blockImage.Source = Settings.BlockedUser_Icon;
                        }

                        var Post_Count = userdata["post_count"].ToString();
                        if (Post_Count == "0")
                        {
                            ButtonStacklayot.IsVisible = false;
                            PostWebLoader.HeightRequest = 230;
                        }
                        else if (Post_Count == "1")
                        {
                            PostWebLoader.HeightRequest = 560;
                        }
                        else if (Post_Count == "2")
                        {
                            PostWebLoader.HeightRequest = 770;
                        }

                        else if (Post_Count == "3")
                        {
                            PostWebLoader.HeightRequest = 940;
                        }
                        else if (Post_Count == "4")
                        {
                            PostWebLoader.HeightRequest = 1050;
                        }
                        else if (Post_Count == "5")
                        {
                            PostWebLoader.HeightRequest = 1350;
                        }
                        else if (Post_Count == "6")
                        {
                            PostWebLoader.HeightRequest = 1550;
                        }
                        else if (Post_Count == "7")
                        {
                            PostWebLoader.HeightRequest = 1750;
                        }
                        else if (Post_Count == "8")
                        {
                            PostWebLoader.HeightRequest = 1950;
                        }
                        CoverImage.Source = ImageSource.FromFile(DependencyService.Get<IPicture>()
                            .GetPictureFromDisk(cover, user_id));
                        if (DependencyService.Get<IPicture>().GetPictureFromDisk(cover, user_id) == "File Dont Exists")
                        {
                            CoverImage.Source = new UriImageSource {Uri = new Uri(cover)};
                            DependencyService.Get<IPicture>().SavePictureToDisk(cover, user_id);
                        }

                        AvatarImage.Source = ImageSource.FromFile(DependencyService.Get<IPicture>()
                            .GetPictureFromDisk(avatar, user_id));
                        if (DependencyService.Get<IPicture>().GetPictureFromDisk(avatar, user_id) == "File Dont Exists")
                        {
                            AvatarImage.Source = new UriImageSource {Uri = new Uri(avatar)};
                            DependencyService.Get<IPicture>().SavePictureToDisk(avatar, user_id);
                        }

                        if (Website == "")
                        {
                            Website = AppResources.Label_Unavailable;
                        }
                        if (S_School == "")
                        {
                            S_School = AppResources.Label_Askme;
                        }
                        if (S_Birthday == "" || S_Birthday.Contains("00"))
                        {
                            S_Birthday = AppResources.Label_Askme;
                        }
                        if (S_About == "" || S_About == " ")
                        {
                            S_About = Settings.PR_AboutDefault;
                        }
                        if (S_Address == "")
                        {
                            S_Address = AppResources.Label_Unavailable;
                        }
                        if (S_Gender == "")
                        {
                            S_Gender = AppResources.Label_Unavailable;
                        }
                        if (S_Working == "")
                        {
                            S_Working = AppResources.Label_Unavailable;
                        }

                        LastseenLabel.Text = Lastseen;

                        if (Lastseen == "online" || Lastseen == "Online" || Lastseen.Contains("sec") ||
                            Lastseen.Contains("Sec"))
                        {
                            LastseenLabel.Text = AppResources.Label_Online;
                        }

                        if (S_Can_follow == "0" && is_following == "0")
                        {
                            if (FriendStatusText.IsVisible)
                            {
                                FriendStatusText.IsVisible = false;
                                FriendStatusImage.IsVisible = false;
                            }
                        }

                        if (is_following == "1")
                        {
                            if (FriendStatusText.Text != F_AlreadyFriend)
                            {
                                FriendStatusText.Text = F_AlreadyFriend;
                                FriendStatusImage.Source = Settings.Likecheked_Icon;
                            }
                        }
                        else if (is_following == "2")
                        {
                            if (FriendStatusText.Text != F_Request)
                            {
                                FriendStatusText.Text = F_Request;
                                FriendStatusImage.Source = Settings.Requested_Icon;
                            }

                        }
                        else if (is_following == "0")
                        {
                            if (FriendStatusText.Text != F_Still_NotFriend)
                            {
                                FriendStatusText.Text = F_Still_NotFriend;
                                FriendStatusImage.Source = Settings.AddUser_Icon;
                            }
                        }

                        Username.Text = S_Name;

                        if (UserprofileListItems.Count > 0)
                        {
                            UserprofileListItems.Clear();
                        }

                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_About,
                            Icon = "\uf040",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Gender,
                            Icon = "\uf228",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Address,
                            Icon = "\uf041",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_phone_number,
                            Icon = "\uf10b",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Website,
                            Icon = "\uf282",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_School,
                            Icon = "\uf19d",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Working,
                            Icon = "\uf0b1",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Facebook,
                            Icon = "\uf09a",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Google,
                            Icon = "\uf0d5",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Twitter,
                            Icon = "\uf099",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_VK,
                            Icon = "\uf189",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Instagram,
                            Icon = "\uf16d",
                        });
                        UserprofileListItems.Add(new Userprofiletems()
                        {
                            Label = S_Youtube,
                            Icon = "\uf167",
                        });

                        UserInfoList.ItemsSource = UserprofileListItems;
                        this.Title = S_Name;
                        HeaderLabel.Text = S_Name;

                        //UserInfoList.HeightRequest = Functions.ListInfoResizer(S_About);

                        using (var Datas = new ContactsFunctions())
                        {
                            var contact = Datas.GetContactUser(user_id);
                            if (contact != null)
                            {
                                if (contact.UserID == user_id &&
                                    (contact.Cover != cover || contact.Avatar != avatar ||
                                     contact.Birthday != S_Birthday || contact.Working != S_Working ||
                                     contact.Name != S_Name || contact.Username != S_Username ||
                                     contact.First_name != First_name || contact.Last_name != Last_name ||
                                     contact.About != S_About || contact.Website != Website ||
                                     contact.School != S_School || contact.Facebook != S_Facebook ||
                                     contact.Google != S_Google || contact.Phone_number != S_phone_number ||
                                     contact.Youtube != S_Youtube || contact.vk != S_VK ||
                                     contact.instagram != S_Instagram ||
                                     contact.Twitter != S_Twitter || contact.Linkedin != S_Linkedin))
                                {

                                    if (contact.Avatar != avatar)
                                    {
                                        DependencyService.Get<IPicture>()
                                            .DeletePictureFromDisk(contact.Avatar, user_id);
                                    }
                                    if (contact.Cover != cover)
                                    {
                                        DependencyService.Get<IPicture>().DeletePictureFromDisk(contact.Cover, user_id);
                                    }

                                    contact.UserID = user_id;
                                    contact.Name = S_Name;
                                    contact.Avatar = avatar;
                                    contact.Cover = cover;
                                    contact.Birthday = S_Birthday;
                                    contact.Address = Functions.StringNullRemover(
                                        Functions.DecodeString(System.Net.WebUtility.HtmlDecode(S_Address)));
                                    contact.Gender = S_Gender;
                                    contact.Email = S_Email;
                                    contact.Username = S_Username;
                                    contact.First_name = Functions.StringNullRemover(First_name);
                                    contact.Last_name = Functions.StringNullRemover(Last_name);
                                    contact.About =
                                        Functions.StringNullRemover(
                                            Functions.DecodeString(System.Net.WebUtility.HtmlDecode(S_About)));
                                    contact.Website = Functions.StringNullRemover(Website);
                                    contact.School = Functions.StringNullRemover(S_School);
                                    contact.lastseen = Lastseen;
                                    contact.Phone_number = Functions.StringNullRemover(S_phone_number);
                                    contact.Working = Functions.StringNullRemover(S_Working);
                                    contact.Facebook = Functions.StringNullRemover(S_Facebook);
                                    contact.Google = Functions.StringNullRemover(S_Google);
                                    contact.Youtube = Functions.StringNullRemover(S_Youtube);
                                    contact.vk = Functions.StringNullRemover(S_VK);
                                    contact.instagram = Functions.StringNullRemover(S_Instagram);
                                    contact.Twitter = Functions.StringNullRemover(S_Twitter);
                                    contact.Linkedin = Functions.StringNullRemover(S_Linkedin);
                                    contact.following_number = S_Following_number;
                                    contact.followers_number = S_Following_number;
                                    contact.Groups_Total = Profile.GroupList.Count.ToString();

                                    Datas.UpdateContactUsers(contact);
                                }
                            }
                        }

                        #endregion

                        #region Followers

                        var followers = userdata["followers"].ToString();
                        JArray datafollowers = JArray.Parse(followers);
                        if (datafollowers != null)
                        {
                            try
                            {
                                Profile.FollowerList.Clear();
                                FollowerUserListItems.Clear();
                                ContactsStack.Children.Clear();

                                foreach (var All in datafollowers)
                                {

                                    Profile.Follower f = new Profile.Follower();

                                    var f_user_id = All["user_id"].ToString();
                                    var f_username = All["username"].ToString();
                                    var f_email = All["email"].ToString();
                                    var f_first_name = All["first_name"].ToString();
                                    var f_last_name = All["last_name"].ToString();
                                    var f_avatar = All["avatar"].ToString();
                                    var f_cover = All["cover"].ToString();
                                    var f_background_image = All["background_image"].ToString();
                                    var f_background_image_status = All["background_image_status"].ToString();
                                    var f_relationship_id = All["relationship_id"].ToString();
                                    var f_address = All["address"].ToString();
                                    var f_working = All["working"].ToString();
                                    var f_working_link = All["working_link"].ToString();
                                    var f_about = All["about"].ToString();
                                    var f_school = All["school"].ToString();
                                    var f_gender = All["gender"].ToString();
                                    var f_birthday = All["birthday"].ToString();
                                    var f_country_id = All["country_id"].ToString();
                                    var f_website = All["website"].ToString();
                                    var f_facebook = All["facebook"].ToString();
                                    var f_google = All["google"].ToString();
                                    var f_twitter = All["twitter"].ToString();
                                    var f_linkedin = All["linkedin"].ToString();
                                    var f_youtube = All["youtube"].ToString();
                                    var f_vk = All["vk"].ToString();
                                    var f_instagram = All["instagram"].ToString();
                                    var f_language = All["language"].ToString();
                                    var f_email_code = All["email_code"].ToString();
                                    var f_src = All["src"].ToString();
                                    var f_ip_address = All["ip_address"].ToString();
                                    var f_follow_privacy = All["follow_privacy"].ToString();
                                    var f_friend_privacy = All["friend_privacy"].ToString();
                                    var f_post_privacy = All["post_privacy"].ToString();
                                    var f_message_privacy = All["message_privacy"].ToString();
                                    var f_confirm_followers = All["confirm_followers"].ToString();
                                    var f_show_activities_privacy = All["show_activities_privacy"].ToString();
                                    var f_birth_privacy = All["birth_privacy"].ToString();
                                    var f_visit_privacy = All["visit_privacy"].ToString();
                                    var f_verified = All["verified"].ToString();
                                    var f_lastseen = All["lastseen"].ToString();
                                    var f_showlastseen = All["showlastseen"].ToString();
                                    var f_emailNotification = All["emailNotification"].ToString();
                                    var f_e_liked = All["e_liked"].ToString();
                                    var f_e_wondered = All["e_wondered"].ToString();
                                    var f_e_shared = All["e_shared"].ToString();
                                    var f_e_followed = All["e_followed"].ToString();
                                    var f_e_commented = All["e_commented"].ToString();
                                    var f_e_visited = All["e_visited"].ToString();
                                    var f_e_liked_page = All["e_liked_page"].ToString();
                                    var f_e_mentioned = All["e_mentioned"].ToString();
                                    var f_e_joined_group = All["e_joined_group"].ToString();
                                    var f_e_accepted = All["e_accepted"].ToString();
                                    var f_e_profile_wall_post = All["e_profile_wall_post"].ToString();
                                    var f_e_sentme_msg = All["e_sentme_msg"].ToString();
                                    var f_e_last_notif = All["e_last_notif"].ToString();
                                    var f_status = All["status"].ToString();
                                    var f_active = All["active"].ToString();
                                    var f_admin = All["admin"].ToString();
                                    var f_type = All["type"].ToString();
                                    var f_registered = All["registered"].ToString();
                                    var f_start_up = All["start_up"].ToString();
                                    var f_start_up_info = All["start_up_info"].ToString();
                                    var f_startup_follow = All["startup_follow"].ToString();
                                    var f_startup_image = All["startup_image"].ToString();
                                    var f_last_email_sent = All["last_email_sent"].ToString();
                                    var f_phone_number = All["phone_number"].ToString();
                                    var f_sms_code = All["sms_code"].ToString();
                                    var f_is_pro = All["is_pro"].ToString();
                                    var f_pro_time = All["pro_time"].ToString();
                                    var f_pro_type = All["pro_type"].ToString();
                                    var f_joined = All["joined"].ToString();
                                    var f_css_file = All["css_file"].ToString();
                                    var f_timezone = All["timezone"].ToString();
                                    var f_referrer = All["referrer"].ToString();
                                    var f_balance = All["balance"].ToString();
                                    var f_paypal_email = All["paypal_email"].ToString();
                                    var f_notifications_sound = All["notifications_sound"].ToString();
                                    var f_order_posts_by = All["order_posts_by"].ToString();
                                    var f_social_login = All["social_login"].ToString();
                                    var f_device_id = All["device_id"].ToString();
                                    var f_web_device_id = All["web_device_id"].ToString();
                                    var f_wallet = All["wallet"].ToString();
                                    var f_lat = All["lat"].ToString();
                                    var f_lng = All["lng"].ToString();
                                    var f_last_location_update = All["last_location_update"].ToString();
                                    var f_share_my_location = All["share_my_location"].ToString();
                                    var f_avatar_org = All["avatar_org"].ToString();
                                    var f_cover_org = All["cover_org"].ToString();
                                    var f_cover_full = All["cover_full"].ToString();
                                    var f_id = All["id"].ToString();
                                    var f_url = All["url"].ToString();
                                    var f_name = All["name"].ToString();
                                    var f_family_member = All["family_member"].ToString();

                                    //style
                                    if (String.IsNullOrEmpty(f_first_name) && String.IsNullOrEmpty(f_first_name))
                                        f.F_full_name = f_username;
                                    else
                                        f.F_full_name = f_first_name + " " + f_last_name;

                                    f.F_user_id = f_user_id;
                                    f.F_username = f_username;
                                    f.F_email = f_email;
                                    f.F_first_name = Functions.StringNullRemover(f_first_name);
                                    f.F_last_name = Functions.StringNullRemover(f_last_name);
                                    f.F_avatar = f_avatar;
                                    f.F_cover = f_cover;
                                    f.F_background_image = f_background_image;
                                    f.F_background_image_status = f_background_image_status;
                                    f.F_relationship_id = f_relationship_id;
                                    f.F_address = f_address;
                                    f.F_working = f_working;
                                    f.F_working_link = f_working_link;
                                    f.F_about = Functions.StringNullRemover(Functions.DecodeString(f_about));
                                    f.F_school = Functions.StringNullRemover(f_school);
                                    f.F_gender = f_gender;
                                    f.F_birthday = f_birthday;
                                    f.F_country_id = f_country_id;
                                    f.F_website = Functions.StringNullRemover(f_website);
                                    f.F_facebook = Functions.StringNullRemover(f_facebook);
                                    f.F_google = Functions.StringNullRemover(f_google);
                                    f.F_twitter = Functions.StringNullRemover(f_twitter);
                                    f.F_linkedin = Functions.StringNullRemover(f_linkedin);
                                    f.F_youtube = Functions.StringNullRemover(f_youtube);
                                    f.F_vk = Functions.StringNullRemover(f_vk);
                                    f.F_instagram = Functions.StringNullRemover(f_instagram);
                                    f.F_language = f_language;
                                    f.F_email_code = f_email_code;
                                    f.F_src = f_src;
                                    f.F_ip_address = f_ip_address;
                                    f.F_follow_privacy = f_follow_privacy;
                                    f.F_friend_privacy = f_friend_privacy;
                                    f.F_post_privacy = f_post_privacy;
                                    f.F_message_privacy = f_message_privacy;
                                    f.F_confirm_followers = f_confirm_followers;
                                    f.F_show_activities_privacy = f_show_activities_privacy;
                                    f.F_birth_privacy = f_birth_privacy;
                                    f.F_visit_privacy = f_visit_privacy;
                                    f.F_verified = f_verified;
                                    f.F_lastseen = f_lastseen;
                                    f.F_showlastseen = f_showlastseen;
                                    f.F_emailNotification = f_emailNotification;
                                    f.F_e_liked = f_e_liked;
                                    f.F_e_wondered = f_e_wondered;
                                    f.F_e_shared = f_e_shared;
                                    f.F_e_followed = f_e_followed;
                                    f.F_e_commented = f_e_commented;
                                    f.F_e_visited = f_e_visited;
                                    f.F_e_liked_page = f_e_liked_page;
                                    f.F_e_mentioned = f_e_mentioned;
                                    f.F_e_joined_group = f_e_joined_group;
                                    f.F_e_accepted = f_e_accepted;
                                    f.F_e_profile_wall_post = f_e_profile_wall_post;
                                    f.F_e_sentme_msg = f_e_sentme_msg;
                                    f.F_e_last_notif = f_e_last_notif;
                                    f.F_status = f_status;
                                    f.F_active = f_active;
                                    f.F_admin = f_admin;
                                    f.F_type = f_type;
                                    f.F_registered = f_registered;
                                    f.F_start_up = f_start_up;
                                    f.F_start_up_info = f_start_up_info;
                                    f.F_startup_follow = f_startup_follow;
                                    f.F_startup_image = f_startup_image;
                                    f.F_last_email_sent = f_last_email_sent;
                                    f.F_phone_number = Functions.StringNullRemover(f_phone_number);
                                    f.F_sms_code = f_sms_code;
                                    f.F_is_pro = f_is_pro;
                                    f.F_pro_time = f_pro_time;
                                    f.F_pro_type = f_pro_type;
                                    f.F_joined = f_joined;
                                    f.F_css_file = f_css_file;
                                    f.F_timezone = f_timezone;
                                    f.F_referrer = f_referrer;
                                    f.F_balance = f_balance;
                                    f.F_paypal_email = f_paypal_email;
                                    f.F_notifications_sound = f_notifications_sound;
                                    f.F_order_posts_by = f_order_posts_by;
                                    f.F_social_login = f_social_login;
                                    f.F_device_id = f_device_id;
                                    f.F_web_device_id = f_web_device_id;
                                    f.F_wallet = f_wallet;
                                    f.F_lat = f_lat;
                                    f.F_lng = f_lng;
                                    f.F_last_location_update = f_last_location_update;
                                    f.F_share_my_location = f_share_my_location;
                                    f.F_avatar_org = f_avatar_org;
                                    f.F_cover_org = f_cover_org;
                                    f.F_cover_full = f_cover_full;
                                    f.F_id = f_id;
                                    f.F_url = f_url;
                                    f.F_name = f_name;
                                    f.F_family_member = f_family_member;

                                    Profile.FollowerUserList.Add(f);

                                    if (itemFollower_Count < 11)
                                    {
                                        FollowerUserListItems.Add(f);
                                        itemFollower_Count++;
                                    }
                                }
                                //ContactListview.ItemsSource = FollowerUserListItems;
                                itemFollower_Count = 0;
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        #endregion

                        #region Groups

                        var groups = userdata["groups"].ToString();
                        JArray datagroups = JArray.Parse(groups);
                        if (datagroups != null)
                        {
                            try
                            {
                                Profile.GroupUserList.Clear();
                                GroupUserListItems.Clear();
                                foreach (var All in datagroups)
                                {

                                    Profile.Group g = new Profile.Group();

                                    var g_id = All["id"].ToString();
                                    var g_user_id = All["user_id"].ToString();
                                    var g_group_name = All["group_name"].ToString();
                                    var g_group_title = All["group_title"].ToString();
                                    var g_avatar = All["avatar"].ToString();
                                    var g_cover = All["cover"].ToString();
                                    var g_about = All["about"].ToString();
                                    var g_category = All["category"].ToString();
                                    var g_privacy = All["privacy"].ToString();
                                    var g_join_privacy = All["join_privacy"].ToString();
                                    var g_active = All["active"].ToString();
                                    var g_registered = All["registered"].ToString();
                                    var g_group_id = All["group_id"].ToString();
                                    var g_url = All["url"].ToString();
                                    var g_name = All["name"].ToString();
                                    var g_category_id = All["category_id"].ToString();
                                    var g_type = All["type"].ToString();
                                    var g_username = All["username"].ToString();

                                    g.G_id = g_id;
                                    g.G_user_id = g_user_id;
                                    g.G_group_name = g_group_name;
                                    g.G_group_title = g_group_title;
                                    g.G_avatar = g_avatar;
                                    g.G_cover = g_cover;
                                    g.G_about = Functions.StringNullRemover(Functions.DecodeString(g_about));
                                    g.G_category = g_category;
                                    g.G_privacy = g_privacy;
                                    g.G_join_privacy = g_join_privacy;
                                    g.G_active = g_active;
                                    g.G_registered = g_registered;
                                    g.G_group_id = g_group_id;
                                    g.G_url = g_url;
                                    g.G_name = g_name;
                                    g.G_category_id = g_category_id;
                                    g.G_type = g_type;
                                    g.G_username = g_username;

                                    Profile.GroupUserList.Add(g);
                                    if (itemGroup_Count < 6)
                                    {
                                        GroupUserListItems.Add(g);
                                        itemGroup_Count++;
                                    }
                                }
                               // GroupListview.ItemsSource = GroupUserListItems;
                                itemGroup_Count = 0;
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        #endregion

                        #region Pages

                        var Pages = userdata["liked_pages"].ToString();
                        JArray dataPages = JArray.Parse(Pages);
                        if (dataPages != null)
                        {
                            try
                            {
                                Profile.Liked_PagesUserList.Clear();
                                Liked_PagesUserListItems.Clear();

                                foreach (var All in dataPages)
                                {

                                    Profile.Liked_Pages p = new Profile.Liked_Pages();

                                    var P_page_id = All["page_id"].ToString();
                                    var P_user_id = All["user_id"].ToString();
                                    var P_page_name = All["page_name"].ToString();
                                    var P_page_title = All["page_title"].ToString();
                                    var P_page_description = All["page_description"].ToString();
                                    var P_avatar = All["avatar"].ToString();
                                    var P_cover = All["cover"].ToString();
                                    var P_page_category = All["page_category"].ToString();
                                    var P_website = All["website"].ToString();
                                    var P_facebook = All["facebook"].ToString();
                                    var P_google = All["google"].ToString();
                                    var P_vk = All["vk"].ToString();
                                    var P_twitter = All["twitter"].ToString();
                                    var P_linkedin = All["linkedin"].ToString();
                                    var P_company = All["company"].ToString();
                                    var P_phone = All["phone"].ToString();
                                    var P_address = All["address"].ToString();
                                    var P_call_action_type = All["call_action_type"].ToString();
                                    var P_call_action_type_url = All["call_action_type_url"].ToString();
                                    var P_background_image = All["background_image"].ToString();
                                    var P_background_image_status = All["background_image_status"].ToString();
                                    var P_instgram = All["instgram"].ToString();
                                    var P_youtube = All["youtube"].ToString();
                                    var P_verified = All["verified"].ToString();
                                    var P_active = All["active"].ToString();
                                    var P_registered = All["registered"].ToString();
                                    var P_boosted = All["boosted"].ToString();
                                    var P_about = All["about"].ToString();
                                    var P_id = All["id"].ToString();
                                    var P_type = All["type"].ToString();
                                    var P_url = All["url"].ToString();
                                    var P_name = All["name"].ToString();
                                    var P_rating = All["rating"].ToString();
                                    var P_category = All["category"].ToString();
                                    var P_is_page_onwer = All["is_page_onwer"].ToString();
                                    var P_username = All["username"].ToString();

                                    p.P_page_id = P_page_id;
                                    p.P_user_id = P_user_id;
                                    p.P_page_name = P_page_name;
                                    p.P_page_title = P_page_title;
                                    p.P_page_description =
                                        Functions.StringNullRemover(Functions.DecodeString(P_page_description));
                                    p.P_avatar = P_avatar;
                                    p.P_cover = P_cover;
                                    p.P_page_category = P_page_category;
                                    p.P_website = P_website;
                                    p.P_facebook = P_facebook;
                                    p.P_google = P_google;
                                    p.P_vk = P_vk;
                                    p.P_twitter = P_twitter;
                                    p.P_linkedin = P_linkedin;
                                    p.P_company = P_company;
                                    p.P_phone = P_phone;
                                    p.P_address = P_address;
                                    p.P_call_action_type = P_call_action_type;
                                    p.P_call_action_type_url = P_call_action_type_url;
                                    p.P_background_image = P_background_image;
                                    p.P_background_image_status = P_background_image_status;
                                    p.P_instgram = P_instgram;
                                    p.P_youtube = P_youtube;
                                    p.P_verified = P_verified;
                                    p.P_active = P_active;
                                    p.P_registered = P_registered;
                                    p.P_boosted = P_boosted;
                                    p.P_about = Functions.StringNullRemover(Functions.DecodeString(P_about));
                                    p.P_id = P_id;
                                    p.P_type = P_type;
                                    p.P_url = P_url;
                                    p.P_name = P_name;
                                    p.P_rating = P_rating;
                                    p.P_category = P_category;
                                    p.P_is_page_onwer = P_is_page_onwer;
                                    p.P_username = P_username;

                                    Profile.Liked_PagesUserList.Add(p);
                                    if (itemPages_Count < 6)
                                    {
                                        Liked_PagesUserListItems.Add(p);
                                        itemPages_Count++;
                                    }
                                }
                                //PagesListview.ItemsSource = Liked_PagesUserListItems;
                                itemPages_Count = 0;
                            }
                            catch (Exception ex)
                            {
                                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            }
                        }

                        #endregion

                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }

        public static async Task<string> SendFriendRequest(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=follow_user",formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
            return null;
        }

        public static async Task<string> SendBlockRequest(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("block_type", "block"),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=block_user",formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
            return null;
        }

        public static async Task<string> SendUnBlockRequest(string recipient_id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("block_type", "un-block"),
                        new KeyValuePair<string, string>("recipient_id", recipient_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=block_user",formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        return "Succes";
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
            return null;
        }

        private void UserProfilePage_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                //throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void UserInfoList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                UserInfoList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void CopyUrlButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Button1.Text = "\uf00c";
                DependencyService.Get<IClipboardService>().CopyToClipboard(S_URL);
                DependencyService.Get<IMessage>().LongAlert(AppResources.Label3_Copyed_URL);

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void OnScroll(object sender, ScrolledEventArgs e)
        {
            try
            {
                var Header = CoverImage.Height * 2;
                //
                var scrollRegion = layeringGrid.Height - outerScrollView.Height;
                var parallexRegion = Header - FirstGridOfprofile.Height;
                var factor = outerScrollView.ScrollY - parallexRegion * (outerScrollView.ScrollY / scrollRegion);

                CoverImage.TranslationY = factor;
                CoverImage.Opacity = 1 - (factor / (FirstGridOfprofile.Height - 20) * 2);
                CoverImage.BackgroundColor = Color.FromHex(Settings.MainColor);

                AvatarImage.TranslationY = factor;
                AvatarImage.Opacity = 1 - (factor / (FirstGridOfprofile.Height - 20) * 2);
                //AvatarImage.BackgroundColor = Color.FromHex(Settings.MainColor);

                headers.Scale = 1 - ((factor) / (Header * 2));

                if (CoverImage.Opacity == 0)
                {
                    HeaderOfpage.BackgroundColor = Color.FromHex(Settings.MainColor);
                    HeaderLabel.IsVisible = true;
                }
                else
                {
                    if (HeaderLabel.IsVisible)
                    {
                        HeaderLabel.IsVisible = false;
                        HeaderOfpage.BackgroundColor = Color.Transparent;
                    }
                }
                //PostWebLoader.Scale = 1 - ((factor) / (Header * 2));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            outerScrollView.Scrolled += OnScroll;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            outerScrollView.Scrolled -= OnScroll;
        }

        private void ShowmoreButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new HyberdPostViewer("User", ""));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnJavascriptResponse(JavascriptResponseDelegate EventObj)
        {
            try
            {
                if (EventObj.Data.Contains("type"))
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(EventObj.Data);
                    string type = data["type"].ToString();
                    if (type == "user")
                    {
                        string Userid = data["profile_id"].ToString();
                        if (Settings.User_id == Userid)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new MyProfilePage());
                            });
                        }
                        else if (Userid == U_UserID)
                        {
                            return;
                        }
                        else
                        {
                            InjectedJavaOpen_UserProfile(Userid);
                        }

                    }
                    else if (type == "lightbox")
                    {
                        string ImageSource = data["image_url"].ToString();
                        if (Settings.ShowAndroidDefaultImageViewer)
                        {
                            UserDialogs.Instance.ShowLoading(AppResources.Label_Please_Wait);
                            var dataViewer = DependencyService.Get<IMethods>();
                            dataViewer.showPhoto(ImageSource);
                        }
                        else
                        {
                            var Image = new UriImageSource
                            {
                                Uri = new Uri(ImageSource),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };
                            InjectedJavaOpen_OpenImage(Image);
                        }
                    }
                    else if (type == "mention")
                    {
                        string user_id = data["user_id"].ToString();
                        InjectedJavaOpen_UserProfile(user_id);
                    }
                    else if (type == "hashtag")
                    {
                        string hashtag = data["tag"].ToString();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushModalAsync(new HyberdPostViewer("Hashtag", hashtag));
                        });
                    }
                    else if (type == "url")
                    {
                        string link = data["link"].ToString();

                        InjectedJavaOpen_PostLinks(link);
                    }
                    else if (type == "page")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_LikePage(Id);
                    }
                    else if (type == "group")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_Group(Id);
                    }
                    else if (type == "post_wonders" || type == "post_likes")
                    {
                        string Id = data["post_id"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Like_Wonder_Viewer_Page(Id, type));
                        });
                    }

                    else if (type == "delete_post")
                    {
                        string Id = data["post_id"].ToString();

                        var Qussion = await DisplayAlert(AppResources.Label_Question,
                            AppResources.Label_Would_You_like_to_delete_this_post, AppResources.Label_Yes,
                            AppResources.Label_NO);
                        if (Qussion)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                PostWebLoader.InjectJavascript(
                                    "$('#post-' + " + Id + ").slideUp(200, function () { $(this).remove();}); ");
                            });

                            Post_Manager("delete_post", Id).ConfigureAwait(false);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_UserProfile(string Userid)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserProfilePage(Userid, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_OpenImage(ImageSource Image)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushModalAsync(new ImageFullScreenPage(Image));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_PostLinks(string link)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Device.OpenUri(new Uri(link));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Hashtag(string word)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", word));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_LikePage(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialPageViewer(id, "", ""));
                });

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Group(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialGroup("id", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Post_Manager(string Type, string postid)
        {
            try
            {
                var Action = " ";
                if (Type == "edit_post")
                {
                    Action = "edit";
                }
                else
                {
                    Action = "delete";
                }
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("action", Action),

                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=post_manager",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (Type == "edit_post")
                        {
                            Action = "edit";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnContentLoaded(ContentLoadedDelegate eventobj)
        {
            try
            {
                //OfflinePost.IsVisible = false;
                //OfflinePost.Source = null;
                //PostWebLoader.IsVisible = true;
                PostWebLoader.RegisterCallback("type", (str) => { });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.PopModalAsync(true);
            }
        }

        private void FeedButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                FeedStack.IsVisible = true;
                AboutStack.IsVisible = false;
                MoreStack.IsVisible = false;

                FeedLabel.FontAttributes = FontAttributes.Bold;
                AboutLabel.FontAttributes = FontAttributes.None;
                MoreLabel.FontAttributes = FontAttributes.None;

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AboutButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                FeedStack.IsVisible = false;
                AboutStack.IsVisible = true;
                MoreStack.IsVisible = false;

                FeedLabel.FontAttributes = FontAttributes.None; 
                AboutLabel.FontAttributes = FontAttributes.Bold;
                MoreLabel.FontAttributes = FontAttributes.None;

                if (UserprofileListItems.Count > 0 )
                {
                    UserInfoList.ItemsSource = UserprofileListItems;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void MoreButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                FeedStack.IsVisible = false;
                AboutStack.IsVisible = false;
                MoreStack.IsVisible = true;

                FeedLabel.FontAttributes = FontAttributes.None;
                AboutLabel.FontAttributes = FontAttributes.None;
                MoreLabel.FontAttributes = FontAttributes.Bold;

                if (FollowerUserListItems.Count > 0)
                {
                    ContactsStack.Children.Clear();
                    ContactsStackLayout.IsVisible = true;
                    ContactsHeader.Text =AppResources.Label3_Contacts + " (" + Profile.FollowerUserList.Count + ")";
                    Add_Contacts_Users(FollowerUserListItems);
                }
                else
                {
                    ContactsStackLayout.IsVisible = false;
                }
                if (Liked_PagesUserListItems.Count > 0)
                {
                    PagesListview.IsVisible = true;
                    PagesHeader.Text = AppResources.Label_Liked_Pages + " (" + Profile.Liked_PagesUserList.Count + ")";
                    PagesListview.ItemsSource = Liked_PagesUserListItems;
                   
                    var Count = (Liked_PagesUserListItems.Count * 60) + 50;
                   
                    if (Liked_PagesUserListItems.Count > 6)
                    {
                        PagesListview.HeightRequest = (6 * 60) + 50;
                    }
                    else
                    {
                        PagesListview.HeightRequest = Count;
                    }
                }
                else
                {
                    PagesListview.IsVisible = false;
                }

                if (GroupUserListItems.Count > 0)
                {
                    GroupListview.IsVisible = true;
                    GroupsHeader.Text = AppResources.Label_Groups +" (" + Profile.GroupUserList.Count + ")";
                    GroupListview.ItemsSource = GroupUserListItems;
                 
                    var Count = GroupUserListItems.Count * 60 + 50;

                    if (GroupUserListItems.Count > 6)
                    {
                        GroupListview.HeightRequest = 6 * 60 + 50; ;
                    }
                    else
                    {
                        GroupListview.HeightRequest = Count;
                    }
                }
                else
                {
                    GroupListview.IsVisible = false;
                }

                if (FollowerUserListItems.Count == 0 && GroupUserListItems.Count == 0 && Liked_PagesUserListItems.Count == 0)
                {
                    EmptyStack.IsVisible = true;
                }
                else
                {
                    EmptyStack.IsVisible = false;
                }             
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #region Photo

        public async void GetMyAlbums()
        {
            try
            {
                AlbumlistUsersItems.Clear();
                var column = LeftColumn;
                column.Children.Clear();
                var columnr = RightColumn;
                columnr.Children.Clear();

                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", U_UserID),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_albums", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var albums = JObject.Parse(json).SelectToken("albums").ToString();
                        JArray ImagedataArray = JArray.Parse(albums);

                        foreach (var Image in ImagedataArray)
                        {
                            var url = Image["url"].ToString();
                            var post_id = Image["post_id"].ToString();
                            var Orginaltext = Image["Orginaltext"].ToString();
                            var post_time = Image["post_time"].ToString();
                            var postFile = Image["postFile_full"].ToString();
                            var limit_comments = Image["post_comments"].ToString();
                            var post_likes = Image["post_likes"].ToString();
                            var post_wonders = Image["post_wonders"].ToString();

                            JObject PublisherImage = JObject.FromObject(Image["publisher"]);
                            var Publishername = PublisherImage["name"].ToString();
                            // var PublisherAvatar = PublisherImage["name"].ToString();

                            var DecodedOrginaltext = System.Net.WebUtility.HtmlDecode(Orginaltext);

                            JArray CommentsArray = JArray.Parse(Image["get_post_comments"].ToString());

                            var image = "";

                            var Commentlist = new List<Albums.Comment>();
                            if (CommentsArray != null)
                            {
                                if (CommentsArray.Count > 0)
                                {
                                    foreach (var Comment in CommentsArray)
                                    {
                                        var comment_likes = Comment["comment_likes"].ToString();
                                        var OrginalText =
                                            System.Net.WebUtility.HtmlDecode(Comment["Orginaltext"].ToString());
                                        var Commenterurl = Comment["url"].ToString();

                                        JObject CommentPublisher = JObject.FromObject(Comment["publisher"]);

                                        var avatar = CommentPublisher["avatar"].ToString();
                                        var cover = CommentPublisher["cover"].ToString();
                                        var NameUser = CommentPublisher["name"].ToString();
                                        var user_id = CommentPublisher["user_id"].ToString();
                                        var Username = CommentPublisher["username"].ToString();
                                        var DecodedCommentOrginaltext = System.Net.WebUtility.HtmlDecode(OrginalText);

                                        var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(avatar, user_id);
                                        var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                        if (DependencyService.Get<IPicture>().GetPictureFromDisk(avatar, user_id) ==
                                            "File Dont Exists")
                                        {
                                            ImageMediaFile = new UriImageSource
                                            {
                                                Uri = new Uri(avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(5, 0, 0, 0)
                                            };
                                            DependencyService.Get<IPicture>().SavePictureToDisk(avatar, user_id);
                                        }

                                        Commentlist.Add(new Albums.Comment()
                                        {
                                            CommentText = DecodedCommentOrginaltext,
                                            CommentLikes = comment_likes,
                                            CommenterProfileUrl = Commenterurl,
                                            NameCommenter = NameUser,
                                            UserID = user_id,
                                            Username = Username,
                                            AvatarUser = ImageMediaFile,
                                        });
                                    }
                                }
                            }

                            AlbumlistUsersItems.Add(new Albums()
                            {
                                Name = "My Album",
                                Image = new UriImageSource
                                {
                                    Uri = new Uri(postFile),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 1, 0, 0)
                                },
                                Imagetext = DecodedOrginaltext,
                                ThumbnailHeight = "100",
                                Postid = post_id,
                                WonderCount = post_wonders,
                                LikesCount = post_likes,
                                CommentsCount = limit_comments,
                                ImageComments = Commentlist,
                                Publishername = Publishername,
                                ImageTime = post_time
                            });
                        }
                        PopulateAlbumLists(AlbumlistUsersItems);
                    }
                    else
                    {
                       // UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
                    }

                    // UserDialogs.Instance.HideLoading();

                    if (AlbumlistUsersItems.Count == 0)
                    {
                            ShowDataImgGrid.IsVisible = false;
                    }
                    else
                    {
                            ShowDataImgGrid.IsVisible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                // UserDialogs.Instance.HideLoading();
                PopulateAlbumLists(AlbumlistUsersItems);
            }
        }

        private void PopulateAlbumLists(List<Albums> AlbumsList)
        {
            try
            {
                var lastHeight = "128";
                int conut = 0;
                var y = 0;
                var column = LeftColumn;
                var productTapGestureRecognizer = new TapGestureRecognizer();
                productTapGestureRecognizer.Tapped += OnProductTapped;

                for (var i = 0; i < AlbumsList.Count; i++)
                {
                    var item = new ImageGriditemTemplate();

                    if (conut < 4)
                    {
                        if (i % 2 == 0)
                        {
                            column = LeftColumn;
                            y++;
                        }
                        else
                        {
                            column = RightColumn;
                        }
                        AlbumsList[i].ThumbnailHeight = lastHeight;
                        item.BindingContext = AlbumsList[i];
                        item.GestureRecognizers.Add(productTapGestureRecognizer);
                        column.Children.Add(item);
                        conut++;
                    }
                }

                if (AlbumlistUsersItems.Count > 0)
                {
                    PhotoStack.IsVisible = true;
                    ShowDataImgGrid.IsVisible = true;

                    PhotoHeader.Text = AppResources.Label_Photo + " (" + AlbumlistUsersItems.Count + ")";
                }
                else
                {
                    PhotoStack.IsVisible = false;
                    ShowDataImgGrid.IsVisible = false;
                }
                if (AlbumlistUsersItems.Count > 4)
                {
                    ShowAllPhotoStackLayout.IsVisible = true;
                }
                else
                {
                    ShowAllPhotoStackLayout.IsVisible = false;
                }
                

               
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnProductTapped(Object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Albums)((ImageGriditemTemplate)sender).BindingContext;
                if (selectedItem != null)
                {
                    var productView = new ImageCommentPage(selectedItem)
                    {
                        BindingContext = selectedItem
                    };

                    await Navigation.PushAsync(productView);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ShowAllPhoto_OnTapped(object sender, EventArgs e)
        {
            try
            {
                if (AlbumlistUsersItems.Count > 4)
                {
                    await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(null, null, null, AlbumlistUsersItems));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion

        #region Gruops

        private async void ShowAllGroups_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(null, Profile.GroupUserList, null, null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void GroupListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                GroupListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void GroupListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var selectedItem = e.Item as Profile.Group;
                if (selectedItem != null)
                {
                    await Navigation.PushAsync(new SocialGroup(selectedItem.G_id, "Joined"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion

        #region Pages

        private void PagesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                PagesListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void PagesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var selectedItem = e.Item as Profile.Liked_Pages;
                if (selectedItem != null)
                {
                    await Navigation.PushAsync(new SocialPageViewer(selectedItem.P_page_id, selectedItem.P_page_name, "Liked"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ShowAllPages_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(null, null, Profile.Liked_PagesUserList, null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion

        #region Contacts

        public void Add_Contacts_Users(ObservableCollection<Profile.Follower> FollowerUserList)
        {
            try
            {
                for (var i = 0; i < FollowerUserList.Count; i++)
                {
                    var TapGestureRecognizer = new TapGestureRecognizer();
                    TapGestureRecognizer.Tapped += OnContactsTapped;
                    var item = new Contact_DataUsers_Template();
                    item.BindingContext = FollowerUserList[i];
                    item.GestureRecognizers.Add(TapGestureRecognizer);
                    ContactsStack.Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in Contact_DataUsers_Template >> open UserProfilePage
        private async void OnContactsTapped(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Profile.Follower)((Contact_DataUsers_Template)sender).BindingContext;
                if (selectedItem != null)
                {
                    await Navigation.PushAsync(new UserProfilePage(selectedItem.F_user_id , "FriendList"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ShowAllContacts_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(Profile.FollowerUserList, null, null,null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion


        private void TapFriend_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (FriendStatusText.Text == F_AlreadyFriend)
                {
                    SendFriendRequest(U_UserID).ConfigureAwait(false);
                    FriendStatusText.Text = F_Still_NotFriend;
                    FriendStatusImage.Source = Settings.AddUser_Icon;
                }
                else if (FriendStatusText.Text == F_Still_NotFriend)
                {

                    SendFriendRequest(U_UserID).ConfigureAwait(false);
                    FriendStatusText.Text = F_AlreadyFriend;
                    FriendStatusImage.Source = Settings.Likecheked_Icon;
                }
                else if (FriendStatusText.Text == F_Request)
                {
                    SendFriendRequest(U_UserID).ConfigureAwait(false);
                    FriendStatusText.Text = F_Still_NotFriend;
                    FriendStatusImage.Source = Settings.AddUser_Icon;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TapMessage_OnClicked(object sender, EventArgs e)
        {
            try
            {
                DependencyService.Get<IMethods>().OpenMessengerApp(Settings.Messenger_Package_Name);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TapBlock_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (BlockLabel.Text == AppResources.Label_Block)
                {
                    SendBlockRequest(U_UserID).ConfigureAwait(false);
                    BlockLabel.Text = AppResources.Label_Blocked;
                    blockImage.Source = Settings.BlockedUser_Icon;
                }
                else if (BlockLabel.Text == AppResources.Label_Blocked)
                {
                    SendUnBlockRequest(U_UserID).ConfigureAwait(false);
                    BlockLabel.Text = AppResources.Label_Block;
                    blockImage.Source = Settings.BlockUser_Icon;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

    }
}