﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Pages.Tabs;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using Newtonsoft.Json.Linq;
using PropertyChanged;
using WowonderPhone.Classes;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Articles;


namespace WowonderPhone.Pages
{

    public partial class MasterMainSlidePage : MasterDetailPage
    {

        #region classse

        [ImplementPropertyChanged]
        public class SliderItems
        {
            public int ID { get; set; }
            public string Label { get; set; }
            public string Heightrow { get; set; }
            public ImageSource Icon { get; set; }

            public string BadgeNumber { get; set; }
            public string Badgecolor { get; set; }
            public string BadgeIsVisible { get; set; }
        }

        [ImplementPropertyChanged]
        public class TopUsersItems
        {
            public string Id { get; set; }
            public string IsVip { get; set; }
            public string User_id { get; set; }
            public string Username { get; set; }
            public string Name { get; set; }
            public ImageSource Image { get; set; }
        }

        [ImplementPropertyChanged]
        public class PagesItems
        {
            public string Page_id { get; set; }
            public string Page_name { get; set; }
            public string Username { get; set; }
            public string Category { get; set; }
            public ImageSource Avatar { get; set; }
            public string BadgeNumber { get; set; }
            public string Badgecolor { get; set; }
            public string BadgeIsVisible { get; set; }
        }

        [ImplementPropertyChanged]
        public class TrendingItems
        {
            public string Label { get; set; }
            public string ID { get; set; }
        }

        #endregion

        #region Lists Items Declaration and Variables

        public static ObservableCollection<SliderItems> ListItemsCollection = new ObservableCollection<SliderItems>();
        public static ObservableCollection<TopUsersItems> TopUsersCollection = new ObservableCollection<TopUsersItems>();
        public static ObservableCollection<PagesItems> PagesItemsCollection = new ObservableCollection<PagesItems>();
        public static ObservableCollection<TrendingItems> TrendingItemsCollection =new ObservableCollection<TrendingItems>();
        public static ObservableCollection<FriendRequests> FriendRequestsItemsCollection =new ObservableCollection<FriendRequests>();

        public bool TimerBool = true;

        #endregion


        public MasterMainSlidePage()
        {
            try
            {

                if (Device.RuntimePlatform == Device.iOS)
                {
                    NavigationPage.SetHasNavigationBar(this, true);
                    //HeaderOfpage.IsVisible = false;
                }
                else
                    NavigationPage.SetHasNavigationBar(this, false);
                InitializeComponent();
                this.Title = Settings.MainPage_HeaderTextLabel;

                GetMyprofile();
                AddItemsToList();

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Get_notifications().ConfigureAwait(false);
                    Device.StartTimer(TimeSpan.FromSeconds(Settings.Notifications_Push_Secounds), () =>
                    {
                        NotifiTimerFunction();
                        return TimerBool;
                    });
                }
                else
                {
                    LoaderSpinner.IsVisible = false;
                    UserDialogs.Instance.Toast(AppResources.Label_Offline_Mode);
                }

                var TapMyProfile = new TapGestureRecognizer();
                TapMyProfile.Tapped += (s, ee) =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("MyProfile", ""));
                };

                if (Username.GestureRecognizers.Count == 0)
                {
                    Username.GestureRecognizers.Add(TapMyProfile);
                }

                if (TopUsersCollection.Count == 0)
                {
                    VipPanelSection.IsVisible = false;
                }
                if (TrendingItemsCollection.Count == 0)
                {
                    VipPanelSection.IsVisible = false;
                }
                if (PagesItemsCollection.Count == 0)
                {
                    PagesList.IsVisible = false;
                }

                TrendingList.ItemsSource = TrendingItemsCollection;
                PagesList.ItemsSource = PagesItemsCollection;
                TopUsersList.ItemsSource = TopUsersCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void NotifiTimerFunction()
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Get_notifications().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //protected override bool OnBackButtonPressed()
        //{
        //    base.OnBackButtonPressed();
        //    return true;
        //}

        public void GetMyprofile()
        {
            try
            {
                using (var Data = new LoginUserProfileFunctions())
                {
                    var TapGestureRecognizerSettings = new TapGestureRecognizer();
                    TapGestureRecognizerSettings.Tapped += async (s, ee) =>
                    {
                        Timeline_Pages.SettingsPage SettingsPage = new Timeline_Pages.SettingsPage();
                        await Navigation.PushAsync(SettingsPage);
                    };

                    if (SettingsImage.GestureRecognizers.Count == 0)
                    {
                        SettingsImage.GestureRecognizers.Add(TapGestureRecognizerSettings);
                    }

                    var Profile = Data.GetProfileCredentialsById(Settings.User_id);
                    var device = Resolver.Resolve<IDevice>();
                    var oNetwork = device.Network;
                    var InternetConnection = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;

                    if (InternetConnection)
                    {
                        if (Profile == null)
                        {
                            Settings.Avatarimage = "default_profile_6_400x400.png";
                            AvatarImage.Source = Settings.Avatarimage;
                            //AvatarImage.BorderColor = Color.FromHex("#c72508");
                        }
                        else
                        {
                            var GetAvatarAvailability =
                                DependencyService.Get<IPicture>().GetPictureFromDisk(Profile.avatar, Settings.User_id);
                            if (GetAvatarAvailability != "File Dont Exists")
                            {
                                Settings.Avatarimage = GetAvatarAvailability;
                                Settings.UserFullName = Profile.name;

                                AvatarImage.Source = Settings.Avatarimage;
                                //AvatarImage.BorderColor = Color.FromHex("#c72508");
                                Username.Text = Profile.name;


                                var tapGestureRecognizer = new TapGestureRecognizer();

                                tapGestureRecognizer.Tapped += async (s, ee) =>
                                {
                                    MyProfilePage MyProfile = new MyProfilePage();
                                    await Navigation.PushAsync(MyProfile);
                                };
                                if (AvatarImage.GestureRecognizers.Count == 0)
                                {
                                    AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Profile != null)
                        {
                            var GetAvatarAvailability =
                                DependencyService.Get<IPicture>().GetPictureFromDisk(Profile.avatar, Settings.User_id);
                            if (GetAvatarAvailability != "File Dont Exists")
                            {
                                Settings.Avatarimage = GetAvatarAvailability;
                                Settings.UserFullName = Profile.name;

                                AvatarImage.Source = Settings.Avatarimage;
                                // AvatarImage.BorderColor = Color.FromHex("#6CDB26");
                                Username.Text = Profile.name;

                                var tapGestureRecognizer = new TapGestureRecognizer();
                                tapGestureRecognizer.Tapped += async (s, ee) =>
                                {
                                    MyProfilePage MyProfile = new MyProfilePage();
                                    await Navigation.PushAsync(MyProfile);
                                };
                                if (AvatarImage.GestureRecognizers.Count == 0)
                                {
                                    AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                                }
                            }
                            else
                            {
                                AvatarImage.Source = new UriImageSource
                                {
                                    Uri = new Uri(Profile.avatar),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 0, 0, 0)
                                };
                                //AvatarImage.BorderColor = Color.FromHex("#6CDB26");
                                Settings.UserFullName = Profile.name;

                                var tapGestureRecognizer = new TapGestureRecognizer();
                                tapGestureRecognizer.Tapped += async (s, ee) =>
                                {
                                    MyProfilePage MyProfile = new MyProfilePage();
                                    await Navigation.PushAsync(MyProfile);
                                };

                                if (AvatarImage.GestureRecognizers.Count == 0)
                                {
                                    AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                                }
                            }
                        }
                        else
                        {
                            LoginUserFunctions.GetSessionProfileData(Settings.User_id, Settings.Session)
                                .ConfigureAwait(false);
                            AvatarImage.Source = Settings.Avatarimage;
                            Username.Text = Settings.UserFullName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void MasterMainSlidePage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                AvatarImage.Source = Settings.Avatarimage;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddItemsToList()
        {
            try
            {
                int sizeHeight = 315;
                if (ListItemsCollection.Count == 0)
                {
                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Notifications,
                        Icon = "G_FeedBack3.png",
                        BadgeNumber = "0",
                        Badgecolor = Settings.MainColor,
                        BadgeIsVisible = "false",
                        Heightrow = "50"

                    });


                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Friends,
                        Icon = "G_Friends.png",
                        BadgeNumber = "0",
                        Badgecolor = Settings.MainColor,
                        BadgeIsVisible = "false",
                        Heightrow = "50"
                    });

                    if (Settings.Messenger_Integration)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Messages,
                            Icon = "G_Messenger.png",
                            BadgeNumber = "0",
                            Badgecolor = Settings.MainColor,
                            BadgeIsVisible = "false",
                            Heightrow = "50"
                        });

                        sizeHeight += 53;
                    }

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Communities,
                        Icon = "G_Groups.png",
                        BadgeNumber = "0",
                        Badgecolor = Settings.MainColor,
                        BadgeIsVisible = "false",
                        Heightrow = "50"
                    });
                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Albums,
                        Icon = "G_Photos.png",
                        BadgeNumber = "0",
                        Badgecolor = Settings.MainColor,
                        BadgeIsVisible = "false",
                        Heightrow = "50"
                    });

                    if (Settings.Show_Market)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Market,
                            Icon = "G_Market.png",
                            BadgeNumber = "0",
                            Badgecolor = Settings.MainColor,
                            BadgeIsVisible = "false",
                            Heightrow = "50"
                        });
                        sizeHeight += 53;
                    }
                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Search,
                        Icon = "G_Search.png",
                        BadgeNumber = "0",
                        Badgecolor = Settings.MainColor,
                        BadgeIsVisible = "false",
                        Heightrow = "50"
                    });

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Saved_Post,
                        Icon = "G_SavedPost.png",
                        BadgeNumber = "0",
                        Badgecolor = Settings.MainColor,
                        BadgeIsVisible = "false",
                        Heightrow = "50"
                    });

                    if (Settings.Show_Articles)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Articles,
                            Icon = "G_Articles.png",
                            BadgeNumber = "0",
                            Badgecolor = Settings.MainColor,
                            BadgeIsVisible = "false",
                            Heightrow = "50"
                        });
                        sizeHeight += 53;
                    }
                }

                itemslist.Height = sizeHeight;
                GeneralList.ItemsSource = ListItemsCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }       
        }

        private void GeneralList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                GeneralList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void GeneralList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as SliderItems;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_Albums)
                    {
                        await Navigation.PushAsync(new AlbumPage());
                    }
                    else if (Item.Label == AppResources.Label_Friends)
                    {
                        await Navigation.PushAsync(new ContactsTab());
                    }
                    else if (Item.Label == AppResources.Label_Messages)
                    {
                        //Device.OpenUri(new Uri("wowonder://compose?text=Wowonder"));
                        DependencyService.Get<IMethods>().OpenMessengerApp(WowonderPhone.Settings.Messenger_Package_Name);
                    }
                    else if (Item.Label == AppResources.Label_Communities)
                    {
                        await Navigation.PushAsync(new CommunitiesPage());
                    }
                    else if (Item.Label == AppResources.Label_Market)
                    {
                        await Navigation.PushAsync(new MarketPage());
                    }
                    else if (Item.Label == AppResources.Label_Notifications)
                    {
                        Item.BadgeIsVisible = "false";
                        Item.BadgeNumber = "0";
                        await Navigation.PushAsync(new NotificationsPage());
                    }
                    else if (Item.Label == AppResources.Label_Search)
                    {
                        await Navigation.PushAsync(new Search_Page());
                    }
                    else if (Item.Label == AppResources.Label_Saved_Post)
                    {
                        await Navigation.PushAsync(new HyberdPostViewer("Saved Post", ""));
                    }
                    else if (Item.Label == AppResources.Label_Articles)
                    {
                        await Navigation.PushAsync(new ArticlesListPage());
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PagesList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as PagesItems;
                if (Item != null)
                {
                    Navigation.PushAsync(new SocialPageViewer(Item.Page_id, Item.Page_name, ""));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TrendingList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                TrendingList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TrendingList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as TrendingItems;
                var replace = Item.Label.Replace("#", "");
                Navigation.PushAsync(new HyberdPostViewer("Hashtag", replace));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PagesList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            PagesList.SelectedItem = null;
        }

        public static string NotifiStoper = "0";
        public static int ProMemberStoper = 3;
        public static ObservableCollection<Notifications> NotificationsList = new ObservableCollection<Notifications>();

        public async Task Get_notifications()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_notifications",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        #region Notification

                        try
                        {
                            var Parser = JObject.Parse(json).SelectToken("notifications").ToString();
                            JArray NotificationsArray = JArray.Parse(Parser);
                            if (NotificationsList.Count <= NotificationsArray.Count)
                            {
                                NotificationsList.Clear();
                            }
                            foreach (var Notification in NotificationsArray)
                            {

                                JObject Data = JObject.FromObject(Notification);
                                JObject Notifier = JObject.FromObject(Data["notifier"]);
                                var Notifier_id = Data["notifier_id"].ToString();
                                var Post_ID = Data["post_id"].ToString();
                                var Page_ID = Data["page_id"].ToString();
                                var Group_ID = Data["group_id"].ToString();
                                var Count_Notification =
                                    JObject.Parse(json).SelectToken("count_notifications").ToString();
                                var Seen = Data["seen"].ToString();
                                var Type_Text = Data["type_text"].ToString();
                                var Time_Text = Data["time_text_string"].ToString();
                                var Type = Data["type"].ToString();
                                var Icon = Data["icon"].ToString();
                                var ID = Data["id"].ToString();
                                var User_id = Notifier["user_id"].ToString();
                                var Username = Notifier["username"].ToString();
                                var Name = Notifier["name"].ToString();
                                var Avatar = Notifier["avatar"].ToString();

                                if (Count_Notification != "0")
                                {
                                    var Alfa =
                                        ListItemsCollection.FirstOrDefault(
                                            a => a.Label == AppResources.Label_Notifications);
                                    if (Alfa.BadgeNumber != Count_Notification)
                                    {
                                        Alfa.BadgeIsVisible = "true";
                                        Alfa.BadgeNumber = Count_Notification;
                                    }
                                }
                                else
                                {
                                    var Alfa =
                                        ListItemsCollection.FirstOrDefault(
                                            a => a.Label == AppResources.Label_Notifications);
                                    Alfa.BadgeIsVisible = "false";
                                    Alfa.BadgeNumber = "0";
                                    Alfa.Badgecolor = "#ffff";
                                }

                                var ColorIcon = NotifiTypeFunction.GetColorFontAwesom(Type);
                                var TypeIcon = NotifiTypeFunction.GetIconFontAwesom(Type);
                                if (Icon == "exclamation-circle" || Icon == "thumbs-down")
                                {
                                    TypeIcon = NotifiTypeFunction.GetIconFontAwesom(Icon);
                                    ColorIcon = NotifiTypeFunction.GetColorFontAwesom(Icon);
                                }

                                var DecodedTypetext = System.Net.WebUtility.HtmlDecode(Type_Text);
                                var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(Avatar, User_id);
                                var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                if (DependencyService.Get<IPicture>().GetPictureFromDisk(Avatar, User_id) ==
                                    "File Dont Exists")
                                {
                                    ImageMediaFile = new UriImageSource
                                    {
                                        Uri = new Uri(Avatar),
                                        CachingEnabled = true,
                                        CacheValidity = new TimeSpan(5, 0, 0, 0)
                                    };
                                    DependencyService.Get<IPicture>().SavePictureToDisk(Avatar, User_id);
                                }

                                var Cheker = NotificationsList.FirstOrDefault(a => a.ID == ID);
                                if (NotificationsList.Contains(Cheker))
                                {

                                }
                                else
                                {
                                    if (Seen == "0" && Settings.NotificationPopup)
                                    {
                                        int NotiID = Int32.Parse(ID);
                                        using (var Notifi = new NotifiFunctions())
                                        {
                                            var Exits = Notifi.GetNotifiDBCredentialsById(NotiID);
                                            if (Exits == null)
                                            {
                                                Notifi.InsertNotifiDBCredentials(new NotifiDB
                                                {
                                                    messageid = NotiID,
                                                    Seen = 0
                                                });
                                            }
                                            var Exits2 = Notifi.GetNotifiDBCredentialsById(NotiID);
                                            if (Exits2.Seen == 0 && NotifiStoper != ID)
                                            {

                                                var Iconavatr = DependencyService.Get<IPicture>()
                                                    .GetPictureFromDisk(Avatar, User_id);
                                                DependencyService.Get<ILocalNotificationService>()
                                                    .CreateLocalNotification(Name, DecodedTypetext, Iconavatr, User_id,
                                                        "Text");
                                                Exits2.Seen = 1;
                                                Notifi.UpdateNotifiDBCredentials(Exits2);
                                            }
                                        }
                                    }

                                    NotificationsList.Add(new Notifications()
                                    {
                                        ID = ID,
                                        Notifier_id = Notifier_id,
                                        Seen = Seen,
                                        Type_Text = DecodedTypetext,
                                        Time_Text = Time_Text,
                                        Type = Type,
                                        User_id = User_id,
                                        Username = Username,
                                        Name = Name,
                                        Avatar = ImageMediaFile,
                                        SeenUnseenColor = "#fff",
                                        Icon_Color_FO = ColorIcon,
                                        Icon_Type_FO = TypeIcon,
                                        Post_ID = Post_ID,
                                        Group_ID = Group_ID,
                                        Page_ID = Page_ID,
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Pro_users

                        try
                        {
                            var Parser2 = JObject.Parse(json).SelectToken("pro_users").ToString();
                            JArray Pro_usersArray = JArray.Parse(Parser2);
                            if (TopUsersCollection.Count > 60)
                            {
                                //if (ProMemberStoper <= 9)
                                //{
                                //    TopUsersCollection.Clear();
                                //}
                            }
                            foreach (var ProVIP in Pro_usersArray)
                            {

                                JObject Data = JObject.FromObject(ProVIP);
                                var User_id = Data["user_id"].ToString();
                                var Username = Data["username"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Name = Data["name"].ToString();

                                var Cheker = TopUsersCollection.FirstOrDefault(a => a.User_id == User_id);
                                if (Cheker != null)
                                {

                                }
                                else
                                {
                                    if (ProMemberStoper == 3)
                                    {
                                        TopUsersCollection.Insert(0, new TopUsersItems()
                                        {
                                            Name = Name,
                                            User_id = User_id,
                                            Username = Username,
                                            IsVip = "true",
                                            Image = new UriImageSource
                                            {
                                                Uri = new Uri(Avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            },
                                        });

                                        var ff = TopUsersCollection.FirstOrDefault(a => a.IsVip == "False");
                                        if (ff != null)
                                        {
                                            var Int = TopUsersCollection.IndexOf(ff);
                                            TopUsersCollection.Move(Int, 0);
                                        }
                                        else
                                        {
                                            TopUsersCollection.Insert(0, new TopUsersItems()
                                            {
                                                IsVip = "False",
                                                Image = "G_Add_ProUsers.png",
                                            });
                                        }

                                    }
                                }
                            }

                            if (ProMemberStoper >= 4)
                            {
                                ProMemberStoper = 0;
                            }
                            else
                            {
                                ProMemberStoper++;
                            }

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                TopUsersList.ItemsSource = null;
                                TopUsersList.ItemsSource = TopUsersCollection;
                            });
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Count_Messages

                        try
                        {
                            var Parser3 = JObject.Parse(json).SelectToken("count_messages").ToString();
                            if (Parser3 != "0")
                            {

                                var Alfa =
                                    ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Messages);
                                if (Alfa.BadgeNumber != Parser3)
                                {
                                    Alfa.BadgeIsVisible = "true";
                                    Alfa.BadgeNumber = Parser3;
                                }

                            }
                            else
                            {
                                var Alfa =
                                    ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Messages);
                                Alfa.BadgeIsVisible = "false";
                                Alfa.BadgeNumber = "0";
                                Alfa.Badgecolor = "#ffff";
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Promoted_pages

                        try
                        {
                            var Parser5 = JObject.Parse(json).SelectToken("promoted_pages").ToString();
                            JArray Promoted_pagesArray = JArray.Parse(Parser5);
                            if (Promoted_pagesArray.Count == 0 && PagesItemsCollection.Count == 0)
                            {
                                PagesList.IsVisible = false;
                            }

                            foreach (var Pages in Promoted_pagesArray)
                            {
                                JObject Data = JObject.FromObject(Pages);
                                var Page_id = Data["page_id"].ToString();
                                var Page_name = Data["page_title"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Category = Data["category"].ToString();
                                var Cheker = PagesItemsCollection.FirstOrDefault(a => a.Page_id == Page_id);
                                if (Cheker != null)
                                {
                                    if (PagesItemsCollection.Count > 3)
                                    {
                                        PagesItemsCollection.RemoveAt(3);
                                    }
                                }
                                else
                                {
                                    if (PagesItemsCollection.Count <= 3)
                                    {
                                        PagesItemsCollection.Add(new PagesItems()
                                        {
                                            Page_id = Page_id,
                                            Page_name = Page_name,
                                            Category = Category,
                                            Avatar = new UriImageSource
                                            {
                                                Uri = new Uri(Avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            }
                                        });

                                    }
                                    else
                                    {
                                        //PagesItemsCollection.Insert(0, new PagesItems()
                                        //{
                                        //    Page_id = Page_id,
                                        //    Page_name = Page_name,
                                        //    Category = Category,
                                        //    Avatar = new UriImageSource
                                        //    {
                                        //        Uri = new Uri(Avatar),
                                        //        CachingEnabled = true,
                                        //        CacheValidity = new TimeSpan(2, 0, 0, 0)
                                        //    }
                                        //});
                                        //PagesItemsCollection.RemoveAt(3);
                                    }

                                }
                            }

                            if (PagesItemsCollection.Count == 1)
                            {
                                PagesList.HeightRequest = 120;
                            }
                            else if (PagesItemsCollection.Count == 2)
                            {
                                PagesList.HeightRequest = 175;
                            }
                            else if (PagesItemsCollection.Count == 3)
                            {
                                PagesList.HeightRequest = 210;
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Trending_Hashtag

                        try
                        {
                            var Parser4 = JObject.Parse(json).SelectToken("trending_hashtag").ToString();
                            JArray Trending_HashtagArray = JArray.Parse(Parser4);
                            if (TrendingItemsCollection.Count > 30)
                            {
                                TrendingItemsCollection.Clear();
                            }
                            foreach (var Hashtag in Trending_HashtagArray)
                            {
                                JObject Data = JObject.FromObject(Hashtag);
                                var id = Data["id"].ToString();
                                var Tag = System.Net.WebUtility.HtmlDecode(Data["tag"].ToString());
                                var trend_use_num = Data["trend_use_num"].ToString();
                                var Cheker = TrendingItemsCollection.FirstOrDefault(a => a.ID == id);

                                if (Cheker != null)
                                {

                                }
                                else
                                {
                                    TrendingItemsCollection.Add(new TrendingItems()
                                    {
                                        ID = id,
                                        Label = "#" + Tag
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        # region Friend Requests

                        try
                        {

                            var Parser7 = JObject.Parse(json).SelectToken("friend_requests").ToString();
                            JArray FriendRequestArray = JArray.Parse(Parser7);

                            foreach (var Friend in FriendRequestArray)
                            {
                                JObject Data = JObject.FromObject(Friend);
                                var User_id = Data["user_id"].ToString();
                                var Username = Data["username"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Name = Data["name"].ToString();

                                var Cheker = FriendRequestsItemsCollection.FirstOrDefault(a => a.User_id == User_id);
                                if (Cheker != null)
                                {

                                }
                                else
                                {
                                    FriendRequestsItemsCollection.Add(new FriendRequests()
                                    {
                                        Name = Name,
                                        User_id = User_id,
                                        Username = Username,
                                        Image = new UriImageSource
                                        {
                                            Uri = new Uri(Avatar),
                                            CachingEnabled = true,
                                            CacheValidity = new TimeSpan(2, 0, 0, 0)
                                        },
                                        AccepetedIsvisbile = "False",
                                        AccepetedText = AppResources.Label_You_are_now_friends,
                                        ConfirmButtonBGColor = Settings.ButtonColorNormal,
                                        ConfirmButtonIsvisbile = "True",
                                        ConfirmButtonText = AppResources.Label_Confirm,
                                        ConfirmTextColor = Settings.ButtonTextColorNormal,
                                        DeleteButtonBGColor = Settings.ButtonLightColor,
                                        DeleteButtonIsvisbile = "True",
                                        DeleteButtonText = AppResources.Label_Delete,
                                        DeleteTextColor = Settings.ButtonTextLightColor,
                                    });
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                    }
                    else if (apiStatus == "400")
                    {
                        try
                        {
                            JObject errors = JObject.FromObject(data["errors"]);
                            var errortext = errors["error_text"].ToString();
                            var errorID = errors["error_id"].ToString();
                            if (errorID == "6")
                            {
                                TimerBool = false;
                                //UserDialogs.Instance.ShowLoading(AppResources.Label_Logging_out, MaskType.Clear);
                                using (var Data = new LoginFunctions())
                                {
                                    var CredentialStatus = Data.GetLoginCredentialsStatus();
                                    Data.DeleteLoginCredential(Settings.Session);
                                    Data.ClearLoginCredentialsList();

                                }
                                using (var Data = new LoginUserProfileFunctions())
                                {
                                    Data.ClearProfileCredentialsList();
                                }
                                using (var Data = new MessagesFunctions())
                                {
                                    Data.ClearMessageList();
                                }
                                using (var Data = new ContactsFunctions())
                                {
                                    Data.DeletAllChatUsersList();
                                }
                                using (var Data = new CommunitiesFunction())
                                {
                                    Data.DeletAllCommunityList();
                                }
                                using (var Data = new NotifiFunctions())
                                {
                                    Data.ClearNotifiDBCredentialsList();
                                }
                                using (var Data = new PrivacyFunctions())
                                {
                                    Data.ClearPrivacyDBCredentialsList();
                                }

                                App.GetLoginPage();
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                            //await Navigation.PushModalAsync(new MainPage());
                        }
                    }
                    LoaderSpinner.IsVisible = false;
                }

                if (TopUsersCollection.Count > 2 && Settings.Show_Pro_Members)
                {
                    if (VipPanelSection.IsVisible == false)
                        VipPanelSection.IsVisible = true;
                }
                if (TrendingItemsCollection.Count >= 1 && Settings.Show_Trending_Hashtags)
                {
                    if (TrendingList.IsVisible == false)
                        TrendingList.IsVisible = true;
                }

                if (PagesItemsCollection.Count > 0 && Settings.Show_Promoted_Pages)
                {
                    if (PagesList.IsVisible == false)
                        PagesList.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Page_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                //TopUsersList.ItemsSource = TopUsersCollection;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
