﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Register_pages
{
    public partial class TwitterView : ContentPage
    {
        public TwitterView()
        {
            try
            {
                InitializeComponent();
                UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.Black);
                HashSet = RandomString(80);
                WebView1.Source = Settings.Website +
                                  "/app_api.php?application=phone&type=user_login_with&provider=Twitter&hash=" + HashSet;
                TimerLogin().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
       
        public static string HashSet = "";
        public static string Result = "false";
        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXdsdaawerthklmnbvcxer46gfdsYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void WebView1_OnNavigated(object sender, WebNavigatedEventArgs e)
        {
            try
            {
                UserDialogs.Instance.HideLoading();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> FacebookLogin()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var response = await client.GetAsync(Settings.Website + "/app_api.php?application=phone&type=check_hash&hash_id=" + HashSet);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        JObject userdata = JObject.FromObject(data["user_data"]);
                        Settings.User_id = userdata["user_id"].ToString();
                        Settings.Session = userdata["session_id"].ToString();
                        Settings.Cookie = userdata["cookie"].ToString();

                        var datasss = new LoginFunctions();

                        LoginTableDB LoginData = new LoginTableDB();
                        LoginData.Session = Settings.Session;
                        LoginData.UserID = Settings.User_id;
                        LoginData.Status = "Active";
                        LoginData.Cookie = Settings.Cookie;
                        datasss.InsertLoginCredentials(LoginData);

                        Result = "True";

                        App.SessionHyberd = true;

                        try
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                try
                                {
                                    await Navigation.PushModalAsync(new WalkthroughVariantPage());
                                    // await Navigation.PopAsync(false);
                                }
                                catch (Exception ex)
                                {
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);

                                    // await Navigation.PushAsync(new WalkThrough_Page3());
                                    // await Navigation.PopModalAsync(true);
                                }
                            });
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                        return "true";
                    }
                    else
                    {
                        //    Result = "True";
                        // HashSet = RandomString(80);
                        //WebView1.Source = Settings.Website + "/app_api.php?application=phone&type=user_login_with&provider=Facebook&hash=" + HashSet;

                        //await Task.Run( async () =>
                        //{
                        //    await Task.Delay(4000);
                        //    Result = "False";

                        //    await TimerLogin();
                        //});
                    }
                }
                return "true";
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "true";
            }
        }

        public async Task<string> TimerLogin()
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                for (int i = 0; i < 5; i++)
                {
                    while (Result == "false")
                    {
                        await Task.Delay(3000);
                        await FacebookLogin();
                        if (Result == "True")
                        {
                            sw.Stop();
                            return "Stop";
                        }
                    }
                }
                return "true";
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return "true";
            }
        }
    }
}