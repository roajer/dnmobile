﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using XLabs;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Register_pages
{
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void RegisterButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection, AppResources.Label_OK);
                }
                else
                {
                    var clickresult = "true";
                    if (Settings.AcceptTerms_Display)
                    {
                        var result = await DisplayAlert(AppResources.Label_Terms, AppResources.Label_Terms_LongText, AppResources.Label_Accept_Terms, AppResources.Label_Dot_Agree);
                        if (result)
                        {
                            clickresult = "true";
                        }
                        else
                        {
                            clickresult = "false";
                        }
                    }

                    if (clickresult == "true")
                    {
                        string Gender = "male";
                        LoadingPanel.IsVisible = true;
                        using (var client = new HttpClient())
                        {

                            Settings.Session = GetTimestamp(DateTime.Now);
                            var formContent = new FormUrlEncodedContent(new[]
                            {
                            new KeyValuePair<string,string>("username", usernameEntry.Text),
                            new KeyValuePair<string,string>("password",passwordEntry.Text),
                            new KeyValuePair<string,string>("confirm_password",passwordConfirmEntry.Text),
                            new KeyValuePair<string,string>("email",EmailEntry.Text),
                            new KeyValuePair<string,string>("gender",Gender),
                            new KeyValuePair<string,string>("s",Settings.Session)

                        });

                            var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=user_registration", formContent);
                            response.EnsureSuccessStatusCode();
                            string json = await response.Content.ReadAsStringAsync();
                            var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                            string apiStatus = data["api_status"].ToString();
                            if (apiStatus == "200")
                            {
                                var successtype = data["success_type"].ToString();
                                using (var DataContactLoader = new ContactsFunctions()) { DataContactLoader.ClearContactTable(); }

                                if (successtype == "registered")
                                {
                                    Settings.Cookie = data["cookie"].ToString();
                                    Settings.User_id = data["user_id"].ToString();
                                    Settings.Session = data["session_id"].ToString();
                                    using (var datas = new LoginFunctions())
                                    {
                                        LoginTableDB LoginData = new LoginTableDB();
                                        LoginData.Password = passwordEntry.Text;
                                        LoginData.Username = usernameEntry.Text;
                                        LoginData.Session = Settings.Session;
                                        LoginData.UserID = Settings.User_id;
                                        LoginData.Cookie = Settings.Cookie;
                                        LoginData.Status = "Registered";
                                        datas.InsertLoginCredentials(LoginData);
                                    }
                                    LoadingPanel.IsVisible = false;
                                    App.UserisStillNew = true;
                                    App.SessionHyberd = true;
                                    try
                                    {
                                        await Navigation.PushAsync(new WalkthroughVariantPage());
                                    }
                                    catch (Exception ex)
                                    {
                                          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                        await Navigation.PushModalAsync(new WalkthroughVariantPage());
                                    }

                                }
                                else if (successtype == "verification")
                                {
                                    var Message = data["message"].ToString();
                                    LoadingPanel.IsVisible = false;
                                    await DisplayAlert(AppResources.Label_Error, Message, AppResources.Label_OK);
                                }
                            }

                            else if (apiStatus == "400")
                            {
                                JObject errors = JObject.FromObject(data["errors"]);
                                var errortext = errors["error_text"].ToString();
                                await DisplayAlert(AppResources.Label_Error, errortext, AppResources.Label_OK);
                                LoadingPanel.IsVisible = false;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }     
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        private async void LoginButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (MainPage.IsPageInNavigationStack<MainPage>(Navigation))
                {
                    await Navigation.PopAsync();
                    return;
                }

                var loginPage = new MainPage();
                try
                {
                    await Navigation.PushAsync(loginPage);
                }
                catch (Exception)
                {
                    await Navigation.PushModalAsync(loginPage);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
