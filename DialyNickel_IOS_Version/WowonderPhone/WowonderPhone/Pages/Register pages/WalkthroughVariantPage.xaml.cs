﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Register_pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WalkthroughVariantPage : CarouselPage
    {
        public static bool LoadPage1 = false;
        public static bool LoadPage2 = false;
        public static bool LoadPage3 = false;

        public WalkthroughVariantPage()
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);
               
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void GoToStep()
        {
            try
            {
                var index = Children.IndexOf(CurrentPage);
                var moveToIndex = 0;
                if (index < Children.Count - 1)
                {
                    moveToIndex = index + 1;

                    SelectedItem = Children[moveToIndex];
                }
                else
                {
                    Close();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void Close()
        {
            try
            {
                await Navigation.PopModalAsync();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                await Navigation.PopAsync(true);
            }
        }

        public void FinshTheWalkThroutPages()
        {
            try
            {
				if (Device.RuntimePlatform == Device.iOS)
				{
					var navigationPage = new NavigationPage(new TabbedPageView()) { };
					navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
					navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
					navigationPage.Title = Settings.MainPage_HeaderTextLabel;
					navigationPage.Padding = new Thickness(0, 0, 0, 0);
					Application.Current.MainPage = navigationPage;
				}
                else{
					var navigationPage = new NavigationPage(new AndroidStyle()) { };
					navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
					navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
					navigationPage.Title = Settings.MainPage_HeaderTextLabel;
					navigationPage.Padding = new Thickness(0, 0, 0, 0);
                    Application.Current.MainPage = navigationPage;
                }


            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Navigation.PopAsync(true);
            }
        }
    }
}
