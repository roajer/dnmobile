﻿using System;
using System.Threading.Tasks;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Register_pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WalkThrough_Page3 : ContentPage
    {
        public WalkThrough_Page3()
        {
            try
            {
                InitializeComponent();

                if (WalkthroughVariantPage.LoadPage3 == false)
                {
                    Functions.GetChatContacts(Settings.User_id, Settings.Session).ConfigureAwait(false);
                    WalkthroughVariantPage.LoadPage3 = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnPrimaryActionButtonClicked(object sender, EventArgs e)
        {
            try
            {
                var parent = (WalkthroughVariantPage)Parent;
                parent.GoToStep();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnCloseButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
      
        public async Task AnimateIn()
        {
            try
            {
                await Task.WhenAll(new[] {
                    AnimateItem (LabelText, 500),
                    AnimateItem (LabelText2, 600),
                    AnimateItem (GpsButton, 700)
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async Task AnimateItem(View uiElement, uint duration)
        {
            try
            {
                await Task.WhenAll(new Task[] {
                    uiElement.ScaleTo(1.5, duration, Easing.CubicIn),
                    uiElement.FadeTo(1, duration/2, Easing.CubicInOut).ContinueWith(
                        _ =>
                        {
                            // Queing on UI to workaround an issue with Forms 2.1
                            Device.BeginInvokeOnMainThread(() => {
                                uiElement.ScaleTo(1, duration, Easing.CubicOut);
                            });
                        })
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ResetAnimation()
        {
            try
            {
                //IconLabel.Opacity = 0;
                //HeaderLabel.Opacity = 0;
                //DescriptionLabel.Opacity = 0;
                //OverlapedButtonContainer.BackgroundColor = BackgroundColor;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void WalkThrough_Page1_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                AnimateIn();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void NextButtonClicked(object sender, EventArgs e)
        {
            try
            {
                using (var Data = new LoginFunctions())
                {

                    var CredentialStatus = Data.GetLoginCredentialsStatus();
                    if (CredentialStatus == "Registered")
                    {
                        var Credential = Data.GetLoginCredentials("Registered");
                        Credential.Status = "Active";
                        Data.UpdateLoginCredentials(Credential);
                        Settings.Session = Credential.Session;
                        Settings.User_id = Credential.UserID;
                        Settings.Username = Credential.Username;
                    }
                }
                var parent = (WalkthroughVariantPage)Parent;
                parent.FinshTheWalkThroutPages();
            }
            catch (Exception)
            {
                Navigation.PushModalAsync(new TabbedPageView());
            }
        }

        private void DounloudClicked(object sender, EventArgs e)
        {
           
        }

        private void GpsButton_OnClickedButtonClicked(object sender, EventArgs e)
        {
            try
            {
                DependencyService.Get<IMethods>().EnableGPSLocation();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
