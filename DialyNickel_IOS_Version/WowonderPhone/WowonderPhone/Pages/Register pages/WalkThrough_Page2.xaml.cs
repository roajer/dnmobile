﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.Pages.Register_pages
{
    public partial class WalkThrough_Page2 : ContentPage
    {
        public WalkThrough_Page2()
        {
            try
            {
                InitializeComponent();
                NavigationPage.SetHasNavigationBar(this, false);

                if (WalkthroughVariantPage.LoadPage2 == false)
                {
                    GetCommunities().ConfigureAwait(false);
                    WalkthroughVariantPage.LoadPage2 = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }



        public async Task<string> GetCommunities()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("user_profile_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_my_community", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var CO_Data = new CommunitiesFunction();
                        var Page = JObject.Parse(json).SelectToken("mypage").ToString();
                        JArray PagedataArray = JArray.Parse(Page);

                        foreach (var PageItem in PagedataArray)
                        {
                            var P_page_id = PageItem["page_id"].ToString();
                            var P_user_id = PageItem["user_id"].ToString();
                            var P_page_name = PageItem["page_name"].ToString();
                            var P_page_title = PageItem["page_title"].ToString();
                            var P_page_description = PageItem["page_description"].ToString();
                            var P_avatar = PageItem["avatar"].ToString();
                            var P_cover = PageItem["cover"].ToString();
                            var P_page_category = PageItem["page_category"].ToString();
                            var P_website = PageItem["website"].ToString();
                            var P_facebook = PageItem["facebook"].ToString();
                            var P_google = PageItem["google"].ToString();
                            var P_vk = PageItem["vk"].ToString();
                            var P_twitter = PageItem["twitter"].ToString();
                            var P_linkedin = PageItem["linkedin"].ToString();
                            var P_company = PageItem["company"].ToString();
                            var P_phone = PageItem["phone"].ToString();
                            var P_address = PageItem["address"].ToString();
                            var P_call_action_type = PageItem["call_action_type"].ToString();
                            var P_call_action_type_url = PageItem["call_action_type_url"].ToString();
                            var P_background_image = PageItem["background_image"].ToString();
                            var P_background_image_status = PageItem["background_image_status"].ToString();
                            var P_instgram = PageItem["instgram"].ToString();
                            var P_youtube = PageItem["youtube"].ToString();
                            var P_verified = PageItem["verified"].ToString();
                            var P_active = PageItem["active"].ToString();
                            var P_registered = PageItem["registered"].ToString();
                            var P_boosted = PageItem["boosted"].ToString();
                            var P_about = PageItem["about"].ToString();
                            var P_id = PageItem["id"].ToString();
                            var P_type = PageItem["type"].ToString();
                            var P_url = PageItem["url"].ToString();
                            var P_name = PageItem["name"].ToString();
                            var P_rating = PageItem["rating"].ToString();
                            var P_category = PageItem["category"].ToString();
                            var P_is_page_onwer = PageItem["is_page_onwer"].ToString();
                            var P_username = PageItem["username"].ToString();

                            var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(P_avatar, P_page_id);
                            var ImageMediaFile = ImageSource.FromFile(Imagepath);

                            if (Imagepath == "File Dont Exists")
                            {
                                ImageMediaFile = new UriImageSource { Uri = new Uri(P_avatar) };
                                DependencyService.Get<IPicture>().SavePictureToDisk(P_avatar, P_page_id);
                            }
                            var Cheke = CommunitiesPage.CommunitiesListItems.FirstOrDefault(a => a.CommunityID == P_page_id);
                            if (CommunitiesPage.CommunitiesListItems.Contains(Cheke))
                            {
                                if (Cheke.CommunityName != P_page_name)
                                {
                                    Cheke.CommunityName = P_page_name;
                                }
                                if (Cheke.ImageUrlString != P_avatar)
                                {
                                    Cheke.CommunityPicture = new UriImageSource { Uri = new Uri(P_avatar) };
                                }
                            }
                            else
                            {
                                CommunitiesPage.CommunitiesListItems.Add(new Communities()
                                {
                                    CommunityID = P_page_id,
                                    CommunityName = P_page_name,
                                    CommunityType = "Pages",
                                    CommunityPicture = ImageMediaFile,
                                    CommunityTypeLabel = P_category,
                                    CommunityTypeID = P_page_category,
                                    ImageUrlString = P_avatar
                                });
                            }
                            CO_Data.InsertCommunities(new CommunitiesDB()
                            {
                                CommunityID = P_page_id,
                                CommunityName = P_page_name,
                                CommunityPicture = P_avatar,
                                CommunityType = "Pages",
                                CommunityTypeLabel = P_category,
                                CommunityTypeID = P_page_category,
                                CommunityUrl = P_url
                            });
                        }

                        var Group = JObject.Parse(json).SelectToken("mygroups").ToString();
                        JArray GroupdataArray = JArray.Parse(Group);

                        if (GroupdataArray.Count <= 0 && PagedataArray.Count <= 0)
                        {
                            //UserDialogs.Instance.HideLoading();
                            //UserDialogs.Instance.ShowError(AppResources.Label_Community_Empty, 2000);

                            //CommunitiesPage.EmptyPage.IsVisible = true;
                            //CommunitiesPage.CommunitiesListview.IsVisible = false;
                            return "emety";
                        }
                        else
                        {
                            foreach (var GroupItem in GroupdataArray)
                            {
                                var g_id = GroupItem["id"].ToString();
                                var g_user_id = GroupItem["user_id"].ToString();
                                var g_group_name = GroupItem["group_name"].ToString();
                                var g_group_title = GroupItem["group_title"].ToString();
                                var g_avatar = GroupItem["avatar"].ToString();
                                var g_cover = GroupItem["cover"].ToString();
                                var g_about = GroupItem["about"].ToString();
                                var g_category = GroupItem["category"].ToString();
                                var g_privacy = GroupItem["privacy"].ToString();
                                var g_join_privacy = GroupItem["join_privacy"].ToString();
                                var g_active = GroupItem["active"].ToString();
                                var g_registered = GroupItem["registered"].ToString();
                                var g_group_id = GroupItem["group_id"].ToString();
                                var g_url = GroupItem["url"].ToString();
                                var g_name = GroupItem["name"].ToString();
                                var g_category_id = GroupItem["category_id"].ToString();
                                var g_type = GroupItem["type"].ToString();
                                var g_username = GroupItem["username"].ToString();

                                var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(g_avatar, g_id);
                                var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                if (Imagepath == "File Dont Exists")
                                {
                                    ImageMediaFile = new UriImageSource { Uri = new Uri(g_avatar) };
                                    DependencyService.Get<IPicture>().SavePictureToDisk(g_avatar, g_id);
                                }
                                var Cheke = CommunitiesPage.CommunitiesListItems.FirstOrDefault(a => a.CommunityID == g_id);
                                if (CommunitiesPage.CommunitiesListItems.Contains(Cheke))
                                {
                                    if (Cheke.CommunityName != g_group_name)
                                    {
                                        Cheke.CommunityName = g_group_name;
                                    }
                                    if (Cheke.ImageUrlString != g_avatar)
                                    {
                                        Cheke.CommunityPicture = new UriImageSource { Uri = new Uri(g_avatar) };
                                    }
                                }
                                else
                                {
                                    CommunitiesPage.CommunitiesListItems.Add(new Communities()
                                    {
                                        CommunityID = g_id,
                                        CommunityName = g_group_name,
                                        CommunityType = "Groups",
                                        CommunityPicture = ImageMediaFile,
                                        CommunityTypeLabel = g_category,
                                        CommunityTypeID = g_category_id,
                                        ImageUrlString = g_avatar
                                    });
                                }
                                CO_Data.InsertCommunities(new CommunitiesDB()
                                {
                                    CommunityID = g_id,
                                    CommunityName = g_group_name,
                                    CommunityPicture = g_avatar,
                                    CommunityType = "Groups",
                                    CommunityTypeLabel = g_category,
                                    CommunityTypeID = g_category_id,
                                    CommunityUrl = g_url
                                });
                            }
                        }

                        //if (CommunitiesListItems.Count > 0)
                        //{
                        //    var GetPages = new ObservableCollection<Communities>(CommunitiesListItems.Where(a => a.CommunityType == "Pages").ToList());
                        //    if (GetPages.Count > 0)
                        //    {
                        //        EmptyPage.IsVisible = false;
                        //        CommunitiesListview.IsVisible = true;

                        //        CommunitiesListview.ItemsSource = GetPages;
                        //    }
                        //    else
                        //    {
                        //        EmptyPage.IsVisible = true;
                        //        CommunitiesListview.IsVisible = false;
                        //    }
                        //}

                        //UserDialogs.Instance.HideLoading();
                    }
                    else if (apiStatus == "400")
                    {
                        json = AppResources.Label_Error;
                    }
                    return json;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                //UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
                return AppResources.Label_Error;
            }
        }


        private void NextButtonClicked(object sender, EventArgs e)
        {
            try
            {
                var parent = (WalkthroughVariantPage)Parent;
                parent.GoToStep();
               // Navigation.PushAsync(new RegisterFriends());
            }
            catch (Exception)
            {
                Navigation.PushModalAsync(new RegisterFriends());
            }
        }

        private void DounloudClicked(object sender, EventArgs e)
        {
            try
            {
                DependencyService.Get<IMethods>().OpenMessengerApp(Settings.Messenger_Package_Name);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task AnimateIn()
        {
            try
            {
                await Task.WhenAll(new[] {
                    AnimateItem (IconLabel, 500),
                    AnimateItem (HeaderLabel, 600),
                    AnimateItem (DescriptionLabel, 700)
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async Task AnimateItem(View uiElement, uint duration)
        {
            try
            {
                if (uiElement == null)
                {
                    return;
                }

                await Task.WhenAll(new Task[] {
                    uiElement.ScaleTo(1.5, duration, Easing.CubicIn),
                    uiElement.FadeTo(1, duration/2, Easing.CubicInOut).ContinueWith(
                        _ =>
                        { 
                            // Queing on UI to workaround an issue with Forms 2.1
                            Device.BeginInvokeOnMainThread(() => {
                                uiElement.ScaleTo(1, duration, Easing.CubicOut);
                            });
                        })
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ResetAnimation()
        {
            //LabelText.Opacity = 0;
            //Dounloud.Opacity = 0;
            //NextButton.Opacity = 0;
        }

        private void WalkThrough_Page2_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                //ResetAnimation();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnPrimaryActionButtonClicked(object sender, EventArgs e)
        {
            try
            {
                var parent = (WalkthroughVariantPage)Parent;
                parent.GoToStep();
                // Navigation.PushAsync(new RegisterFriends());
            }
            catch (Exception)
            {
                Navigation.PushModalAsync(new RegisterFriends());
            }
        }

        private void WalkThrough_Page2_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                AnimateIn();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
