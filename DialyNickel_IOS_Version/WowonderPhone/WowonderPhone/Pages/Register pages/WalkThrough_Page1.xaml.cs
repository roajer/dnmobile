﻿using System;
using System.Threading.Tasks;
using WowonderPhone.Controls;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Register_pages
{
    public partial class WalkThrough_Page1 : ContentPage
    {
        public WalkThrough_Page1()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
           
           // Webviewcooki.Source = WowonderPhone.Settings.Website + "/app_api.php?application=phone&type=set_c&c=" + WowonderPhone.Settings.Cookie;
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    
                }
                else
                {
                    LoadContacts();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        public async void LoadContacts()
        {
            try
            {
                if (WalkthroughVariantPage.LoadPage1 == false)
                {
                    LoginUserFunctions.GetSessionProfileData(Settings.User_id, Settings.Session).ConfigureAwait(false);

                    
                    WalkthroughVariantPage.LoadPage1 = true;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnPrimaryActionButtonClicked(object sender, EventArgs e)
        {
            try
            {
                var parent = (WalkthroughVariantPage)Parent;
                parent.GoToStep();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void OnCloseButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

       
        protected override void OnDisappearing()
        {
            base.OnDisappearing();

           // ResetAnimation();
        }

        public async Task AnimateIn()
        {
            try
            {
                await Task.WhenAll(new[] {
                    AnimateItem (IconLabel, 500),
                    AnimateItem (HeaderLabel, 600),
                    AnimateItem (DescriptionLabel, 700)
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async Task AnimateItem(View uiElement, uint duration)
        {
            try
            {
                await Task.WhenAll(new Task[] {
                    uiElement.ScaleTo(1.5, duration, Easing.CubicIn),
                    uiElement.FadeTo(1, duration/2, Easing.CubicInOut).ContinueWith(
                        _ =>
                        {
                            // Queing on UI to workaround an issue with Forms 2.1
                            Device.BeginInvokeOnMainThread(() => {
                                uiElement.ScaleTo(1, duration, Easing.CubicOut);
                            });
                        })
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ResetAnimation()
        {
            try
            {
                IconLabel.Opacity = 0;
                HeaderLabel.Opacity = 0;
                DescriptionLabel.Opacity = 0;
                OverlapedButtonContainer.BackgroundColor = BackgroundColor;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void NextButtonClicked(object sender, EventArgs e)
        {
            
        }

        private void WalkThrough_Page1_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                AnimateIn();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void WalkThrough_Page1_OnDisappearing(object sender, EventArgs e)
        {
            try
            {
                // Webviewcooki.Source = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
