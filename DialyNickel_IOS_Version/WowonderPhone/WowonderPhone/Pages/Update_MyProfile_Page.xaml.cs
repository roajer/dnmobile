﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WowonderPhone.Controls;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Timeline_Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Update_MyProfile_Page : ContentPage
	{
	    #region Variables

	    public static string Firstname_E = "";
	    public static string LastName_E = "";
	    public static string AboutmeEntry_E = "";
	    public static string LocationEntery_E = "";
	    public static string Username_E = "";
	    public static string Facebook_E = "";
	    public static string Google_E = "";
	    public static string Linkedin_E = "";
	    public static string VK_E = "";
	    public static string Instagram_E = "";
	    public static string Twitter_E = "";
	    public static string Youtube_E = "";
	    public static string Following_Number_E = "";
	    public static string Followers_Number_E = "";

	    private LoginUserProfileTableDB _DataRow;

        #endregion

        public Update_MyProfile_Page (LoginUserProfileTableDB DataRow)
		{
		    try
		    {
		        InitializeComponent();

		        _DataRow = DataRow;

		        Firstname.Text = DataRow.first_name;
		        LastName.Text = DataRow.last_name;
		        AboutmeEntry.Text = Functions.DecodeString(DataRow.about);
		        LocationEntery.Text = DataRow.address;
                FacebookEntery.Text = DataRow.facebook;
		        GoogleEntery.Text = DataRow.google;
		        LinkedinEntery.Text = DataRow.linkedin;
		        VkEntery.Text = DataRow.vk;
		        InstagramEntery.Text = DataRow.instagram;
		        TwitterEntery.Text = DataRow.twitter;
		        YoutubeEntery.Text = DataRow.youtube;
		        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		        Firstname_E = DataRow.first_name;
		        LastName_E = DataRow.last_name;
		        AboutmeEntry_E = Functions.DecodeString(DataRow.about);
		        LocationEntery_E = DataRow.address;
		        Username_E = DataRow.name;
		        Facebook_E = DataRow.facebook;
		        Google_E = DataRow.google;
		        Linkedin_E = DataRow.linkedin;
		        VK_E = DataRow.vk;
		        Instagram_E = DataRow.instagram;
		        Twitter_E = DataRow.twitter;
		        Youtube_E = DataRow.youtube;
            }
		    catch (Exception ex)
		    {
		          System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
		    }
        }

        private void Btn_Updatedata_OnClicked(object sender, EventArgs e)
        {
            try
            {
                if (Firstname.Text == null)
                {
                    Firstname.Text = "";
                }
                if (LastName.Text == null)
                {
                    LastName.Text = "";
                }
                if (AboutmeEntry.Text == null)
                {
                    AboutmeEntry.Text = "";
                }
                if (LocationEntery.Text == null)
                {
                    LocationEntery.Text = "";
                }

                if (Firstname.Text != Firstname_E || LastName.Text != LastName_E || AboutmeEntry.Text != AboutmeEntry_E
                    || FacebookEntery.Text != Facebook_E || VkEntery.Text != VK_E || GoogleEntery.Text != Google_E ||
                    InstagramEntery.Text != Instagram_E || YoutubeEntery.Text != Youtube_E
                    || TwitterEntery.Text != Twitter_E || LinkedinEntery.Text != Linkedin_E
                )
                {
                    var dictionary = new Dictionary<string, string>
                    {
                        {"first_name", Firstname.Text},
                        {"last_name", LastName.Text},
                        {"about", AboutmeEntry.Text},
                        {"facebook", FacebookEntery.Text},
                        {"google", GoogleEntery.Text},
                        {"linkedin", LinkedinEntery.Text},
                        {"vk", VkEntery.Text},
                        {"instagram", InstagramEntery.Text},
                        {"twitter", TwitterEntery.Text},
                        {"youtube", YoutubeEntery.Text},
                    };

                    UpdateSettings(JsonConvert.SerializeObject(dictionary)).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        private async Task<string> UpdateSettings(string Data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("type", "profile_settings"),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("user_data", Data)
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=update_user_data", formContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var DataLoader = new LoginUserProfileFunctions();
                        var DataRow = DataLoader.GetProfileCredentialsById(Settings.User_id);
                        DataRow.about = AboutmeEntry.Text;
                        DataRow.last_name = LastName.Text;
                        DataRow.first_name = Firstname.Text;
                        DataRow.facebook = FacebookEntery.Text;
                        DataRow.google = GoogleEntery.Text;
                        DataRow.linkedin = LinkedinEntery.Text;
                        DataRow.instagram = InstagramEntery.Text;
                        DataRow.vk = VkEntery.Text;
                        DataRow.youtube = YoutubeEntery.Text;
                        DataLoader.UpdateProfileCredentials(DataRow);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }
    }
}