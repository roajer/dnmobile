﻿using System;
using System.Linq;
using System.Threading.Tasks;

using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Tabs;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Exception = System.Exception;

namespace WowonderPhone.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPageView : TabbedPage
    {
        #region Variables

        TimelinePostsTab Timeline;

        #endregion

        public TabbedPageView()
        {
            try
			{
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                Timeline = new TimelinePostsTab { Icon = "Feedly.png", Title = "" };
                var Notifications = new NotificationsPage { Icon = "bell.png", Title = "" };
                var ItemListPage = new ItemListPage { Icon = "NewsFeed.png", Title = "" };
                //var Story = new Status_Page { Icon = "Stories.png", Title = "" };
			    var Trending = new TrendingPage { Icon = "bolt.png", Title = "" };

                Children.Add(Timeline);
                Children.Add(Notifications);
				Children.Add(Trending);
                Children.Add(ItemListPage);

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Settings_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new SettingsPage());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Searchcontact_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new Search_Page());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddPost_OnClicked_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var dd = this.Children.Last();
                Navigation.PushAsync(new AddPost(Timeline));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        protected  override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            try
            {
                Task<bool> Qussion = DisplayAlert(AppResources.Label_Question,
                    AppResources.Label2_Are_you_close_the_app, AppResources.Label_Yes, AppResources.Label_NO);
                Qussion.ContinueWith(task =>
                {
                    if (task.Result)
                    {
                        DependencyService.Get<IMethods>().Close_App();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                });
                return true;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return true;
            }
        }

        private async void TabbedPageView_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                //await Task.Run(async () =>
                //{
                //    await Task.Delay(6000);
                //    if (App.UserisStillNew)
                //    {
                //        Device.BeginInvokeOnMainThread(() =>
                //        {
                //            Navigation.PushModalAsync(new UploudPicPage());
                //        });
                //        App.UserisStillNew = false;
                //    }
                //});
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
