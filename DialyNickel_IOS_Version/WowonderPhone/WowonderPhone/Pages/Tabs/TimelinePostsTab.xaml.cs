﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Newtonsoft.Json;
using PropertyChanged;
using Refractored.XamForms.PullToRefresh;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.CustomCells;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.Pages.Timeline_Pages.Java_Inject_Pages;
using WowonderPhone.SQLite;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using Exception = System.Exception;

namespace WowonderPhone.Pages.Tabs
{
    public partial class TimelinePostsTab : ContentPage
    {
        #region classes

        public class TestViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<string> Items { get; set; }
            TimelinePostsTab pageChosen;
            public TestViewModel(TimelinePostsTab page)
            {

                this.pageChosen = page;
                Items = new ObservableCollection<string>();
            }

            bool isBusy;

            public bool IsBusy
            {
                get { return isBusy; }
                set
                {
                    if (isBusy == value)
                        return;

                    isBusy = value;
                    OnPropertyChanged("IsBusy");
                }
            }

            ICommand refreshCommand;

            public ICommand RefreshCommand
            {
                get { return refreshCommand ?? (refreshCommand = new Command(async () => await ExecuteRefreshCommand())); }
            }

            async Task ExecuteRefreshCommand()
            {
                if (IsBusy)
                    return;

                IsBusy = true;


                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx)
                {
                    // var service = DependencyService.Get<IMethods>();
                    // service.ClearWebViewCache();
                    pageChosen.PostWebLoader.IsVisible = false;
                    pageChosen.OfflinePage.IsVisible = true;

                }
                else
                {
                    pageChosen.OfflinePage.IsVisible = false;
                    pageChosen.PostWebLoader.IsVisible = true;
                    pageChosen.PostWebLoader.Source = WowonderPhone.Settings.Website + "/get_news_feed";
                    IsBusy = false;
                }
            }

            #region INotifyPropertyChanged implementation

            public event PropertyChangedEventHandler PropertyChanged;

            #endregion

            public void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }

        }

        [ImplementPropertyChanged]
        public class TimelineItems
        {
            public string BackgroundColor { get; set; }
            public string Icon { get; set; }
            public string Label { get; set; }
        }


        #endregion

        #region Lists Items Declaration

        public static ObservableCollection<ItemListPage.TopUsersItems> TopUsersCollection = new ObservableCollection<ItemListPage.TopUsersItems>();
        public static ObservableCollection<TimelineItems> ListItemsCollection = new ObservableCollection<TimelineItems>();
        #endregion

        public TimelinePostsTab()
        {
            try
            {
                InitializeComponent();

                BindingContext = new TestViewModel(this);

                if (Settings.Cookie != null)
                {
                    var device = Resolver.Resolve<IDevice>();
                    var oNetwork = device.Network;
                    var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                    if (!xx)
                    {
                        //OfflinePost.IsVisible = true;
                        PostWebLoader.IsVisible = true;
                        var service = DependencyService.Get<IMethods>();
                        service.ClearWebViewCache();

                        PullToRefreshLayoutView.SetBinding<TestViewModel>(PullToRefreshLayout.IsRefreshingProperty, vm => vm.IsBusy, BindingMode.OneWay);
                        PullToRefreshLayoutView.SetBinding<TestViewModel>(PullToRefreshLayout.RefreshCommandProperty, vm => vm.RefreshCommand);

                        if (App.SessionHyberd)
                        {
                            PostWebLoader.Source = Settings.Website + "/app_api.php?application=phone&type=set_c&c=" + Settings.Cookie;
                        }
                        else
                        {
                            PostWebLoader.Source = Settings.Website + "/get_news_feed";
                        }

                        Device.StartTimer(TimeSpan.FromSeconds(Settings.LoadMore_Post_Secounds), () =>
                        {
                            LoadMoreFunction();
                            return true;
                        });
                    }
                    else
                    {
                        PostWebLoader.IsVisible = false;

                        OfflinePage.IsVisible = true;
                        GridHieght.RowDefinitions.Clear();
                        RowDefinition ff = new RowDefinition();
                        ff.Height = 600;
                        GridHieght.RowDefinitions.Add(ff);
                    }
                }
                AddItemsToList();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void LoadMoreFunction()
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewPosts();");
                    });
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void EditCommentDone(string ID, string Text)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    var JavaCode = "$('#post-' + " + ID + ").find('#edit-post').attr('onclick', '{*type*:*edit_post*,*post_id*:*" + ID + "*,*edit_text*:*" + Text + "*}');";
                    //var JavaCode = "$('#post-' + " + ID + ").find('#edit-post').attr('onclick', 'rr');";
                    var service = DependencyService.Get<IMethods>();
                    service.ClearWebViewCache();
                    var Decode = JavaCode.Replace("*", "&quot;");
                    PostWebLoader.InjectJavascript(Decode);

                    PostWebLoader.InjectJavascript("$('#post-' + " + ID + ").find('.post-description p').html('" + Text + "');");
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void PostAjaxRefresh(string type)
        {
            try
            {
                if (type == "Hashtag")
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewHashTagPosts();");
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PostWebLoader.InjectJavascript("Wo_GetNewPosts();");
                    });
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_UserProfile(string Userid)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserProfilePage(Userid, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_OpenImage(ImageSource Image)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushModalAsync(new ImageFullScreenPage(Image));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_PostLinks(string link)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Device.OpenUri(new Uri(link));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Hashtag(string word)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", word));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_LikePage(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialPageViewer(id, "", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Group(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialGroup(id, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddPost_OnClicked_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new AddPost(this));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TimelinePostsTab_OnAppearing(object sender, EventArgs e)
        {

        }

        public async Task Post_Manager(string Type, string postid)
        {
            try
            {
                var Action = " ";
                if (Type == "edit_post")
                {
                    Action = "edit";
                }
                else
                {
                    Action = "delete";
                }
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", WowonderPhone.Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s",WowonderPhone.Settings.Session),
                        new KeyValuePair<string, string>("action",Action),

                    });

                    var response = await client.PostAsync(WowonderPhone.Settings.Website + "/app_api.php?application=phone&type=post_manager", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (Type == "edit_post")
                        {
                            Action = "edit";
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnContentLoaded(ContentLoadedDelegate eventobj)
        {
            try
            {
                PostWebLoader.IsVisible = true;
                PullToRefreshLayoutView.IsRefreshing = false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);

                PostWebLoader.IsVisible = true;
                PullToRefreshLayoutView.IsRefreshing = false;
             
            }
        }

        private NavigationRequestedDelegate PostWebLoader_OnOnNavigationStarted(NavigationRequestedDelegate eventobj)
        {
            try
            {
                if (eventobj.Uri == Settings.Website + "get_news_feed" || eventobj.Uri == Settings.Website + "/get_news_feed" || eventobj.Uri.Contains(Settings.Cookie))
                {
                    PullToRefreshLayoutView.IsRefreshing = true;
                    return eventobj;
                }
                else
                {
                    if (eventobj.Uri == Settings.Website + "welcome" || eventobj.Uri == Settings.Website + "/welcome")
                    {
                        PostWebLoader.Source = Settings.Website + "/app_api.php?application=phone&type=set_c&c=" + Settings.Cookie;
                        eventobj.Uri = Settings.Website + "/app_api.php?application=phone&type=set_c&c=" + Settings.Cookie;

                        DisplayAlert(AppResources.Label2_Session_Timeout, AppResources.Label2_The_Application_needs_Restart, AppResources.Label_OK);

                        using (var Data = new LoginFunctions())
                        {
                            Data.ClearLoginCredentialsList();
                        }
                        using (var Data = new LoginUserProfileFunctions())
                        {
                            Data.ClearProfileCredentialsList();
                        }
                        using (var Data = new MessagesFunctions())
                        {
                            Data.ClearMessageList();
                        }
                        using (var Data = new ContactsFunctions())
                        {
                            Data.DeletAllChatUsersList();
                        }
                        using (var Data = new CommunitiesFunction())
                        {
                            Data.DeletAllCommunityList();
                        }
                        using (var Data = new NotifiFunctions())
                        {
                            Data.ClearNotifiDBCredentialsList();
                        }
                        using (var Data = new PrivacyFunctions())
                        {
                            Data.ClearPrivacyDBCredentialsList();
                        }
                        using (var Data = new ChatActivityFunctions())
                        {

                            Data.ClearChatUserTable();
                            Data.DeletAllChatUsersList();

                            try
                            {
                                UserDialogs.Instance.HideLoading();
                                Settings.ReLogin = true;
                                DependencyService.Get<IMethods>().Close_App();
                                Navigation.PopAsync();
                                App.GetLoginPage();
                            }
                            catch
                            {
                                UserDialogs.Instance.HideLoading();
                                Navigation.RemovePage(this);
                                App.GetLoginPage();
                                DependencyService.Get<IMethods>().Close_App();
                            }
                        }
                        eventobj.Cancel = true;
                        return eventobj;
                    }
                    eventobj.Cancel = true;
                    return eventobj;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                eventobj.Cancel = true;
                return eventobj;
            }
        }

        private async void PostWebLoader_OnOnJavascriptResponse(JavascriptResponseDelegate eventobj)
        {
            try
            {
                if (eventobj.Data.Contains("type"))
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(eventobj.Data);
                    string type = data["type"].ToString();

                    if (type == "scorll")
                    {
                        try
                        {
                            string value = data["num"].ToString();
                            if (value == "1")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 1;
                                //    PullToRefreshLayoutView.TranslationY = 0;
                                //    StatusListView.IsVisible = true;
                                //    PullToRefreshLayoutView.IsVisible = true;

                                //    RowDefinition ff = new RowDefinition();
                                //    ff.Height = 85;
                                //    GridHieght.RowDefinitions[0].Height = 85;
                                //});
                            }
                            else if (value == "2")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 0.6;
                                //    // PullToRefreshLayoutView.HeightRequest = 65;
                                //    PullToRefreshLayoutView.TranslationY = -10;
                                //});
                            }
                            else if (value == "3")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 0.5;
                                //    // StatusListView.HeightRequest = 35;
                                //    PullToRefreshLayoutView.TranslationY = -15;

                                //    //  GridHieght.RowDefinitions[0].Height = 48;
                                //});
                            }
                            else if (value == "4")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 0.2;
                                //    PullToRefreshLayoutView.TranslationY = -20;

                                //    //GridHieght.RowDefinitions[0].Height = 22;
                                //});
                            }
                            else if (value == "5")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 0;
                                //    //PullToRefreshLayoutView.HeightRequest = 0;
                                //    PullToRefreshLayoutView.TranslationY = -30;
                                //    //GridHieght.RowDefinitions.Clear();

                                //    GridHieght.RowDefinitions[0].Height = 1;
                                //    StatusListView.IsVisible = false;
                                //    PullToRefreshLayoutView.IsVisible = false;
                                //});
                            }
                            else if (value == "6")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 0;
                                //    //PullToRefreshLayoutView.HeightRequest = 0;
                                //    PullToRefreshLayoutView.TranslationY = -30;
                                //    //GridHieght.RowDefinitions.Clear();

                                //    GridHieght.RowDefinitions[0].Height = 1;
                                //    StatusListView.IsVisible = false;
                                //    PullToRefreshLayoutView.IsVisible = false;
                                //});
                            }
                            else if (value == "7")
                            {
                                // Device.BeginInvokeOnMainThread(() =>
                                // {
                                //     StatusListView.Opacity = 0;
                                //     //PullToRefreshLayoutView.HeightRequest = 0;
                                //     PullToRefreshLayoutView.TranslationY = -30;
                                //     //GridHieght.RowDefinitions.Clear();

                                //     GridHieght.RowDefinitions[0].Height = 1;
                                //     StatusListView.IsVisible = false;
                                //     PullToRefreshLayoutView.IsVisible = false;
                                //});
                            }
                            else if (value == "8")
                            {
                                //Device.BeginInvokeOnMainThread(() =>
                                //{
                                //    StatusListView.Opacity = 0;
                                //    //PullToRefreshLayoutView.HeightRequest = 0;
                                //    PullToRefreshLayoutView.TranslationY = -30;
                                //    //GridHieght.RowDefinitions.Clear();

                                //    GridHieght.RowDefinitions[0].Height = 1;
                                //    StatusListView.IsVisible = false;
                                //    PullToRefreshLayoutView.IsVisible = false;
                                //});
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                    if (type == "user")
                    {
                        string Userid = data["profile_id"].ToString();
                        if (Settings.User_id == Userid)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new MyProfilePage());
                            });
                        }
                        else
                        {
                            InjectedJavaOpen_UserProfile(Userid);
                        }
                    }
                    else if (type == "lightbox")
                    {
                        string ImageSource = data["image_url"].ToString();

                        if (Settings.ShowAndroidDefaultImageViewer)
                        {
                            if (Device.RuntimePlatform == Device.iOS)
                            {
                                var Image = new UriImageSource
                                {
                                    Uri = new Uri(ImageSource),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(2, 0, 0, 0)
                                };
                                InjectedJavaOpen_OpenImage(Image);
                            }
                            else
                            {
                                UserDialogs.Instance.ShowLoading(AppResources.Label_Please_Wait);
                                var dataViewer = DependencyService.Get<IMethods>();
                                dataViewer.showPhoto(ImageSource);
                            }
                        }
                        else
                        {
                            var Image = new UriImageSource
                            {
                                Uri = new Uri(ImageSource),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };
                            InjectedJavaOpen_OpenImage(Image);
                        }
                    }
                    else if (type == "mention")
                    {
                        string user_id = data["user_id"].ToString();
                        InjectedJavaOpen_UserProfile(user_id);
                    }
                    else if (type == "hashtag")
                    {
                        string hashtag = data["tag"].ToString();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushModalAsync(new HyberdPostViewer("Hashtag", hashtag));
                        });
                    }
                    else if (type == "url")
                    {
                        string link = data["link"].ToString();

                        InjectedJavaOpen_PostLinks(link);
                    }
                    else if (type == "page")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_LikePage(Id);
                    }
                    else if (type == "group")
                    {
                        string Ids = data["profile_id"].ToString();

                        InjectedJavaOpen_Group(Ids);
                    }
                    else if (type == "post_wonders" || type == "post_likes")
                    {
                        string Id = data["post_id"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Like_Wonder_Viewer_Page(Id, type));
                        });
                    }
                    else if (type == "edit_post")
                    {
                        string Id = data["post_id"].ToString();
                        string Edit_text = data["edit_text"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Edit_Post_For_Hyberd_Page(Edit_text, Id, null, this));
                        });
                    }
                    else if (type == "delete_post")
                    {
                        string Id = data["post_id"].ToString();

                        var Qussion = await DisplayAlert(AppResources.Label_Question, AppResources.Label_Would_You_like_to_delete_this_post, AppResources.Label_Yes, AppResources.Label_NO);
                        if (Qussion)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                PostWebLoader.InjectJavascript("$('#post-' + " + Id + ").slideUp(200, function () { $(this).remove();}); ");
                            });
                            Post_Manager("delete_post", Id).ConfigureAwait(false);
                        }
                    }
                    else if (type == "publisher-box")
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new AddPost(this));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnNavigationError(NavigationErrorDelegate eventobj)
        {
            try
            {
                if (Settings.HandelPostViewerErrors && eventobj.ErrorCode != 416)
                {
                    if (eventobj.ErrorCode == 503)
                    {
                        if (Settings.TurnHost503ErrorOn)
                        {
                            LabelOfflineError.Text = AppResources.Label2_ErrorCode503;
                        }
                       
                    }
                    else if (eventobj.ErrorCode == 504)
                    {
                        if (Settings.TurnHost504ErrorOn)
                        {
                            LabelOfflineError.Text = AppResources.Label2_ErrorCode504;
                        }
                       
                    }
                    else
                    {
                        LabelOfflineError.Text = AppResources.Label2_Error_Internet;
                    }
                    OfflinePage.IsVisible = true;
                    PostWebLoader.IsVisible = false;
                    PostWebLoader.Source = Settings.Website + "/get_news_feed";
                }
                else
                {
                    PostWebLoader.Source = Settings.Website + "/get_news_feed";
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TryButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    OfflinePage.IsVisible = false;
                    PostWebLoader.IsVisible = false;
                    PostWebLoader.Source = Settings.Website + "/get_news_feed";
                }
                else
                {
                    OfflinePage.IsVisible = true;
                    PostWebLoader.IsVisible = false;
                    UserDialogs.Instance.Toast(AppResources.Label_You_are_still_Offline);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Add Item Page 
        public void AddItemsToList()
        {
            try
            {
                TimelineStack.Children.Clear();
                ListItemsCollection.Clear();

                if (ListItemsCollection.Count == 0)
                {
                    ListItemsCollection.Add(new TimelineItems()
                    {
                        Label = AppResources.Label_My_Profile,
                        Icon = Ionic_Font.SocialReddit,
                        BackgroundColor = Settings.MainColor,
                    });

                    ListItemsCollection.Add(new TimelineItems()
                    {
                        Label = AppResources.Label3_Story,
                        Icon = Ionic_Font.LoadA,
                        BackgroundColor = "#292b2c",
                    });

                    if (Settings.ConnectivitySystem == "1")
                    {
                        ListItemsCollection.Add(new TimelineItems()
                        {
                            Label = AppResources.Label_Following,
                            Icon = Ionic_Font.PersonStalker,
                            BackgroundColor = "#0084ff",
                        });
                    }
                    else
                    {
                        ListItemsCollection.Add(new TimelineItems()
                        {
                            Label = AppResources.Label_Friends,
                            Icon = Ionic_Font.PersonStalker,
                            BackgroundColor = "#0084ff",
                        });
                    }

                    ListItemsCollection.Add(new TimelineItems()
                    {
                        Label = AppResources.Label_Pages,
                        Icon = Ionic_Font.Thumbsup,
                        BackgroundColor = "#5cb85c",
                    });

                    ListItemsCollection.Add(new TimelineItems()
                    {
                        Label = AppResources.Label_Groups,
                        Icon = Ionic_Font.IosPeople,
                        BackgroundColor = "#ffc300",
                    });

                    ListItemsCollection.Add(new TimelineItems()
                    {
                        Label = AppResources.Label_Search,
                        Icon = Ionic_Font.Search,
                        BackgroundColor = "#391c8c",
                    });
                }
                Add_Timeline_Items(ListItemsCollection);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void Add_Timeline_Items(ObservableCollection<TimelineItems> TimelineList)
        {
            try
            {
                for (var i = 0; i < TimelineList.Count; i++)
                {
                    var TapGestureRecognizer = new TapGestureRecognizer();
                    TapGestureRecognizer.Tapped += OnTimelineTapped;
                    var item = new Items_Timeline_Templet();
                    item.BindingContext = TimelineList[i];
                    item.GestureRecognizers.Add(TapGestureRecognizer);
                    TimelineStack.Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in Items_Timeline_Templet >> open  
        private async void OnTimelineTapped(object sender, EventArgs e)
        {
            try
            {
                var Item = (TimelineItems)((Items_Timeline_Templet)sender).BindingContext;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_My_Profile)
                    {
                        await Navigation.PushAsync(new MyProfilePage());
                    }
                    else if (Item.Label == AppResources.Label3_Story)
                    {
                        await Navigation.PushAsync(new Status_Page());
                    }
                    else if (Item.Label == AppResources.Label_Friends || Item.Label == AppResources.Label_Following)
                    {
                        await Navigation.PushAsync(new ContactsTab());
                    }
                    else if (Item.Label == AppResources.Label_Pages)
                    {
                        await Navigation.PushAsync(new CommunitiesPage());
                    }
                    else if (Item.Label == AppResources.Label_Groups)
                    {
                        await Navigation.PushAsync(new GroupsListPage());
                    }
                    else if (Item.Label == AppResources.Label_Search)
                    {
                        await Navigation.PushAsync(new Search_Page());
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
