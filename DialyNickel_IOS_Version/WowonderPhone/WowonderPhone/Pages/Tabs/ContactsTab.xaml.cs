﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using PropertyChanged;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.Friend_System;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Tabs
{

    #region Classes

    [ImplementPropertyChanged]
    public class DetailsViewModel
    {
        public static bool LoadingImage = true;
    }

    #endregion

    public partial class ContactsTab : ContentPage
    {
        public ContactsTab()
        {
            try
            {
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
					HeaderOfpage.IsVisible = false;
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                BindingContext = new DetailsViewModel();


                ContactsLoader();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ContactListview_OnRefreshing(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    ContactListview.ItemsSource = null;
                    ContactListview.ItemsSource = Functions.ChatContactsList;
                }
                ContactListview.EndRefresh();
            }
            catch (Exception ex)
            {
                ContactListview.EndRefresh();
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void ContactsLoader()
        {
            try
            {
                UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
              
                    using (var data = new ContactsFunctions())
                    {
                        if (Functions.ChatContactsList.Count > 0)
                        {
                           ContactListview.ItemsSource = Functions.ChatContactsList;
                           UserDialogs.Instance.HideLoading();
                           return;
                        }
                        var GetContactList = data.GetContactCacheList();

                        if (GetContactList.Count > 0)
                        {
                            UserDialogs.Instance.HideLoading();
                            ContactListview.ItemsSource = GetContactList;
                        }
                        else
                        {
                            if (xx == false)
                            {
                            ContactListview.ItemsSource = null;
                            await Functions.GetChatContacts(Settings.User_id, Settings.Session);
                            if (Functions.ChatContactsList.Count > 0)
                            {

                                UserDialogs.Instance.HideLoading();
                                ContactListview.ItemsSource = Functions.ChatContactsList;
                                DetailsViewModel.LoadingImage = false;
                            }
                            else
                            {
                                UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection);
                            }
                        } 
                    }
                }
             
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void OnDelete(object sender, EventArgs e)
        {
            try
            {
                var mi = ((MenuItem) sender);
                var answer = await DisplayAlert(
                    AppResources.Label_Delete + mi.CommandParameter + AppResources.Label2_from_my_contact_list, null,
                    AppResources.Label_Yes, AppResources.Label_NO);
                if (answer)
                {
                    #region Delete from list

                    var getuser = Functions.ChatContactsList.Where(a => a.Name == mi.CommandParameter.ToString())
                        .ToList()
                        .FirstOrDefault();
                    if (getuser != null)
                    {
                        if (getuser.Username == mi.CommandParameter.ToString())
                        {
                            Functions.ChatContactsList.Remove(getuser);
                        }
                    }

                    #endregion

                    #region Delete from SqlTable (Cashe)

                    using (var data = new ContactsFunctions())
                    {
                        var contact = data.GetContactUserByUsername(mi.CommandParameter.ToString());
                        if (contact != null)
                        {
                            data.DeleteContactRow(contact);
                        }

                        #region Delete from website

                        try
                        {
                            using (var client = new HttpClient())
                            {
                                var formContent = new FormUrlEncodedContent(new[]
                                {
                                    new KeyValuePair<string, string>("user_id", Settings.User_id),
                                    new KeyValuePair<string, string>("recipient_id", contact.UserID),
                                    new KeyValuePair<string, string>("s", Settings.Session)
                                });

                                var response = await client
                                    .PostAsync(Settings.Website + "/app_api.php?application=phone&type=follow_user",
                                        formContent)
                                    .ConfigureAwait(false);
                                response.EnsureSuccessStatusCode();
                                string json = await response.Content.ReadAsStringAsync();
                                var data2 = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                                string apiStatus = data2["api_status"].ToString();
                                if (apiStatus == "200")
                                {

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in Contact List >> open UserProfilePage
        private void ContactListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                //ContactListview.SelectedItem = null;
                var user = e.Item as UserContacts;
                if (user != null)
                {
                    Navigation.PushAsync(new UserProfilePage(user.UserID, "FriendList"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ContactListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                ContactListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Refresh
        private async void Refresh_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network;
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == false)
                {
                    UserDialogs.Instance.ShowLoading(AppResources.Label_Loading, MaskType.None);
                    await Functions.GetChatContacts(Settings.User_id, Settings.Session);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        ContactListview.ItemsSource = Functions.ChatContactsList;
                    });
                }
                else
                {
                    UserDialogs.Instance.ShowError(AppResources.Label_CheckYourInternetConnection, 2000);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                UserDialogs.Instance.ShowError(AppResources.Label_Connection_Lost, 2000);
            }
        }

        //Event click back Icon
        private void OnbackIconTapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PopAsync(true);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
   
}
