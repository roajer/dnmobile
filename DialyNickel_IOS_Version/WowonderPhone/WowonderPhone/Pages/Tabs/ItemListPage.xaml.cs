﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BottomBar.XamarinForms;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Movies;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Articles;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Events;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Near_by;
using WowonderPhone.Pages.Timeline_Pages.SettingsNavPages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Tabs
{

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemListPage : ContentPage
    {

        #region Classes

        [ImplementPropertyChanged]
        public class SliderItems
        {
            public int ID { get; set; }
            public string Label { get; set; }
            public string Heightrow { get; set; }
            public string Icon { get; set; }

            public string BadgeNumber { get; set; }
            public string BackgroundColor { get; set; }
            public string BadgeIsVisible { get; set; }
        }

        [ImplementPropertyChanged]
        public class TopUsersItems
        {
            public string Id { get; set; }
            public string IsVip { get; set; }
            public string User_id { get; set; }
            public string Username { get; set; }
            public string Name { get; set; }
            public ImageSource Image { get; set; }
        }

        [ImplementPropertyChanged]
        public class PagesItems
        {
            public string Page_id { get; set; }
            public string Page_name { get; set; }
            public string Username { get; set; }
            public string Category { get; set; }
            public ImageSource Avatar { get; set; }
            public string BadgeNumber { get; set; }
            public string Badgecolor { get; set; }
            public string BadgeIsVisible { get; set; }
        }

        [ImplementPropertyChanged]
        public class TrendingItems
        {
            public string Label { get; set; }
            public string ID { get; set; }
        }


        #endregion

        #region Variables

        public static string NotifiStoper = "0";
        public static int ProMemberStoper = 3;
        public static int _CountNotification = 0;
        public bool TimerBool = true;
        public static bool PromotedVisible = false;

        #endregion

        #region Lists Items Declaration 

        public static ObservableCollection<Notifications> NotificationsList = new ObservableCollection<Notifications>();
        public static ObservableCollection<SliderItems> ListItemsCollection = new ObservableCollection<SliderItems>();
        public static ObservableCollection<SliderItems> SettingsListItemsCollection = new ObservableCollection<SliderItems>();
        public static ObservableCollection<TopUsersItems> TopUsersCollection = new ObservableCollection<TopUsersItems>();
        public static ObservableCollection<PagesItems> PagesItemsCollection = new ObservableCollection<PagesItems>();
        public static ObservableCollection<TrendingItems> TrendingItemsCollection = new ObservableCollection<TrendingItems>();
        public static ObservableCollection<FriendRequests> FriendRequestsItemsCollection = new ObservableCollection<FriendRequests>();

        #endregion

        public ItemListPage()
        {
            try
            {
                InitializeComponent();
				if (Device.RuntimePlatform == Device.iOS)
				{
					NavigationPage.SetHasNavigationBar(this, true);
				}
				else
				{
					NavigationPage.SetHasNavigationBar(this, false);
				}
                GetMyprofile();
                AddItemsToList();

                if (Settings.Show_ADMOB_On_ItemList)
                {
                    AdMobView.IsVisible = true;
                }
                else
                {
                    AdMobView.IsVisible = false;
                }

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Get_notifications().ConfigureAwait(false);
                    Device.StartTimer(TimeSpan.FromSeconds(Settings.Notifications_Push_Secounds), () =>
                    {
                        NotifiTimerFunction();
                        return TimerBool;
                    });
                }
                else
                {
                    //LoaderSpinner.IsVisible = false;
                    UserDialogs.Instance.Toast(AppResources.Label_Offline_Mode);
                }

                var TapMyProfile = new TapGestureRecognizer();
                TapMyProfile.Tapped += (s, ee) =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("MyProfile", ""));
                };

                if (Lbl_Username.GestureRecognizers.Count == 0)
                {
                    Lbl_Username.GestureRecognizers.Add(TapMyProfile);
                }
                Lbl_Username.Text = Settings.UserFullName;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void NotifiTimerFunction()
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (!xx)
                {
                    Get_notifications().ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Get My profile
        public void GetMyprofile()
        {
            try
            {
                using (var Data = new LoginUserProfileFunctions())
                {
                    var TapGestureRecognizerSettings = new TapGestureRecognizer();
                    TapGestureRecognizerSettings.Tapped += (s, ee) =>
                    {
                        //SettingsPage SettingsPage = new SettingsPage();
                        //await Navigation.PushAsync(SettingsPage);
                        ClickLogoutIcon();
                    };

                    if (LogoutImage.GestureRecognizers.Count == 0)
                    {
                        LogoutImage.GestureRecognizers.Add(TapGestureRecognizerSettings);
                    }

                    var Profile = Data.GetProfileCredentialsById(Settings.User_id);
                    var device = Resolver.Resolve<IDevice>();
                    var oNetwork = device.Network;
                    var InternetConnection = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                    if (InternetConnection)
                    {
                        if (Profile == null)
                        {
                            Settings.Avatarimage = "default_profile_6_400x400.png";
                            AvatarImage.Source = Settings.Avatarimage;
                            AvatarImage.BorderColor = Color.FromHex("#c72508");
                        }
                        else
                        {
                            var GetAvatarAvailability =DependencyService.Get<IPicture>().GetPictureFromDisk(Profile.avatar, Settings.User_id);
                            if (GetAvatarAvailability != "File Dont Exists")
                            {
                                Settings.Avatarimage = GetAvatarAvailability;
                                if (String.IsNullOrEmpty(Profile.first_name) && String.IsNullOrEmpty(Profile.last_name))
                                    Settings.UserFullName = Profile.name;
                                else
                                    Settings.UserFullName = Profile.first_name + " " + Profile.last_name;

                                AvatarImage.Source = Settings.Avatarimage;
                                AvatarImage.BorderColor = Color.FromHex("#c72508");

                                Lbl_Username.Text = Settings.UserFullName;

                                var tapGestureRecognizer = new TapGestureRecognizer();

                                tapGestureRecognizer.Tapped += async (s, ee) =>
                                {
                                    MyProfilePage MyProfile = new MyProfilePage();
                                    await Navigation.PushAsync(MyProfile);
                                };
                                if (AvatarImage.GestureRecognizers.Count == 0)
                                {
                                    AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Profile != null)
                        {
                            var GetAvatarAvailability = DependencyService.Get<IPicture>().GetPictureFromDisk(Profile.avatar, Settings.User_id);
                            if (GetAvatarAvailability != "File Dont Exists")
                            {
                                Settings.Avatarimage = GetAvatarAvailability;
                                Settings.UserFullName = Profile.name;

                                AvatarImage.Source = Settings.Avatarimage;
                                AvatarImage.BorderColor = Color.FromHex("#6CDB26");
                                if (String.IsNullOrEmpty(Profile.first_name) && String.IsNullOrEmpty(Profile.last_name))
                                    Settings.UserFullName = Profile.name;
                                else
                                    Settings.UserFullName = Profile.first_name + " " + Profile.last_name;

                                Lbl_Username.Text = Settings.UserFullName;

                                var tapGestureRecognizer = new TapGestureRecognizer();
                                tapGestureRecognizer.Tapped += async (s, ee) =>
                                {
                                    MyProfilePage MyProfile = new MyProfilePage();
                                    await Navigation.PushAsync(MyProfile);
                                };
                                if (AvatarImage.GestureRecognizers.Count == 0)
                                {
                                    AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                                }
                            }
                            else
                            {
                                AvatarImage.Source = new UriImageSource
                                {
                                    Uri = new Uri(Profile.avatar),
                                    CachingEnabled = true,
                                    CacheValidity = new TimeSpan(5, 0, 0, 0)
                                };
                                AvatarImage.BorderColor = Color.FromHex("#6CDB26");
                                Settings.UserFullName = Profile.name;

                                var tapGestureRecognizer = new TapGestureRecognizer();
                                tapGestureRecognizer.Tapped += async (s, ee) =>
                                {
                                    MyProfilePage MyProfile = new MyProfilePage();
                                    await Navigation.PushAsync(MyProfile);
                                };

                                if (AvatarImage.GestureRecognizers.Count == 0)
                                {
                                    AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                                }
                            }
                        }
                        else
                        {
                            LoginUserFunctions.GetSessionProfileData(Settings.User_id, Settings.Session).ConfigureAwait(false);
                            AvatarImage.Source = Settings.Avatarimage;
                            Lbl_Username.Text = Settings.UserFullName;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
        
        private async void OnViewyourprofileTapped(object sender, EventArgs e)
        {
            try
            {
                if (Lbl_Username.GestureRecognizers.Count != 0)
                {
                    await Navigation.PushAsync(new MyProfilePage());
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //AvatarImage Source
        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            try
            {
                AvatarImage.Source = Settings.Avatarimage;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Add Item Page 
        public void AddItemsToList()
        {
            try
            {
                if (ListItemsCollection.Count == 0)
                {
                    if (Settings.ConnectivitySystem == "1")
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Following,
                            Icon = Ionic_Font.PersonStalker,
                            BadgeNumber = "0",
                            BackgroundColor = "#0084ff",
                            BadgeIsVisible = "false",
                            Heightrow = "60"
                        });
                    }
                    else
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Friends,
                            Icon = Ionic_Font.PersonStalker,
                            BadgeNumber = "0",
                            BackgroundColor = "#0084ff",
                            BadgeIsVisible = "false",
                            Heightrow = "60"
                        });
                    }

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Pages,
                        Icon = Ionic_Font.Thumbsup,
                        BadgeNumber = "0",
                        BackgroundColor = "#a84849",
                        BadgeIsVisible = "false",
                        Heightrow = "60"

                    });

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Groups,
                        Icon = Ionic_Font.IosPeople,
                        BadgeNumber = "0",
                        BackgroundColor = "#ffc300",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });

                    if (Settings.Messenger_Integration)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Messages,
                            Icon = Ionic_Font.Chatboxes,
                            BadgeNumber = "0",
                            BackgroundColor = "#391c8c",
                            BadgeIsVisible = "false",
                            Heightrow = "60"
                        });
                    }

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Albums,
                        Icon = Ionic_Font.Images,
                        BadgeNumber = "0",
                        BackgroundColor = "#ff5ca1",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });

                    if (Settings.Show_Movies)
                    {
                        if (Device.RuntimePlatform == Device.iOS)
                        {
                           
                        }
                        else
                        {
                            ListItemsCollection.Add(new SliderItems()
                            {
                                Label = AppResources.Label3_Movies,
                                Icon = Ionic_Font.IosVideocam,
                                BadgeNumber = "0",
                                BackgroundColor = "#a695c7",
                                BadgeIsVisible = "false",
                                Heightrow = "60"
                            });
                        }
                       
                    }
                    if (Settings.Show_Market)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Market,
                            Icon = Ionic_Font.AndroidCart,
                            BadgeNumber = "0",
                            BackgroundColor = "#67b868",
                            BadgeIsVisible = "false",
                            Heightrow = "60"
                        });
                    }

                    if (Settings.Show_Event_Pages)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Events,
                            Icon = Ionic_Font.AndroidCalendar,
                            BadgeNumber = "0",
                            BackgroundColor = "rgb(250, 60, 76)",
                            BadgeIsVisible = "false",
                            Heightrow = "60"
                        });
                    }

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Search,
                        Icon = Ionic_Font.Search,
                        BadgeNumber = "0",
                        BackgroundColor = "#6699cc",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });

                    ListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Saved_Post,
                        Icon = Ionic_Font.Bookmark,
                        BadgeNumber = "0",
                        BackgroundColor = "#008484",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });

                    if (Settings.Show_Articles)
                    {
                        ListItemsCollection.Add(new SliderItems()
                        {
                            Label = AppResources.Label_Articles,
                            Icon = Ionic_Font.DocumentText,
                            BadgeNumber = "0",
                            BackgroundColor = "#ff7e29",
                            BadgeIsVisible = "false",
                            Heightrow = "60"
                        });
                    }

                    if (Settings.Show_NearBy)
                    {
                        if (Device.RuntimePlatform == Device.iOS)
                        {

                        }
                        else
                        {
                            ListItemsCollection.Add(new SliderItems()
                           {
                               Label = AppResources.Label2_Near_Me,
                               Icon = Ionic_Font.IosLocation,
                               BadgeNumber = "0",
                               BackgroundColor = "#44bec7",
                               BadgeIsVisible = "false",
                               Heightrow = "60"
                           });
                        }

                    }

                    //ListItemsCollection.Add(new SliderItems()
                    //{
                    //    Label = AppResources.Label_Settings,
                    //    Icon = Ionic_Font.Settings,
                    //    BadgeNumber = "0",
                    //    BackgroundColor = "#777",
                    //    BadgeIsVisible = "false",
                    //    Heightrow = "60"
                    //});

                    // Settings
                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Notifications,
                        Icon = Ionic_Font.IosBell,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });
                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Account,
                        Icon = Ionic_Font.Person,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });
                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Privacy,
                        Icon = Ionic_Font.Eye,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60"

                    });
                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Blocked_Users,
                        Icon = Ionic_Font.MinusRound,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });

                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_invite,
                        Icon = Ionic_Font.Email,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60",
                    });
                    //
                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Help,
                        Icon = Ionic_Font.Help,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });
                    SettingsListItemsCollection.Add(new SliderItems()
                    {
                        Label = AppResources.Label_Report_TO_us,
                        Icon = Ionic_Font.Flag,
                        BadgeNumber = "0",
                        BackgroundColor = "#777",
                        BadgeIsVisible = "false",
                        Heightrow = "60"
                    });
                }

                GeneralList.HeightRequest = ListItemsCollection.Count * 66;
                GeneralList.ItemsSource = ListItemsCollection;

                SettingsList.HeightRequest = SettingsListItemsCollection.Count * 75;
                SettingsList.ItemsSource = SettingsListItemsCollection;

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void GeneralList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                GeneralList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in General List >> open Timeline Pages
        private async void GeneralList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as SliderItems;
                if (Item != null)
                {
                    if (Item.Label == AppResources.Label_Albums)
                    {
                        await Navigation.PushAsync(new AlbumPage());
                    }
                    else if (Item.Label == AppResources.Label_Friends || Item.Label == AppResources.Label_Following)
                    {
                        await Navigation.PushAsync(new ContactsTab());
                    }
                    else if (Item.Label == AppResources.Label_Messages)
                    {
                        //Device.OpenUri(new Uri("wowonder://compose?text=Wowonder"));
                        DependencyService.Get<IMethods>().OpenMessengerApp(Settings.Messenger_Package_Name);
                    }
                    else if (Item.Label == AppResources.Label_Pages)
                    {
                        await Navigation.PushAsync(new CommunitiesPage());
                    }
                    else if (Item.Label == AppResources.Label3_Movies)
                    {
                        await Navigation.PushAsync(new MoviesPage());
                    }
                    else if (Item.Label == AppResources.Label_Market)
                    {
                        await Navigation.PushAsync(new MarketPage());
                    }
                    else if (Item.Label == AppResources.Label_Groups)
                    {
                        await Navigation.PushAsync(new GroupsListPage());
                    }
                    else if (Item.Label == AppResources.Label_Search)
                    {
                        await Navigation.PushAsync(new Search_Page());
                    }
                    else if (Item.Label == AppResources.Label_Saved_Post)
                    {
                        await Navigation.PushAsync(new HyberdPostViewer("Saved Post", ""));
                    }
                    else if (Item.Label == AppResources.Label_Articles)
                    {
                        await Navigation.PushAsync(new ArticlesListPage());
                    } 
                    else if (Item.Label == AppResources.Label_Events)
                    {
                        await Navigation.PushAsync(new Events_Page());
                    }
                    else if (Item.Label == AppResources.Label2_Near_Me)
                    {
                        await Navigation.PushAsync(new Main_Near_by_Page());
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Get Notifications and add to list
        public async Task Get_notifications()
        {
            try
            {
                //await Task.Delay(5000);
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("device_id", Settings.Device_ID),
                        new KeyValuePair<string, string>("s", Settings.Session)
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_notifications", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        #region Notification

                        try
                        {
                            var Parser = JObject.Parse(json).SelectToken("notifications").ToString();
                            JArray NotificationsArray = JArray.Parse(Parser);
                            if (NotificationsList.Count <= NotificationsArray.Count)
                            {
                                NotificationsList.Clear();
                            }
                            foreach (var Notification in NotificationsArray)
                            {
                                JObject Data = JObject.FromObject(Notification);
                                JObject Notifier = JObject.FromObject(Data["notifier"]);
                                var Notifier_id = Data["notifier_id"].ToString();
                                var Post_ID = Data["post_id"].ToString();
                                var Page_ID = Data["page_id"].ToString();
                                var Group_ID = Data["group_id"].ToString();
                                var Count_Notification = JObject.Parse(json).SelectToken("count_notifications").ToString();
                                var Seen = Data["seen"].ToString();
                                var Type_Text = Data["type_text"].ToString();
                                var Time_Text = Data["time_text_string"].ToString();
                                var Type = Data["type"].ToString();
                                var Icon = Data["icon"].ToString();
                                var ID = Data["id"].ToString();
                                var User_id = Notifier["user_id"].ToString();
                                var Username = Notifier["username"].ToString();
                                var Name = Notifier["name"].ToString();
                                var Avatar = Notifier["avatar"].ToString();

                                //if (Count_Notification != "0")
                                //{
                                //    var Alfa = ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Notifications);
                                //    if (Alfa.BadgeNumber != Count_Notification)
                                //    {
                                //        Alfa.BadgeIsVisible = "true";
                                //        Alfa.BadgeNumber = Count_Notification;
                                //    }
                                //}
                                //else
                                //{
                                //    var Alfa = ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Notifications);
                                //    Alfa.BadgeIsVisible = "false";
                                //    Alfa.BadgeNumber = "0";
                                //    Alfa.Badgecolor = "#ffff";
                                //}

                                _CountNotification = Convert.ToInt32(Count_Notification);

                                var ColorIcon = NotifiTypeFunction.GetColorFontAwesom(Type);
                                var TypeIcon = NotifiTypeFunction.GetIconFontAwesom(Type);
                                if (Icon == "exclamation-circle" || Icon == "thumbs-down")
                                {
                                    TypeIcon = NotifiTypeFunction.GetIconFontAwesom(Icon);
                                    ColorIcon = NotifiTypeFunction.GetColorFontAwesom(Icon);
                                }

                                var DecodedTypetext = System.Net.WebUtility.HtmlDecode(Type_Text);

                                var Imagepath = DependencyService.Get<IPicture>().GetPictureFromDisk(Avatar, User_id);
                                var ImageMediaFile = ImageSource.FromFile(Imagepath);

                                if (DependencyService.Get<IPicture>().GetPictureFromDisk(Avatar, User_id) == "File Dont Exists")
                                {
                                    ImageMediaFile = new UriImageSource
                                    {
                                        Uri = new Uri(Avatar),
                                        CachingEnabled = true,
                                        CacheValidity = new TimeSpan(5, 0, 0, 0)
                                    };
                                    DependencyService.Get<IPicture>().SavePictureToDisk(Avatar, User_id);
                                }

                                var Cheker = NotificationsList.FirstOrDefault(a => a.ID == ID);
                                if (NotificationsList.Contains(Cheker))
                                {

                                }
                                else
                                {
                                    if (Seen == "0" && Settings.NotificationPopup)
                                    {
                                        int NotiID = Int32.Parse(ID);
                                        using (var Notifi = new NotifiFunctions())
                                        {
                                            var Exits = Notifi.GetNotifiDBCredentialsById(NotiID);
                                            if (Exits == null)
                                            {
                                                Notifi.InsertNotifiDBCredentials(new NotifiDB
                                                {
                                                    messageid = NotiID,
                                                    Seen = 0
                                                });
                                            }
                                            var Exits2 = Notifi.GetNotifiDBCredentialsById(NotiID);
                                            if (Exits2.Seen == 0 && NotifiStoper != ID)
                                            {
                                                var Iconavatr = DependencyService.Get<IPicture>()
                                                    .GetPictureFromDisk(Avatar, User_id);
                                                DependencyService.Get<ILocalNotificationService>()
                                                    .CreateLocalNotification(Name, DecodedTypetext, Iconavatr, User_id,
                                                        "Text");
                                                Exits2.Seen = 1;
                                                Notifi.UpdateNotifiDBCredentials(Exits2);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        
                                    }
                                    if (Type != "admin_notification")
                                    {
                                        if (_CountNotification != 0)
                                        {
                                            NotificationsList.Insert(0,new Notifications()
                                            {
                                                ID = ID,
                                                Notifier_id = Notifier_id,
                                                Seen = Seen,
                                                Type_Text = DecodedTypetext,
                                                Time_Text = Time_Text,
                                                Type = Type,
                                                User_id = User_id,
                                                Username = Username,
                                                Name = Name,
                                                Avatar = ImageMediaFile,
                                                SeenUnseenColor = "#fff",
                                                Icon_Color_FO = ColorIcon,
                                                Icon_Type_FO = TypeIcon,
                                                Post_ID = Post_ID,
                                                Group_ID = Group_ID,
                                                Page_ID = Page_ID,
                                            });
                                        }
                                        else
                                        {
                                            NotificationsList.Add(new Notifications()
                                            {
                                                ID = ID,
                                                Notifier_id = Notifier_id,
                                                Seen = Seen,
                                                Type_Text = DecodedTypetext,
                                                Time_Text = Time_Text,
                                                Type = Type,
                                                User_id = User_id,
                                                Username = Username,
                                                Name = Name,
                                                Avatar = ImageMediaFile,
                                                SeenUnseenColor = "#fff",
                                                Icon_Color_FO = ColorIcon,
                                                Icon_Type_FO = TypeIcon,
                                                Post_ID = Post_ID,
                                                Group_ID = Group_ID,
                                                Page_ID = Page_ID,
                                            });
                                        }
                                    }
                                }
                            }
                            if (_CountNotification != 0)
                            {
                                BottomBarPageExtensions.SetBadgeCount(AndroidStyle.NotificationsPage, NotificationsList.Count);
                              
                            }
                            else
                            {
                                BottomBarPageExtensions.SetBadgeCount(AndroidStyle.NotificationsPage, _CountNotification);
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Pro_users

                        try
                        {
                            var Parser2 = JObject.Parse(json).SelectToken("pro_users").ToString();
                            JArray Pro_usersArray = JArray.Parse(Parser2);
                            if (TopUsersCollection.Count > 60)
                            {
                                if (ProMemberStoper <= 9)
                                {
                                    TopUsersCollection.Clear();
                                }
                            }
                            foreach (var ProVIP in Pro_usersArray)
                            {
                                JObject Data = JObject.FromObject(ProVIP);
                                var User_id = Data["user_id"].ToString();
                                var Username = Data["username"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Name = Data["name"].ToString();

                                var Cheker = TopUsersCollection.FirstOrDefault(a => a.User_id == User_id);
                                if (Cheker != null)
                                {

                                }
                                else
                                {
                                    if (ProMemberStoper == 3)
                                    {
                                        TopUsersCollection.Insert(0, new TopUsersItems()
                                        {
                                            Name = Name,
                                            User_id = User_id,
                                            Username = Username,
                                            IsVip = "true",
                                            Image = new UriImageSource
                                            {
                                                Uri = new Uri(Avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            }
                                        });

                                        var ff = TopUsersCollection.FirstOrDefault(a => a.IsVip == "False");
                                        if (ff != null)
                                        {
                                            var Int = TopUsersCollection.IndexOf(ff);
                                            TopUsersCollection.Move(Int, 0);
                                        }
                                        else
                                        {
                                            TopUsersCollection.Insert(0, new TopUsersItems()
                                            {
                                                IsVip = "False",
                                                Image = "G_Add_ProUsers.png",
                                            });
                                        }
                                    }
                                }
                            }

                            if (ProMemberStoper >= 4)
                            {
                                ProMemberStoper = 0;
                            }
                            else
                            {
                                ProMemberStoper++;
                            }

                            //Device.BeginInvokeOnMainThread(() =>
                            //{
                            //    TopUsersList.ItemsSource = null;
                            //    TopUsersList.ItemsSource = TopUsersCollection;
                            //});
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Count_Messages

                        try
                        {
                            var Parser3 = JObject.Parse(json).SelectToken("count_messages").ToString();
                            if (Parser3 != "0")
                            {
                                var Alfa = ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Messages);
                                if (Alfa.BadgeNumber != Parser3)
                                {
                                    Alfa.BadgeIsVisible = "true";
                                    Alfa.BadgeNumber = Parser3;
                                    Alfa.BackgroundColor = "#391c8c";
                                    Alfa.Icon = Ionic_Font.Chatboxes;
                                }
                            }
                            else
                            {
                                var Alfa = ListItemsCollection.FirstOrDefault(a => a.Label == AppResources.Label_Messages);
                                Alfa.BadgeIsVisible = "false";
                                Alfa.BadgeNumber = "0";
                                Alfa.BackgroundColor = "#391c8c";
                                Alfa.Icon = Ionic_Font.Chatboxes;
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Promoted_pages

                        try
                        {
                            var Parser5 = JObject.Parse(json).SelectToken("promoted_pages").ToString();
                            JArray Promoted_pagesArray = JArray.Parse(Parser5);
                            if (Promoted_pagesArray.Count == 0 && PagesItemsCollection.Count == 0)
                            {
                                //PagesList.IsVisible = false;
                                PromotedVisible = false;

                            }

                            foreach (var Pages in Promoted_pagesArray)
                            {
                                JObject Data = JObject.FromObject(Pages);
                                var Page_id = Data["page_id"].ToString();
                                var Page_name = Data["page_title"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Category = Data["category"].ToString();
                                var Cheker = PagesItemsCollection.FirstOrDefault(a => a.Page_id == Page_id);
                                if (Cheker != null)
                                {
                                    if (PagesItemsCollection.Count > 3)
                                    {
                                        PagesItemsCollection.RemoveAt(3);
                                    }
                                }
                                else
                                {
                                    if (PagesItemsCollection.Count <= 3)
                                    {
                                        PagesItemsCollection.Add(new PagesItems()
                                        {
                                            Page_id = Page_id,
                                            Page_name = Page_name,
                                            Category = Category,
                                            Avatar = new UriImageSource
                                            {
                                                Uri = new Uri(Avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            }
                                       });
                                    }
                                    else
                                    {
                                        PagesItemsCollection.Insert(0, new PagesItems()
                                        {
                                            Page_id = Page_id,
                                            Page_name = Page_name,
                                            Category = Category,
                                            Avatar = new UriImageSource
                                            {
                                                Uri = new Uri(Avatar),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            }
                                        });
                                        PagesItemsCollection.RemoveAt(3);
                                    }
                                }
                            }
                            if (PagesItemsCollection.Count > 0)
                            {
                                PromotedVisible = true;
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        #region Trending_Hashtag

                        try
                        {
                            var Parser4 = JObject.Parse(json).SelectToken("trending_hashtag").ToString();
                            JArray Trending_HashtagArray = JArray.Parse(Parser4);
                            if (TrendingItemsCollection.Count > 30)
                            {
                                TrendingItemsCollection.Clear();
                            }
                            foreach (var Hashtag in Trending_HashtagArray)
                            {
                                JObject Data = JObject.FromObject(Hashtag);
                                var id = Data["id"].ToString();
                                var Tag = System.Net.WebUtility.HtmlDecode(Data["tag"].ToString());
                                var trend_use_num = Data["trend_use_num"].ToString();
                                var Cheker = TrendingItemsCollection.FirstOrDefault(a => a.ID == id);

                                if (Cheker != null)
                                {

                                }
                                else
                                {
                                    TrendingItemsCollection.Add(new TrendingItems()
                                    {
                                        ID = id,
                                        Label = "#" + Tag
                                    });
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion

                        # region Friend Requests

                        try
                        {
                            var Parser7 = JObject.Parse(json).SelectToken("friend_requests").ToString();
                            JArray FriendRequestArray = JArray.Parse(Parser7);

                            foreach (var Friend in FriendRequestArray)
                            {
                                JObject Data = JObject.FromObject(Friend);
                                var User_id = Data["user_id"].ToString();
                                var Username = Data["username"].ToString();
                                var Avatar = Data["avatar"].ToString();
                                var Name = Data["name"].ToString();

                                var Cheker = FriendRequestsItemsCollection.FirstOrDefault(a => a.User_id == User_id);
                                if (Cheker != null)
                                {

                                }
                                else
                                {
                                    FriendRequestsItemsCollection.Add(new FriendRequests()
                                    {
                                        Name = Name,
                                        User_id = User_id,
                                        Username = Username,
                                        Image = new UriImageSource
                                        {
                                            Uri = new Uri(Avatar),
                                            CachingEnabled = true,
                                            CacheValidity = new TimeSpan(2, 0, 0, 0)
                                        },
                                        AccepetedIsvisbile = "False",
                                        AccepetedText = AppResources.Label_You_are_now_friends,
                                        ConfirmButtonBGColor = Settings.ButtonColorNormal,
                                        ConfirmButtonIsvisbile = "True",
                                        ConfirmButtonText = AppResources.Label_Confirm,
                                        ConfirmTextColor = Settings.ButtonTextColorNormal,
                                        DeleteButtonBGColor = Settings.ButtonLightColor,
                                        DeleteButtonIsvisbile = "True",
                                        DeleteButtonText = AppResources.Label_Delete,
                                        DeleteTextColor = Settings.ButtonTextLightColor,
                                    });
                                }

                            }
                        }
                        catch (Exception ex)
                        {
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }

                        #endregion
                    }
                    else if (apiStatus == "400")
                    {
                        try
                        {
                            JObject errors = JObject.FromObject(data["errors"]);
                            var errortext = errors["error_text"].ToString();
                            var errorID = errors["error_id"].ToString();
                            if (errorID == "6")
                            {
                                TimerBool = false;
                                //UserDialogs.Instance.ShowLoading(AppResources.Label_Logging_out, MaskType.Clear);
                                using (var Data = new LoginFunctions())
                                {
                                    var CredentialStatus = Data.GetLoginCredentialsStatus();
                                    Data.DeleteLoginCredential(Settings.Session);
                                    Data.ClearLoginCredentialsList();
                                }
                                using (var Data = new LoginUserProfileFunctions())
                                {
                                    Data.ClearProfileCredentialsList();
                                }
                                using (var Data = new MessagesFunctions())
                                {
                                    Data.ClearMessageList();
                                }
                                using (var Data = new ContactsFunctions())
                                {
                                    Data.DeletAllChatUsersList();
                                }
                                using (var Data = new CommunitiesFunction())
                                {
                                    Data.DeletAllCommunityList();
                                }
                                using (var Data = new NotifiFunctions())
                                {
                                    Data.ClearNotifiDBCredentialsList();
                                }
                                using (var Data = new PrivacyFunctions())
                                {
                                    Data.ClearPrivacyDBCredentialsList();
                                }
                                App.GetLoginPage();
                            }
                        }
                        catch (Exception ex)
                        {
                            await Navigation.PushModalAsync(new MainPage());
                              System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event Click Logout Icon
        public async void ClickLogoutIcon()
        {
            try
            {
                var Qusiion = await DisplayAlert(AppResources.Label2_Note, AppResources.Label2_The_Application_needs_Restart, AppResources.Label_Yes, AppResources.Label_NO);
                if (Qusiion)
                {
                    await DisplayAlert(AppResources.Label2_Note, AppResources.Label2_Session_Dead, AppResources.Label_OK);

                    UserDialogs.Instance.ShowLoading(AppResources.Label2_Clearing_Cach);

                    using (var Data = new LoginFunctions())
                    {
                        Data.ClearLoginCredentialsList();
                    }
                    using (var Data = new LoginUserProfileFunctions())
                    {
                        Data.ClearProfileCredentialsList();
                    }
                    using (var Data = new MessagesFunctions())
                    {
                        Data.ClearMessageList();
                    }
                    using (var Data = new ContactsFunctions())
                    {
                        Data.DeletAllChatUsersList();
                    }
                    using (var Data = new CommunitiesFunction())
                    {
                        Data.DeletAllCommunityList();
                    }
                    using (var Data = new NotifiFunctions())
                    {
                        Data.ClearNotifiDBCredentialsList();
                    }
                    using (var Data = new PrivacyFunctions())
                    {
                        Data.ClearPrivacyDBCredentialsList();
                    }
                    using (var Data = new ChatActivityFunctions())
                    {
                        Data.ClearChatUserTable();
                        Data.DeletAllChatUsersList();

                        try
                        {
                            UserDialogs.Instance.HideLoading();
                            WowonderPhone.Settings.ReLogin = true;
                            DependencyService.Get<IMethods>().Close_App();
                            await Navigation.PopAsync();
                            App.GetLoginPage();
                        }
                        catch
                        {
                            UserDialogs.Instance.HideLoading();
                            Navigation.RemovePage(this);
                            App.GetLoginPage();
                            DependencyService.Get<IMethods>().Close_App();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                UserDialogs.Instance.HideLoading();
                Navigation.RemovePage(this);
                App.GetLoginPage();
            }
        }

        private void SettingsList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                SettingsList.SelectedItem = null;

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void SettingsList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {

                var Item = e.Item as SliderItems;
                if (Item != null)
                {

                    if (Item.Label == AppResources.Label_Help)
                    {
                        await Navigation.PushAsync(new HelpPage());
                    }
                    else if (Item.Label == AppResources.Label_Report_TO_us)
                    {
                        Device.OpenUri(new Uri(Settings.Website + "/contact-us"));
                    }
                    else if (Item.Label == AppResources.Label_Notifications)
                    {
                        await Navigation.PushAsync(new Notification_Page());
                    }
                    else if (Item.Label == AppResources.Label_invite)
                    {
                        await Navigation.PushAsync(new InviteFriendsPage());
                    }
                    else if (Item.Label == AppResources.Label_Blocked_Users)
                    {
                        await Navigation.PushAsync(new BlockedUsersPage());
                    }
                    else if (Item.Label == AppResources.Label_Account)
                    {
                        await Navigation.PushAsync(new Account_Page());
                    }
                    else if (Item.Label == AppResources.Label_Privacy)
                    {
                        await Navigation.PushAsync(new Privacy_Page());
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}
