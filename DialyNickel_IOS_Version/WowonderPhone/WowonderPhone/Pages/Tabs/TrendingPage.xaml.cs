﻿using System;
using System.Threading.Tasks;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WowonderPhone.Pages.Tabs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrendingPage : ContentPage
    {
        
        public TrendingPage()
        {
            try
            {
                InitializeComponent();

                if (Settings.Show_ADMOB_On_ItemList)
                {
                    AdMobView.IsVisible = true;
                }
                else
                {
                    AdMobView.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void AddItems()
        {
            try
            {
                await Task.Run(async () =>
                {
                    await Task.Delay(5000);

                    if (ItemListPage.TopUsersCollection.Count == 0)
                    {
                        VipPanelSection.IsVisible = false;
                    }
                    if (ItemListPage.TrendingItemsCollection.Count == 0)
                    {
                        VipPanelSection.IsVisible = false;
                    }

                    if (ItemListPage.PromotedVisible)
                    {
                        if (ItemListPage.PagesItemsCollection.Count == 1)
                        {
                            PagesList.HeightRequest = 120;
                        }
                        else if (ItemListPage.PagesItemsCollection.Count == 2)
                        {
                            PagesList.HeightRequest = 175;
                        }
                        else if (ItemListPage.PagesItemsCollection.Count == 3)
                        {
                            PagesList.HeightRequest = 210;
                        }
                    }
                    else
                    {
                        if (ItemListPage.PagesItemsCollection.Count == 0)
                        {
                            PagesList.IsVisible = false;
                        }
                    }

                    if (ItemListPage.TopUsersCollection.Count > 2 && Settings.Show_Pro_Members)
                    {
                        if (VipPanelSection.IsVisible == false)
                            VipPanelSection.IsVisible = true;
                    }
                    if (ItemListPage.TrendingItemsCollection.Count >= 1 && Settings.Show_Trending_Hashtags)
                    {
                        if (TrendingList.IsVisible == false)
                            TrendingList.IsVisible = true;
                    }

                    if (ItemListPage.PagesItemsCollection.Count > 0 && Settings.Show_Promoted_Pages)
                    {
                        if (PagesList.IsVisible == false)
                            PagesList.IsVisible = true;
                    }

                    TrendingList.ItemsSource = ItemListPage.TrendingItemsCollection;
                    PagesList.ItemsSource = ItemListPage.PagesItemsCollection;
                    TopUsersList.ItemsSource = ItemListPage.TopUsersCollection;
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PagesList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                PagesList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in Pages List >> open SocialPageViewer
        private void PagesList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as ItemListPage.PagesItems;
                if (Item != null)
                {
                    Navigation.PushAsync(new SocialPageViewer(Item.Page_id, Item.Page_name, ""));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TrendingList_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                TrendingList.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in Trending List >> open HyberdPostViewer
        private void TrendingList_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var Item = e.Item as ItemListPage.TrendingItems;
                if (Item != null)
                {
                    var replace = Item.Label.Replace("#", "");
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", replace));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void TrendingPage_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                if (ItemListPage.TopUsersCollection.Count > 2 && Settings.Show_Pro_Members)
                {
                    if (VipPanelSection.IsVisible == false)
                        VipPanelSection.IsVisible = true;
                }
                else
                {
                    VipPanelSection.IsVisible = false;
                }
                if (ItemListPage.TrendingItemsCollection.Count >= 1 && Settings.Show_Trending_Hashtags)
                {
                    if (TrendingList.IsVisible == false)
                        TrendingList.IsVisible = true;
                }
                else
                {
                    TrendingList.IsVisible = false;
                }

                if (ItemListPage.PagesItemsCollection.Count > 0 && Settings.Show_Promoted_Pages)
                {
                    if (PagesList.IsVisible == false)
                        PagesList.IsVisible = true;
                }
                else
                {
                    PagesList.IsVisible = false;
                }

                TrendingList.ItemsSource = ItemListPage.TrendingItemsCollection;
                PagesList.ItemsSource = ItemListPage.PagesItemsCollection;
                TopUsersList.ItemsSource = ItemListPage.TopUsersCollection;

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}