﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.Stories;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages.Tabs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Status_Page : ContentPage
    {
        public static ObservableCollection<Story> StoriesListCollection = new ObservableCollection<Story>();
        public static ObservableCollection<Story> AddStoryList= new ObservableCollection<Story>();

        public Status_Page()
        {
            try
            {
                InitializeComponent();

                var addme = AddStoryList.FirstOrDefault(a => a.Title == "Add New");
                if (addme == null)
                {
                    AddStoryList.Add(new Story()
                    {
                        Thumbnail = "G_Add_ProUsers.png",
                        ID = Settings.User_id,
                        User_id = Settings.User_id,
                        Title = "Add New",
                        Description = "Add New",
                        Is_Owner = "True",
                        User_Name = AppResources.Label2_Add_Story,
                    });
                }
                AddStoryListview.ItemsSource = AddStoryList;

                GetStoryRequest();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async void GetStoryRequest()
        {
            try
            {
                AddStoryListview.IsRefreshing = true;

                StoriesListCollection.Clear();

                await Task.Delay(2000);
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=get_stories", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        var Parser = JObject.Parse(json).SelectToken("stories").ToString();
                        JArray storiesArray = JArray.Parse(Parser);

                        if (storiesArray.Count > 0)
                        {
                            foreach (var Story in storiesArray)
                            {
                                var id = Story["id"].ToString();
                                var user_id = Story["user_id"].ToString();
                                var title = Story["title"].ToString();
                                var description = Story["description"].ToString();
                                var thumbnail = Story["thumbnail"].ToString();
                                var is_owner = Story["is_owner"].ToString();

                                List<ImageSource> Images = new List<ImageSource>();
                                try
                                {
                                    JArray storiesimagesArray = JArray.Parse(Story["images"].ToString());
                                    if (storiesimagesArray.Count > 0)
                                    {
                                        Images.Add(new UriImageSource
                                        {
                                            Uri = new Uri(thumbnail),
                                            CachingEnabled = true,
                                            CacheValidity = new TimeSpan(2, 0, 0, 0)
                                        });

                                        foreach (var Ima in storiesimagesArray)
                                        {
                                            var filename = Ima["filename"].ToString();
                                            //var user_id = Ima["user_id"].ToString();
                                            //var title = Ima["title"].ToString();
                                            //var description = Ima["description"].ToString();
                                            Images.Add(new UriImageSource
                                            {
                                                Uri = new Uri(filename),
                                                CachingEnabled = true,
                                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                                            });
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                }

                                List<string> videos = new List<string>();
                                string filename_Url = "";

                                try
                                {
                                    JArray storiesvideosArray = JArray.Parse(Story["videos"].ToString());
                                    if (storiesvideosArray.Count > 0)
                                    {
                                        foreach (var vi in storiesvideosArray)
                                        {
                                            filename_Url = vi["filename"].ToString(); // Url vidoe
                                            var type = vi["type"].ToString();
                                            //var user_id = Ima["user_id"].ToString();
                                            //var title = Ima["title"].ToString();
                                            //var description = Ima["description"].ToString();
                                            videos = new List<string>
                                            {
                                                filename_Url,
                                            };
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                }

                                var Userdata = Story["user_data"];
                                var nameofuser = Userdata["name"].ToString();
                                var avatar = Userdata["avatar"].ToString();
                                var Profile_url = Userdata["url"].ToString();

                                var chek = StoriesListCollection.FirstOrDefault(a => a.ID == id);
                                if (chek == null)
                                {
                                    StoriesListCollection.Add(new Story()
                                    {
                                        Thumbnail = new UriImageSource
                                        {
                                            Uri = new Uri(thumbnail),
                                            CachingEnabled = true,
                                            CacheValidity = new TimeSpan(2, 0, 0, 0)
                                        },
                                        User_avatar = new UriImageSource
                                        {
                                            Uri = new Uri(avatar),
                                            CachingEnabled = true,
                                            CacheValidity = new TimeSpan(2, 0, 0, 0)
                                        },
                                        ID = id,
                                        User_id = user_id,
                                        Title = Functions.StringNullRemover(title),
                                        Description = description,
                                        Is_Owner = is_owner,
                                        User_Name = nameofuser,
                                        ListofImages = Images,
                                        Listofvideos = videos,
                                        filename = filename_Url
                                    });
                                }
                            }
                        }
                        StoryListview.ItemsSource = StoriesListCollection;
                        AddStoryListview.IsRefreshing = false;
                    }
                    AddStoryListview.IsRefreshing = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                AddStoryListview.IsRefreshing = false;
            }
        }

        //delete_story API
        public static async void delete_story_Http()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("story_id", Story_Page.Trachdata.ID),
                    });

                    var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=delete_story", formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();                  
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void Status_Page_OnAppearing(object sender, EventArgs e)
        {
            try
            {
                if (Story_Page.Trachdata != null)
                {
                    var chack = StoriesListCollection.FirstOrDefault(a => a.ID == Story_Page.Trachdata.ID);
                    if (chack != null)
                    {
                        StoriesListCollection.Remove(chack);
                    }

                    StoryListview.ItemsSource = StoriesListCollection;

                    delete_story_Http();

                    Story_Page.Trachdata = null;
                }
                else
                {
                    StoryListview.ItemsSource = StoriesListCollection;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddStoryListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                AddStoryListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void AddStoryListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var selectedItem = e.Item as Story;
                if (selectedItem != null)
                {
                    if (selectedItem.Title.Contains("Add New"))
                    {
                        await Navigation.PushAsync(new Create_Story());
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void StoryListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                StoryListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void StoryListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var selectedItem = e.Item as Story;
                if (selectedItem != null)
                {
                    if (!String.IsNullOrEmpty(selectedItem.filename))
                    {
                        await Navigation.PushAsync(new Storyvideos_Page(selectedItem));
                    }
                    else if (selectedItem.ListofImages.Count == 0)
                    {
                        await Navigation.PushAsync(new Story_Page(selectedItem));
                    }
                    else
                    {
                        await Navigation.PushAsync(new Story_Carousel_Page(selectedItem));
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AddStoryListview_OnRefreshing(object sender, EventArgs e)
        {
            var device = Resolver.Resolve<IDevice>();
            var oNetwork = device.Network; // Create Interface to Network-functions
            var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
            if (xx)
            {
                DependencyService.Get<IMessage>().LongAlert(AppResources.Label_CheckYourInternetConnection);
            }
            else
            {
                //Run code
               GetStoryRequest();
            }
        }
    }
}