﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Plugin.Media;
using PropertyChanged;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages.CustomCells;
using WowonderPhone.Pages.Timeline_Pages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages.UsersPages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xam.Plugin.Abstractions.Events.Inbound;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone.Pages
{
    public partial class MyProfilePage : ContentPage
    {
        [ImplementPropertyChanged]
        public class ProfileUser
        {
            public string Label { get; set; }
            public string Icon { get; set; }
        }

        #region Variables

        private static int Count;
        private LoginUserProfileTableDB _DataRow;

        public static ObservableCollection<ProfileUser> ProfileUserList = new ObservableCollection<ProfileUser>();
        public static ObservableCollection<Profile.Follower> Follower_ListItems =new ObservableCollection<Profile.Follower>();
        public static ObservableCollection<Profile.Group> Group_ListItems = new ObservableCollection<Profile.Group>();
        public static ObservableCollection<Profile.Liked_Pages> Liked_Pages_ListItems = new ObservableCollection<Profile.Liked_Pages>();

        #endregion

        public MyProfilePage()
        {
            try
            {
                InitializeComponent();
                Title = AppResources.Label_My_Profile;

                using (var DataLoader = new LoginUserProfileFunctions())
                {
                    var DataRow = DataLoader.GetProfileCredentialsById(Settings.User_id);
                    if (DataRow != null)
                    {
                        LoginUserFunctions.GetSessionProfileData(Settings.User_id, Settings.Session).ConfigureAwait(false);
                        var NewDataRow = DataLoader.GetProfileCredentialsById(Settings.User_id);
                        _DataRow = NewDataRow;
                    }
                    else
                    {
                        LoginUserFunctions.GetSessionProfileData(Settings.User_id, Settings.Session).ConfigureAwait(false);
                        var NewDataRow = DataLoader.GetProfileCredentialsById(Settings.User_id);
                        _DataRow = NewDataRow;
                    }

                    if (_DataRow != null)
                    {
                        AvatarImage.Source = DependencyService.Get<IPicture>().GetPictureFromDisk(_DataRow.avatar, Settings.User_id);
                        CoverImage.Source = DependencyService.Get<IPicture>().GetPictureFromDisk(_DataRow.cover, Settings.User_id);
                        UploadAvatar.BackgroundColor = Color.FromHex(Settings.MainColor);
                        UploadCover.BackgroundColor = Color.FromHex(Settings.MainColor);

                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        var device = Resolver.Resolve<IDevice>();
                        var oNetwork = device.Network; // Create Interface to Network-functions
                        var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                        if (!xx)
                        {
                            // this.Title = AppResources.Label_Loading;

                            PostWebLoader.Source = Settings.Website + "/get_news_feed?user_id=" + Settings.User_id;
                            PostWebLoader.OnJavascriptResponse += OnJavascriptResponse;
                            PostWebLoader.RegisterCallback("type", (str) => { });
                        }
                        else
                        {
                            //PostWebLoader.Source = Settings.HTML_LoadingPost_Page;
                        }

                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                        var tapGestureRecognizer = new TapGestureRecognizer();
                        var tapGestureRecognizerCover = new TapGestureRecognizer();

                        tapGestureRecognizer.Tapped += (s, ee) =>
                        {
                            DependencyService.Get<IMethods>().OpenImage("Disk", _DataRow.avatar, _DataRow.UserID);
                        };
                        tapGestureRecognizerCover.Tapped += (s, ee) =>
                        {
                            DependencyService.Get<IMethods>().OpenImage("Disk", _DataRow.cover, _DataRow.UserID);
                        };
                        AvatarImage.GestureRecognizers.Add(tapGestureRecognizer);
                        CoverImage.GestureRecognizers.Add(tapGestureRecognizerCover);

                        if (String.IsNullOrEmpty(_DataRow.first_name) && String.IsNullOrEmpty(_DataRow.last_name))
                            Username.Text = _DataRow.username;
                        else
                            Username.Text = _DataRow.first_name + " " + _DataRow.last_name;

                        AddlistItems();
                    }

                    Functions.Ad_Interstitial();
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void AddlistItems()
        {
            try
            {
                FeedIcon.Text = Ionic_Font.IosListOutline;
                AboutIcon.Text = Ionic_Font.Compose;
                MoreIcon.Text = Ionic_Font.AndroidMoreHorizontal;

                FeedStack.IsVisible = true;
                AboutStack.IsVisible = false;
                MoreStack.IsVisible = false;

                FeedLabel.FontAttributes = FontAttributes.Bold;
                AboutLabel.FontAttributes = FontAttributes.None;
                MoreLabel.FontAttributes = FontAttributes.None;

                var datauser = Convert_UserData();
                if (datauser != null)
                {
                    if (ProfileUserList.Count > 0)
                    {
                        ProfileUserList.Clear();
                    }

                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_about,
                        Icon = "\uf040",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_gender,
                        Icon = "\uf228",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_address,
                        Icon = "\uf041",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_phone_number,
                        Icon = "\uf10b",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_website,
                        Icon = "\uf282",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_school,
                        Icon = "\uf19d",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_working,
                        Icon = "\uf0b1",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_facebook,
                        Icon = "\uf09a",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_google,
                        Icon = "\uf0d5",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_twitter,
                        Icon = "\uf099",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_vk,
                        Icon = "\uf189",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_instagram,
                        Icon = "\uf16d",
                    });
                    ProfileUserList.Add(new ProfileUser()
                    {
                        Label = datauser.UD_youtube,
                        Icon = "\uf167",
                    });

                    UserInfoListView.ItemsSource = ProfileUserList;
                    ProgressBarLabel.Text = Count.ToString() + " %";

                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public Profile.User_Data Convert_UserData()
        {
            try
            {
                Profile.User_Data user = new Profile.User_Data();

                user.UD_user_id = _DataRow.UserID;
                user.UD_username = _DataRow.username;
                user.UD_email = _DataRow.email;
                user.UD_first_name = _DataRow.first_name;
                user.UD_last_name = _DataRow.last_name;
                user.UD_avatar = _DataRow.avatar;
                user.UD_cover = _DataRow.cover;
                user.UD_relationship_id = _DataRow.relationship_id;
                user.UD_address = _DataRow.address;
                user.UD_phone_number = _DataRow.phone_number;
                user.UD_working = _DataRow.working;
                user.UD_working_link = _DataRow.working_link;
                user.UD_about = _DataRow.about;
                user.UD_school = _DataRow.school;
                user.UD_gender = _DataRow.gender;
                user.UD_birthday = _DataRow.birthday;
                user.UD_website = _DataRow.website;
                user.UD_facebook = _DataRow.facebook;
                user.UD_google = _DataRow.google;
                user.UD_twitter = _DataRow.twitter;
                user.UD_linkedin = _DataRow.linkedin;
                user.UD_youtube = _DataRow.youtube;
                user.UD_vk = _DataRow.vk;
                user.UD_instagram = _DataRow.instagram;
                user.UD_language = _DataRow.language;

                if (ProfileUserList.Count == 0)
                {
                    MainProgressBar.Progress = 0.0;

                    if (_DataRow.about != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.about))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.gender != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.gender))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.address != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.address))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.phone_number != AppResources.Label_Empty &&
                        !String.IsNullOrEmpty(_DataRow.phone_number))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.website != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.website))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.school != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.school))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.facebook != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.facebook))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.google != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.google))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.twitter != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.twitter))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.linkedin != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.linkedin))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.youtube != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.youtube))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.vk != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.vk))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                    if (_DataRow.instagram != AppResources.Label_Empty && !String.IsNullOrEmpty(_DataRow.instagram))
                    {
                        MainProgressBar.Progress += 0.07;
                        Count += 7;
                    }
                }

                return user;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        private async void UploadAvatar_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image,
                        AppResources.Label_OK);
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync();

                if (file == null)
                    return;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_CannotUpload, AppResources.Label_CheckYourInternetConnection,
                        AppResources.Label_OK);
                }
                else
                {
                    var streamReader = new StreamReader(file.GetStream());
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                    }
                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        UploadPhoto(file.GetStream(), file.Path, MimeTipe, "avatar");
                    });

                    AvatarImage.Source = ImageSource.FromFile(file.Path);
                    Settings.Avatarimage = file.Path;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task<string> UploadPhoto(Stream stream, string image, string Mimetype, string image_type)
        {
            try
            {
                string Imagename = image.Split('/').Last();
                StreamContent scontent = new StreamContent(stream);
                scontent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = Imagename,
                    Name = "image"
                };
                scontent.Headers.ContentType = new MediaTypeHeaderValue(Mimetype);

                var client = new HttpClient();
                var multi = new MultipartFormDataContent();
                var values = new[]
                {
                    new KeyValuePair<string, string>("user_id", Settings.User_id),
                    new KeyValuePair<string, string>("s", Settings.Session),
                    new KeyValuePair<string, string>("image_type", image_type)
                };
                foreach (var keyValuePair in values)
                {
                    multi.Add(new StringContent(keyValuePair.Value), keyValuePair.Key);
                }
                multi.Add(scontent);
                client.BaseAddress = new Uri(Settings.Website);
                var result = client.PostAsync("/app_api.php?application=phone&type=update_profile_picture", multi)
                    .Result;
                string json = await result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                string apiStatus = data["api_status"].ToString();

                string ImageFile = data["avatar"].ToString();

                if (apiStatus == "200")
                {
                    using (var DataLoader = new LoginUserProfileFunctions())
                    {
                        var DataRow = DataLoader.GetProfileCredentialsById(Settings.User_id);
                        if (image_type == "avatar")
                        {
                            DataRow.avatar = ImageFile;
                            DataLoader.UpdateProfileCredentials(DataRow);
                            DependencyService.Get<IPicture>().SavePictureToDisk(ImageFile, Settings.User_id);
                            Settings.Avatarimage = image;
                        }
                        if (image_type == "cover")
                        {
                            DataRow.cover = ImageFile;
                            DataLoader.UpdateProfileCredentials(DataRow);
                            DependencyService.Get<IPicture>().SavePictureToDisk(ImageFile, Settings.User_id);
                            Settings.Coverimage = image;
                        }
                    }
                    return "Success";
                }
                else
                {
                    return AppResources.Label_Error;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return AppResources.Label_Error;
            }
        }

        private async void UploadCover_OnClicked(object sender, EventArgs e)
        {

            try
            {
                await CrossMedia.Current.Initialize();
                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert(AppResources.Label2_Oops, AppResources.Label2_You_Cannot_pick_an_image,
                        AppResources.Label_OK);
                    return;
                }
                var file = await CrossMedia.Current.PickPhotoAsync();

                if (file == null)
                    return;

                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_CannotUpload, AppResources.Label_CheckYourInternetConnection,
                        AppResources.Label_OK);
                }
                else
                {
                    var streamReader = new StreamReader(file.GetStream());
                    var bytes = default(byte[]);
                    using (var memstream = new MemoryStream())
                    {
                        streamReader.BaseStream.CopyTo(memstream);
                        bytes = memstream.ToArray();
                    }
                    string MimeTipe = MimeType.GetMimeType(bytes, file.Path);

                    System.Threading.Tasks.Task.Factory.StartNew(() =>
                    {
                        UploadPhoto(file.GetStream(), file.Path, MimeTipe, "cover").ConfigureAwait(false);
                    });

                    CoverImage.Source = ImageSource.FromFile(file.Path);
                    Settings.Coverimage = file.Path;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void UserInfoListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                UserInfoListView.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void FeedButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                FeedStack.IsVisible = true;

                AboutStack.IsVisible = false;
                MoreStack.IsVisible = false;

                FeedLabel.FontAttributes = FontAttributes.Bold;
                AboutLabel.FontAttributes = FontAttributes.None;
                MoreLabel.FontAttributes = FontAttributes.None;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void AboutButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                FeedStack.IsVisible = false;
                AboutStack.IsVisible = true;
                MoreStack.IsVisible = false;
                FeedLabel.FontAttributes = FontAttributes.None;
                AboutLabel.FontAttributes = FontAttributes.Bold;
                MoreLabel.FontAttributes = FontAttributes.None;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void MoreButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                FeedStack.IsVisible = false;
                AboutStack.IsVisible = false;
                MoreStack.IsVisible = true;

                FeedLabel.FontAttributes = FontAttributes.None;
                AboutLabel.FontAttributes = FontAttributes.None;
                MoreLabel.FontAttributes = FontAttributes.Bold;

                if (Follower_ListItems.Count > 0)
                {
                    ContactsStack.Children.Clear();
                    ContactsStackLayout.IsVisible = true;
                    ContactsHeader.Text = AppResources.Label3_Contacts + " (" + Profile.FollowerList.Count + ")";
                    Add_Contacts_Users(Follower_ListItems);
                }
                else
                {
                    ContactsStackLayout.IsVisible = false;
                }
                if (Liked_Pages_ListItems.Count > 0)
                {
                    PagesListview.IsVisible = true;
                    PagesHeader.Text = AppResources.Label_Liked_Pages + " (" + Profile.Liked_PagesList.Count + ")";
                    PagesListview.ItemsSource = Liked_Pages_ListItems;
                 
                    var Count = (Liked_Pages_ListItems.Count * 60) + 50;

                    if (Liked_Pages_ListItems.Count > 6)
                    {
                        PagesListview.HeightRequest = (6 * 60) + 50; 
                    }
                    else
                    {
                        PagesListview.HeightRequest = Count;
                    }
                }
                else
                {
                    PagesListview.IsVisible = false;
                }

                if (Group_ListItems.Count > 0)
                {
                    GroupListview.IsVisible = true;
                    GroupsHeader.Text = AppResources.Label_Groups + " (" + Profile.GroupList.Count + ")";
                    GroupListview.ItemsSource = Group_ListItems;
                   
                    var Count = Group_ListItems.Count * 60 + 50;

                    if (Group_ListItems.Count > 6)
                    {
                        GroupListview.HeightRequest = 6 * 60 + 50; ;
                    }
                    else
                    {
                        GroupListview.HeightRequest = Count;
                    }

                }
                else
                {
                    GroupListview.IsVisible = false;
                }

                if (Follower_ListItems.Count == 0 && Group_ListItems.Count == 0 && Liked_Pages_ListItems.Count == 0)
                {
                    EmptyStack.IsVisible = true;
                }
                else
                {
                    EmptyStack.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void Btn_Edit_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new Update_MyProfile_Page(_DataRow));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #region Feeds

        private async void OnJavascriptResponse(JavascriptResponseDelegate EventObj)
        {
            try
            {
                if (EventObj.Data.Contains("type"))
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(EventObj.Data);
                    string type = data["type"].ToString();
                    if (type == "user")
                    {
                        string Userid = data["profile_id"].ToString();
                        if (WowonderPhone.Settings.User_id == Userid)
                        {
                            return;
                        }
                        else if (Userid != Settings.User_id)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Navigation.PushAsync(new UserProfilePage(Userid, "FriendList"));
                            });
                        }
                        else
                        {
                            InjectedJavaOpen_UserProfile(Userid);
                        }

                    }
                    else if (type == "lightbox")
                    {
                        string ImageSource = data["image_url"].ToString();
                        if (Settings.ShowAndroidDefaultImageViewer)
                        {
                            UserDialogs.Instance.ShowLoading(AppResources.Label_Please_Wait);
                            var dataViewer = DependencyService.Get<IMethods>();
                            dataViewer.showPhoto(ImageSource);
                        }
                        else
                        {
                            var Image = new UriImageSource
                            {
                                Uri = new Uri(ImageSource),
                                CachingEnabled = true,
                                CacheValidity = new TimeSpan(2, 0, 0, 0)
                            };
                            InjectedJavaOpen_OpenImage(Image);
                        }
                    }
                    else if (type == "mention")
                    {
                        string user_id = data["user_id"].ToString();
                        InjectedJavaOpen_UserProfile(user_id);
                    }
                    else if (type == "hashtag")
                    {
                        string hashtag = data["tag"].ToString();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushModalAsync(new HyberdPostViewer("Hashtag", hashtag));
                        });
                    }
                    else if (type == "url")
                    {
                        string link = data["link"].ToString();

                        InjectedJavaOpen_PostLinks(link);
                    }
                    else if (type == "page")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_LikePage(Id);
                    }
                    else if (type == "group")
                    {
                        string Id = data["profile_id"].ToString();

                        InjectedJavaOpen_Group(Id);
                    }
                    else if (type == "post_wonders" || type == "post_likes")
                    {
                        string Id = data["post_id"].ToString();

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Navigation.PushAsync(new Like_Wonder_Viewer_Page(Id, type));
                        });
                    }

                    else if (type == "delete_post")
                    {
                        string Id = data["post_id"].ToString();

                        var Qussion = await DisplayAlert(AppResources.Label_Question,AppResources.Label_Would_You_like_to_delete_this_post, AppResources.Label_Yes,AppResources.Label_NO);
                        if (Qussion)
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                PostWebLoader.InjectJavascript(
                                    "$('#post-' + " + Id + ").slideUp(200, function () { $(this).remove();}); ");
                            });

                            Post_Manager("delete_post", Id).ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_UserProfile(string Userid)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new UserProfilePage(Userid, ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_OpenImage(ImageSource Image)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushModalAsync(new ImageFullScreenPage(Image));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_PostLinks(string link)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Device.OpenUri(new Uri(link));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Hashtag(string word)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new HyberdPostViewer("Hashtag", word));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_LikePage(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialPageViewer(id, "", ""));
                });

            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InjectedJavaOpen_Group(string id)
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Navigation.PushAsync(new SocialGroup("id", ""));
                });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public async Task Post_Manager(string Type, string postid)
        {
            try
            {
                var Action = " ";
                if (Type == "edit_post")
                {
                    Action = "edit";
                }
                else
                {
                    Action = "delete";
                }
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user_id", Settings.User_id),
                        new KeyValuePair<string, string>("post_id", postid),
                        new KeyValuePair<string, string>("s", Settings.Session),
                        new KeyValuePair<string, string>("action", Action),
                    });

                    var response =await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=post_manager",formContent);
                    response.EnsureSuccessStatusCode();
                    string json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    string apiStatus = data["api_status"].ToString();
                    if (apiStatus == "200")
                    {
                        if (Type == "edit_post")
                        {
                            Action = "edit";
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PostWebLoader_OnOnContentLoaded(Xam.Plugin.Abstractions.Events.Inbound.ContentLoadedDelegate eventObj)
        {
            try
            {
                //OfflinePost.IsVisible = false;
                //OfflinePost.Source = null;
                //PostWebLoader.IsVisible = true;
                PostWebLoader.RegisterCallback("type", (str) => { });
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void ShowmoreButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new HyberdPostViewer("MyProfile", ""));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion

        #region Groups

        private void GroupListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                GroupListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void GroupListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var selectedItem = e.Item as Profile.Group;
                if (selectedItem != null)
                {
                  await  Navigation.PushAsync(new SocialGroup(selectedItem.G_id, "Joined"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ShowAllGroups_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(null, Profile.GroupList,null, null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        #endregion

        #region Pages

        private async void PagesListview_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                var selectedItem = e.Item as Profile.Liked_Pages;
                if (selectedItem != null)
                {
                  await Navigation.PushAsync(new SocialPageViewer(selectedItem.P_page_id, selectedItem.P_page_name, "Liked"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private void PagesListview_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            try
            {
                PagesListview.SelectedItem = null;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ShowAllPages_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(null, null, Profile.Liked_PagesList, null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }


        #endregion

        #region Contacts

        public void Add_Contacts_Users(ObservableCollection<Profile.Follower> FollowerList)
        {
            try
            {
                for (var i = 0; i < FollowerList.Count; i++)
                {
                    var TapGestureRecognizer = new TapGestureRecognizer();
                    TapGestureRecognizer.Tapped += OnContactsTapped;
                    var item = new Contact_DataUsers_Template();
                    item.BindingContext = FollowerList[i];
                    item.GestureRecognizers.Add(TapGestureRecognizer);
                    ContactsStack.Children.Add(item);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        //Event click Item in Contact_DataUsers_Template >> open UserProfilePage
        private async void OnContactsTapped(object sender, EventArgs e)
        {
            try
            {
                var selectedItem = (Profile.Follower)((Contact_DataUsers_Template)sender).BindingContext;
                if (selectedItem != null)
                {
                    await Navigation.PushAsync(new UserProfilePage(selectedItem.F_user_id, "FriendList"));
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void ShowAllContacts_OnTapped(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new SeeAll_CommunitiesUsers_Page(Profile.FollowerList, null, null, null));
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        #endregion

    }
}