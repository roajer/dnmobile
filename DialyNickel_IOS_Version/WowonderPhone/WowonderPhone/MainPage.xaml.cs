﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;using PropertyChanged;
//using Acr.UserDialogs;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages;
using WowonderPhone.Pages.Register_pages;
using WowonderPhone.SQLite;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services;

namespace WowonderPhone
{

    #region classes

    [ImplementPropertyChanged]
    public class Catigories
    {
        public string Id_Catigories { get; set; }
        public string Name_Catigories { get; set; }
    }

    #endregion

    public partial class MainPage : ContentPage
    {
        #region Lists Items Declaration

        public static ObservableCollection<Catigories> ListCatigories = new ObservableCollection<Catigories>();

        #endregion

        public MainPage()
        {
            try
            {
                NavigationPage.SetHasNavigationBar(this, false);
                InitializeComponent();

                usernameEntry.Completed += (s, a) => passwordEntry.Focus();
                passwordEntry.Completed += (s, a) => OnLoginClicked(s, a);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        async void OnLoginClicked(object sender, EventArgs e)
        {
            try
            {
                var device = Resolver.Resolve<IDevice>();
                var oNetwork = device.Network; // Create Interface to Network-functions
                var xx = oNetwork.InternetConnectionStatus() == NetworkStatus.NotReachable;
                if (xx == true)
                {
                    await DisplayAlert(AppResources.Label_Error, AppResources.Label_CheckYourInternetConnection,
                        AppResources.Label_OK);
                }
                else
                {
                    if (String.IsNullOrEmpty(usernameEntry.Text) || usernameEntry.Text == " ")
                    {
                        DependencyService.Get<IMessage>().LongAlert(AppResources.Label3_Please_enter_username);
                        usernameEntry.Focus();
                    }
                    else if (String.IsNullOrEmpty(passwordEntry.Text) || passwordEntry.Text == " ")
                    {
                        DependencyService.Get<IMessage>().LongAlert(AppResources.Label3_Please_enter_password);
                        passwordEntry.Focus();
                    }
                    else
                    {
                        var StatusApiKey = "";
                        using (var client = new HttpClient())
                        {
                            var PushID = "";
                            Settings.Session = RandomString(70);
                            LoadingPanel.IsVisible = true;

                            var SettingsValues = new FormUrlEncodedContent(new[]
                            {
                                new KeyValuePair<string, string>("windows_app_version", Settings.Version),
                            });

                            var responseSettings = await client.PostAsync(
                                Settings.Website + "/app_api.php?application=phone&type=get_settings", SettingsValues);
                            responseSettings.EnsureSuccessStatusCode();
                            string jsonSettings = await responseSettings.Content.ReadAsStringAsync();
                            var dataSettings = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonSettings);
                            string apiStatusSettings = dataSettings["api_status"].ToString();

                            //======================= json Settings =======================
                            //await DisplayAlert("json Settings", jsonSettings, AppResources.Label_OK);
                            //===============================================================

                            if (apiStatusSettings == "200")
                            {
                                try
                                {
                                    //Settings.Onesignal_APP_ID = PushID;
                                    JObject settings = JObject.FromObject(dataSettings["config"]);

                                    if (Settings.API_ID == settings["widnows_app_api_id"].ToString() &&
                                        Settings.API_KEY == settings["widnows_app_api_key"].ToString())
                                    {
                                        StatusApiKey = "true";
                                        PushID = settings["push_id"].ToString();
                                        Settings.Onesignal_APP_ID = PushID;
                                        OneSignalNotificationController.RegisterNotificationDevice();
                                    }
                                    else
                                    {
                                        StatusApiKey = "false";
                                    }
                                    if (settings["footer_background_n"].ToString() != "#aaa")
                                    {
                                        await DisplayAlert(AppResources.Label_Security, Functions.Error,
                                            AppResources.Label_Yes);
                                        LoadingPanel.IsVisible = false;
                                        return;
                                    }
                                    //var settings_cat = settings["page_categories"];
                                    //JObject cats_data = JObject.FromObject(settings_cat);
                                }
                                catch (Exception ex)
                                {
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                    // await DisplayAlert("Exception apiStatusSettings", ex.ToString(), AppResources.Label_OK);
                                }
                            }

                            if (StatusApiKey == "true")
                            {
                                var TimeZoneContry = "UTC";
                                try
                                {
                                    var formContenst = new FormUrlEncodedContent(new[]
                                        {new KeyValuePair<string, string>("username", usernameEntry.Text),});

                                    var responseTime = await client.PostAsync("http://ip-api.com/json/", formContenst);
                                    string jsonres = await responseTime.Content.ReadAsStringAsync();

                                    var datares = JsonConvert.DeserializeObject<Dictionary<string, object>>(jsonres);

                                    string ResulttimeZone = datares["status"].ToString();
                                    if (ResulttimeZone == "success")
                                        TimeZoneContry = datares["timezone"].ToString();
                                }
                                catch (Exception)
                                {
                                    LoadingPanel.IsVisible = false;
                                }

                                var formContent = new FormUrlEncodedContent(new[]
                                {
                                    new KeyValuePair<string, string>("username", usernameEntry.Text),
                                    new KeyValuePair<string, string>("password", passwordEntry.Text),
                                    new KeyValuePair<string, string>("timezone", TimeZoneContry),
                                    new KeyValuePair<string, string>("device_id", Settings.Device_ID),
                                    new KeyValuePair<string, string>("s", Settings.Session)
                                });

                                var response = await client.PostAsync(Settings.Website + "/app_api.php?application=phone&type=user_login&cookie=1",formContent);
                                response.EnsureSuccessStatusCode();
                                string json = await response.Content.ReadAsStringAsync();
                                var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                                string apiStatus = data["api_status"].ToString();

                                //======================= json user_login =======================
                                // await DisplayAlert("json user_login", json, AppResources.Label_OK);
                                //===============================================================

                                try
                                {
                                    if (apiStatus == "200")
                                    {
                                        Settings.Cookie = data["cookie"].ToString();
                                        Settings.User_id = data["user_id"].ToString();

                                        //await LoginUserFunctions.GetSessionProfileData(Settings.User_id, Settings.Session);

                                        using (var datas = new LoginFunctions())
                                        {
                                            LoginTableDB LoginData = new LoginTableDB();
                                            LoginData.ID = 1;
                                            LoginData.Password = passwordEntry.Text;
                                            LoginData.Username = usernameEntry.Text;
                                            LoginData.Session = Settings.Session;
                                            LoginData.UserID = Settings.User_id;
                                            LoginData.Status = "Active";
                                            LoginData.Cookie = Settings.Cookie;
                                            LoginData.PrivacyPostValue = "0";
                                            LoginData.Onesignal_APP_ID = PushID;

                                            datas.InsertLoginCredentials(LoginData);
                                        }
                                        LoadingPanel.IsVisible = false;

                                        try
                                        {
                                            App.UserisStillNew = false;
                                            App.SessionHyberd = true;

                                            if (Settings.ReLogin)
                                            {
                                                await Navigation.PopAsync();
                                                if (Device.RuntimePlatform == Device.iOS)
                                                {
                                                    var navigationPage = new NavigationPage(new TabbedPageView()) { };
                                                    navigationPage.BarBackgroundColor =Color.FromHex(Settings.MainPage_HeaderBackround_Color);
                                                    navigationPage.BarTextColor =Color.FromHex(Settings.MainPage_HeaderText_Color);
                                                    navigationPage.Title = Settings.MainPage_HeaderTextLabel;
                                                    navigationPage.Padding = new Thickness(0, 0, 0, 0);
                                                    Application.Current.MainPage = navigationPage;
                                                }
                                                else
                                                {
                                                    var navigationPage = new NavigationPage(new AndroidStyle()) { };
                                                    navigationPage.BarBackgroundColor =Color.FromHex(Settings.MainPage_HeaderBackround_Color);
                                                    navigationPage.BarTextColor =Color.FromHex(Settings.MainPage_HeaderText_Color);
                                                    navigationPage.Title = Settings.MainPage_HeaderTextLabel;
                                                    navigationPage.Padding = new Thickness(0, 0, 0, 0);
                                                    Application.Current.MainPage = navigationPage;
                                                }
                                            }
                                            else
                                            {
                                                await Navigation.PushModalAsync(new WalkthroughVariantPage());
                                                LoadingPanel.IsVisible = false;
                                            }
                                        }
                                        catch
                                        {
                                            LoadingPanel.IsVisible = false;
                                            await Navigation.PopModalAsync();
                                        }
                                    }
                                    else if (apiStatus == "400")
                                    {
                                        JObject errors = JObject.FromObject(data["errors"]);
                                        var errortext = errors["error_text"].ToString();
                                        var ErrorMSG = await DisplayAlert(AppResources.Label_Security, errortext,
                                            AppResources.Label_Retry, AppResources.Label_Forget_My_Password);
                                        if (ErrorMSG == false)
                                        {
                                            // var URI = new Uri(Settings.Website + "/forgot-password");
                                            await Navigation.PushModalAsync(new ForgetPasswordpage());
                                        }
                                        LoadingPanel.IsVisible = false;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LoadingPanel.IsVisible = false;
                                      System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                                    //await DisplayAlert("Exception apiStatus", ex.ToString(), AppResources.Label_OK);
                                }
                            }
                            else
                            {
                                await DisplayAlert(AppResources.Label_Security, Functions.Error,AppResources.Label_Yes);
                                LoadingPanel.IsVisible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                LoadingPanel.IsVisible = false;
                await DisplayAlert(AppResources.Label_Security, exception.ToString(), AppResources.Label_Yes);
            }
        }

        //protected override bool OnBackButtonPressed()
        //{
        //    base.OnBackButtonPressed();
        //    if (Settings.ReLogin)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        private static Random random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXdsdaawerthklmnbvcxer46gfdsYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public static bool IsPageInNavigationStack<TPage>(INavigation navigation) where TPage : Page
        {
            try
            {
                if (navigation.NavigationStack.Count > 1)
                {
                    var last = navigation.NavigationStack[navigation.NavigationStack.Count - 2];

                    if (last is TPage)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return false;
            }
        }

        private async void LoginsocialButton_OnClicked(object sender, EventArgs e)
        {
            try
            {
                var socials = "";

                if (Settings.Show_Facebook_Login == true)
                {
                    socials += "Facebook,";
                }
                if (Settings.Show_Google_Login == true)
                {
                    socials += "Google,";
                }
                if (Settings.Show_Twitter_Login == true)
                {
                    socials += "Twitter,";
                }
                if (Settings.Show_Vkontakte_Login == true)
                {
                    socials += "Vkontakte,";
                }
                if (Settings.Show_Instagram_Login == true)
                {
                    socials += "Instagram";
                }
                if(Settings.Show_Instagram_Login && Settings.Show_Instagram_Login && Settings.Show_Vkontakte_Login && Settings.Show_Twitter_Login && Settings.Show_Google_Login && Settings.Show_Facebook_Login)
                {
                    
                }

                var socialsArray = socials.Split(',');

                var Actionclick = await DisplayActionSheet(AppResources.Label_Login_with, AppResources.Label_Cancel,null,socialsArray);

                #region Login Clicks

                if (Actionclick == "Facebook")
                {
                    try
                    {
                        await Navigation.PushAsync(new FacebookView());
                    }
                    catch
                    {
                        await Navigation.PushModalAsync(new FacebookView());
                    }
                }
                else if (Actionclick == "Google")
                {
                    try
                    {
                        await Navigation.PushAsync(new GoogleView());
                    }
                    catch
                    {
                        await Navigation.PushModalAsync(new GoogleView());
                    }
                }
                else if (Actionclick == "Twitter")
                {
                    try
                    {
                        await Navigation.PushAsync(new TwitterView());
                    }
                    catch
                    {
                        await Navigation.PushModalAsync(new TwitterView());
                    }
                }
                else if (Actionclick == "Vkontakte")
                {
                    try
                    {
                        await Navigation.PushAsync(new VkontakteView());
                    }
                    catch
                    {
                        await Navigation.PushModalAsync(new VkontakteView());
                    }
                }

                else if (Actionclick == "Instagram")
                {
                    try
                    {
                        await Navigation.PushAsync(new InstegramView());
                    }
                    catch
                    {
                        await Navigation.PushModalAsync(new InstegramView());
                    }
                }

                

                #endregion
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        private async void Btn_ForgetPassword_OnClicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushModalAsync(new ForgetPasswordpage());
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }
    }
}