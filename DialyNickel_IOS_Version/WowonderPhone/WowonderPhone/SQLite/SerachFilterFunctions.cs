﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
   class SearchFilterFunctions : IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }
        public SearchFilterFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "SearchFilterDB.db3"));
            Connection.CreateTable<SearchFilterDB>();
        }
        public void InsertSearchFilter(SearchFilterDB SearchFilterDB)
        {
            Connection.Insert(SearchFilterDB);
        }
        public void InsertSearchFilter(List<SearchFilterDB> SearchFilterDB)
        {
            Connection.Insert(SearchFilterDB);
        }
        public void UpdateSearchFilter(SearchFilterDB SearchFilterDB)
        {
            Connection.Update(SearchFilterDB);
        }
        public void DeleteSearchFilter(SearchFilterDB SearchFilterDB)
        {
            Connection.Delete(SearchFilterDB);
        }
        public void ClearSearchFilterTable()
        {
            Connection.DeleteAll<SearchFilterDB>();
        }

        public SearchFilterDB GetSearchFilterById(string ID)
        {
            return Connection.Table<SearchFilterDB>().FirstOrDefault(c => c.UserID == ID);
        }
        public SearchFilterDB GetSearchFilterUsername(string Username)
        {
            return Connection.Table<SearchFilterDB>().FirstOrDefault(c => c.UserID == Username);
        }
    }
}
