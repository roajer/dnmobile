﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
    class LoginUserProfileFunctions: IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }

        public LoginUserProfileFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform,Path.Combine(config.DirectoriDB, "ProfileDB.db3"));
            Connection.CreateTable<LoginUserProfileTableDB>();
        }

        public void InsertProfileCredentials(LoginUserProfileTableDB ProfileCredentials)
        {
            Connection.Insert(ProfileCredentials);
        }
        public void UpdateProfileCredentials(LoginUserProfileTableDB ProfileCredentials)
        {
            Connection.Update(ProfileCredentials);
        }
        public void DeleteProfileRow(LoginUserProfileTableDB ProfileCredentials)
        {
            Connection.Delete(ProfileCredentials);
        }
        public void DeleteProfileCredential(string userid)
        {
            var asd = Connection.Table<LoginTableDB>().FirstOrDefault(c => c.UserID == userid);

            Connection.Delete(asd);
        }

       

        public void ClearProfileCredentialsList()
        {
            Connection.DeleteAll<LoginUserProfileTableDB>();
        }

        public LoginUserProfileTableDB GetProfileCredentialsById(string ID)
        {
            return Connection.Table<LoginUserProfileTableDB>().FirstOrDefault(c => c.UserID == ID);

        }
        public string GetProfileCredentials()
        {
            var Status = Connection.Table<LoginUserProfileTableDB>().FirstOrDefault(c => c.status == "Active" || c.status == "Registered");
            var result = "NoSession";
            if (Status == null) { return result; }
            if (Status.status != "Active") { result = "NoSession"; }
            else if (Status.status == "Active") { result = "Active"; }

            if (Status.status == "Registered")
            {
                result = "Registered";
            }
            else if (Status.status != "Registered")
            {
                result = "NoSession";
            }
            return result;

        }

        public List<LoginUserProfileTableDB> GetProfileCredentialsList()
        {

            return Connection.Table<LoginUserProfileTableDB>().ToList();
        }
    }
}
