﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
    class ChatActivityFunctions : IDisposable
    {
        private SQLiteConnection Connection;

        public void Dispose()
        {
            Connection.Dispose();
        }

        public ChatActivityFunctions()
        {
            try
            {
                var config = DependencyService.Get<SQLiteData>();
                Connection = new SQLiteConnection(config.Platform,
                    Path.Combine(config.DirectoriDB, "ChatActivityDB.db3"));
                Connection.CreateTable<ChatActivityDB>();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InsertChatUsers(ChatActivityDB ChatActivity)
        {
            try
            {
                Connection.Insert(ChatActivity);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void InsertChatUsers(List<ChatActivityDB> ChatActivity)
        {
            try
            {
                Connection.Insert(ChatActivity);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }

        }

        public void UpdateChatUsers(ChatActivityDB ChatActivity)
        {
            try
            {
                Connection.Update(ChatActivity);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void DeleteChatUserRow(ChatActivityDB ChatActivity)
        {
            try
            {
                Connection.Delete(ChatActivity);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public void ClearChatUserTable()
        {
            try
            {
                Connection.DeleteAll<ChatActivityDB>();
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }
        }

        public ChatActivityDB GetChatUser(string ID)
        {
            try
            {
                return Connection.Table<ChatActivityDB>().FirstOrDefault(c => c.UserID == ID);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        public ChatActivityDB GetChatUserByUsername(string Username)
        {
            try
            {
                return Connection.Table<ChatActivityDB>().FirstOrDefault(c => c.Username == Username);
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        public ObservableCollection<ChatUsers> GetChatUsersCacheList()
        {
            try
            {
                var CachedChatlist = Functions.ChatList;
                CachedChatlist.Clear();
                //var CacshedChatlist = new ObservableCollection<ChatUsers>();
                foreach (var Item in Connection.Table<ChatActivityDB>().ToList().OrderByDescending(a => a.TimeLong))

                    if (Item.SeenMessageOrNo == Settings.UnseenMesageColor)
                    {
                        CachedChatlist.Insert(0, new ChatUsers()
                        {
                            Username = Item.Username,
                            profile_picture = ImageSource.FromFile(DependencyService.Get<IPicture>()
                                .GetPictureFromDisk(Item.ProfilePicture, Item.UserID)),
                            TextMessage = Item.TextMessage,
                            LastMessageDateTime = Item.LastMessageDateTime,
                            //verified = ChatUser_verified_bitmap,
                            SeenMessageOrNo = Item.SeenMessageOrNo,
                            ChekeSeen = Item.ChekeSeen,
                            UserID = Item.UserID,
                            lastseen = Item.lastseen
                        });
                    }
                    else
                    {
                        CachedChatlist.Add(new ChatUsers()
                        {
                            Username = Item.Username,
                            profile_picture = ImageSource.FromFile(DependencyService.Get<IPicture>()
                                .GetPictureFromDisk(Item.ProfilePicture, Item.UserID)),
                            TextMessage = Item.TextMessage,
                            LastMessageDateTime = Item.LastMessageDateTime,
                            //verified = ChatUser_verified_bitmap,
                            SeenMessageOrNo = Item.SeenMessageOrNo,
                            ChekeSeen = Item.ChekeSeen,
                            UserID = Item.UserID,

                        });
                    }
                return CachedChatlist;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }
        }

        public void DeletAllChatUsersList()
        {
            try
            {
                Connection.DeleteAll<ChatActivityDB>();
                foreach (var Activity in Connection.Table<ChatActivityDB>())
                {
                    Connection.Delete(Activity);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
            }

        }
    }
}
