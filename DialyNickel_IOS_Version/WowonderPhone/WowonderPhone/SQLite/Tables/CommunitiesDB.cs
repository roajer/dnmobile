﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace WowonderPhone.SQLite.Tables
{
    class CommunitiesDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string CommunityID { get; set; }
        public string CommunityName { get; set; }
        public string CommunityPicture { get; set; }
        public string CommunityUrl { get; set; }
        public string CommunityType { get; set; }
        public string CommunityTypeLabel { get; set; }
        public string CommunityTypeID { get; set; }
        
        //public string CommunityButtonColor { get; set; }
        //public string CommunityButtonTextColor { get; set; }
        //public string CommunityButtonImage { get; set; }

        public override string ToString()
        {
            return string.Format("CommunityID : {0}, CommunityName : {1}, CommunityPicture: {2} , Url : {3}, CommunityType: {4}", CommunityID, CommunityName, CommunityPicture, CommunityUrl, CommunityType);
        }
    }
}
