﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace WowonderPhone.SQLite.Tables
{
   public class Near_meDB
    {
        
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string UserID { get; set; }
            public int DistanceValue { get; set; }
            public int Gender { get; set; }
            public int Status { get; set; }

    }
}
