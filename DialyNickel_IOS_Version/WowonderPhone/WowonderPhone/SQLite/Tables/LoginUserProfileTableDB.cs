﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WowonderPhone.SQLite.Tables
{
   public class LoginUserProfileTableDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string UserID { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string avatar { get; set; }
        public string cover { get; set; }
        public string relationship_id { get; set; }
        public string address { get; set; }
        public string working { get; set; }
        public string working_link { get; set; }
        public string about { get; set; }
        public string school { get; set; }
        public string gender { get; set; }
        public string birthday { get; set; }
        public string website { get; set; }
        public string facebook { get; set; }
        public string google { get; set; }
        public string twitter { get; set; }
        public string linkedin { get; set; }
        public string youtube { get; set; }
        public string vk { get; set; }
        public string instagram { get; set; }
        public string language { get; set; }
        public string ip_address { get; set; }
        public string follow_privacy { get; set; }
        public string friend_privacy { get; set; }
        public string post_privacy { get; set; }
        public string message_privacy { get; set; }
        public string confirm_followers { get; set; }
        public string show_activities_privacy { get; set; }
        public string birth_privacy { get; set; }
        public string visit_privacy { get; set; }
        public string verified { get; set; }
        public string lastseen { get; set; }
        public string showlastseen { get; set; }
        public string e_sentme_msg { get; set; }
        public string e_last_notif { get; set; }
        public string status { get; set; }
        public string active { get; set; }
        public string admin { get; set; }
        public string registered { get; set; }
        public string phone_number { get; set; }
        public string is_pro { get; set; }
        public string pro_type { get; set; }
        public string joined { get; set; }
        public string timezone { get; set; }
        public string referrer { get; set; }
        public string balance { get; set; }
        public string paypal_email { get; set; }
        public string notifications_sound { get; set; }
        public string order_posts_by { get; set; }
        public string social_login { get; set; }
        public string device_id { get; set; }
        public string web_device_id { get; set; }
        public string wallet { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string last_location_update { get; set; }
        public string share_my_location { get; set; }
        public string url { get; set; }
        public string name { get; set; }
        public string is_following { get; set; }
        public string can_follow { get; set; }
        public string post_count { get; set; }
        public string gender_text { get; set; }
        public string lastseen_time_text { get; set; }
        public string is_blocked { get; set; }
        public string following_number { get; set; }
        public string followers_number { get; set; }
        public string Pages_Total { get; set; }
        public string Groups_Total { get; set; }

    }
}
