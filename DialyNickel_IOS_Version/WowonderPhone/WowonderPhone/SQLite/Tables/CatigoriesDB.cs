﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace WowonderPhone.SQLite.Tables
{
    public class CatigoriesDB
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Catigories_Id { get; set; }
        public string Catigories_Name { get; set; }


        public override string ToString()
        {
            return string.Format("Catigories_Id : {0}, Catigories_Name : {1} ", Catigories_Id, Catigories_Name);
        }
    }
}
