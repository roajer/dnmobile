﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;
using System.IO;

namespace WowonderPhone.SQLite
{
    class PrivacyFunctions : IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }
        public PrivacyFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "PrivacyDB.db3"));
            Connection.CreateTable<PrivacyDB>();
        }


        public void InsertPrivacyDBCredentials(PrivacyDB PrivacyDB)
        {
            Connection.Insert(PrivacyDB);
        }
        public void UpdatePrivacyDBCredentials(PrivacyDB PrivacyDB)
        {
            var PRV = Connection.Table<PrivacyDB>().FirstOrDefault(c => c.UserID == PrivacyDB.UserID);
            PRV.UserID = PrivacyDB.UserID;
            PRV.WhoCanFollowMe = PrivacyDB.WhoCanFollowMe;
            PRV.WhoCanMessageMe = PrivacyDB.WhoCanMessageMe;
            PRV.WhoCanSeeMyBirday = PrivacyDB.WhoCanSeeMyBirday;
            Connection.Update(PRV);
        }
        public void DeletePrivacyDBRow(PrivacyDB PrivacyDB)
        {
            Connection.Delete(PrivacyDB);
        }
        public void DeletePrivacyDBCredential(string userid)
        {
            var asd = Connection.Table<PrivacyDB>().FirstOrDefault(c => c.UserID == userid);

            Connection.Delete(asd);
        }
        public void ClearPrivacyDBCredentialsList()
        {
            Connection.DeleteAll<PrivacyDB>();
        }

        public PrivacyDB GetPrivacyDBCredentialsById(string ID)
        {
            return Connection.Table<PrivacyDB>().FirstOrDefault(c => c.UserID == ID);

        }
    }
}
