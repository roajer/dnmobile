﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
    class NotifiFunctions :IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }

        public NotifiFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "NotifiDB.db3"));
            Connection.CreateTable<NotifiDB>();
        }

        public void InsertNotifiDBCredentials(NotifiDB NotifiDB)
        {
            Connection.Insert(NotifiDB);
        }
        public void UpdateNotifiDBCredentials(NotifiDB NotifiDB)
        {
            Connection.Update(NotifiDB);
        }
        public void DeletePrivacyDBRow(NotifiDB NotifiDB)
        {
            Connection.Delete(NotifiDB);
        }
        public void DeleteNotifiDBCredential(int Id)
        {
            var asd = Connection.Table<NotifiDB>().FirstOrDefault(c => c.ID == Id);

            Connection.Delete(asd);
        }
        public void ClearNotifiDBCredentialsList()
        {
            Connection.DeleteAll<NotifiDB>();
        }

        public NotifiDB GetNotifiDBCredentialsById(int ID)
        {
            return Connection.Table<NotifiDB>().FirstOrDefault(c => c.messageid == ID);

        }
    }
}
