﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
    class LoginFunctions:IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }

        public LoginFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "LoginTableDB.db3"));
            Connection.CreateTable<LoginTableDB>();

        }

        public void InsertLoginCredentials(LoginTableDB Credentials)
        {
            Connection.Insert(Credentials);
        }
        public void UpdateLoginCredentials(LoginTableDB Credentials)
        {
            Connection.Update(Credentials);
        }
        public void DeleteLoginCredential(string Session)
        {
            var asd = Connection.Table<LoginTableDB>().FirstOrDefault(c => c.Session == Session);

            Connection.Delete(asd);
        }
        public void ClearLoginCredentialsList()
        {
            Connection.DeleteAll<LoginTableDB>();
        }

        public LoginTableDB GetLoginCredentials(string ID)
        {
            return Connection.Table<LoginTableDB>().FirstOrDefault(c => c.Status == ID);

        }

        public LoginTableDB GetLoginCredentialsByUserID(string ID)
        {
            return Connection.Table<LoginTableDB>().FirstOrDefault(c => c.UserID == ID);

        }
        public string GetLoginCredentialsStatus()
        {
            var Status = Connection.Table<LoginTableDB>().FirstOrDefault(c => c.Status == "Active" || c.Status == "Registered");
            var result = "NoSession";
            if (Status == null) { return result; }
            if (Status.Status != "Active") { result = "NoSession"; }
            else if (Status.Status == "Active") { result = "Active"; }

            if (Status.Status == "Registered")
            {
                result = "Registered";
            }
            else if (Status.Status != "Registered" && Status.Status != "Active")
            {
                result = "NoSession";
            }
            return result;
        }

        public List<LoginTableDB> GetLoginCredentialsList()
        {

            return Connection.Table<LoginTableDB>().ToList();
        }

    }
}
