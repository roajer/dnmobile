﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
   public class NearMe_FilterFunction : IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }
        public NearMe_FilterFunction()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "NearmeDB.db3"));
            Connection.CreateTable<Near_meDB>();
        }


        public void InsertNearmeDBCredentials(Near_meDB Near_DB)
        {
            Connection.Insert(Near_DB);
        }
        public void UpdateNearmeDBCredentials(Near_meDB Near_eDB)
        {
            var PRV = Connection.Table<Near_meDB>().FirstOrDefault(c => c.UserID == Near_eDB.UserID);
            PRV.UserID = Near_eDB.UserID;
            PRV.DistanceValue = Near_eDB.DistanceValue;
            PRV.Gender = Near_eDB.Gender;
            PRV.Status = Near_eDB.Status;
            Connection.Update(PRV);
        }


        public Near_meDB SelecteNearmeDBCredentials()
        {
            var PRV = Connection.Table<Near_meDB>().FirstOrDefault(c => c.UserID == Settings.User_id);

            return PRV;
        }

    }
}
