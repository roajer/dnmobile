﻿using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WowonderPhone.Classes;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Pages;
using WowonderPhone.Pages.Timeline_Pages.AddPostNavPages;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
    class ContactsFunctions : IDisposable
    {
        private SQLiteConnection Connection;
        public void Dispose()
        {
            Connection.Dispose();
        }
        public ContactsFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "ContactsTableDB.db3"));
            Connection.CreateTable<ContactsTableDB>();
        }
        public void InsertContactUsers(ContactsTableDB ContactsTable)
        {
            Connection.Insert(ContactsTable);
        }
        public void InsertContactUsers(List<ContactsTableDB> ContactsTable)
        {
            Connection.Insert(ContactsTable);
        }
        public void UpdateContactUsers(ContactsTableDB ContactsTable)
        {
            Connection.Update(ContactsTable);
        }
        public void DeleteContactRow(ContactsTableDB ContactsTable)
        {
            Connection.Delete(ContactsTable);
        }
        public void ClearContactTable()
        {
            Connection.DeleteAll<ContactsTableDB>();
        }

        public ContactsTableDB GetContactUser(string ID)
        {
            return Connection.Table<ContactsTableDB>().FirstOrDefault(c => c.UserID == ID );
        }

        public ContactsTableDB GetContactUserByUsername(string Username)
        {
            return Connection.Table<ContactsTableDB>().FirstOrDefault(c => c.Username == Username);
        }

        public ObservableCollection<UserContacts> GetContactCacheList()
        {
            try
            {
                var CachedContactlist = Functions.ChatContactsList;
                CachedContactlist.Clear();

                foreach (var Item in Connection.Table<ContactsTableDB>().ToList())
                {
                    CachedContactlist.Add(new UserContacts()
                    {
                        Username = Item.Username,
                        profile_picture = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Item.ProfilePicture, Item.UserID)),
                        TextMessage = Item.TextMessage,
                        LastMessageDateTime = Item.LastMessageDateTime,
                        Name = Item.Name,
                        UserID = Item.UserID,
                        Platform = Item.Platform,
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor
                    });

                }
                return CachedContactlist;
            }
            catch
            {
                return null;
            }
        }


        public ObservableCollection<UserContacts> GetContactTagSearchList(string name)
        {
            try
            {
              
                if (Functions.ChatContactsList.Count > 0)
                {
                    Functions.ChatContactsList.Clear();
                }
                else
                {
                    
                }

                foreach (var Itemss in Connection.Table<ContactsTableDB>().ToList().Where(a => a.Name.ToLower().Contains(name)))
                {
                    Functions.ChatContactsList.Add(new UserContacts()
                    {
                        Username = Itemss.Username,
                        profile_picture = ImageSource.FromFile( DependencyService.Get<IPicture>() .GetPictureFromDisk(Itemss.ProfilePicture, Itemss.UserID)),
                        TextMessage = Itemss.TextMessage,
                        LastMessageDateTime = Itemss.LastMessageDateTime,
                        Checkicon = "\uf1db",
                        CheckiconColor = Settings.MainColor,
                        UserID = Itemss.UserID,
                        Name = Itemss.Name,

                    });
                }

                return Functions.ChatContactsList;
            }
            catch
            {              
                return null;
            }
        }

        public void DeletAllChatUsersList()
        {
            Connection.DeleteAll<ContactsTableDB>();
            foreach (var contact in Connection.Table<ContactsTableDB>().ToList())
            {
                Connection.Delete(contact);
            }
        }
    }
}
