﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
     class MessagesFunctions : IDisposable
    {
        private SQLiteConnection Connection;

        public void Dispose()
        {
            Connection.Dispose();
        }

        public MessagesFunctions()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "MessagesDB.db3"));
            Connection.CreateTable<MessagesDB>();
        }

        public void InsertMessage(MessagesDB MessagesDB)
        {

            Connection.Insert(MessagesDB);

        }

        public string CheckMessage(string message_id)
        {
            try
            {
                var ss =
                    Connection.ExecuteScalar<int>("SELECT COUNT(ID) FROM MessagesDB WHERE message_id =" + message_id);
                if (ss == 0)
                {
                    return "0";
                }
                return "1";
            }
            catch (Exception)
            {
                return "1";
            }

        }

        public void UpdateMessage(MessagesDB MessagesDB)
        {
            Connection.Update(MessagesDB);
        }

        public void DeleteMessage(string from_id, string to_id)
        {
            try
            {
              Connection.Query<MessagesDB>("Delete FROM MessagesDB WHERE ((from_id =" + from_id + " and to_id=" + to_id + ") OR (from_id =" + to_id + " and to_id=" + from_id + "))");
            }
            catch (Exception)
            {
            }
        }
        public void ClearMessageList()
        {
            Connection.DeleteAll<MessagesDB>();
        }

        public MessagesDB GetMessages(string ID)
        {
            return Connection.Table<MessagesDB>().FirstOrDefault(c => c.from_id == ID);

        }
        public MessagesDB GetMessagesbyMessageID(int ID)
        {
            return Connection.Table<MessagesDB>().FirstOrDefault(c => c.message_id == ID);

        }
        public string GetMessages()
        {
            
            return "";
        }

        public IEnumerable<MessagesDB> GetMessageList(string from_id , string to_id, string before_message_id)
        {
            try
            {
                var before_q = "";
                if (before_message_id != "0")
                {
                    before_q = "AND message_id < " + before_message_id + " AND message_id <> " + before_message_id + " ";
                }
                
                var ss1 = Connection.Query<MessagesDB>("SELECT * FROM MessagesDB WHERE ((from_id =" + from_id + " and to_id=" + to_id + ") OR (from_id =" + to_id + " and to_id=" + from_id + ")) " + before_q);

                var query_limit_from = ss1.Count - 25;
                if (query_limit_from < 1) {
                    query_limit_from = 0;
                }

                var Query = "SELECT * FROM MessagesDB WHERE ((from_id =" + from_id + " and to_id=" + to_id +
                          ") OR (from_id =" + to_id + " and to_id=" + from_id + ")) " + before_q +
                          " ORDER BY message_id ASC LIMIT " + query_limit_from + ", 25";

                var Result = Connection.Query<MessagesDB>(Query);
               
                return Result;
            }
            catch (Exception)
            {
                return null;
            }
           
        }
    }
}
