﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using SQLite.Net;
using WowonderPhone.Classes;
using WowonderPhone.Dependencies;
using WowonderPhone.Pages.Timeline_Pages.DefaultPages;
using WowonderPhone.SQLite.Tables;
using Xamarin.Forms;

namespace WowonderPhone.SQLite
{
    class CommunitiesFunction : IDisposable
    {
        private SQLiteConnection Connection;

        public void Dispose()
        {
            Connection.Dispose();
        }

        public CommunitiesFunction()
        {
            var config = DependencyService.Get<SQLiteData>();
            Connection = new SQLiteConnection(config.Platform, Path.Combine(config.DirectoriDB, "CommunityDB.db3"));
            Connection.CreateTable<CommunitiesDB>();
        }

        public void InsertCommunities(CommunitiesDB CommunitiesDB)
        {
            try
            {
                var Cheker = Connection.Table<CommunitiesDB>().FirstOrDefault(c => c.CommunityID == CommunitiesDB.CommunityID);
                if (Cheker == null)
                {
                    Connection.Insert(CommunitiesDB);
                }
                else
                {
                    Connection.Update(CommunitiesDB);
                }
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                Connection.Insert(CommunitiesDB);
            }

        }

        public void InsertCommunities(List<CommunitiesDB> CommunitiesDB)
        {
            Connection.Insert(CommunitiesDB);
        }

        public void UpdateCommunities(CommunitiesDB CommunitiesDB)
        {
            Connection.Update(CommunitiesDB);
        }

        public void DeleteCommunitiesRow(CommunitiesDB CommunitiesDB)
        {
            Connection.Delete(CommunitiesDB);
        }

        public void ClearCommunitiesTable()
        {
            Connection.DeleteAll<CommunitiesDB>();
        }

        public CommunitiesDB GetCommunityByID(string ID)
        {
            return Connection.Table<CommunitiesDB>().FirstOrDefault(c => c.CommunityID == ID);
        }

        public CommunitiesDB GetCommunityBy(string Name)
        {
            return Connection.Table<CommunitiesDB>().FirstOrDefault(c => c.CommunityName == Name);
        }

        public ObservableCollection<Communities> GetPagesCacheList()
        {
            try
            {
                var CachedCommunitieslist = CommunitiesPage.CommunitiesListItems;
                CachedCommunitieslist.Clear();
                var ff = Connection.Table<CommunitiesDB>().Where(a => a.CommunityType == "Pages").ToList();
                foreach (var Item in ff)
                {

                    var ss =
                        ImageSource.FromFile(DependencyService.Get<IPicture>()
                            .GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID));
                    CachedCommunitieslist.Add(new Communities()
                    {
                        CommunityID = Item.CommunityID,
                        CommunityName = Item.CommunityName,
                        CommunityType = Item.CommunityType,
                        CommunityTypeLabel = Item.CommunityTypeLabel,
                        CommunityPicture = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID)),
                    });
                }


                return CachedCommunitieslist;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ObservableCollection<Communities> GetGroupsCacheList()
        {
            try
            {
                var CachedCommunitieslist = GroupsListPage.GroupsListItems;
                CachedCommunitieslist.Clear();
                var ff = Connection.Table<CommunitiesDB>().Where(a => a.CommunityType == "Groups").ToList();
                foreach (var Item in ff)
                {

                    var ss =
                        ImageSource.FromFile(DependencyService.Get<IPicture>()
                            .GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID));
                    CachedCommunitieslist.Add(new Communities()
                    {
                        CommunityID = Item.CommunityID,
                        CommunityName = Item.CommunityName,
                        CommunityType = Item.CommunityType,
                        CommunityTypeLabel = Item.CommunityTypeLabel,
                        CommunityPicture = ImageSource.FromFile(DependencyService.Get<IPicture>()
                            .GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID)),
                    });
                }
                return CachedCommunitieslist;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public ObservableCollection<Communities> GetGroupSearchList(string name)
        {
            try
            {
                var SearchContactlist = GroupsListPage.GroupsListItems;

                var ff = Connection.Table<CommunitiesDB>().Where(a => a.CommunityType == "Groups").ToList();

                if (SearchContactlist.Count > 0)
                {
                    SearchContactlist.Clear();
                    foreach (var Item in ff.ToList().Where(a => a.CommunityName.ToLower().Contains(name)))
                    {
                        SearchContactlist.Add(new Communities()
                        {
                            CommunityID = Item.CommunityID,
                            CommunityName = Item.CommunityName,
                            CommunityType = Item.CommunityType,
                            CommunityTypeLabel = Item.CommunityTypeLabel,
                            CommunityPicture = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID)),
                        });
                    }
                }
                else
                {
                    foreach (var Item in ff.ToList().OrderByDescending(a => a.CommunityName))
                    {
                        var ss =
                            ImageSource.FromFile(DependencyService.Get<IPicture>()
                                .GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID));
                        SearchContactlist.Add(new Communities()
                        {
                            CommunityID = Item.CommunityID,
                            CommunityName = Item.CommunityName,
                            CommunityType = Item.CommunityType,
                            CommunityTypeLabel = Item.CommunityTypeLabel,
                            CommunityPicture = ImageSource.FromFile(DependencyService.Get<IPicture>()
                                .GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID)),
                        });
                    }
                }
                return SearchContactlist;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }

        }

        public ObservableCollection<Communities> GetCommunitySearchList(string name)
        {
            try
            {
                var SearchContactlist = CommunitiesPage.CommunitiesListItems;
                var ff = Connection.Table<CommunitiesDB>().Where(a => a.CommunityType == "Pages").ToList();
                if (SearchContactlist.Count > 0)
                {
                    SearchContactlist.Clear();

                    foreach (var Item in ff.ToList().Where(a => a.CommunityName.ToLower().Contains(name)))
                    {
                        SearchContactlist.Add(new Communities()
                        {
                            CommunityID = Item.CommunityID,
                            CommunityName = Item.CommunityName,
                            CommunityType = Item.CommunityType,
                            CommunityTypeLabel = Item.CommunityTypeLabel,
                            CommunityPicture = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID)),
                        });
                    }
                }
                else
                {
                    foreach (var Item in ff.ToList().OrderByDescending(a => a.CommunityName))
                    {

                        var ss =
                            ImageSource.FromFile(DependencyService.Get<IPicture>()
                                .GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID));
                        SearchContactlist.Add(new Communities()
                        {
                            CommunityID = Item.CommunityID,
                            CommunityName = Item.CommunityName,
                            CommunityType = Item.CommunityType,
                            CommunityTypeLabel = Item.CommunityTypeLabel,
                            CommunityPicture = ImageSource.FromFile(DependencyService.Get<IPicture>().GetPictureFromDisk(Item.CommunityPicture, Item.CommunityID)),
                        });
                    }
                }
                return SearchContactlist;
            }
            catch (Exception ex)
            {
                  System.Diagnostics.Debug.WriteLine("==========================================\n\n Error \n\n"+ ex);
                return null;
            }

        }

        public void DeletAllCommunityList()
        {

            Connection.DeleteAll<CommunitiesDB>();
            foreach (var Activity in Connection.Table<CommunitiesDB>())
            {
                Connection.Delete(Activity);
            }
        }
    }
}
