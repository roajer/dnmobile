﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Acr.UserDialogs;
using FFImageLoading.Forms.Touch;
using Foundation;
using ImageCircle.Forms.Plugin.iOS;
using Plugin.Media;
using UIKit;
using UserNotifications;
using UXDivers.Artina.Grial;
using Xamarin.Forms.Platform.iOS;
using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services.Geolocation;

namespace WowonderPhone.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : FormsApplicationDelegate
    {
        #region Computed Properties
        public override UIWindow Window { get; set; }
		//public SoundRecord AudioManager { get; set; } = new SoundRecord();
		#endregion

		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            bool appBool =false;
            try
            {

                global::Xamarin.Forms.Forms.Init();
                var settings = UIUserNotificationSettings.GetSettingsForTypes(UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
                UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

             

                var resolverContainer = new SimpleContainer();
                resolverContainer.Register<IDevice>(t => AppleDevice.CurrentDevice)
                                 .Register<IDisplay>(t => t.Resolve<IDevice>().Display)
                                 .Register<IDependencyContainer>(t => resolverContainer);

                Resolver.ResetResolver();
                Resolver.SetResolver(resolverContainer.GetResolver());

                var workaround = typeof(UXDivers.Artina.Shared.CircleImage);
                
                CrossMedia.Current.Initialize();
                ImageCircleRenderer.Init();
                Appearance.Configure();
                CachedImageRenderer.Init();
                var xamApp = new App();
                LoadApplication(xamApp);

                 appBool = base.FinishedLaunching(app, options);
            }
            catch(Exception ex)
            {
                Debug.WriteLine(" Error ==> FinishedLaunching() ===> " + ex);
            }
            return appBool;
        }
		#region Override Methods
      
		public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
		{
			// show an alert
			UIAlertController okayAlertController = UIAlertController.Create(notification.AlertAction, notification.AlertBody, UIAlertControllerStyle.Alert);
			okayAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

			Window.RootViewController.PresentViewController(okayAlertController, true, null);

			// reset our badge
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
            base.ReceivedLocalNotification(application, notification);

        }

		public override void OnResignActivation(UIApplication application)
		{
            base.OnResignActivation(application);
        }

		public override void DidEnterBackground(UIApplication application)
		{
            //AudioManager.SuspendBackgroundMusic();
            //AudioManager.DeactivateAudioSession();
            base.DidEnterBackground(application);
        }

		public override void WillEnterForeground(UIApplication application)
		{
            //AudioManager.ReactivateAudioSession();
            //AudioManager.RestartBackgroundMusic();
            base.WillEnterForeground(application);
        }

		public override void OnActivated(UIApplication application)
		{
            base.OnActivated(application);

        }

		public override void WillTerminate(UIApplication application)
		{
            //AudioManager.StopBackgroundMusic();
            //AudioManager.DeactivateAudioSession();
            base.WillTerminate(application);

        }
		#endregion
	}

	

	public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
	{
		#region Constructors
		public UserNotificationCenterDelegate()
		{
		}
		#endregion

		#region Override Methods
		public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
		{
			// Do something with the notification
			Console.WriteLine("Active Notification: {0}", notification);

			completionHandler(UNNotificationPresentationOptions.Alert);
		}

		public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
		{
			// Take action based on Action ID
			switch (response.ActionIdentifier)
			{
				case "reply":
					// Do something
					break;
				default:
					// Take action based on identifier
					switch (response.ActionIdentifier)
					{
						case "":
                    // Handle default
                  
                    break;
						case "dfd":
                    // Handle dismiss
               
                    break;
					}
					break;
			}

			// Inform caller it has been handled
			completionHandler();
		}
		#endregion
	}
}
