﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using Acr.UserDialogs;
using Com.OneSignal;
using Com.OneSignal.Abstractions;
using WowonderPhone.Controls;
using WowonderPhone.Dependencies;
using WowonderPhone.Languish;
using WowonderPhone.Pages;
using WowonderPhone.Pages.Register_pages;
using WowonderPhone.SQLite;
using Xamarin.Forms;
using XLabs.Forms.Services;

namespace WowonderPhone
{
    public partial class App : Application
    {

        public static INavigation Navigation { get; set; }

        public App()
        {
            //if (Page == "messenger")
            //{
            //    MainPage = new NavigationPage(new ChatWindow(Page, "454545"));
            //    UserDialogs.Instance.Alert(Page);
            //    return;
            //}

            //UserDialogs.Instance.Alert(Page);
            try
            {
                InitializeComponent();
                DependencyService.Register<ILocale>();
                L10n.SetLocale();
                var netLanguage = DependencyService.Get<ILocale>().GetCurrent();
                AppResources.Culture = new CultureInfo(netLanguage);

                SQL_Entity.Connect();
                
                //Data.ClearLoginCredentialsList();
                var CredentialStatus = SQL_Commander.GetLoginCredentialsStatus();
                

                if (CredentialStatus == "Active")
                {
                    var Credential = SQL_Commander.GetLoginCredentials("Active");
                    Settings.Session = Credential.Session;
                    Settings.User_id = Credential.UserID;
                    Settings.Username = Credential.Username;
                    Settings.Onesignal_APP_ID = Credential.Onesignal_APP_ID;
                    if (Credential.NotificationLedColor != "")
                    {

                        Settings.NotificationVibrate = Credential.NotificationVibrate;
                        Settings.NotificationSound = Credential.NotificationSound;
                        Settings.NotificationPopup = Credential.NotificationPopup;
                        Settings.NotificationLedColor = Credential.NotificationLedColor;
                        Settings.NotificationLedColorName = Credential.NotificationLedColor;

                    }
                    else
                    {
                        Credential.NotificationVibrate = true;
                        Credential.NotificationLedColor = Settings.MainColor;
                        Credential.NotificationLedColorName = AppResources.Label_Led_Color;
                        Credential.NotificationSound = true;
                        Credential.NotificationPopup = true;
                        SQL_Commander.UpdateLoginCredentials(Credential);
                        Settings.NotificationVibrate = true;
                        Settings.NotificationSound = true;
                        Settings.NotificationPopup = true;
                        Settings.NotificationLedColor = Settings.MainColor;
                        Settings.NotificationLedColorName = AppResources.Label_Led_Color;
                    }


                    //Start Onesignal
                    OneSignalNotificationController.RegisterNotificationDevice();
                    var navigationPage = new NavigationPage(new MasterMain()) { };
                    navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
                    navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
                    navigationPage.Title = Settings.MainPage_HeaderTextLabel;
                    navigationPage.Padding = new Thickness(0, 0, 0, 0);
                    MainPage = navigationPage;


                }
                else
                {
                    if (CredentialStatus == "Registered")
                    {
                        var Credential = SQL_Commander.GetLoginCredentials("Registered");
                        Settings.Session = Credential.Session;
                        Settings.User_id = Credential.UserID;
                        Settings.Username = Credential.Username;
                        MainPage = new NavigationPage(new UploudPicPage());

                    }
                    else
                    {

                        MainPage = new NavigationPage(new WelcomePage());

                    }

                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine("Error => App() ===> "+ex);
            }

        }
        public static void GetMainPage()
        {
            try
            {
                var navigationPage = new NavigationPage(new MasterMain()) { };
                navigationPage.BarBackgroundColor = Color.FromHex(Settings.MainPage_HeaderBackround_Color);
                navigationPage.BarTextColor = Color.FromHex(Settings.MainPage_HeaderText_Color);
                navigationPage.Title = Settings.MainPage_HeaderTextLabel;
                navigationPage.Padding = new Thickness(0, 0, 0, 0);
                App.Current.MainPage = navigationPage;
            }
            catch
            {
                App.Current.MainPage = new MasterMain();
            }

        }


        public static void GetRegisterPage()
        {

            App.Current.MainPage = new NavigationPage(new RegisterFriends());
        }

        public static void GetLoginPage()
        {
            App.Current.MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            base.OnStart();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            base.OnSleep();
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            base.OnResume();
        }


    }
}
