﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WoWonder.ProDevCode
{
        public partial class ActivitiesObject
        {
            [JsonProperty("api_status")]
            public long ApiStatus { get; set; }
             
            [JsonProperty("activities")]
            public List<mActivity> Activities { get; set; }
        }

        public partial class mActivity
        {
            [JsonProperty("id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Id { get; set; }

            [JsonProperty("user_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long UserId { get; set; }

            [JsonProperty("post_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostId { get; set; }

            [JsonProperty("reply_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ReplyId { get; set; }

            [JsonProperty("comment_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long CommentId { get; set; }

            [JsonProperty("follow_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowId { get; set; }

            [JsonProperty("activity_type")]
            public string ActivityType { get; set; }

            [JsonProperty("time")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Time { get; set; }

            [JsonProperty("postData")]
            public PostData PostData { get; set; }

            [JsonProperty("activator")]
            public Activator Activator { get; set; }

            [JsonProperty("activity_text")]
            public string ActivityText { get; set; }
        }

        public partial class Activator
        {
            [JsonProperty("user_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long UserId { get; set; }

            [JsonProperty("username")]
            public UsernameEnum Username { get; set; }

            [JsonProperty("email")]
            public Email Email { get; set; }

            [JsonProperty("first_name")]
            public FirstName FirstName { get; set; }

            [JsonProperty("last_name")]
            public LastName LastName { get; set; }

            [JsonProperty("avatar")]
            public Uri Avatar { get; set; }

            [JsonProperty("cover")]
            public Uri Cover { get; set; }

            [JsonProperty("background_image")]
            public string BackgroundImage { get; set; }

            [JsonProperty("relationship_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long RelationshipId { get; set; }

            [JsonProperty("address")]
            public Address Address { get; set; }

            [JsonProperty("working")]
            public ActivatorWorking Working { get; set; }

            [JsonProperty("working_link")]
            public Uri WorkingLink { get; set; }

            [JsonProperty("about")]
            public ActivatorAbout About { get; set; }

            [JsonProperty("school")]
            public ActivatorSchool School { get; set; }

            [JsonProperty("gender")]
            public Gender Gender { get; set; }

            [JsonProperty("birthday")]
            public Birthday Birthday { get; set; }

            [JsonProperty("country_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long CountryId { get; set; }

            [JsonProperty("website")]
            public Uri Website { get; set; }

            [JsonProperty("facebook")]
            public UsernameEnum Facebook { get; set; }

            [JsonProperty("google")]
            public string Google { get; set; }

            [JsonProperty("twitter")]
            public string Twitter { get; set; }

            [JsonProperty("linkedin")]
            public string Linkedin { get; set; }

            [JsonProperty("youtube")]
            public Youtube Youtube { get; set; }

            [JsonProperty("vk")]
            public string Vk { get; set; }

            [JsonProperty("instagram")]
            public string Instagram { get; set; }

            [JsonProperty("language")]
            public Language Language { get; set; }

            [JsonProperty("ip_address")]
            public IpAddress IpAddress { get; set; }

            [JsonProperty("follow_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowPrivacy { get; set; }

            [JsonProperty("friend_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FriendPrivacy { get; set; }

            [JsonProperty("post_privacy")]
            public PostPrivacy PostPrivacy { get; set; }

            [JsonProperty("message_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long MessagePrivacy { get; set; }

            [JsonProperty("confirm_followers")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ConfirmFollowers { get; set; }

            [JsonProperty("show_activities_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ShowActivitiesPrivacy { get; set; }

            [JsonProperty("birth_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long BirthPrivacy { get; set; }

            [JsonProperty("visit_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long VisitPrivacy { get; set; }

            [JsonProperty("verified")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Verified { get; set; }

            [JsonProperty("lastseen")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Lastseen { get; set; }

            [JsonProperty("emailNotification")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EmailNotification { get; set; }

            [JsonProperty("e_liked")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ELiked { get; set; }

            [JsonProperty("e_wondered")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EWondered { get; set; }

            [JsonProperty("e_shared")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EShared { get; set; }

            [JsonProperty("e_followed")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EFollowed { get; set; }

            [JsonProperty("e_commented")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ECommented { get; set; }

            [JsonProperty("e_visited")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EVisited { get; set; }

            [JsonProperty("e_liked_page")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ELikedPage { get; set; }

            [JsonProperty("e_mentioned")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EMentioned { get; set; }

            [JsonProperty("e_joined_group")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EJoinedGroup { get; set; }

            [JsonProperty("e_accepted")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EAccepted { get; set; }

            [JsonProperty("e_profile_wall_post")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EProfileWallPost { get; set; }

            [JsonProperty("e_sentme_msg")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ESentmeMsg { get; set; }

            [JsonProperty("e_last_notif")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ELastNotif { get; set; }

            [JsonProperty("notification_settings")]
            public string NotificationSettings { get; set; }

            [JsonProperty("status")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Status { get; set; }

            [JsonProperty("active")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Active { get; set; }

            [JsonProperty("admin")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Admin { get; set; }

            [JsonProperty("registered")]
            public Registered Registered { get; set; }

            [JsonProperty("phone_number")]
            public string PhoneNumber { get; set; }

            [JsonProperty("is_pro")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long IsPro { get; set; }

            [JsonProperty("pro_type")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ProType { get; set; }

            [JsonProperty("timezone")]
            public ActivatorTimezone Timezone { get; set; }

            [JsonProperty("referrer")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Referrer { get; set; }

            [JsonProperty("balance")]
            public string Balance { get; set; }

            [JsonProperty("paypal_email")]
            public string PaypalEmail { get; set; }

            [JsonProperty("notifications_sound")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long NotificationsSound { get; set; }

            [JsonProperty("order_posts_by")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long OrderPostsBy { get; set; }

            [JsonProperty("device_id")]
            public Guid DeviceId { get; set; }

            [JsonProperty("web_device_id")]
            public string WebDeviceId { get; set; }

            [JsonProperty("wallet")]
            public string Wallet { get; set; }

            [JsonProperty("lat")]
            public string Lat { get; set; }

            [JsonProperty("lng")]
            public string Lng { get; set; }

            [JsonProperty("last_location_update")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastLocationUpdate { get; set; }

            [JsonProperty("share_my_location")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ShareMyLocation { get; set; }

            [JsonProperty("last_data_update")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastDataUpdate { get; set; }

            [JsonProperty("details")]
            public ActivatorDetails Details { get; set; }

            [JsonProperty("last_avatar_mod")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastAvatarMod { get; set; }

            [JsonProperty("last_cover_mod")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastCoverMod { get; set; }

            [JsonProperty("points")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Points { get; set; }

            [JsonProperty("last_follow_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastFollowId { get; set; }

            [JsonProperty("share_my_data")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ShareMyData { get; set; }

            [JsonProperty("last_login_data")]
            public string LastLoginData { get; set; }

            [JsonProperty("two_factor")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long TwoFactor { get; set; }

            [JsonProperty("avatar_full")]
            public string AvatarFull { get; set; }

            [JsonProperty("url")]
            public Uri Url { get; set; }

            [JsonProperty("name")]
            public Name Name { get; set; }

            [JsonProperty("mutual_friends_data")]
            [JsonConverter(typeof(DecodeArrayConverter))]
            public List<long> MutualFriendsData { get; set; }

            [JsonProperty("lastseen_unix_time")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastseenUnixTime { get; set; }

            [JsonProperty("lastseen_status")]
            public LastseenStatus LastseenStatus { get; set; }
        }

        public partial class ActivatorDetails
        {
            [JsonProperty("post_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostCount { get; set; }

            [JsonProperty("album_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long AlbumCount { get; set; }

            [JsonProperty("following_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowingCount { get; set; }

            [JsonProperty("followers_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowersCount { get; set; }

            [JsonProperty("groups_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long GroupsCount { get; set; }

            [JsonProperty("likes_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LikesCount { get; set; }

            [JsonProperty("mutual_friends_count")]
            public long MutualFriendsCount { get; set; }
        }

        public partial class PostData
        {
            [JsonProperty("id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Id { get; set; }

            [JsonProperty("post_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostId { get; set; }

            [JsonProperty("user_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long UserId { get; set; }

            [JsonProperty("recipient_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long RecipientId { get; set; }

            [JsonProperty("postText")]
            public string PostText { get; set; }

            [JsonProperty("page_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PageId { get; set; }

            [JsonProperty("group_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long GroupId { get; set; }

            [JsonProperty("event_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EventId { get; set; }

            [JsonProperty("page_event_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PageEventId { get; set; }

            [JsonProperty("postLink")]
            public string PostLink { get; set; }

            [JsonProperty("postLinkTitle")]
            public string PostLinkTitle { get; set; }

            [JsonProperty("postLinkImage")]
            public string PostLinkImage { get; set; }

            [JsonProperty("postLinkContent")]
            public string PostLinkContent { get; set; }

            [JsonProperty("postVimeo")]
            public string PostVimeo { get; set; }

            [JsonProperty("postDailymotion")]
            public string PostDailymotion { get; set; }

            [JsonProperty("postFacebook")]
            public string PostFacebook { get; set; }

            [JsonProperty("postFile")]
            public string PostFile { get; set; }

            [JsonProperty("postRecord")]
            public string PostRecord { get; set; }

            [JsonProperty("postSticker")]
            public Uri PostSticker { get; set; }

            [JsonProperty("shared_from")]
            public object SharedFrom { get; set; }

            [JsonProperty("post_url")]
            public object PostUrl { get; set; }

            [JsonProperty("parent_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ParentId { get; set; }

            [JsonProperty("cache")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Cache { get; set; }

            [JsonProperty("comments_status")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long CommentsStatus { get; set; }

            [JsonProperty("postFileName")]
            public string PostFileName { get; set; }

            [JsonProperty("postFileThumb")]
            public string PostFileThumb { get; set; }

            [JsonProperty("postYoutube")]
            public PostYoutube PostYoutube { get; set; }

            [JsonProperty("postVine")]
            public string PostVine { get; set; }

            [JsonProperty("postSoundCloud")]
            public string PostSoundCloud { get; set; }

            [JsonProperty("postPlaytube")]
            public string PostPlaytube { get; set; }

            [JsonProperty("postMap")]
            public string PostMap { get; set; }

            [JsonProperty("postShare")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostShare { get; set; }

            [JsonProperty("postPrivacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostPrivacy { get; set; }

            [JsonProperty("postType")]
            public PostType PostType { get; set; }

            [JsonProperty("postFeeling")]
            public string PostFeeling { get; set; }

            [JsonProperty("postListening")]
            public string PostListening { get; set; }

            [JsonProperty("postTraveling")]
            public string PostTraveling { get; set; }

            [JsonProperty("postWatching")]
            public string PostWatching { get; set; }

            [JsonProperty("postPlaying")]
            public string PostPlaying { get; set; }

            [JsonProperty("postPhoto")]
            public string PostPhoto { get; set; }

            [JsonProperty("time")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Time { get; set; }

            [JsonProperty("registered")]
            public string Registered { get; set; }

            [JsonProperty("album_name")]
            public string AlbumName { get; set; }

            [JsonProperty("multi_image")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long MultiImage { get; set; }

            [JsonProperty("boosted")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Boosted { get; set; }

            [JsonProperty("product_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ProductId { get; set; }

            [JsonProperty("poll_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PollId { get; set; }

            [JsonProperty("blog_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long BlogId { get; set; }

            [JsonProperty("videoViews")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long VideoViews { get; set; }

            [JsonProperty("publisher")]
            public PublisherUnion Publisher { get; set; }

            [JsonProperty("limit_comments")]
            public long LimitComments { get; set; }

            [JsonProperty("limited_comments")]
            public bool LimitedComments { get; set; }

            [JsonProperty("is_group_post")]
            public bool IsGroupPost { get; set; }

            [JsonProperty("group_recipient_exists")]
            public bool GroupRecipientExists { get; set; }

            [JsonProperty("group_admin")]
            public bool GroupAdmin { get; set; }

            [JsonProperty("postText_API")]
            public string PostTextApi { get; set; }

            [JsonProperty("Orginaltext")]
            public string Orginaltext { get; set; }

            [JsonProperty("post_time")]
            public string PostTime { get; set; }

            [JsonProperty("page")]
            public long Page { get; set; }

            [JsonProperty("url")]
            public Uri Url { get; set; }

            [JsonProperty("via_type")]
            public string ViaType { get; set; }

            [JsonProperty("recipient_exists")]
            public bool RecipientExists { get; set; }

            [JsonProperty("recipient")]
            public string Recipient { get; set; }

            [JsonProperty("admin")]
            public bool Admin { get; set; }

            [JsonProperty("is_post_saved")]
            public bool IsPostSaved { get; set; }

            [JsonProperty("is_post_reported")]
            public bool IsPostReported { get; set; }

            [JsonProperty("is_post_boosted")]
            public long IsPostBoosted { get; set; }

            [JsonProperty("is_liked")]
            public bool IsLiked { get; set; }

            [JsonProperty("is_wondered")]
            public bool IsWondered { get; set; }

            [JsonProperty("post_comments")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostComments { get; set; }

            [JsonProperty("post_shares")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostShares { get; set; }

            [JsonProperty("post_likes")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostLikes { get; set; }

            [JsonProperty("post_wonders")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostWonders { get; set; }

            [JsonProperty("is_post_pinned")]
            public bool IsPostPinned { get; set; }

            [JsonProperty("photo_album")]
            public List<object> PhotoAlbum { get; set; }

            [JsonProperty("options")]
            public List<object> Options { get; set; }

            [JsonProperty("postFile_full")]
            public string PostFileFull { get; set; }

            [JsonProperty("group_recipient", NullValueHandling = NullValueHandling.Ignore)]
            public GroupRecipient GroupRecipient { get; set; }

            [JsonProperty("product", NullValueHandling = NullValueHandling.Ignore)]
            public Product Product { get; set; }
        }

        public partial class GroupRecipient
        {
            [JsonProperty("id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Id { get; set; }

            [JsonProperty("user_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long UserId { get; set; }

            [JsonProperty("group_name")]
            public string GroupName { get; set; }

            [JsonProperty("group_title")]
            public string GroupTitle { get; set; }

            [JsonProperty("avatar")]
            public Uri Avatar { get; set; }

            [JsonProperty("cover")]
            public Uri Cover { get; set; }

            [JsonProperty("about")]
            public string About { get; set; }

            [JsonProperty("category")]
            public string Category { get; set; }

            [JsonProperty("privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Privacy { get; set; }

            [JsonProperty("join_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long JoinPrivacy { get; set; }

            [JsonProperty("active")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Active { get; set; }

            [JsonProperty("registered")]
            public string Registered { get; set; }

            [JsonProperty("group_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long GroupId { get; set; }

            [JsonProperty("url")]
            public Uri Url { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("category_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long CategoryId { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }
        }

        public partial class Product
        {
            [JsonProperty("id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Id { get; set; }

            [JsonProperty("user_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long UserId { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("description")]
            public string Description { get; set; }

            [JsonProperty("category")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Category { get; set; }

            [JsonProperty("price")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Price { get; set; }

            [JsonProperty("location")]
            public string Location { get; set; }

            [JsonProperty("status")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Status { get; set; }

            [JsonProperty("type")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Type { get; set; }

            [JsonProperty("currency")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Currency { get; set; }

            [JsonProperty("lng")]
            public string Lng { get; set; }

            [JsonProperty("lat")]
            public string Lat { get; set; }

            [JsonProperty("time")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Time { get; set; }

            [JsonProperty("active")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Active { get; set; }

            [JsonProperty("images")]
            public List<Image> Images { get; set; }

            [JsonProperty("time_text")]
            public string TimeText { get; set; }

            [JsonProperty("post_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostId { get; set; }

            [JsonProperty("edit_description")]
            public string EditDescription { get; set; }

            [JsonProperty("url")]
            public Uri Url { get; set; }
        }

        public partial class Image
        {
            [JsonProperty("id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Id { get; set; }

            [JsonProperty("image")]
            public Uri ImageImage { get; set; }

            [JsonProperty("product_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ProductId { get; set; }

            [JsonProperty("image_org")]
            public Uri ImageOrg { get; set; }
        }

        public partial class PublisherClass
        {
            [JsonProperty("user_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long UserId { get; set; }

            [JsonProperty("username")]
            public string Username { get; set; }

            [JsonProperty("email")]
            public string Email { get; set; }

            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("avatar")]
            public Uri Avatar { get; set; }

            [JsonProperty("cover")]
            public Uri Cover { get; set; }

            [JsonProperty("background_image")]
            public string BackgroundImage { get; set; }

            [JsonProperty("relationship_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long RelationshipId { get; set; }

            [JsonProperty("address")]
            public string Address { get; set; }

            [JsonProperty("working")]
            public WorkingUnion Working { get; set; }

            [JsonProperty("working_link")]
            public string WorkingLink { get; set; }

            [JsonProperty("about")]
            public AboutUnion About { get; set; }

            [JsonProperty("school")]
            public PublisherSchool School { get; set; }

            [JsonProperty("gender")]
            public Gender Gender { get; set; }

            [JsonProperty("birthday")]
            public string Birthday { get; set; }

            [JsonProperty("country_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long CountryId { get; set; }

            [JsonProperty("website")]
            public string Website { get; set; }

            [JsonProperty("facebook")]
            public PublisherFacebook Facebook { get; set; }

            [JsonProperty("google")]
            public string Google { get; set; }

            [JsonProperty("twitter")]
            public string Twitter { get; set; }

            [JsonProperty("linkedin")]
            public string Linkedin { get; set; }

            [JsonProperty("youtube")]
            public string Youtube { get; set; }

            [JsonProperty("vk")]
            public string Vk { get; set; }

            [JsonProperty("instagram")]
            public string Instagram { get; set; }

            [JsonProperty("language")]
            public string Language { get; set; }

            [JsonProperty("ip_address")]
            public string IpAddress { get; set; }

            [JsonProperty("follow_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowPrivacy { get; set; }

            [JsonProperty("friend_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FriendPrivacy { get; set; }

            [JsonProperty("post_privacy")]
            public PostPrivacy PostPrivacy { get; set; }

            [JsonProperty("message_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long MessagePrivacy { get; set; }

            [JsonProperty("confirm_followers")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ConfirmFollowers { get; set; }

            [JsonProperty("show_activities_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ShowActivitiesPrivacy { get; set; }

            [JsonProperty("birth_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long BirthPrivacy { get; set; }

            [JsonProperty("visit_privacy")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long VisitPrivacy { get; set; }

            [JsonProperty("verified")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Verified { get; set; }

            [JsonProperty("lastseen")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Lastseen { get; set; }

            [JsonProperty("emailNotification")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EmailNotification { get; set; }

            [JsonProperty("e_liked")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ELiked { get; set; }

            [JsonProperty("e_wondered")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EWondered { get; set; }

            [JsonProperty("e_shared")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EShared { get; set; }

            [JsonProperty("e_followed")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EFollowed { get; set; }

            [JsonProperty("e_commented")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ECommented { get; set; }

            [JsonProperty("e_visited")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EVisited { get; set; }

            [JsonProperty("e_liked_page")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ELikedPage { get; set; }

            [JsonProperty("e_mentioned")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EMentioned { get; set; }

            [JsonProperty("e_joined_group")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EJoinedGroup { get; set; }

            [JsonProperty("e_accepted")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EAccepted { get; set; }

            [JsonProperty("e_profile_wall_post")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long EProfileWallPost { get; set; }

            [JsonProperty("e_sentme_msg")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ESentmeMsg { get; set; }

            [JsonProperty("e_last_notif")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ELastNotif { get; set; }

            [JsonProperty("notification_settings")]
            public string NotificationSettings { get; set; }

            [JsonProperty("status")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Status { get; set; }

            [JsonProperty("active")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Active { get; set; }

            [JsonProperty("admin")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Admin { get; set; }

            [JsonProperty("registered")]
            public string Registered { get; set; }

            [JsonProperty("phone_number")]
            public PhoneNumberUnion PhoneNumber { get; set; }

            [JsonProperty("is_pro")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long IsPro { get; set; }

            [JsonProperty("pro_type")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ProType { get; set; }

            [JsonProperty("timezone")]
            public PublisherTimezone Timezone { get; set; }

            [JsonProperty("referrer")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Referrer { get; set; }

            [JsonProperty("balance")]
            public string Balance { get; set; }

            [JsonProperty("paypal_email")]
            public string PaypalEmail { get; set; }

            [JsonProperty("notifications_sound")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long NotificationsSound { get; set; }

            [JsonProperty("order_posts_by")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long OrderPostsBy { get; set; }

            [JsonProperty("device_id")]
            public string DeviceId { get; set; }

            [JsonProperty("web_device_id")]
            public string WebDeviceId { get; set; }

            [JsonProperty("wallet")]
            public string Wallet { get; set; }

            [JsonProperty("lat")]
            public string Lat { get; set; }

            [JsonProperty("lng")]
            public string Lng { get; set; }

            [JsonProperty("last_location_update")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastLocationUpdate { get; set; }

            [JsonProperty("share_my_location")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ShareMyLocation { get; set; }

            [JsonProperty("last_data_update")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastDataUpdate { get; set; }

            [JsonProperty("details")]
            public PublisherDetails Details { get; set; }

            [JsonProperty("last_avatar_mod")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastAvatarMod { get; set; }

            [JsonProperty("last_cover_mod")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastCoverMod { get; set; }

            [JsonProperty("points")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long Points { get; set; }

            [JsonProperty("last_follow_id")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastFollowId { get; set; }

            [JsonProperty("share_my_data")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long ShareMyData { get; set; }

            [JsonProperty("last_login_data")]
            public string LastLoginData { get; set; }

            [JsonProperty("two_factor")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long TwoFactor { get; set; }

            [JsonProperty("avatar_full", NullValueHandling = NullValueHandling.Ignore)]
            public string AvatarFull { get; set; }

            [JsonProperty("url")]
            public Uri Url { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("mutual_friends_data")]
            public MutualFriendsData MutualFriendsData { get; set; }

            [JsonProperty("lastseen_unix_time")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LastseenUnixTime { get; set; }

            [JsonProperty("lastseen_status")]
            public LastseenStatus LastseenStatus { get; set; }
        }

        public partial class PublisherDetails
        {
            [JsonProperty("post_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long PostCount { get; set; }

            [JsonProperty("album_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long AlbumCount { get; set; }

            [JsonProperty("following_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowingCount { get; set; }

            [JsonProperty("followers_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long FollowersCount { get; set; }

            [JsonProperty("groups_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long GroupsCount { get; set; }

            [JsonProperty("likes_count")]
            [JsonConverter(typeof(ParseStringConverter))]
            public long LikesCount { get; set; }

            [JsonProperty("mutual_friends_count")]
            public MutualFriendsCount MutualFriendsCount { get; set; }
        }

        public enum ActivatorAbout { I039MDeenILoveCoding };

        public enum Address { İstanbulTürkiye };

        public enum Birthday { The1997528 };

        public enum Email { DeendoughouzGmailCom };

        public enum UsernameEnum { Deendoughouz };

        public enum FirstName { Deen };

        public enum Gender { Male };

        public enum IpAddress { The7818141109 };

        public enum Language { English };

        public enum LastName { Doughouz };

        public enum LastseenStatus { Off, On };

        public enum Name { DeenDoughouz };

        public enum PostPrivacy { Everyone, Ifollow, Nobody };

        public enum Registered { The000000 };

        public enum ActivatorSchool { Php };

        public enum ActivatorTimezone { EuropeIstanbul };

        public enum ActivatorWorking { WoWonder };

        public enum Youtube { ChannelUCmJYhKckKQyUgiefU9MA };

        public enum PostType { Post };

        public enum PostYoutube { Empty, The4Ix2J0NqUg };

        public enum AboutAbout { Empty, I039MHereToSampleThisWonderfulPlace, Prova };

        public enum PublisherFacebook { Empty, The960793834062587 };

        public enum PhoneNumberEnum { Empty, The3487518097 };

        public enum PublisherSchool { Empty, UniversidadTecnológicaDeLaVi };

        public enum PublisherTimezone { EuropeRome, Utc };

        public enum WorkingWorking { Empty, TinyBoss, Wowonder };

        public partial struct AboutUnion
        {
            public AboutAbout? Enum;
            public Uri PurpleUri;

            public static implicit operator AboutUnion(AboutAbout Enum) => new AboutUnion { Enum = Enum };
            public static implicit operator AboutUnion(Uri PurpleUri) => new AboutUnion { PurpleUri = PurpleUri };
        }

        public partial struct MutualFriendsCount
        {
            public bool? Bool;
            public long? Integer;

            public static implicit operator MutualFriendsCount(bool Bool) => new MutualFriendsCount { Bool = Bool };
            public static implicit operator MutualFriendsCount(long Integer) => new MutualFriendsCount { Integer = Integer };
        }

        public partial struct MutualFriendsData
        {
            public string String;
            public List<long> StringArray;

            public static implicit operator MutualFriendsData(string String) => new MutualFriendsData { String = String };
            public static implicit operator MutualFriendsData(List<long> StringArray) => new MutualFriendsData { StringArray = StringArray };
        }

        public partial struct PhoneNumberUnion
        {
            public PhoneNumberEnum? Enum;
            public long? Integer;

            public static implicit operator PhoneNumberUnion(PhoneNumberEnum Enum) => new PhoneNumberUnion { Enum = Enum };
            public static implicit operator PhoneNumberUnion(long Integer) => new PhoneNumberUnion { Integer = Integer };
        }

        public partial struct WorkingUnion
        {
            public WorkingWorking? Enum;
            public Uri PurpleUri;

            public static implicit operator WorkingUnion(WorkingWorking Enum) => new WorkingUnion { Enum = Enum };
            public static implicit operator WorkingUnion(Uri PurpleUri) => new WorkingUnion { PurpleUri = PurpleUri };
        }

        public partial struct PublisherUnion
        {
            public List<object> AnythingArray;
            public PublisherClass PublisherClass;

            public static implicit operator PublisherUnion(List<object> AnythingArray) => new PublisherUnion { AnythingArray = AnythingArray };
            public static implicit operator PublisherUnion(PublisherClass PublisherClass) => new PublisherUnion { PublisherClass = PublisherClass };
        }

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters =
            {
                ActivatorAboutConverter.Singleton,
                AddressConverter.Singleton,
                BirthdayConverter.Singleton,
                EmailConverter.Singleton,
                UsernameEnumConverter.Singleton,
                FirstNameConverter.Singleton,
                GenderConverter.Singleton,
                IpAddressConverter.Singleton,
                LanguageConverter.Singleton,
                LastNameConverter.Singleton,
                LastseenStatusConverter.Singleton,
                NameConverter.Singleton,
                PostPrivacyConverter.Singleton,
                RegisteredConverter.Singleton,
                ActivatorSchoolConverter.Singleton,
                ActivatorTimezoneConverter.Singleton,
                ActivatorWorkingConverter.Singleton,
                YoutubeConverter.Singleton,
                PostTypeConverter.Singleton,
                PostYoutubeConverter.Singleton,
                PublisherUnionConverter.Singleton,
                AboutUnionConverter.Singleton,
                AboutAboutConverter.Singleton,
                MutualFriendsCountConverter.Singleton,
                PublisherFacebookConverter.Singleton,
                MutualFriendsDataConverter.Singleton,
                PhoneNumberUnionConverter.Singleton,
                PhoneNumberEnumConverter.Singleton,
                PublisherSchoolConverter.Singleton,
                PublisherTimezoneConverter.Singleton,
                WorkingUnionConverter.Singleton,
                WorkingWorkingConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }

        internal class ActivatorAboutConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(ActivatorAbout) || t == typeof(ActivatorAbout?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "I&#039;m Deen, I love coding !")
                {
                    return ActivatorAbout.I039MDeenILoveCoding;
                }
                throw new Exception("Cannot unmarshal type ActivatorAbout");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (ActivatorAbout)untypedValue;
                if (value == ActivatorAbout.I039MDeenILoveCoding)
                {
                    serializer.Serialize(writer, "I&#039;m Deen, I love coding !");
                    return;
                }
                throw new Exception("Cannot marshal type ActivatorAbout");
            }

            public static readonly ActivatorAboutConverter Singleton = new ActivatorAboutConverter();
        }

        internal class ParseStringConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                long l;
                if (Int64.TryParse(value, out l))
                {
                    return l;
                }
                throw new Exception("Cannot unmarshal type long");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (long)untypedValue;
                serializer.Serialize(writer, value.ToString());
                return;
            }

            public static readonly ParseStringConverter Singleton = new ParseStringConverter();
        }

        internal class AddressConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Address) || t == typeof(Address?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "İstanbul, Türkiye")
                {
                    return Address.İstanbulTürkiye;
                }
                throw new Exception("Cannot unmarshal type Address");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Address)untypedValue;
                if (value == Address.İstanbulTürkiye)
                {
                    serializer.Serialize(writer, "İstanbul, Türkiye");
                    return;
                }
                throw new Exception("Cannot marshal type Address");
            }

            public static readonly AddressConverter Singleton = new AddressConverter();
        }

        internal class BirthdayConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Birthday) || t == typeof(Birthday?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "1997-5-28")
                {
                    return Birthday.The1997528;
                }
                throw new Exception("Cannot unmarshal type Birthday");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Birthday)untypedValue;
                if (value == Birthday.The1997528)
                {
                    serializer.Serialize(writer, "1997-5-28");
                    return;
                }
                throw new Exception("Cannot marshal type Birthday");
            }

            public static readonly BirthdayConverter Singleton = new BirthdayConverter();
        }

        internal class EmailConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Email) || t == typeof(Email?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "deendoughouz@gmail.com")
                {
                    return Email.DeendoughouzGmailCom;
                }
                throw new Exception("Cannot unmarshal type Email");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Email)untypedValue;
                if (value == Email.DeendoughouzGmailCom)
                {
                    serializer.Serialize(writer, "deendoughouz@gmail.com");
                    return;
                }
                throw new Exception("Cannot marshal type Email");
            }

            public static readonly EmailConverter Singleton = new EmailConverter();
        }

        internal class UsernameEnumConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(UsernameEnum) || t == typeof(UsernameEnum?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "deendoughouz")
                {
                    return UsernameEnum.Deendoughouz;
                }
                throw new Exception("Cannot unmarshal type UsernameEnum");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (UsernameEnum)untypedValue;
                if (value == UsernameEnum.Deendoughouz)
                {
                    serializer.Serialize(writer, "deendoughouz");
                    return;
                }
                throw new Exception("Cannot marshal type UsernameEnum");
            }

            public static readonly UsernameEnumConverter Singleton = new UsernameEnumConverter();
        }

        internal class FirstNameConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(FirstName) || t == typeof(FirstName?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "Deen")
                {
                    return FirstName.Deen;
                }
                throw new Exception("Cannot unmarshal type FirstName");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (FirstName)untypedValue;
                if (value == FirstName.Deen)
                {
                    serializer.Serialize(writer, "Deen");
                    return;
                }
                throw new Exception("Cannot marshal type FirstName");
            }

            public static readonly FirstNameConverter Singleton = new FirstNameConverter();
        }

        internal class GenderConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Gender) || t == typeof(Gender?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "male")
                {
                    return Gender.Male;
                }
                throw new Exception("Cannot unmarshal type Gender");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Gender)untypedValue;
                if (value == Gender.Male)
                {
                    serializer.Serialize(writer, "male");
                    return;
                }
                throw new Exception("Cannot marshal type Gender");
            }

            public static readonly GenderConverter Singleton = new GenderConverter();
        }

        internal class IpAddressConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(IpAddress) || t == typeof(IpAddress?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "78.181.41.109")
                {
                    return IpAddress.The7818141109;
                }
                throw new Exception("Cannot unmarshal type IpAddress");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (IpAddress)untypedValue;
                if (value == IpAddress.The7818141109)
                {
                    serializer.Serialize(writer, "78.181.41.109");
                    return;
                }
                throw new Exception("Cannot marshal type IpAddress");
            }

            public static readonly IpAddressConverter Singleton = new IpAddressConverter();
        }

        internal class LanguageConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Language) || t == typeof(Language?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "english")
                {
                    return Language.English;
                }
                throw new Exception("Cannot unmarshal type Language");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Language)untypedValue;
                if (value == Language.English)
                {
                    serializer.Serialize(writer, "english");
                    return;
                }
                throw new Exception("Cannot marshal type Language");
            }

            public static readonly LanguageConverter Singleton = new LanguageConverter();
        }

        internal class LastNameConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(LastName) || t == typeof(LastName?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "Doughouz")
                {
                    return LastName.Doughouz;
                }
                throw new Exception("Cannot unmarshal type LastName");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (LastName)untypedValue;
                if (value == LastName.Doughouz)
                {
                    serializer.Serialize(writer, "Doughouz");
                    return;
                }
                throw new Exception("Cannot marshal type LastName");
            }

            public static readonly LastNameConverter Singleton = new LastNameConverter();
        }

        internal class LastseenStatusConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(LastseenStatus) || t == typeof(LastseenStatus?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "off":
                        return LastseenStatus.Off;
                    case "on":
                        return LastseenStatus.On;
                }
                throw new Exception("Cannot unmarshal type LastseenStatus");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (LastseenStatus)untypedValue;
                switch (value)
                {
                    case LastseenStatus.Off:
                        serializer.Serialize(writer, "off");
                        return;
                    case LastseenStatus.On:
                        serializer.Serialize(writer, "on");
                        return;
                }
                throw new Exception("Cannot marshal type LastseenStatus");
            }

            public static readonly LastseenStatusConverter Singleton = new LastseenStatusConverter();
        }

        internal class DecodeArrayConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(List<long>);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                reader.Read();
                var value = new List<long>();
                while (reader.TokenType != JsonToken.EndArray)
                {
                    var converter = ParseStringConverter.Singleton;
                    var arrayItem = (long)converter.ReadJson(reader, typeof(long), null, serializer);
                    value.Add(arrayItem);
                    reader.Read();
                }
                return value;
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (List<long>)untypedValue;
                writer.WriteStartArray();
                foreach (var arrayItem in value)
                {
                    var converter = ParseStringConverter.Singleton;
                    converter.WriteJson(writer, arrayItem, serializer);
                }
                writer.WriteEndArray();
                return;
            }

            public static readonly DecodeArrayConverter Singleton = new DecodeArrayConverter();
        }

        internal class NameConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Name) || t == typeof(Name?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "Deen Doughouz")
                {
                    return Name.DeenDoughouz;
                }
                throw new Exception("Cannot unmarshal type Name");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Name)untypedValue;
                if (value == Name.DeenDoughouz)
                {
                    serializer.Serialize(writer, "Deen Doughouz");
                    return;
                }
                throw new Exception("Cannot marshal type Name");
            }

            public static readonly NameConverter Singleton = new NameConverter();
        }

        internal class PostPrivacyConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PostPrivacy) || t == typeof(PostPrivacy?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "everyone":
                        return PostPrivacy.Everyone;
                    case "ifollow":
                        return PostPrivacy.Ifollow;
                    case "nobody":
                        return PostPrivacy.Nobody;
                }
                throw new Exception("Cannot unmarshal type PostPrivacy");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PostPrivacy)untypedValue;
                switch (value)
                {
                    case PostPrivacy.Everyone:
                        serializer.Serialize(writer, "everyone");
                        return;
                    case PostPrivacy.Ifollow:
                        serializer.Serialize(writer, "ifollow");
                        return;
                    case PostPrivacy.Nobody:
                        serializer.Serialize(writer, "nobody");
                        return;
                }
                throw new Exception("Cannot marshal type PostPrivacy");
            }

            public static readonly PostPrivacyConverter Singleton = new PostPrivacyConverter();
        }

        internal class RegisteredConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Registered) || t == typeof(Registered?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "00/0000")
                {
                    return Registered.The000000;
                }
                throw new Exception("Cannot unmarshal type Registered");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Registered)untypedValue;
                if (value == Registered.The000000)
                {
                    serializer.Serialize(writer, "00/0000");
                    return;
                }
                throw new Exception("Cannot marshal type Registered");
            }

            public static readonly RegisteredConverter Singleton = new RegisteredConverter();
        }

        internal class ActivatorSchoolConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(ActivatorSchool) || t == typeof(ActivatorSchool?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "PHP")
                {
                    return ActivatorSchool.Php;
                }
                throw new Exception("Cannot unmarshal type ActivatorSchool");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (ActivatorSchool)untypedValue;
                if (value == ActivatorSchool.Php)
                {
                    serializer.Serialize(writer, "PHP");
                    return;
                }
                throw new Exception("Cannot marshal type ActivatorSchool");
            }

            public static readonly ActivatorSchoolConverter Singleton = new ActivatorSchoolConverter();
        }

        internal class ActivatorTimezoneConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(ActivatorTimezone) || t == typeof(ActivatorTimezone?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "Europe/Istanbul")
                {
                    return ActivatorTimezone.EuropeIstanbul;
                }
                throw new Exception("Cannot unmarshal type ActivatorTimezone");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (ActivatorTimezone)untypedValue;
                if (value == ActivatorTimezone.EuropeIstanbul)
                {
                    serializer.Serialize(writer, "Europe/Istanbul");
                    return;
                }
                throw new Exception("Cannot marshal type ActivatorTimezone");
            }

            public static readonly ActivatorTimezoneConverter Singleton = new ActivatorTimezoneConverter();
        }

        internal class ActivatorWorkingConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(ActivatorWorking) || t == typeof(ActivatorWorking?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "WoWonder")
                {
                    return ActivatorWorking.WoWonder;
                }
                throw new Exception("Cannot unmarshal type ActivatorWorking");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (ActivatorWorking)untypedValue;
                if (value == ActivatorWorking.WoWonder)
                {
                    serializer.Serialize(writer, "WoWonder");
                    return;
                }
                throw new Exception("Cannot marshal type ActivatorWorking");
            }

            public static readonly ActivatorWorkingConverter Singleton = new ActivatorWorkingConverter();
        }

        internal class YoutubeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(Youtube) || t == typeof(Youtube?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "channel/UCm_jYhKckK_QY-ugiefU9mA")
                {
                    return Youtube.ChannelUCmJYhKckKQyUgiefU9MA;
                }
                throw new Exception("Cannot unmarshal type Youtube");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (Youtube)untypedValue;
                if (value == Youtube.ChannelUCmJYhKckKQyUgiefU9MA)
                {
                    serializer.Serialize(writer, "channel/UCm_jYhKckK_QY-ugiefU9mA");
                    return;
                }
                throw new Exception("Cannot marshal type Youtube");
            }

            public static readonly YoutubeConverter Singleton = new YoutubeConverter();
        }

        internal class PostTypeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PostType) || t == typeof(PostType?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "post")
                {
                    return PostType.Post;
                }
                throw new Exception("Cannot unmarshal type PostType");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PostType)untypedValue;
                if (value == PostType.Post)
                {
                    serializer.Serialize(writer, "post");
                    return;
                }
                throw new Exception("Cannot marshal type PostType");
            }

            public static readonly PostTypeConverter Singleton = new PostTypeConverter();
        }

        internal class PostYoutubeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PostYoutube) || t == typeof(PostYoutube?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "":
                        return PostYoutube.Empty;
                    case "4Ix2J_0nqUg":
                        return PostYoutube.The4Ix2J0NqUg;
                }
                throw new Exception("Cannot unmarshal type PostYoutube");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PostYoutube)untypedValue;
                switch (value)
                {
                    case PostYoutube.Empty:
                        serializer.Serialize(writer, "");
                        return;
                    case PostYoutube.The4Ix2J0NqUg:
                        serializer.Serialize(writer, "4Ix2J_0nqUg");
                        return;
                }
                throw new Exception("Cannot marshal type PostYoutube");
            }

            public static readonly PostYoutubeConverter Singleton = new PostYoutubeConverter();
        }

        internal class PublisherUnionConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PublisherUnion) || t == typeof(PublisherUnion?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.StartObject:
                        var objectValue = serializer.Deserialize<PublisherClass>(reader);
                        return new PublisherUnion { PublisherClass = objectValue };
                    case JsonToken.StartArray:
                        var arrayValue = serializer.Deserialize<List<object>>(reader);
                        return new PublisherUnion { AnythingArray = arrayValue };
                }
                throw new Exception("Cannot unmarshal type PublisherUnion");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (PublisherUnion)untypedValue;
                if (value.AnythingArray != null)
                {
                    serializer.Serialize(writer, value.AnythingArray);
                    return;
                }
                if (value.PublisherClass != null)
                {
                    serializer.Serialize(writer, value.PublisherClass);
                    return;
                }
                throw new Exception("Cannot marshal type PublisherUnion");
            }

            public static readonly PublisherUnionConverter Singleton = new PublisherUnionConverter();
        }

        internal class AboutUnionConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(AboutUnion) || t == typeof(AboutUnion?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.String:
                    case JsonToken.Date:
                        var stringValue = serializer.Deserialize<string>(reader);
                        switch (stringValue)
                        {
                            case "":
                                return new AboutUnion { Enum = AboutAbout.Empty };
                            case "I&#039;m here to sample this wonderful place!":
                                return new AboutUnion { Enum = AboutAbout.I039MHereToSampleThisWonderfulPlace };
                            case "prova":
                                return new AboutUnion { Enum = AboutAbout.Prova };
                        }
                        try
                        {
                            var uri = new Uri(stringValue);
                            return new AboutUnion { PurpleUri = uri };
                        }
                        catch (UriFormatException) { }
                        break;
                }
                throw new Exception("Cannot unmarshal type AboutUnion");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (AboutUnion)untypedValue;
                if (value.Enum != null)
                {
                    switch (value.Enum)
                    {
                        case AboutAbout.Empty:
                            serializer.Serialize(writer, "");
                            return;
                        case AboutAbout.I039MHereToSampleThisWonderfulPlace:
                            serializer.Serialize(writer, "I&#039;m here to sample this wonderful place!");
                            return;
                        case AboutAbout.Prova:
                            serializer.Serialize(writer, "prova");
                            return;
                    }
                }
                if (value.PurpleUri != null)
                {
                    serializer.Serialize(writer, value.PurpleUri.ToString());
                    return;
                }
                throw new Exception("Cannot marshal type AboutUnion");
            }

            public static readonly AboutUnionConverter Singleton = new AboutUnionConverter();
        }

        internal class AboutAboutConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(AboutAbout) || t == typeof(AboutAbout?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "":
                        return AboutAbout.Empty;
                    case "I&#039;m here to sample this wonderful place!":
                        return AboutAbout.I039MHereToSampleThisWonderfulPlace;
                    case "prova":
                        return AboutAbout.Prova;
                }
                throw new Exception("Cannot unmarshal type AboutAbout");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (AboutAbout)untypedValue;
                switch (value)
                {
                    case AboutAbout.Empty:
                        serializer.Serialize(writer, "");
                        return;
                    case AboutAbout.I039MHereToSampleThisWonderfulPlace:
                        serializer.Serialize(writer, "I&#039;m here to sample this wonderful place!");
                        return;
                    case AboutAbout.Prova:
                        serializer.Serialize(writer, "prova");
                        return;
                }
                throw new Exception("Cannot marshal type AboutAbout");
            }

            public static readonly AboutAboutConverter Singleton = new AboutAboutConverter();
        }

        internal class MutualFriendsCountConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(MutualFriendsCount) || t == typeof(MutualFriendsCount?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.Integer:
                        var integerValue = serializer.Deserialize<long>(reader);
                        return new MutualFriendsCount { Integer = integerValue };
                    case JsonToken.Boolean:
                        var boolValue = serializer.Deserialize<bool>(reader);
                        return new MutualFriendsCount { Bool = boolValue };
                }
                throw new Exception("Cannot unmarshal type MutualFriendsCount");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (MutualFriendsCount)untypedValue;
                if (value.Integer != null)
                {
                    serializer.Serialize(writer, value.Integer.Value);
                    return;
                }
                if (value.Bool != null)
                {
                    serializer.Serialize(writer, value.Bool.Value);
                    return;
                }
                throw new Exception("Cannot marshal type MutualFriendsCount");
            }

            public static readonly MutualFriendsCountConverter Singleton = new MutualFriendsCountConverter();
        }

        internal class PublisherFacebookConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PublisherFacebook) || t == typeof(PublisherFacebook?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "":
                        return PublisherFacebook.Empty;
                    case "960793834062587":
                        return PublisherFacebook.The960793834062587;
                }
                throw new Exception("Cannot unmarshal type PublisherFacebook");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PublisherFacebook)untypedValue;
                switch (value)
                {
                    case PublisherFacebook.Empty:
                        serializer.Serialize(writer, "");
                        return;
                    case PublisherFacebook.The960793834062587:
                        serializer.Serialize(writer, "960793834062587");
                        return;
                }
                throw new Exception("Cannot marshal type PublisherFacebook");
            }

            public static readonly PublisherFacebookConverter Singleton = new PublisherFacebookConverter();
        }

        internal class MutualFriendsDataConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(MutualFriendsData) || t == typeof(MutualFriendsData?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.String:
                    case JsonToken.Date:
                        var stringValue = serializer.Deserialize<string>(reader);
                        return new MutualFriendsData { String = stringValue };
                    case JsonToken.StartArray:
                        var arrayValue = serializer.Deserialize<List<long>>(reader);
                        return new MutualFriendsData { StringArray = arrayValue };
                }
                throw new Exception("Cannot unmarshal type MutualFriendsData");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (MutualFriendsData)untypedValue;
                if (value.String != null)
                {
                    serializer.Serialize(writer, value.String);
                    return;
                }
                if (value.StringArray != null)
                {
                    var converter = DecodeArrayConverter.Singleton;
                    converter.WriteJson(writer, value.StringArray, serializer);
                    return;
                }
                throw new Exception("Cannot marshal type MutualFriendsData");
            }

            public static readonly MutualFriendsDataConverter Singleton = new MutualFriendsDataConverter();
        }

        internal class PhoneNumberUnionConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PhoneNumberUnion) || t == typeof(PhoneNumberUnion?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.String:
                    case JsonToken.Date:
                        var stringValue = serializer.Deserialize<string>(reader);
                        switch (stringValue)
                        {
                            case "":
                                return new PhoneNumberUnion { Enum = PhoneNumberEnum.Empty };
                            case "3487518097":
                                return new PhoneNumberUnion { Enum = PhoneNumberEnum.The3487518097 };
                        }
                        long l;
                        if (Int64.TryParse(stringValue, out l))
                        {
                            return new PhoneNumberUnion { Integer = l };
                        }
                        break;
                }
                throw new Exception("Cannot unmarshal type PhoneNumberUnion");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (PhoneNumberUnion)untypedValue;
                if (value.Enum != null)
                {
                    switch (value.Enum)
                    {
                        case PhoneNumberEnum.Empty:
                            serializer.Serialize(writer, "");
                            return;
                        case PhoneNumberEnum.The3487518097:
                            serializer.Serialize(writer, "3487518097");
                            return;
                    }
                }
                if (value.Integer != null)
                {
                    serializer.Serialize(writer, value.Integer.Value.ToString());
                    return;
                }
                throw new Exception("Cannot marshal type PhoneNumberUnion");
            }

            public static readonly PhoneNumberUnionConverter Singleton = new PhoneNumberUnionConverter();
        }

        internal class PhoneNumberEnumConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PhoneNumberEnum) || t == typeof(PhoneNumberEnum?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "":
                        return PhoneNumberEnum.Empty;
                    case "3487518097":
                        return PhoneNumberEnum.The3487518097;
                }
                throw new Exception("Cannot unmarshal type PhoneNumberEnum");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PhoneNumberEnum)untypedValue;
                switch (value)
                {
                    case PhoneNumberEnum.Empty:
                        serializer.Serialize(writer, "");
                        return;
                    case PhoneNumberEnum.The3487518097:
                        serializer.Serialize(writer, "3487518097");
                        return;
                }
                throw new Exception("Cannot marshal type PhoneNumberEnum");
            }

            public static readonly PhoneNumberEnumConverter Singleton = new PhoneNumberEnumConverter();
        }

        internal class PublisherSchoolConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PublisherSchool) || t == typeof(PublisherSchool?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "":
                        return PublisherSchool.Empty;
                    case "Universidad Tecnológica de la vi":
                        return PublisherSchool.UniversidadTecnológicaDeLaVi;
                }
                throw new Exception("Cannot unmarshal type PublisherSchool");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PublisherSchool)untypedValue;
                switch (value)
                {
                    case PublisherSchool.Empty:
                        serializer.Serialize(writer, "");
                        return;
                    case PublisherSchool.UniversidadTecnológicaDeLaVi:
                        serializer.Serialize(writer, "Universidad Tecnológica de la vi");
                        return;
                }
                throw new Exception("Cannot marshal type PublisherSchool");
            }

            public static readonly PublisherSchoolConverter Singleton = new PublisherSchoolConverter();
        }

        internal class PublisherTimezoneConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(PublisherTimezone) || t == typeof(PublisherTimezone?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "Europe/Rome":
                        return PublisherTimezone.EuropeRome;
                    case "UTC":
                        return PublisherTimezone.Utc;
                }
                throw new Exception("Cannot unmarshal type PublisherTimezone");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (PublisherTimezone)untypedValue;
                switch (value)
                {
                    case PublisherTimezone.EuropeRome:
                        serializer.Serialize(writer, "Europe/Rome");
                        return;
                    case PublisherTimezone.Utc:
                        serializer.Serialize(writer, "UTC");
                        return;
                }
                throw new Exception("Cannot marshal type PublisherTimezone");
            }

            public static readonly PublisherTimezoneConverter Singleton = new PublisherTimezoneConverter();
        }

        internal class WorkingUnionConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(WorkingUnion) || t == typeof(WorkingUnion?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                switch (reader.TokenType)
                {
                    case JsonToken.String:
                    case JsonToken.Date:
                        var stringValue = serializer.Deserialize<string>(reader);
                        switch (stringValue)
                        {
                            case "":
                                return new WorkingUnion { Enum = WorkingWorking.Empty };
                            case "Tiny Boss":
                                return new WorkingUnion { Enum = WorkingWorking.TinyBoss };
                            case "Wowonder":
                                return new WorkingUnion { Enum = WorkingWorking.Wowonder };
                        }
                        try
                        {
                            var uri = new Uri(stringValue);
                            return new WorkingUnion { PurpleUri = uri };
                        }
                        catch (UriFormatException) { }
                        break;
                }
                throw new Exception("Cannot unmarshal type WorkingUnion");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (WorkingUnion)untypedValue;
                if (value.Enum != null)
                {
                    switch (value.Enum)
                    {
                        case WorkingWorking.Empty:
                            serializer.Serialize(writer, "");
                            return;
                        case WorkingWorking.TinyBoss:
                            serializer.Serialize(writer, "Tiny Boss");
                            return;
                        case WorkingWorking.Wowonder:
                            serializer.Serialize(writer, "Wowonder");
                            return;
                    }
                }
                if (value.PurpleUri != null)
                {
                    serializer.Serialize(writer, value.PurpleUri.ToString());
                    return;
                }
                throw new Exception("Cannot marshal type WorkingUnion");
            }

            public static readonly WorkingUnionConverter Singleton = new WorkingUnionConverter();
        }

        internal class WorkingWorkingConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(WorkingWorking) || t == typeof(WorkingWorking?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                switch (value)
                {
                    case "":
                        return WorkingWorking.Empty;
                    case "Tiny Boss":
                        return WorkingWorking.TinyBoss;
                    case "Wowonder":
                        return WorkingWorking.Wowonder;
                }
                throw new Exception("Cannot unmarshal type WorkingWorking");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (WorkingWorking)untypedValue;
                switch (value)
                {
                    case WorkingWorking.Empty:
                        serializer.Serialize(writer, "");
                        return;
                    case WorkingWorking.TinyBoss:
                        serializer.Serialize(writer, "Tiny Boss");
                        return;
                    case WorkingWorking.Wowonder:
                        serializer.Serialize(writer, "Wowonder");
                        return;
                }
                throw new Exception("Cannot marshal type WorkingWorking");
            }

            public static readonly WorkingWorkingConverter Singleton = new WorkingWorkingConverter();
        }
}