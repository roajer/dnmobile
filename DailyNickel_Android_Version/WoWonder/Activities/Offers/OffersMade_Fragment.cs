﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using WoWonder.Helpers;
using SettingsConnecter;
using Fragment = Android.Support.V4.App.Fragment;
using Microsoft.AppCenter.Crashes;
using Android.Support.V7.Widget;
using Android.Support.V4.Widget;
using WoWonder.Activities.Offers.Adapters;
using Android.Views.Animations;
using System.Collections.ObjectModel;
using WoWonder_API.Classes.Product;
using FFImageLoading;
using WoWonder_API.Requests;
using WoWonder_API.Classes.Global;
using WoWonder.Activities.Offers.MockData;

namespace WoWonder.Activities.Offers
{
    public class OffersMade_Fragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            try
            {
                var view = MyContextWrapper.GetContentView(Context, Settings.Lang, Resource.Layout.OffersMade_Layout);
                if (view == null) view = inflater.Inflate(Resource.Layout.OffersMade_Layout, container, false);

                // Views initialization
                Offers_Empty = view.FindViewById<LinearLayout>(Resource.Id.OffersMade_LinerEmpty);
                MainRecyclerView = view.FindViewById<RecyclerView>(Resource.Id.OffersMade_Recyler);
                swipeRefreshLayout = view.FindViewById<SwipeRefreshLayout>(Resource.Id.swipeRefreshLayout);
                IconEmpty = view.FindViewById<TextView>(Resource.Id.Market_icon);

                // Set market icon
                IMethods.Set_TextViewIcon("2", IconEmpty, "\uf07a");

                // Set color scheme for SwipeRefresh layout
                swipeRefreshLayout.SetColorSchemeResources(Android.Resource.Color.HoloBlueLight,
                    Android.Resource.Color.HoloGreenLight, Android.Resource.Color.HoloOrangeLight,
                    Android.Resource.Color.HoloRedLight);
                swipeRefreshLayout.Refreshing = true;
                swipeRefreshLayout.Enabled = true;

                // Show Recycler view and hide view for empty offers
                MainRecyclerView.Visibility = ViewStates.Visible;
                Offers_Empty.Visibility = ViewStates.Gone;

                OffersMadeAdapter = new OffersMadeAdapter(Activity);
                layoutManager = new GridLayoutManager(Activity, 1);
                MainRecyclerView.SetLayoutManager(layoutManager);
                MainRecyclerView.AddItemDecoration(new GridSpacingItemDecoration(2, 10, true));
                var animation = AnimationUtils.LoadAnimation(Activity, Resource.Animation.slideUpAnim);
                MainRecyclerView.StartAnimation(animation);
                OffersMadeAdapter.OffersList = new ObservableCollection<OffersMade.Product>();
                MainRecyclerView.SetAdapter(OffersMadeAdapter);

                MainRecyclerView.HasFixedSize = true;
                MainRecyclerView.SetItemViewCacheSize(5);
                MainRecyclerView.GetLayoutManager().ItemPrefetchEnabled = true;
                MainRecyclerView.DrawingCacheEnabled = true;

                Get_Data_Local();

                return view;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return null;
            }
        }

        public override void OnResume()
        {
            try
            {
                base.OnResume();

                // Add Events
                //OffersGotAdapter.ItemClick += OffersGotAdapter_OnItemClick;
                swipeRefreshLayout.Refresh += Offers_SwipeRefreshLayoutOnRefresh;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override void OnPause()
        {
            try
            {
                base.OnPause();

                // Close Events
                //OffersGotAdapter.ItemClick -= OffersGotAdapter_OnItemClick;
                swipeRefreshLayout.Refresh -= Offers_SwipeRefreshLayoutOnRefresh;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public void Get_Data_Local()
        {
            try
            {
                if (OffersMadeAdapter != null)
                    if (Classes.ListChachedData_Product.Count > 0)
                    {
                        OffersMadeAdapter.OffersList = Classes.ListCachedData_OffersMade_Product;
                        OffersMadeAdapter.BindEnd();
                    }

                Get_DataOffers_Api();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public async void Get_DataOffers_Api(string offset = "")
        {
            try
            {
                if (!IMethods.CheckConnectivity())
                {
                    Activity.RunOnUiThread(() => { swipeRefreshLayout.Refreshing = false; });
                    Toast.MakeText(Activity, Activity.GetString(Resource.String.Lbl_CheckYourInternetConnection),
                        ToastLength.Short).Show();
                }
                else
                {
                    //var (Api_status, Respond) = await Client.Market.Get_Products("", "35", offset);
                    var (Api_status, Respond) = await MockData.MockData.GetOffersMadeAsync();
                    if (Api_status == 200)
                    {
                        if (Respond is OffersMade result)
                            Activity.RunOnUiThread(() =>
                            {
                                if (result.Products.Count <= 0)
                                {
                                    if (swipeRefreshLayout.Refreshing)
                                        swipeRefreshLayout.Refreshing = false;
                                }
                                else if (result.Products.Count > 0)
                                {
                                    if (OffersMadeAdapter.OffersList.Count <= 0)
                                    {
                                        OffersMadeAdapter.OffersList =
                                            new ObservableCollection<OffersMade.Product>(result.Products);
                                        OffersMadeAdapter.BindEnd();

                                        var animation = AnimationUtils.LoadAnimation(Activity,
                                            Resource.Animation.slideUpAnim);
                                        MainRecyclerView.StartAnimation(animation);
                                    }
                                    else
                                    {
                                        //Bring new item
                                        //var exceptList = result.products?.ToList().Except(OffersGotAdapter?.OffersList).ToList();

                                        var listnew = result.Products?.Where(c =>
                                            !OffersMadeAdapter.OffersList.Select(fc => fc.OfferId).Contains(c.OfferId)).ToList();
                                        if (listnew.Count > 0)
                                        {
                                            var lastCountItem = OffersMadeAdapter.ItemCount;

                                            //Results differ
                                            Classes.AddRange(OffersMadeAdapter.OffersList, listnew);
                                            OffersMadeAdapter.NotifyItemRangeInserted(lastCountItem, listnew.Count);
                                        }

                                        if (swipeRefreshLayout.Refreshing)
                                            swipeRefreshLayout.Refreshing = false;
                                    }
                                }
                            });
                    }
                    else if (Api_status == 400)
                    {
                        if (Respond is Error_Object error)
                        {
                            var errortext = error._errors.Error_text;
                            //Toast.MakeText(this.Activity, errortext, ToastLength.Short).Show();

                            if (errortext.Contains("Invalid or expired access_token"))
                                API_Request.Logout(Activity);
                        }
                    }
                    else if (Api_status == 404)
                    {
                        var error = Respond.ToString();
                        //Toast.MakeText(this.Activity, error, ToastLength.Short).Show();
                    }
                }

                //Show Empty Page >> 
                //===============================================================
                Activity.RunOnUiThread(() =>
                {
                    swipeRefreshLayout.Refreshing = false;

                    if (OffersMadeAdapter.OffersList.Count > 0)
                    {
                        if (MainRecyclerView.Visibility == ViewStates.Gone)
                            MainRecyclerView.Visibility = ViewStates.Visible;

                        if (Offers_Empty.Visibility == ViewStates.Visible)
                            Offers_Empty.Visibility = ViewStates.Gone;
                    }
                    else if (OffersMadeAdapter.OffersList.Count <= 0)
                    {
                        MainRecyclerView.Visibility = ViewStates.Gone;
                        Offers_Empty.Visibility = ViewStates.Visible;
                    }

                    //Set Event Scroll
                    //if (MarketMainScrolEvent == null)
                    //{
                    //    var xamarinRecyclerViewOnScrollListener =
                    //        new XamarinRecyclerViewOnScrollListener(mLayoutManager, swipeRefreshLayout);
                    //    MarketMainScrolEvent = xamarinRecyclerViewOnScrollListener;
                    //    MarketMainScrolEvent.LoadMoreEvent += Market_FragmentOnScroll_OnLoadMoreEvent;
                    //    MainRecyclerView.AddOnScrollListener(MarketMainScrolEvent);
                    //    MainRecyclerView.AddOnScrollListener(new ScrollDownDetector());
                    //}
                    //else
                    //{
                    //    MarketMainScrolEvent.IsLoading = false;
                    //}
                });
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                Get_DataOffers_Api(offset);
            }
        }

        private void Offers_SwipeRefreshLayoutOnRefresh(object sender, EventArgs e)
        {
            try
            {
                OffersMadeAdapter.Clear();
                Get_DataOffers_Api("");
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        public override void OnLowMemory()
        {
            try
            {
                GC.Collect(GC.MaxGeneration);
                base.OnLowMemory();
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        public override void OnDestroy()
        {
            try
            {
                if (OffersMadeAdapter.OffersList.Count > 0)
                    Classes.ListCachedData_OffersMade_Product = OffersMadeAdapter.OffersList;

                ImageService.Instance.InvalidateMemoryCache();
                base.OnDestroy();
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        #region Variables Basic

        public LinearLayout Offers_Empty;
        public RecyclerView MainRecyclerView;

        public GridLayoutManager layoutManager;

        public OffersMadeAdapter OffersMadeAdapter;

        private TextView IconEmpty;

        public SwipeRefreshLayout swipeRefreshLayout;

        #endregion
    }
}