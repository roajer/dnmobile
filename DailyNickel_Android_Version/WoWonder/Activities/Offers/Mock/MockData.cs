﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static WoWonder.Activities.Offers.MockData.OffersGot;
using static WoWonder.Activities.Offers.MockData.OffersMade;

/// <summary>
/// Contains Models for OffersGot and OffersMade.
/// Also contains Mock Data for each model and
/// functions to get data of each.
/// </summary>
namespace WoWonder.Activities.Offers.MockData
{
    public class MockData
    {
        public MockData() { }

        public static async Task<(int, dynamic)> GetOffersGotAsync()
        {
            const string monkeyImg = "https://dnrepository.s3.amazonaws.com/upload/photos/2019/03/kNvgtJeVVLNRaqzEdNRA_21_c27e3ae2ebcfd22b5e850abc3d29fc43_image_small.jpg";
            const string miBandImg = "https://dnrepository.s3.amazonaws.com/upload/photos/2019/03/AHrEXCUShwAVPztAEpje_13_0ad3cb13cca67cc3f8b61a796c85c985_image_small.jpg";

            var offersGot = new OffersGot
            {
                ApiStatus = 200,
                Products = new List<MyProduct>
                {
                    new MyProduct
                    {
                        Id = 1,
                        Name = "Xamarin Monkey",
                        Category = "Gifts & Occasions",
                        Price = "25",
                        Image = monkeyImg,
                        Offers = new List<ProductOffer>
                        {
                            new ProductOffer
                            {
                                Id = 1,
                                Username = "johndoe",
                                Name = "John Doe",
                                OfferPrice = "25",
                                OfferStatus= "Pending",
                                OfferTime = DateTime.Now.Date,
                                OfferComment = "This is my final offer",
                                OfferDemandPrice = "21"
                            },

                            new ProductOffer
                            {
                                Id = 1,
                                Username = "annarise",
                                Name = "Anna Rise",
                                OfferPrice = "25",
                                OfferStatus = "Pending",
                                OfferTime = DateTime.Now.Date,
                                OfferComment = "This is my first offer",
                                OfferDemandPrice = "23"
                            }
                        }
                    },
                    new MyProduct
                    {
                        Id = 2,
                        Name = "Mi band 2",
                        Category = "Electronics",
                        Price = "16",
                        Image = miBandImg,
                        Offers = new List<ProductOffer>
                        {
                            new ProductOffer
                            {
                                Id = 1,
                                Username = "johndoe",
                                Name = "John Doe",
                                OfferPrice = "16",
                                OfferStatus= "Pending",
                                OfferTime = DateTime.Now.Date,
                                OfferComment = "This is my final offer",
                                OfferDemandPrice = "11"
                            },

                            new ProductOffer
                            {
                                Id = 1,
                                Username = "annarise",
                                Name = "Anna Rise",
                                OfferPrice = "16",
                                OfferStatus = "Pending",
                                OfferTime = DateTime.Now.Date,
                                OfferComment = "This is my first offer",
                                OfferDemandPrice = "12"
                            }
                        }
                    },
                }
            };

            int api_status = offersGot.ApiStatus;
            dynamic result = offersGot;
            return await Task.FromResult<(int, dynamic)>((api_status, result));
        }

        public static async Task<(int, dynamic)> GetOffersMadeAsync()
        {
            const string img = "https://dnrepository.s3.amazonaws.com/upload/photos/2019/03/UMoBlJQLx3XFdEZRS2E3_15_414ceee532b39821a6d9d17631b69a66_image_small.jpeg";

            var offersMade = new OffersMade
            {
                ApiStatus = 200,
                Products = new List<Product>
                {
                    new Product
                    {
                        OfferId = 11,
                        PostId = 139,
                        ProductUrl = "https://stg2.dailynickel.com/demo1/post/139",
                        ProductName = "Refrigerator",
                        ProductCategory = "Consumer Electronics",
                        ProductImage = img,
                        SellerName = "John Doe",
                        SellerUsername = "johndoe",
                        OfferTime = DateTime.Now.ToLocalTime(),
                        OfferComment = "Lroem ipsum dolor ink set...",
                        OfferPrice = "2",
                        OfferStatus = "Accepted",
                        CounterPrice = "3.5"
                    },

                    new Product
                    {
                        OfferId = 3,
                        PostId = 46,
                        ProductUrl = "https://stg2.dailynickel.com/demo1/post/46",
                        ProductName = "Electrical Work",
                        ProductCategory = "Apparel & Accessories",
                        ProductImage = img,
                        SellerName = "Austin Kinder",
                        SellerUsername = "austink",
                        OfferTime = DateTime.Now.ToLocalTime(),
                        OfferComment = "Lroem ipsum dolor ink set...",
                        OfferPrice = "22",
                        OfferStatus = "Pending",
                        CounterPrice = "24"
                    },
                }
            };

            int apiStatus = offersMade.ApiStatus;
            dynamic result = offersMade;
            return await Task.FromResult<(int, dynamic)>((apiStatus, result));
        }
    }

    public class OffersGot
    {
        public int ApiStatus { get; set; }
        public List<MyProduct> Products { get; set; }

        public class MyProduct
        {
            public int Id { get; set; }
            public string Image { get; set; }
            public string Name { get; set; }
            public string Category { get; set; }
            public string Price { get; set; }
            public string OffersCount => Offers.Count.ToString();
            public string Notifications { get; set; }
            public List<ProductOffer> Offers { get; set; }
        }

        public class ProductOffer
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime OfferTime { get; set; }
            public string Username { get; set; }
            public string OfferPrice { get; set; }
            public string OfferStatus { get; set; }
            public string OfferComment { get; set; }
            public string OfferDemandPrice { get; set; }
            public string OfferEventId { get; set; }
        }
    }

    public class OffersMade
    {
        public int ApiStatus { get; set; }
        public List<Product> Products { get; set; }

        public class Product
        {
            public int OfferId { get; set; }
            public int PostId { get; set; }
            public string ProductUrl { get; set; }
            public string ProductImage { get; set; }
            public string ProductName { get; set; }
            public string ProductCategory { get; set; }
            public string SellerName { get; set; }
            public string SellerUsername { get; set; }
            public string OfferPrice { get; set; }
            public string CounterPrice { get; set; }
            public string PushNotifications { get; set; }
            public DateTime OfferTime { get; set; }
            public string OfferComment { get; set; }
            public string OfferStatus { get; set; }
            public string OfferEventId { get; set; }
        }
    }
}