﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using WoWonder.Helpers;
using SettingsConnecter;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Content.PM;
using Android.Support.Design.Widget;
using WoWonder.Adapters;
using Microsoft.AppCenter.Crashes;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using FFImageLoading;

namespace WoWonder.Activities.Offers
{
    [Activity(Label = "TabbedOffers_Activity", Theme = "@style/MyTheme",
        ConfigurationChanges = ConfigChanges.Locale | ConfigChanges.Orientation)]
    public class TabbedOffers_Activity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            try
            {
                // Check if we're running on Android 5.0 or higher
                if ((int)Build.VERSION.SdkInt < 23)
                {
                }
                else
                {
                    Window.AddFlags(WindowManagerFlags.LayoutNoLimits);
                    // Window.AddFlags(WindowManagerFlags.TranslucentNavigation);
                }

                base.OnCreate(savedInstanceState);

            // Create your application here
            var view = MyContextWrapper.GetContentView(this, Settings.Lang, Resource.Layout.EventMain_Layout);
            if (view != null)
                SetContentView(view);
            else
                SetContentView(Resource.Layout.EventMain_Layout);

            var toolBar = FindViewById<Toolbar>(Resource.Id.toolbar);
                if (toolBar != null)
                {
                    if (Settings.Show_Toolbar_Offers)
                    {
                        toolBar.Title = GetText(Resource.String.Lbl_Offers);

                        SetSupportActionBar(toolBar);
                        SupportActionBar.SetDisplayShowCustomEnabled(true);
                        SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                        SupportActionBar.SetHomeButtonEnabled(true);
                        SupportActionBar.SetDisplayShowHomeEnabled(true);
                    }
                    else
                    {
                        toolBar.Title = " ";
                        SetSupportActionBar(toolBar);
                    }

                    viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
                    tab_Layout = FindViewById<TabLayout>(Resource.Id.tabs);

                    FloatingActionButtonView = FindViewById<FloatingActionButton>(Resource.Id.floatingActionButtonView);
                    FloatingActionButtonView.Visibility = ViewStates.Gone;

                    viewPager.OffscreenPageLimit = 2;
                    SetUpViewPager(viewPager);
                    tab_Layout.SetupWithViewPager(viewPager);
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        public override void OnTrimMemory(TrimMemory level)
        {
            try
            {
                ImageService.Instance.InvalidateMemoryCache();
                GC.Collect(GC.MaxGeneration, GCCollectionMode.Forced);
                base.OnTrimMemory(level);
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        public override void OnLowMemory()
        {
            try
            {
                GC.Collect(GC.MaxGeneration);
                base.OnLowMemory();
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        protected override void OnDestroy()
        {
            try
            {
                ImageService.Instance.InvalidateMemoryCache();
                base.OnDestroy();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        #region Variables Basic

        public ViewPager viewPager;

        public FragmentManager frgmanager;
        public OffersGot_Fragment OffersGot_Fragment_Tab;
        public OffersMade_Fragment OffersMade_Fragment_Tab;
        public TabLayout tab_Layout;

        private FloatingActionButton FloatingActionButtonView;

        #endregion


        #region Set Tab
        private void SetUpViewPager(ViewPager viewPager)
        {
            try
            {
                OffersGot_Fragment_Tab = new OffersGot_Fragment();
                OffersMade_Fragment_Tab = new OffersMade_Fragment();

                var adapter = new Main_Tab_Adapter(SupportFragmentManager);
                adapter.AddFragment(OffersGot_Fragment_Tab, GetText(Resource.String.Lbl_OffersGot));
                adapter.AddFragment(OffersMade_Fragment_Tab, GetText(Resource.String.Lbl_OffersMade));

                viewPager.CurrentItem = 2;

                //viewPager.PageScrolled += ViewPager_PageScrolled;
                viewPager.Adapter = adapter;
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        private void ViewPager_PageScrolled(object sender, ViewPager.PageScrolledEventArgs page)
        {
            //try
            //{
            //    var p = page.Position;
            //    if (p == 0)
            //        Event_Fragment_Tab.Get_Event_Api();
            //    else if (p == 1) MyEvent_Fragment_Tab.Get_MyEvent_Api();
            //}
            //catch (Exception exception)
            //{
            //    Crashes.TrackError(exception);
            //}
        }

        #endregion
    }
}