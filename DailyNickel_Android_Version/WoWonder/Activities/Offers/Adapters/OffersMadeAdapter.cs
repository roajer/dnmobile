﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Content;
using Microsoft.AppCenter.Crashes;
using System.Collections.ObjectModel;
using WoWonder_API.Classes.Product;
using WoWonder.Helpers;
using SettingsConnecter;
using WoWonder.Activities.Offers.MockData;

namespace WoWonder.Activities.Offers.Adapters
{
    public class OffersMadeAdapter : RecyclerView.Adapter
    {
        public int _position;
        public Context context;

        // Replace offers list by below
        public ObservableCollection<OffersMade.Product> OffersList =
            new ObservableCollection<OffersMade.Product>();

        public OffersMadeAdapter(Context context)
        {
            try
            {
                this.context = context;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override int ItemCount => OffersList != null ? OffersList.Count : 0;

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            try
            {
                //Setup your layout here
                View itemView = null;
                var id = Resource.Layout.Style_Offer_view;
                itemView = LayoutInflater.From(parent.Context).
                       Inflate(id, parent, false);

                var vh = new OffersMadeAdapterViewHolder(itemView, OnClick, OnLongClick);
                return vh;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return null;
            }

        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            try
            {
                _position = position;
                if (viewHolder is OffersMadeAdapterViewHolder holder)
                {
                    var item = OffersList[_position];
                    if (item != null) Initialize(holder, item);
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public void Initialize(OffersMadeAdapterViewHolder holder, OffersMade.Product item)
        {
            try
            {
                //if (holder.mappinIcon.Text != IonIcons_Fonts.IosLocation)
                //    IMethods.Set_TextViewIcon("1", holder.mappinIcon, IonIcons_Fonts.IosLocation);

                // Thumbnail
                if (holder.Thumbnail.Tag?.ToString() != "loaded")
                {
                    ImageCacheLoader.LoadImage(item.ProductImage, holder.Thumbnail, false, false);
                    holder.Thumbnail.Tag = "loaded";
                }

                // Title
                if (holder.Title.Tag?.ToString() != "true")
                {
                    holder.Title.Tag = "true";
                    holder.Title.Text = IMethods.Fun_String.DecodeString(IMethods.Fun_String.DecodeStringWithEnter(item.ProductName));
                }

                // Category
                if (holder.Category.Tag?.ToString() != "true")
                {
                    holder.Category.Tag = "true";
                    holder.Category.Text = IMethods.Fun_String.DecodeString(IMethods.Fun_String.DecodeStringWithEnter(item.ProductCategory));
                }

                // Price
                if (holder.Price.Tag?.ToString() != "true")
                {
                    holder.Price.Tag = "true";
                    holder.Price.Text = $"{Settings.Market_curency}{item.OfferPrice}";
                }

                // Seller Name
                if (holder.SellerName.Tag?.ToString() != "true")
                {
                    holder.SellerName.Tag = "true";
                    holder.SellerName.Text = item.SellerName;
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public void BindEnd()
        {
            try
            {
                NotifyDataSetChanged();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        // Add Offer to List
        //public void Add(Get_Products_Object.Product product)
        //{
        //    try
        //    {
        //        var check = OffersList.FirstOrDefault(a => a.id == product.id);
        //        if (check == null)
        //        {
        //            OffersList.Add(product);
        //            NotifyItemInserted(OffersList.IndexOf(OffersList.Last()));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Crashes.TrackError(e);
        //    }
        //}

        public void Clear()
        {
            try
            {
                OffersList.Clear();
                NotifyDataSetChanged();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override long GetItemId(int position)
        {
            try
            {
                return position;
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
                return 0;
            }
        }

        public override int GetItemViewType(int position)
        {
            try
            {
                return position;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return 0;
            }
        }

        public event EventHandler<OffersMadeAdapterClickEventArgs> ItemClick;
        public event EventHandler<OffersMadeAdapterClickEventArgs> ItemLongClick;

        void OnClick(OffersMadeAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(OffersMadeAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class OffersMadeAdapterViewHolder : RecyclerView.ViewHolder
    {
        public OffersMadeAdapterViewHolder(View itemView, Action<OffersMadeAdapterClickEventArgs> clickListener,
                            Action<OffersMadeAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            try
            {
                MainView = itemView;

                Thumbnail = MainView.FindViewById<ImageView>(Resource.Id.product_image);
                Title = MainView.FindViewById<TextView>(Resource.Id.product_name);
                Category = MainView.FindViewById<TextView>(Resource.Id.product_category);
                Price = MainView.FindViewById<TextView>(Resource.Id.product_price);
                OfferCountLayout = MainView.FindViewById<LinearLayout>(Resource.Id.offer_count_layout);
                OffersCount = MainView.FindViewById<TextView>(Resource.Id.product_offers_count);
                SellerNameLayout = MainView.FindViewById<LinearLayout>(Resource.Id.seller_name_layout);
                SellerName = MainView.FindViewById<TextView>(Resource.Id.product_seller_name);
                OffersView = MainView.FindViewById<ImageButton>(Resource.Id.offersView);

                SellerNameLayout.Visibility = ViewStates.Visible;
                OfferCountLayout.Visibility = ViewStates.Gone;


                OffersView.Click += (sender, e) => clickListener(new OffersMadeAdapterClickEventArgs
                { View = OffersView, Position = AdapterPosition });

                // Event
                ItemView.Click += (sender, e) => clickListener(new OffersMadeAdapterClickEventArgs
                { View = ItemView, Position = AdapterPosition });
                ItemView.LongClick += (sender, e) => longClickListener(new OffersMadeAdapterClickEventArgs
                { View = ItemView, Position = AdapterPosition });
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
            //TextView = v;
            itemView.Click += (sender, e) => clickListener(new OffersMadeAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new OffersMadeAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }

        #region Variables Basic

        //Your adapter views to re-use
        public View MainView { get; }

        public event EventHandler<int> ItemClick;
        public ImageView Thumbnail { get; set; }
        public TextView Title { get; set; }
        public TextView Category { get; set; }
        public TextView Price { get; set; }
        public LinearLayout OfferCountLayout { get; set; }
        public TextView OffersCount { get; set; }
        public LinearLayout SellerNameLayout { get; set; }
        public TextView SellerName { get; set; }
        public ImageButton OffersView { get; set; }

        #endregion
    }

    public class OffersMadeAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}