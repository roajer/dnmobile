﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Microsoft.AppCenter.Crashes;
using WoWonder.Helpers;
using WoWonder_API.Classes.Product;
using SettingsConnecter;
using WoWonder.Activities.Offers.MockData;

namespace WoWonder.Activities.Offers.Adapters
{
    public class OffersGotAdapter : RecyclerView.Adapter
    {

        public int _position;
        public Context context;

        // Replace offers list by below
        public ObservableCollection<OffersGot.MyProduct> OffersList =
            new ObservableCollection<OffersGot.MyProduct>();

        public OffersGotAdapter(Context context)
        {
            try
            {
                this.context = context;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override int ItemCount => OffersList != null ? OffersList.Count : 0;

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            try
            {
                // Set up layout here
                var itemView = LayoutInflater.From(parent.Context)
                    .Inflate(Resource.Layout.Style_Offer_view, parent, false);
                var vh = new OffersGotAdapterViewHolder(itemView, OnClick, OnLongClick);
                return vh;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return null;
            }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            try
            {
                _position = position;
                if (viewHolder is OffersGotAdapterViewHolder holder)
                {
                    var item = OffersList[_position];
                    if (item != null) Initialize(holder, item);
                }
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
            }
        }

        public void Initialize(OffersGotAdapterViewHolder holder, OffersGot.MyProduct item)
        {
            try
            {
                //if (holder.mappinIcon.Text != IonIcons_Fonts.IosLocation)
                //    IMethods.Set_TextViewIcon("1", holder.mappinIcon, IonIcons_Fonts.IosLocation);

                // Thumbnail
                if (holder.Thumbnail.Tag?.ToString() != "loaded")
                {
                    //ImageCacheLoader.LoadImage(item.images[0].image, holder.Thumbnail, false, false);
                    ImageCacheLoader.LoadImage(item.Image, holder.Thumbnail, false, false);
                    holder.Thumbnail.Tag = "loaded";
                }

                // Title
                if (holder.Title.Tag?.ToString() != "true")
                {
                    holder.Title.Tag = "true";
                    holder.Title.Text = IMethods.Fun_String.DecodeString(IMethods.Fun_String.DecodeStringWithEnter(item.Name));
                }

                // Category
                if (holder.Category.Tag?.ToString() != "true")
                {
                    holder.Category.Tag = "true";
                    holder.Category.Text = IMethods.Fun_String.DecodeString(IMethods.Fun_String.DecodeStringWithEnter(item.Category));
                }

                // Price
                if (holder.Price.Tag?.ToString() != "true")
                {
                    holder.Price.Tag = "true";
                    holder.Price.Text = $"{Settings.Market_curency}{item.Price}";
                }

                // Offers Count
                if (holder.OffersCount.Tag?.ToString() != "true")
                {
                    holder.OffersCount.Tag = "true";
                    holder.OffersCount.Text = $"{item.OffersCount}";
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public void BindEnd()
        {
            try
            {
                NotifyDataSetChanged();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        // Add Offer to List
        public void Add(OffersGot.MyProduct product)
        {
            try
            {
                var check = OffersList.FirstOrDefault(a => a.Id == product.Id);
                if (check == null)
                {
                    OffersList.Add(product);
                    NotifyItemInserted(OffersList.IndexOf(OffersList.Last()));
                }
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public void Clear()
        {
            try
            {
                OffersList.Clear();
                NotifyDataSetChanged();
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        //public override Get_Products_Object.Product GetItem(int position)
        //{
        //    return OffersList[position];
        //}

        public override long GetItemId(int position)
        {
            try
            {
                return position;
            }
            catch (Exception exception)
            {
                Crashes.TrackError(exception);
                return 0;
            }
        }

        public override int GetItemViewType(int position)
        {
            try
            {
                return position;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return 0;
            }
        }

        public event EventHandler<OfferAdapterClickEventArgs> ItemClick;
        public event EventHandler<OfferAdapterClickEventArgs> ItemLongClick;

        private void OnClick(OfferAdapterClickEventArgs args)
        {
            ItemClick?.Invoke(this, args);
        }

        private void OnLongClick(OfferAdapterClickEventArgs args)
        {
            ItemLongClick?.Invoke(this, args);
        }
    }

    public class OffersGotAdapterViewHolder : RecyclerView.ViewHolder
    {
        public OffersGotAdapterViewHolder(View itemView, Action<OfferAdapterClickEventArgs> clickListener,
            Action<OfferAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            try
            {
                MainView = itemView;

                Thumbnail = MainView.FindViewById<ImageView>(Resource.Id.product_image);
                Title = MainView.FindViewById<TextView>(Resource.Id.product_name);
                Category = MainView.FindViewById<TextView>(Resource.Id.product_category);
                Price = MainView.FindViewById<TextView>(Resource.Id.product_price);
                OffersCount = MainView.FindViewById<TextView>(Resource.Id.product_offers_count);
                OffersView = MainView.FindViewById<ImageButton>(Resource.Id.offersView);

                OfferCountLayout.Visibility = ViewStates.Visible;
                SellerNameLayout.Visibility = ViewStates.Gone;

                OffersView.Click += (sender, e) => clickListener(new OfferAdapterClickEventArgs
                { View = OffersView, Position = AdapterPosition });

                // Event
                ItemView.Click += (sender, e) => clickListener(new OfferAdapterClickEventArgs
                { View = ItemView, Position = AdapterPosition });
                ItemView.LongClick += (sender, e) => longClickListener(new OfferAdapterClickEventArgs
                { View = ItemView, Position = AdapterPosition });
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        #region Variables Basic

        //Your adapter views to re-use
        public View MainView { get; }

        public event EventHandler<int> ItemClick;
        public ImageView Thumbnail { get; set; }
        public TextView Title { get; set; }
        public TextView Category { get; set; }
        public TextView Price { get; set; }
        public LinearLayout OfferCountLayout { get; set; }
        public TextView OffersCount { get; set; }
        public LinearLayout SellerNameLayout { get; set; }
        public TextView SellerName { get; set; }
        public ImageButton OffersView { get; set; }

        #endregion
    }

    public class OfferAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}