﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Microsoft.AppCenter.Crashes;
using WoWonder.Helpers;
using Settings = SettingsConnecter.Settings;
using Fragment = Android.Support.V4.App.Fragment;
using WoWonder.Activities.Market;
using Android;
using WoWonder.Activities.AddPost;
using Android.Support.V4.View;
using WoWonder.Adapters;
using Android.Gms.Maps;
using Android.Graphics;

namespace WoWonder.Activities.Tabbes
{
    public class Search_Fragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            try
            {
                base.OnCreateView(inflater, container, savedInstanceState);

                // Use this to return your custom view for this Fragment
                var view = MyContextWrapper.GetContentView(Context, Settings.Lang, Resource.Layout.Tab_Search_Layout);
                if (view == null) view = inflater.Inflate(Resource.Layout.Tab_Search_Layout, container, false);

                // Buttons Initialization
                GoodsButton = view.FindViewById<Button>(Resource.Id.goodsButton);
                SkillsButton = view.FindViewById<Button>(Resource.Id.skillsButton);
                MapViewButton = view.FindViewById<ImageButton>(Resource.Id.mapViewButton);
                BoxViewButton = view.FindViewById<ImageButton>(Resource.Id.boxViewButton);

                // View Fragments Initialization
                MapViewFragment = new SupportMapFragment();
                BoxViewFragment = new Market_Fragment();

                if (MapViewFragment != null)
                {
                    ft = FragmentManager.BeginTransaction();

                    // Adds MapView inside Container and
                    // set primary fragment to MapView
                    ft.Add(Resource.Id.myFrameContainer, MapViewFragment)
                        .SetPrimaryNavigationFragment(MapViewFragment)
                        .Commit();
                }

                return view;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
                return null;
            }
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
        }

        public override void OnResume()
        {
            try
            {
                base.OnResume();

                // Add Events
                GoodsButton.Click += OnGoodsBtn_Clicked;
                SkillsButton.Click += OnSkillsBtn_Clicked;
                MapViewButton.Click += OnMapViewBtn_Clicked;
                BoxViewButton.Click += OnBoxViewBtn_Clicked;

            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        public override void OnPause()
        {
            try
            {
                base.OnPause();

                // Close Events
                GoodsButton.Click -= OnGoodsBtn_Clicked;
                SkillsButton.Click -= OnSkillsBtn_Clicked;
                MapViewButton.Click -= OnMapViewBtn_Clicked;
                BoxViewButton.Click -= OnBoxViewBtn_Clicked;
            }
            catch (Exception e)
            {
                Crashes.TrackError(e);
            }
        }

        private void OnGoodsBtn_Clicked(object sender, EventArgs e)
        {

        }

        private void OnSkillsBtn_Clicked(object sender, EventArgs e)
        {

        }

        private void OnMapViewBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (MapViewFragment != null)
                {
                    if (!MapViewFragment.IsAdded && !MapViewFragment.IsVisible)
                    {
                        // Set tint color
                        MapViewButton.SetColorFilter(Color.Black);
                        BoxViewButton.SetColorFilter(Color.Gray);

                        ft = FragmentManager.BeginTransaction();
                        // Replace MapView with BoxView
                        ft.Replace(Resource.Id.myFrameContainer, MapViewFragment)
                            .Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }

        private void OnBoxViewBtn_Clicked(object sender, EventArgs e)
        {
            try
            {
                if (BoxViewFragment != null)
                {
                    if (!BoxViewFragment.IsAdded && !BoxViewFragment.IsVisible)
                    {
                        BoxViewButton.SetColorFilter(Color.Black);
                        MapViewButton.SetColorFilter(Color.Gray);

                        ft = FragmentManager.BeginTransaction();
                        // Replace BoxView with MapView
                        ft.Replace(Resource.Id.myFrameContainer, BoxViewFragment)
                            .Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }

        #region Variables Basic

        Android.Support.V4.App.FragmentTransaction ft;
        public SupportMapFragment MapViewFragment;
        public Market_Fragment BoxViewFragment;

        Button GoodsButton;
        Button SkillsButton;

        ImageButton MapViewButton;
        ImageButton BoxViewButton;

        #endregion
    }
}