﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WoWonder_API.Classes.Global;
using WoWonder_API.Classes.Group;
using WoWonder_API.Classes.User;
using WoWonder_API.Classes.Event;
using WoWonder_API.Classes.Story;
using WoWonder_API.Classes.Message;
using WoWonder_API.Classes.Movies;
using WoWonder_API.Classes.Page;
using WoWonder_API.Classes.Product;
using WoWonder.Helpers;
using System.IO;
using System.Collections.Generic;
using System.Net.Http;
using FFImageLoading;
using FFImageLoading.Work;
using FFImageLoading.Cache;
using WoWonder.Functions;

namespace WoWonder.ProDevCode
{
    public class ProDevAPICode
    {
        //======================Config======================================//

        public const string WebSiteUrl = "https://demo.wowonder.com";//"https://stg2.dailynickel.com/demo/";//"https://demo.wowonder.com";
        public const string ServerKey = "b37bb1cd53d0bc21c13c1644e98a58ca";//"9f1c90293f914071950e63cc6be50e75";//"b37bb1cd53d0bc21c13c1644e98a58ca";

        //======================Initialize======================================//
        private ProDevAPICode() { }
        private static ProDevAPICode instance = new ProDevAPICode();
        public static ProDevAPICode Instance
        {
            get => instance ?? new ProDevAPICode();
        }
        //=======================Paths======================================//
        #region API_Names
        private const string Api_GetSiteSettings = "/api/get-site-settings";
        private const string Api_Auth = "/api/auth";
        private const string Api_DeleteUser = "/api/delete-user";
        private const string Api_GetActivities = "/api/get-activities";
        private const string Api_Search = "/api/search";
        private const string Api_SendMessage = "/api/send-message";
        private const string Api_GetUserSuggestion = "/api/get-user-suggestions";
        private const string Api_GetGenralData = "/api/get-general-data";
        private const string Api_GetCommunity = "/api/get-community";
        private const string Api_DeleteConversation = "/api/delete-conversation";
        private const string Api_GetStories = "/api/get-stories";
        private const string Api_GetBlockUsers = "/api/get-blocked-users";
        private const string Api_GetNearByUsers = "/api/get-nearby-users";
        private const string Api_CreatePage = "/api/create-page";
        private const string Api_CreateGroup = "/api/create-group";
        private const string Api_GetMovies = "/api/get-movies";
        private const string Api_UpdatePage = "/api/update-page-data";
        private const string Api_SocialLogin = "/api/update-group-data";
        private const string Api_UpdateUserData = "/api/update-user-data";
        private const string Api_GetPostData = "/api/get-post-data";
        private const string Api_GetNewsFeed = "/get_news_feed";
        private const string Api_GetPageData = "/api/get-page-data";
        private const string Api_SetGroupData = "/api/get-group-data";
        private const string Api_GetArticales = "/api/get-articles";
        private const string Api_ChangeChatColor = "/api/change-chat-color";
        private const string Api_SetChatTypeingStatus = "/api/set-chat-typing-status";
        private const string Api_DelectStory = "/api/delete-story";
        private const string Api_CreateStory = "/api/create-story";
        private const string Api_JoinGroup = "/api/join-group";
        private const string Api_BlockUser = "/api/block-user";
        private const string Api_PostAction = "/api/post-actions";
        private const string Api_LikePage = "/api/like-page";
        private const string Api_FollowRequestAction = "/api/follow-request-action";
        private const string Api_FollowUser = "/api/follow-user";
        private const string Api_CreateEvent = "/api/create-event";
        private const string Api_InterestEvent = "/api/interest-event";
        private const string Api_GoToEvent = "/api/go-to-event";
        private const string Api_GetEvernt = "/api/get-events";
        private const string Api_CreateProduct = "/api/create-product";
        private const string Api_GetManyUserData = "/api/get-many-users-data";
        private const string Api_GetUserData = "/api/get-user-data";
        private const string Api_SendRestPasswordEmail = "/api/send-reset-password-email";
        private const string Api_DeleteAccessToken = "/api/delete-access-token";
        private const string Api_SetBrowerCookie = "/api/set-browser-cookie";
        private const string Api_CreateAccount = "/api/create-account";

        #endregion

        #region API_Calls

        public async Task<JObject> Call_GetSiteSettings()
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetSiteSettings), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey
                    });

                    return JObject.Parse(Encoding.UTF8.GetString(response));
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return null;
            }
        }
        public async Task<(int, dynamic)> Call_GetCategories()
        {
            var json = await Call_GetSiteSettings();
            if ((int)json["api_status"] == 200)
            {
                return (200, JsonConvert.DeserializeObject<CategoriesObject>(json.ToString()));
            }
            else
            {
                return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
            }
        }
        public async Task<(int, dynamic)> Call_Auth(string userName, string password, string timeZone, string deviceID)
        {
            var name = ProDevHelperCode.CurrentMethodName;
            using (var httpClient = new WebClient())
            {
                try
                { 
                    var nameas = ProDevHelperCode.CurrentMethodName;
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_Auth), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["username"] = userName,
                        ["password"] = password,
                        ["timeone"] = timeZone,
                        ["device_id"] = deviceID
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                       // var (data , resp) = await Call_SetBrowerCookie();
                        return (200, JsonConvert.DeserializeObject<AuthObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);
            }

        }
        public async Task<(int, dynamic)> Call_CreateAccount(string userName, string password, string rePassword, string email, string deviceID)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_CreateAccount), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["username"] = userName,
                        ["password"] = password,
                        ["confirm_password"] = rePassword,
                        ["email"] = email,
                        ["device_id"] = deviceID
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<CreatAccountObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);
            }

        }
        public async Task<(int, dynamic)> Call_SetBrowerCookie()
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_SetBrowerCookie), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<JObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);
            }


        }
        public async Task<(int, dynamic)> Call_DeleteAccessToken()
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_DeleteAccessToken, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<JObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);
            }

        }
        public async Task<(int, dynamic)> Call_SendRestPasswordEmail(string email)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_SendRestPasswordEmail, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["email"] = email
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<ApiStatusObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);
            }
        }
        public async Task<(int, dynamic)> Call_GetUserData(string userID)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetUserData, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = userID,
                        ["fetch"] = "user_data,followers,following,liked_pages,joined_groups"
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<GetUserDataObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }
        }
        public void Call_GetManyUserData()
        {
            var asdas = new WebClient();

        }
        public void Call_CreateProduct()
        {
            var asdas = new WebClient();

        }
        public void Call_GetEvernt()
        {
            var asdas = new WebClient();

        }
        public void Call_GoToEvent()
        {
            var asdas = new WebClient();

        }
        public void Call_InterestEvent()
        {
            var asdas = new WebClient();

        }
        public void Call_CreateEvent()
        {
            var asdas = new WebClient();

        }
        public async Task<(int, dynamic)> Call_FollowUser(string userID)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_FollowUser, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = userID
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<JObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public async Task<(int, dynamic)> Call_FollowRequestAction(string userID, bool action)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_FollowRequestAction, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = userID,
                        ["request_action"] = action ? "accept" : "decline"
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<JObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }
        }
        public void Call_LikePage()
        {
            var asdas = new WebClient();

        }
        public async Task<(int, dynamic)> Call_PostAction(string postID, string action , bool isAction=false, string data = null)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var requestPayLoad = new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["post_id"] = postID,
                        ["action"] = action
                    };
                    if (data!=null)
                    {
                        requestPayLoad["text"] = data;
                    }
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_PostAction, UserDetails.access_token), requestPayLoad);
                 
                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        if (isAction) { return (200, JsonConvert.DeserializeObject<PostActionsObject>(json.ToString())); }
                        else { return (200, JsonConvert.DeserializeObject<GetPostDataObject>(json.ToString())); }
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public async Task<(int, dynamic)> Call_GetBlockUsers(string accessToken, string userID)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetBlockUsers, accessToken), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = userID,
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<GetBlockedUsersObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public void Call_JoinGroup()
        {
            var asdas = new WebClient();

        }
        public void Call_CreateStory()
        {
            var asdas = new WebClient();

        }
        public void Call_DelectStory()
        {
            var asdas = new WebClient();

        }
        public void Call_SetChatTypeingStatus()
        {
            var asdas = new WebClient();

        }
        public void Call_ChangeChatColor()
        {
            var asdas = new WebClient();

        }
        public void Call_GetArticales()
        {
            var asdas = new WebClient();

        }
        public void Call_SetGroupData()
        {
            var asdas = new WebClient();

        }
        public void Call_GetPageData()
        {
            var asdas = new WebClient();

        }
        public void Call_GetNewsFeed()
        {
            var asdas = new WebClient();

        }
        public async Task<(int, dynamic)> Call_UpdateUserData(Dictionary<string, string>  dictionary)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    //var coverImageStream = File.OpenRead(path);
                    //var coverImageStream = new StreamReader(File.OpenRead(path));
                    var requestPayLoad = new NameValueCollection()
                    {
                        ["server_key"] = ServerKey
                    };
                    foreach (var dicItem in dictionary) 
                    {
                        requestPayLoad[dicItem.Key] = dicItem.Value;
                    }

                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_UpdateUserData, UserDetails.access_token), requestPayLoad);

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<MessageObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public async Task<(int, dynamic)> Call_UpdateUserData_Avatar(string path)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var file = Android.Net.Uri.FromFile(new Java.IO.File(path));
                    var ImageTrancform = ImageService.Instance.LoadFile(file.Path);
                    ImageTrancform.LoadingPlaceholder("ImagePlacholder.jpg", ImageSource.CompiledResource);
                    ImageTrancform.ErrorPlaceholder("ImagePlacholder.jpg", ImageSource.CompiledResource);
                    ImageTrancform.DownSampleMode(InterpolationMode.Medium);
                    ImageTrancform.Retry(3, 5000);
                    ImageTrancform.WithCache(CacheType.All);
                    //ImageTrancform.Into(UserProfileImage);

                    //var coverImageStream = File.OpenRead(path);

                    //MemoryStream ms = new MemoryStream();
                    //var sttt = new StreamReader();
                    // var ms = new MemoryStream();
                    //File.ReadAllBytes(path);

                    HttpContent stringContent = new StringContent(ServerKey);
                    HttpContent fileStreamContent = new StreamContent(await ImageTrancform.AsPNGStreamAsync());//File.ReadAllBytes(path));

                    // HttpContent bytesContent = new ByteArrayContent(paramFileBytes);
                    using (var client = new HttpClient())
                    {
                        using (var formData = new MultipartFormDataContent())
                        {
                            formData.Add(stringContent, "server_key");
                            formData.Add(fileStreamContent, "avatar"); 
                            //formData.Add(bytesContent, "file2", "file2");
                            var serResponse = await client.PostAsync(Get_API_Url(Api_UpdateUserData, UserDetails.access_token), formData);
                            if (!serResponse.IsSuccessStatusCode)
                            {
                                return (404, null);
                            }
                            //Console.WriteLine(await serResponse.Content.ReadAsStringAsync());
                            var jsonString = await serResponse.Content.ReadAsStringAsync();
                            var json = JObject.Parse(jsonString);

                            if ((int)json["api_status"] == 200)
                            {
                                return (200, JsonConvert.DeserializeObject<MessageObject>(json.ToString()));
                            }
                            else
                            {
                                return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                            }
                            //return (200, new JObject()); //await response.Content.ReadAsStreamAsync();
                        }
                    }
                    ////var coverImageStream = File.OpenRead(path);
                    //var coverImageStream = new StreamReader(File.OpenRead(path));
                    //var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_UpdateUserData, UserDetails.access_token), new NameValueCollection()
                    //{
                    //    ["server_key"] = ServerKey,
                    //    ["cover"] = coverImageStream.ReadToEnd()
                    //});

                    //var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    //if ((int)json["api_status"] == 200)
                    //{
                    //    return (200, JsonConvert.DeserializeObject<Update_User_Data_Object>(json.ToString()));
                    //}
                    //else
                    //{
                    //    return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    //}
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }
        }
        public async Task<(int, dynamic)> Call_UpdateUserData_Cover(string path)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var file = Android.Net.Uri.FromFile(new Java.IO.File(path));
                    var ImageTrancform = ImageService.Instance.LoadFile(file.Path);
                    ImageTrancform.LoadingPlaceholder("ImagePlacholder.jpg", ImageSource.CompiledResource);
                    ImageTrancform.ErrorPlaceholder("ImagePlacholder.jpg", ImageSource.CompiledResource);
                    ImageTrancform.DownSampleMode(InterpolationMode.Medium);
                    ImageTrancform.Retry(3, 5000);
                    ImageTrancform.WithCache(CacheType.All);
                    //ImageTrancform.Into(UserProfileImage);

                    //var coverImageStream = File.OpenRead(path);

                    //MemoryStream ms = new MemoryStream();
                    //var sttt = new StreamReader();
                    // var ms = new MemoryStream();
                    //File.ReadAllBytes(path);

                    HttpContent stringContent = new StringContent(ServerKey);
                    HttpContent fileStreamContent = new StreamContent(await ImageTrancform.AsPNGStreamAsync());//File.ReadAllBytes(path));

                    // HttpContent bytesContent = new ByteArrayContent(paramFileBytes);
                    using (var client = new HttpClient())
                    {
                        using (var formData = new MultipartFormDataContent())
                        {
                            formData.Add(stringContent, "server_key");
                            formData.Add(fileStreamContent, "avatar");
                            //formData.Add(bytesContent, "file2", "file2");
                            var serResponse = await client.PostAsync(Get_API_Url(Api_UpdateUserData, UserDetails.access_token), formData);
                            if (!serResponse.IsSuccessStatusCode)
                            {
                                return (404, null);
                            }
                            //Console.WriteLine(await serResponse.Content.ReadAsStringAsync());
                            var jsonString = await serResponse.Content.ReadAsStringAsync();
                            var json = JObject.Parse(jsonString);

                            if ((int)json["api_status"] == 200)
                            {
                                return (200, JsonConvert.DeserializeObject<MessageObject>(json.ToString()));
                            }
                            else
                            {
                                return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                            }
                            //return (200, new JObject()); //await response.Content.ReadAsStreamAsync();
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message); 
                }
                return (404, null);
            }
        }
        public async Task<(int, dynamic)> Call_SocialLogin(string accessToken, string provider, string googleKey = "")
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_SocialLogin), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["access_token"] = accessToken,
                        ["provider"] = provider,
                        ["google_key"] = googleKey
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<AuthObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }
        }
        public void Call_UpdatePage()
        {
            var asdas = new WebClient();

        }
        public void Call_GetMovies()
        {
            var asdas = new WebClient();

        }
        public void Call_GetNearByUsers()
        {
            var asdas = new WebClient();

        }
        public void Call_CreateGroup()
        {
            var asdas = new WebClient();

        }
        public void Call_CreatePage()
        {
            var asdas = new WebClient();

        }
        public async Task<(int, dynamic)> Call_GetStories()
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetStories, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<GetStoriesObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);
            }

        }
        public async Task<(int, dynamic)> Call_BlockUser(string userID, bool action =  true) //"block"/* block, un-block */)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_BlockUser,UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = userID,
                        ["block_action"] = action ? "block":"un-block"
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<BlockUserObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);



            }
        }
        public void Call_DeleteConversation()
        {
            var asdas = new WebClient();

        }
        public async Task<(int, dynamic)> Call_GetCommunity(string userID)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetCommunity, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = userID
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<GetCommunityObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public async Task<(int, dynamic)> Call_GetGenralData(bool seeNotifications)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var fetchList = seeNotifications ? "friend_requests,pro_users,promoted_pages,trending_hashtag,count_new_messages"
                                                     : "notifications,friend_requests,pro_users,promoted_pages,trending_hashtag,count_new_messages";
                    //notifications,friend_requests,pro_users,promoted_pages,trending_hashtag,count_new_messages
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetGenralData, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["user_id"] = UserDetails.User_id,
                        ["device_id"] = UserDetails.Device_ID,
                        ["fetch"] = fetchList 
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<GetGeneralDataObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public void Call_GetUserSuggestion()
        {
            var asdas = new WebClient();

        }
        public void Call_SendMessage()
        {
            var asdas = new WebClient();

        }
        public async Task<(int, dynamic)> Call_Search(string userID,  string key,   string limit, string user_offset,  string group_offset,  string page_offset,  string Filter_gender)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_Search, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["search_key"] = key,
                        ["limit"] = limit,
                        ["user_offset"] = user_offset,
                        ["page_offset"] = page_offset,
                        ["Filter_gender"] = Filter_gender
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<GetSearchObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }
        public async Task<(int, dynamic)> Call_GetActivities()
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_GetActivities, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey
                    });

                    var jsonString = Encoding.UTF8.GetString(response);
                    var json = JsonConvert.DeserializeObject<JObject>(jsonString);//JObject.Parse(jsonString);
                    
                   // Console.Write("\n\n================\n\n"+json+"\n\n================\n\n");
                    if ((int)json["api_status"] == 200)
                    {

                        foreach (var activity in json["activities"]) 
                        {
                            var jsonArray = activity?["postData"];
                            if (jsonArray.Type != JTokenType.Null)
                            {
                                jsonArray = jsonArray["publisher"];

                                if (jsonArray.Type!=JTokenType.Null)//var aa = new JToken();
                                {
                                    if (jsonArray.Type == JTokenType.Array)
                                    {
                                        activity["postData"]["publisher"] = null;
                                    }
                                }
                            }
                        }


                        //  return (200, JsonConvert.DeserializeObject<Activities_Object>(json.ToString()));
                        return (200, JsonConvert.DeserializeObject<ActivitiesObject>(json.ToString()));
                        
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                    return (404, ex.Message);
                }
            }

        }
        public async Task<(int, dynamic)> Call_DeleteUser(string password)
        {
            using (var httpClient = new WebClient())
            {
                try
                {
                    var response = await httpClient.UploadValuesTaskAsync(Get_API_Url(Api_DeleteUser, UserDetails.access_token), new NameValueCollection()
                    {
                        ["server_key"] = ServerKey,
                        ["password"] = password
                    });

                    var json = JObject.Parse(Encoding.UTF8.GetString(response));

                    if ((int)json["api_status"] == 200)
                    {
                        return (200, JsonConvert.DeserializeObject<MessageObject>(json.ToString()));
                    }
                    else
                    {
                        return (400, JsonConvert.DeserializeObject<ErrorObject>(json.ToString()));
                    }
                    // dynamic json = JsonConvert.DeserializeObject<AuthObject>(resp_string);
                    // var json = JObject.Parse(Encoding.UTF8.GetString(response));
                    //return ((int)json["status"], json);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
                return (404, null);

            }

        }

        #endregion

        #region API_Helpers

        public string Get_API_Url(string path, string accessToken = null)
        {
            if (accessToken == null)
            {
                return WebSiteUrl + path;
            }
            else
            {
                return WebSiteUrl + path + "?access_token=" + accessToken;
            }
        }

        #endregion
    }
}
