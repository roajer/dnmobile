﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace WoWonder.ProDevCode
{
    public class ProDevHelperCode
    {
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(4);

            return sf.GetMethod().Name;
            
        }
        
        public static string CurrentMethodName  
        {
            get => GetCurrentMethod(); 

        }
    }
}